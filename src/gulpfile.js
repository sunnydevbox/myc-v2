var gulp = require('gulp');
//var elixir = require('laravel-elixir');
var livereload = require('gulp-livereload');
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var	aglio = require('gulp-aglio');
var chmod = require('gulp-chmod');
var sass = require('gulp-ruby-sass');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var template = require('gulp-template-compile');
var merge = require('merge-stream');

gulp.task('flat-ui', function () {
    gulp.src('resources/assets/less/flat-ui/flat-ui-pro.less')
        .pipe(less())
        .pipe(gulp.dest('public/assets/css/'));
});

gulp.task('sass-app', function() {
    return sass('resources/assets/sass/app.scss', {container: 'gulp-ruby-sass-app'})
        .on('error', function (err) {
            console.error('Error', err.message);
        })
        .pipe(gulp.dest('public/assets/css'));
});

gulp.task('sass-site', function() {
    return sass('resources/assets/sass/location.scss', {container: 'gulp-ruby-sass-site'})
        .on('error', function (err) {
            console.error('Error', err.message);
        })
        .pipe(gulp.dest('public/assets/css'));
});

gulp.task('sass-map', function() {
    return sass('resources/assets/sass/map.scss', {container: 'gulp-ruby-sass-map'})
        .on('error', function (err) { console.error('Error', err.message); })
        .pipe(autoprefixer())
        .pipe(gulp.dest('public/assets/css'));
});

gulp.task('js-map', function() {
    var templates = gulp.src('resources/assets/js/map/**/*.html')
        .pipe(template());
    var scripts = gulp.src('resources/assets/js/map/**/*.js');
    return merge(templates, scripts)
        .pipe(concat('map.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('public/assets/js'));
});

/*gulp.task('sass', function() {
    elixir(function(mix) {
        mix.sass('app.scss', 'public/assets/css');
        mix.sass('location.scss', 'public/assets/css');
    });
});*/

gulp.task('compress', function() {
    gulp.src('resources/assets/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('public/assets/js/'));
});

gulp.task('reload', function() {
    livereload.reload();
});

/**
 * task - 'live-monitor'
 * monitors everything
 */
gulp.task('live-monitor', function() {
    livereload.listen();
    gulp.watch('resources/views/**/*.blade.php', ['laravel-views']);
    gulp.watch('resources/assets/sass/*.scss', ['sass-site', 'sass-app', 'sass-map', 'reload']);
    gulp.watch('resources/assets/less/flat-ui/*.less', ['flat-ui', 'reload']);
    gulp.watch('resources/assets/js/*.js', ['compress', 'reload']);
    gulp.watch('docs/*.md', ['docs']);
});

gulp.task('watch-map', function() {
    livereload.listen();
    gulp.watch('resources/assets/sass/**/*.scss', ['sass-map', 'reload']);
    gulp.watch('resources/assets/js/map/**/*', ['js-map', 'reload']);
});

gulp.task('docs', function() {
    return gulp.src('docs/api.md')
        .pipe(aglio({template: 'flatly'}))
        .pipe(chmod(644))
        .pipe(gulp.dest('docs'));
});

gulp.task('laravel-views', function() {
    gulp.src('resources/views/**/*.blade.php')
        .pipe(livereload());
});

gulp.task('default', ['live-monitor']);
