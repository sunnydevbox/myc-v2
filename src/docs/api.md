FORMAT: 1A
HOST: https://dev.mapyour.city

# Map Your City API
Revitalizing Buildings & Spaces together!

# Group Users
## /users
### Register a User [POST]
Register a new User. All fields are required except `last_name_prefix`.

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
          "data": {
            "email": "john@doe.com",
            "first_name": "John",
            "last_name": "Doe",
            "last_name_prefix": null,
            "password": "somepassword",
            "password_confirmation": "somepassword"
          }
        }

# Group Authentication
## /oauth/access_token
### Login [POST]
An access token is provided upon succesful authentication. The token must be included in the header on every request. The API currently does not provide an endpoint for destroying/invalidating access tokens. For logout the access token can be deleted client-side. Expired tokens will be cleanup up automatically.

+ Request (application/json)
   + Headers
           Accept: application/json
   + Body
       {
         "client_id": "r52VqHJqXOTFz8K6",
         "client_secret": "qPLxO4AChAv9fIAOH9xhwjBHt9ZBcoCD",
         "grant_type": "password",
         "username": "john@doe.com",
         "password": "somepassword"
       }

+ Response 201 (application/json)
    + Body
        {
          "data": {
            "access_token": "7RcI32dP9AGIYz0he0jqlfgSBMdS0mrIqglnFGHj",
            "token_type": "Bearer",
            "expires_in": 604800
          }
        }

## /oauth/facebook
### Facebook Login [POST]
Similar to the `oauth/access_token` endpoint a Facebook short-lived token can be provided for authentication. If needed a User will be created using the Facebook account details. [Facebook docs](https://developers.facebook.com/docs/facebook-login/access-tokens#extending).

+ Request (application/json)
   + Headers
           Accept: application/json
   + Body
       {
         "client_id": "r52VqHJqXOTFz8K6",
         "client_secret": "qPLxO4AChAv9fIAOH9xhwjBHt9ZBcoCD",
         "grant_type": "facebook",
         "fb_exchange_token": "CAAWHfs9aW2oBAJsfTXxcyw8zzcjx9KLj8uxZCSikkIVoHVZCt6eF1NcXsjrFxaXl6D8DXuT1rCYKJ7IN2dzZCfdx7IlJX7QBMYly4ZAoAgBaPM52A55eUaMzRZAZCeSaV0EovlXKGfef2pkV2CXGg4DsuJaST3cnmlmamogZBpWwpO7ZCzrtVj7ZBLZBhDYy73WQAMSDBqDDCnOEf5XBtanROYOyuKAyk9eGK8CyZCP5j5PswZDZD ",
       }

+ Response 201 (application/json)
    + Body
        {
          "data": {
            "access_token": "zBSXtjJWs05jB0ocLsmBNhYArs3bchxBudYuFgnE",
            "token_type": "Bearer",
            "expires_in": 604800
          }
        }

## /auth/confirm
### Confirm Account [POST]
After creating a User an email is sent providing a confirmation code to verify the User's email. Using this confirmation code a User account can be verified.

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
            "confirmation_code": "e5a5e1ab4801ad1f960c21a305c8b7d1dcb8d029b7e14e3dce5c1dbe5f8c31f5",
            "email": "john@doe.com"
        }

+ Response (application/json)
    + Body
        {
          "message": "Account confirmed successfully"
        }

## /auth/passwords/email
### Password Reset Link [POST]
Request a link to reset the User's password. Upon succesful validation of the email a link will be sent.

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
            "email": "john@doe.com"
        }

## /auth/passwords/reset
### Reset Password [POST]
Reset the User's password using the reset token provided in the email.

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
            "token": "88bd740e171e217e2164a4310b4a5de13c1960104e112472fac2bc603700ba2e",
            "email": "john@doe.com",
            "password": "anotherpassword",
            "password_confirmation": "anotherpassword"
        }

## /auth/user
### Authenticated User Details [GET]
Show full details of authenticated user.
+ Request (application/json)
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22
        Accept: application/json
+ Response 200 (application/json)
    + Body
    {
        "data": {
            "id": "vm38xr2w",
            "email": "john@doe.com",
            "first_name": "John",
            "last_name": "Doe",
            "last_name_prefix": null,
            "image": null,
            "created_at": "2015-03-30T00:18:53+0200"
        }
    }


# Group Pins
The Pins resource is the entry of the Map Your City app. The collection can contain elements of type `buckets` in case multiple
points of interest are grouped or a element of type `location` when grouping is not needed.

## Pins Collection [/pins]
### List Pins [GET]

+ Parameters
  + bounding_box (required, comma separated list of decimals, `90.0,-180.0,-90.0,180.0`)
    The geographic bounding box to filter by. Required format: `top_left_lat,top_left_Lon,bottom_right_lat,bottom_right_lon`.
  + tags (optional, comma separated list of integers, `23,12,43`)
    Filter by list of Tag IDs. For a list of available Tags use the Categories resource.
  + q (optional, string, `Gashouder Amsterdam`)
    Filter by name (and other fields?). Performs a full-text search.
  + favorited (optional, Boolean)
    Filter by User Favorites (login required).
  + filter_by_user (optional, Boolean)
    It filters by the user that created the location. It will return the locations that the logged in user created (login required).

+ Response 200 (application/json)

  + Body
    {
      "data": [
        {
          "type": "buckets",
          "id": "u177kjn",
          "location_count": 2,
          "lat": 52.630252,
          "lon": 4.7556165,
          "bounding_box": {
            "top_left": {
              "lat": 52.6302911,
              "lon": 4.7555625
            },
            "bottom_right": {
              "lat": 52.6302129,
              "lon": 4.7556705
            }
          },
          "links": {
            "self": "http://myc.dev/pins?bounding_box=52.6302911%2C4.7555625%2C52.6302129%2C4.7556705"
          }
        },
        {
          "type": "locations",
          "id": "y13om1rn",
          "icon": "building",
          "lat": 52.6367783,
          "lon": 4.758309,
          "name": "Munnikenweg 90 Alkmaar",
          "street_name": "Munnikenweg",
          "street_number": "90",
          "postal_code": "1823DD",
          "city": "Alkmaar",
          "country": "Nederland",
          "favorite_count": 12,
          "favorited": false,
          "links": {
            "self": "http://myc.dev/locations/y13om1rn",
            "images": "http://myc.dev/locations/y13om1rn/images"
          }
        }
      ]
    }

# Group Organizations

## Follow an organization [/organizations/{id}/follow]
### Follow [POST]

Add the logged in user as an organization follower

+ Parameters
  + id (string, `ldpvgzp2`) ... ID of the Organization in the form of a hash (8 alphanumeric characters).

+ Response 204

## Unfollow an organization [/organizations/{id}/unfollow]
### Unfollow [DELETE]

Removes the logged in user from the organization's followers

+ Parameters
  + id (string, `ldpvgzp2`) ... ID of the Organization in the form of a hash (8 alphanumeric characters).

+ Response 204

# Group Locations

## Location [/location/{id}]
+ Model (application/json)
    + Body
      {
        "data": {
          "type": "locations",
          "id": "ldpvgzp2",
          "icon": "building",
          "lat": 52.6325414,
          "lon": 4.7457702,
          "name": "Hoogstraat 10 Alkmaar",
          "street_name": "Hoogstraat",
          "street_number": "10",
          "postal_code": "1811KV",
          "city": "Alkmaar",
          "country": "Nederland",
          "favorite_count": 2,
          "favorited": false,
          "links": {
            "self": "http://myc.dev/locations/ldpvgzp2",
            "images": "http://myc.dev/locations/ldpvgzp2/images"
          },
          "images": {
            "data": [
              {
                "type": "images",
                "id": "ldpvgzp2",
                "url": "http://myc.dev/images/96b/96bc652f-746b-4fa7-9ca6-c745dc387b7f.jpg",
                "mime_type": "image/jpeg",
                "original_width": 850,
                "original_height": 1274
              }
            ]
          },
          "tags": {
            "data": [
              {
                "id": 1,
                "name": "Building"
              }
            ]
          },
          "comments": {
            "data": [
              {
                "type": "comments",
                "id": "ldpvz326",
                "body": "A comment on this location.",
                "created_at": "2015-03-30T00:52:52+0200",
                "favorite_count": 0,
                "favorited": false,
                "user": {
                  "data": {
                    "id": "vm38xr2w",
                    "display_name": "John Doe",
                    "image": null
                  }
                },
                "comments": {
                  "data": [
                    {
                      "type": "comments",
                      "id": "ldpvz326",
                      "body": "A comment on this comment.",
                      "created_at": "2015-03-30T10:52:52+0200",
                      "favorite_count": 0,
                      "favorited": false,
                      "user": {
                        "data": {
                          "id": "ldpvz326",
                          "display_name": "Jane Doe",
                          "image": null
                        }
                      }
                    }
                  ]
                }
              }
            ]
          },
          "organizations": {
            "data": []
          }
        }
      }

### Show Location [GET]
Show a single Location. Images, Comments and Organizations are included by default. An authorization is optional for this request. When this is provided, the `favorited` flag is included.

+ Parameters
  + id (string, `ldpvgzp2`) ... ID of the Location in the form of a hash (8 alphanumeric characters).

+ Response 200
    [Location][]

### Update Location [PUT]
Update Location details. The `favorite` flag can be set on this request as well. The embedded resource `tags` can be included in this request.

+ Parameters
  + id (string, `ldpvgzp2`) ... ID of the Location in the form of a hash (8 alphanumeric characters).

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
          "data": {
            "type": "locations",
            "lat": 4343,
            "lon": 4.8890502,
            "name": "Qikker Online HQ",
            "street_name": "Van Diemenstraat",
            "street_number": "103",
            "postal_code": "1013 CN",
            "city": "Amsterdam",
            "country": "Netherlands",
            "favorited": true,
            "tags": {
                "data": [
                    {
                        "id": 1
                    },
                    {
                        "id": 3
                    }
                ]
            }
          }
        }

+ Response 200

    [Location][]

## Locations Collection [/locations]

### List Locations [GET]

+ Parameters
  + limit (optional, number, `40`)
    The number of results to return
  + bounding_box (required, comma separated list of decimals, `90.0,-180.0,-90.0,180.0`)
    The geographic bounding box to filter by. See Pins resource.
  + latlon (optional, comma separated list, `90.0,-180.0`)
    The coordinate used to filter Locations nearby. Required format: `lat,lon`. A distance parameter may also be provided in combination with this parameter (see below).
  + distance = `100m` (optional, integer + unit, `20km`) ... The radius to look for Locations from latlon.
    The distance in `m` or `km` from `latlon`.
  + tags (optional, comma separated list of integers, `23,12,43`)
    Filter by list of Tag IDs. For a list of available Tags use the Categories resource.
  + q (optional, string, `Gashouder Amsterdam`)
    Filter by name (and other fields?). Performs a full-text search.
  + favorited (optional, Boolean)
    Filter by User Favorites (login required).
  + organization_id (optional, string, `n9vop38p`)
    Filter by an organization
  + filter_by_user (optional, Boolean)
    It filters by the user that created the location. It will return the locations that the logged in user created (login required).

+ Response 200  (application/json)
    + Body
        {
          "data": [
            {
              "type": "locations",
              "id": "ldpvgzp2",
              "icon": "building",
              "lat": 52.6325414,
              "lon": 4.7457702,
              "name": "Hoogstraat 10 Alkmaar",
              "street_name": "Hoogstraat",
              "street_number": "10",
              "postal_code": "1811KV",
              "city": "Alkmaar",
              "country": "Nederland",
              "favorite_count": 34,
              "favorited": false,
              "links": {
                "self": "http://myc.dev/locations/ldpvgzp2",
                "images": "http://myc.dev/locations/ldpvgzp2/images"
              }
            },
            {
              "type": "locations",
              "id": "6z3yozpx",
              "icon": "building",
              "lat": 52.6325576,
              "lon": 4.7457197,
              "name": "Hoogstraat 12 Alkmaar",
              "street_name": "Hoogstraat",
              "street_number": "12",
              "postal_code": "1811KV",
              "city": "Alkmaar",
              "country": "Nederland",
              "favorite_count": 24,
              "favorite": false,
              "links": {
                "self": "http://myc.dev/locations/6z3yozpx",
                "images": "http://myc.dev/locations/6z3yozpx/images"
              }
            }
          ],
          "meta": {
              "pagination": {
                "total": 100,
                "count": 10,
                "per_page": 2,
                "current_page": 2,
                "total_pages": 50,
                "links": {
                  "previous": "/?page=1",
                  "next": "/?page=3"
                }
              }
            }
        }

### Create Location [POST]
You must provide a JSON object containg latitude and longtitude. Geocoding is performed on the server and a draft Location resource containing address, country, etc. is returned. Using a PUT action the resource can be adjusted and the Location can be published. A `name` attribute is optional. Address will be used as name if none is provided.

+ Request (application/json)
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22
        Accept: application/json
    + Body
        {
            "data": {
                "name": "Qikker Online HQ",
                "type": "locations",
                "lat": 52.39008,
                "lon": 4.8890502
            }
        }

+ Response 201 (application/json)
    + Body
        {
          "data": {
            "type": "locations",
            "id": "q4re2lry",
            "icon": "building",
            "lat": 52.3900351,
            "lon": 4.8890506,
            "name": "Qikker Online HQ",
            "street_name": "Van Diemenstraat",
            "street_number": "116",
            "postal_code": "1013CN",
            "city": "Amsterdam",
            "country": "Nederland",
            "favorite_count": 34,
            "favorited": false,
            "links": {
              "self": "http://myc.dev/locations/q4re2lry"
            }
          }
        }

## Favorites [/locations/{id}/favorite]

### Favorite a Location [POST]

+ Parameters
    + id (string, `ldpvgzp2`) ... ID of the Location in the form of a hash (8 alphanumeric characters).

+ Request
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22

+ Response 204

### Unfavorite a Location [DELETE]

+ Parameters
    + id (string, `ldpvgzp2`) ... ID of the Location in the form of a hash (8 alphanumeric characters).

+ Request
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22

+ Response 204

## Images [/locations/{id}/images]

### List Images [GET]
List all images associated with a Location.

+ Response 200 (application/json)
    + Body
        {
          "data": [
            {
              "type": "images",
              "id": "ldpvgzp2",
              "url": "http://myc.dev/images/96b/96bc652f-746b-4fa7-9ca6-c745dc387b7f.jpg",
              "mime_type": "image/jpeg",
              "original_width": 850,
              "original_height": 1274
            },
            {
              "type": "images",
              "id": "6z3yozpx",
              "url": "http://myc.dev/images/172/1720d45f-4970-4907-8bd7-267e9bf47254.jpg",
              "mime_type": "image/jpeg",
              "original_width": 850,
              "original_height": 1274
            }
          ]
        }


### Create an Image [POST]
Upload image data and associate image with Location. Post as form data. The image metadata is analyzed and a UUID is assigned upon creation.

+ Request (multipart/form-data)
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22
+ Response 201
    + Body
        {
          "data": {
            "type": "images",
            "id": "ldpvgzp2",
            "url": "http://myc.dev/images/96b/96bc652f-746b-4fa7-9ca6-c745dc387b7f.jpg",
            "mime_type": "image/jpeg",
            "original_width": 850,
            "original_height": 1274
          }
        }

### Delete an Image [DELETE]

+ Request
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22
    + Response 204

## /locations/{id}/images/sort_order
### Order Images [PUT]

## Image Disapprovals [/images/{id}/dispprovals]
### Disapprove an Image [POST]
Create a message to let the administrator know you disapprove this image. Authorization header is optional. When no authorization is provided the disapproval is anonymous.

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
          "data": {
            "type": "disapprovals",
            "body": "This picture is copyrighted, please remove it."
          }
        }

# Group Comments
## Comments on Locations [/location/{id}/comments]
## Create a Comment on Location [POST]
Create a Comment on a Location.

+ Request
    + Headers
        Accept: application/json
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22
    + Body
        {
            "data": {
                "type": "comments",
                "body": "Some comment on a location."
            }
        }

+ Response 201
    + Body
        {
          "data": {
            "type": "comments",
            "id": "ldpvz326",
            "body": "Some comment on a location.",
            "created_at": "2015-03-30T09:52:52+0200",
            "favorite_count": 0,
            "user": {
              "data": {
                "id": "vm38xr2w",
                "display_name": "John Doe",
                "image": null
              }
            },
            "comments": {
              "data": []
            }
          }
        }

## Comments on Comments [/comments/{id}/comments]

## Create a Comment on Comment [POST]
Create a Comment on a Comment. This is only allowed for 1 level.

+ Request
    + Headers
        Accept: application/json
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22
    + Body
        {
            "data": {
                "type": "comments",
                "body": "Some comment on a comment."
            }
        }

+ Response 201
    + Body
        {
          "data": {
            "type": "comments",
            "id": "ldpvz326",
            "body": "Some comment on a comment.",
            "created_at": "2015-03-30T09:52:52+0200",
            "favorite_count": 0,
            "user": {
              "data": {
                "id": "vm38xr2w",
                "display_name": "John Doe",
                "image": null
              }
            },
            "comments": {
              "data": []
            }
          }
        }

## Comment Favorites [/comments/{id}/favorite]
### Favorite a Comment [POST]

+ Request
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22

+ Response 204

### Unfavorite a Comment [DELETE]

+ Request
    + Headers
        Authorization: Bearer 7aGTrxifAfIBplul53i2JHTY7ujDNlw6cMZNQj22

+ Response 204

## Comment Disapprovals [/comments/{id}/disapprovals]
### Disapprove a Comment [POST]
Create a message to let the administrator know you disapprove this comment. Authorization header is optional. When no authorization is provided the disapproval is anonymous.

+ Request (application/json)
    + Headers
        Accept: application/json
    + Body
        {
          "data": {
            "type": "disapprovals",
            "body": "This comment is offends me."
          }
        }

# Group Categories

## Categories [/categories]
Categories contain tags which have a unique ID. This ID can be used in the `tags` parameter in the `/pins` and `/locations` endpoints to filter results. As opposed to other IDs throughout the Map Your City API, all tag IDs are of type integer.

## List Categories [GET]

+ Response 200 (application/json)
    + Body
        {
          "data": [
            {
              "id": 1,
              "name": "Main",
              "tags": {
                "data": [
                  {
                    "id": 1,
                    "name": "Open Space"
                  },
                  {
                    "id": 2,
                    "name": "Building"
                  },
                  {
                    "id": 3,
                    "name": "Industrial Zone"
                  }
                ]
              }
            },
            {
              "id": 2,
              "name": "Advanced",
              "tags": {
                "data": [
                  {
                    "id": 6,
                    "name": "Harbour Front"
                  },
                  {
                    "id": 7,
                    "name": "Park"
                  },
                  {
                    "id": 8,
                    "name": "Public Transit Access"
                  }
                ]
              }
            }
          ]
        }

# Group Geocode
## Geocode [/geocode]
### Geocode Lat Lon [GET]
Utility endpoint to return the nearest known address from the provided coordinate.

+ Parameters
    + latlon (required, comma separated list, `90.0,-180.0`)
        The coordinate to geocode.

+ Response (application/json)
    + Body
        {
          "data": {
            "type": "addresses",
            "lat": 52.1463497,
            "lon": 5.1852564,
            "street_name": "Bantamlaan",
            "street_number": "15",
            "postal_code": "3738 CX",
            "city": "Maartensdijk",
            "country": "Netherlands"
          }
        }
