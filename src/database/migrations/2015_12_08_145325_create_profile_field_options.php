<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileFieldOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profile_field_options', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('profile_field_id')->unsigned();
            $table->foreign('profile_field_id')->references('id')->on('profile_fields');

            $table->string('value');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_field_options', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
