<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnDeleteCascadeToProfileFieldOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_field_entry_profile_field_option', function(Blueprint $table)
		{
			$table->dropForeign('profile_field_entry_id_foreign');
            $table->dropForeign('profile_field_option_id_foreign');

            $table->foreign('profile_field_entry_id', 'profile_field_entry_id_foreign')->references('id')->on('profile_field_entries')->onDelete('cascade');
            $table->foreign('profile_field_option_id', 'profile_field_option_id_foreign')->references('id')->on('profile_field_options')->onDelete('cascade');
		});

        Schema::table('profile_field_options', function(Blueprint $table)
        {
            $table->dropForeign('profile_field_options_profile_field_id_foreign');
            $table->foreign('profile_field_id')->references('id')->on('profile_fields')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_field_entry_profile_field_option', function(Blueprint $table)
		{
            $table->dropForeign('profile_field_entry_id_foreign');
            $table->dropForeign('profile_field_option_id_foreign');

            $table->foreign('profile_field_entry_id', 'profile_field_entry_id_foreign')->references('id')->on('profile_field_entries');
            $table->foreign('profile_field_option_id', 'profile_field_option_id_foreign')->references('id')->on('profile_field_options');
		});

        Schema::table('profile_field_options', function(Blueprint $table)
        {
            $table->dropForeign('profile_field_options_profile_field_id_foreign');
            $table->foreign('profile_field_id')->references('id')->on('profile_fields');
        });
	}

}
