<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinAndMaxValueToProfileFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_fields', function(Blueprint $table)
		{
			$table->integer('min_value')->nullable()->after('is_active');
            $table->integer('max_value')->nullable()->after('min_value');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_fields', function(Blueprint $table)
		{
			$table->dropColumn('min_value');
            $table->dropColumn('max_value');
		});
	}

}
