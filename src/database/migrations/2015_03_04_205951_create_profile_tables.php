<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('organization_id')->unsigned()->index();
			$table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');

			$table->timestamps();
		});

		Schema::create('profile_fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('profile_id')->unsigned()->index();
			$table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
			$table->string('field_type')->index();
			$table->string('label');

			// When there are predefined options store them here
			// Why not just JSON encode
			$table->text('options')->nullable();

			$table->smallInteger('sort_order')->unsigned();
			$table->boolean('is_required')->default(false);

			$table->timestamps();
		});

		Schema::create('profile_field_entries', function(Blueprint $table)
		{
			$table->integer('profile_field_id')->unsigned()->index();
			$table->foreign('profile_field_id')->references('id')->on('profile_fields')->onDelete('cascade');

			$table->integer('location_id')->unsigned()->index();
			$table->foreign('location_id')->references('id')->on('locations');

            $table->unique(['profile_field_id', 'location_id']);

			$table->text('value');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profile_field_entries');
		Schema::drop('profile_fields');
		Schema::drop('profiles');
	}

}
