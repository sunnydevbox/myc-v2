<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('locations', function(Blueprint $table)
		{
			$table->enum('status', [\Myc\Domain\Locations\Location::STATUS_PUBLISHED, \Myc\Domain\Locations\Location::STATUS_DRAFT, Myc\Domain\Locations\Location::STATUS_DRAFT_MAP])
                ->after('favorite_count');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('locations', function(Blueprint $table)
		{
            $table->dropColumn('status');
		});
	}

}
