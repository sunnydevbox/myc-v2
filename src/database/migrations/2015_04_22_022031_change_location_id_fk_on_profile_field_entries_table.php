<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLocationIdFkOnProfileFieldEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_field_entries', function(Blueprint $table)
		{
			$table->dropForeign('profile_field_entries_location_id_foreign');
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_field_entries', function(Blueprint $table)
		{
			$table->dropForeign('profile_field_entries_location_id_foreign');
			$table->foreign('location_id')->references('id')->on('locations');
		});
	}

}
