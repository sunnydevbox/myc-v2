<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToProfileFieldEntries extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_field_entries', function(Blueprint $table)
		{
			$table->increments('id')->after('profile_field_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_field_entries', function(Blueprint $table)
		{
			$table->dropColumn('id');
		});
	}

}
