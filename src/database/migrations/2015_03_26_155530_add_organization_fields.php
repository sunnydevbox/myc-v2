<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrganizationFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('organizations', function($table)
		{
		    $table->string('pay_off')->nullable();
		    $table->string('website')->nullable();
		    $table->string('email_address')->nullable();
		    $table->text('description')->nullable();
		    //for billing
		    $table->string('company_name')->nullable();
		    $table->string('first_name')->nullable();
		    $table->string('last_name')->nullable();
		    $table->string('street_number')->nullable();
			$table->string('street_name')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('locality')->nullable(); // locality or city
			$table->string('country')->nullable();
		    $table->string('phone_number')->nullable();
		    $table->string('email_address_billing')->nullable();
		    //payment plan
		    $table->string('plan')->nullable();
		    $table->date('started_at')->nullable();
		    $table->integer('building_number')->nullable();
		    $table->integer('user_number')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('organizations', function($table)
		{
		    $table->dropColumn('pay_off');
		    $table->dropColumn('website');
		    $table->dropColumn('email_address');
		    $table->dropColumn('description');
		    $table->dropColumn('company_name');
		    $table->dropColumn('first_name');
		    $table->dropColumn('last_name');
		    $table->dropColumn('street_number');
			$table->dropColumn('street_name');
			$table->dropColumn('postal_code');
			$table->dropColumn('locality');
			$table->dropColumn('country');
		    $table->dropColumn('phone_number');
		    $table->dropColumn('email_address_billing');
		    $table->dropColumn('plan');
		    $table->dropColumn('started_at');
		    $table->dropColumn('building_number');
		    $table->dropColumn('user_number');
		});

	}

}
