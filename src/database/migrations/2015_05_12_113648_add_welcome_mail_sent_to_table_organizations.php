<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWelcomeMailSentToTableOrganizations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('organizations', function(Blueprint $table)
		{
			$table->boolean('welcome_mail_sent')->default(false)->after('user_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('organizations', function(Blueprint $table)
		{
            $table->dropColumn('welcome_mail_sent');
		});
	}
}
