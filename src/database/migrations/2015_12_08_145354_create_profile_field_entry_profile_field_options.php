<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileFieldEntryProfileFieldOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profile_field_entry_profile_field_option', function(Blueprint $table)
		{
			$table->integer('profile_field_entry_id')->unsigned();
            //$table->foreign('profile_field_entry_id', 'profile_field_entry_id_foreign')->references('id')->on('profile_field_entries');

            $table->integer('profile_field_option_id')->unsigned();
            //$table->foreign('profile_field_option_id', 'profile_field_option_id_foreign')->references('id')->on('profile_field_options');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_field_entry_profile_field_option', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
