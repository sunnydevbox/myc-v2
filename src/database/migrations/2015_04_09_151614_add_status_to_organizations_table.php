<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToOrganizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('organizations', function(Blueprint $table)
		{
			$table->enum('status', [\Myc\Domain\Organizations\Organization::STATUS_ACTIVE, \Myc\Domain\Organizations\Organization::STATUS_DISABLED]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('organizations', function(Blueprint $table)
		{
            $table->dropColumn('status');
		});
	}

}
