<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('last_name_prefix')->nullable();
			$table->string('email')->index()->unique();
			$table->string('password', 60)->nullable();
			$table->rememberToken();
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code')->index()->nullable();
            $table->string('device_token')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
