<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProfileFieldEntryProfileFieldOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_field_entry_profile_field_option', function(Blueprint $table)
		{
            $table->foreign('profile_field_entry_id', 'profile_field_entry_id_foreign')->references('id')->on('profile_field_entries');
            $table->foreign('profile_field_option_id', 'profile_field_option_id_foreign')->references('id')->on('profile_field_options');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_field_entry_profile_field_option', function(Blueprint $table)
		{
			$table->dropForeign('profile_field_entry_id');
            $table->dropForeign('profile_field_option_id');
		});
	}

}
