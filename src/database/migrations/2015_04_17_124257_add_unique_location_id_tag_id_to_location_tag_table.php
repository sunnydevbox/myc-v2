<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueLocationIdTagIdToLocationTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('location_tag', function(Blueprint $table)
		{
			$table->unique(['location_id', 'tag_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('location_tag', function(Blueprint $table)
		{
			$table->dropUnique('location_tag_location_id_tag_id_unique');
		});
	}

}
