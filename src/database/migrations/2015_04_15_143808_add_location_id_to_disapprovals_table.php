<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationIdToDisapprovalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('disapprovals', function(Blueprint $table)
		{
			$table->integer('location_id')->unsigned()->index()->nullable()->after('comment_id');
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('disapprovals', function(Blueprint $table)
		{
			$table->dropForeign('disapprovals_location_id_foreign');
            $table->dropColumn('location_id');
		});
	}

}
