<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');

			$table->string('name')->nullable();

			$table->decimal('lat', 10, 8);
			$table->decimal('lon', 11, 8);

			$table->string('street_number')->nullable();
			$table->string('street_name')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('locality')->nullable(); // locality or city
			$table->string('sub_locality')->nullable(); // city district of sublocality
			$table->string('country')->nullable();
			$table->string('country_code')->nullable(); // ISO country code
			$table->string('timezone')->nullable();

            $table->integer('favorite_count')->unsigned()->index()->default(0);

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
