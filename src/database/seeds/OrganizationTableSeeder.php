<?php
use Illuminate\Database\Seeder;
use Myc\Domain\Organizations\Organization;

class OrganizationTableSeeder extends Seeder {

    public function run()
    {
        $imageService = app()->make('Myc\Domain\Images\ImageService');
        $fileInfo = $imageService->storeRemoteImage('http://www.amsterdamstudentenstad.nl/userfiles/image/Stadsdeel%20West.jpg');
        Organization::create([
            'name' => 'Amsterdam Stadsdeel West',
            'image' => $fileInfo['path']
        ]);

        $fileInfo = $imageService->storeRemoteImage('http://mediascope.nl/wp-content/uploads/2012/12/CRS_GemeenteDenHaag-1000x667.png');
        Organization::create([
            'name' => 'Gemeente Den Haag',
            'image' => $fileInfo['path']
        ]);

        $fileInfo = $imageService->storeRemoteImage('https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Flag_Haarlem.svg/1600px-Flag_Haarlem.svg.png');
        Organization::create([
            'name' => 'Gemeente Haarlem',
            'image' => $fileInfo['path']
        ]);

        $now = \Carbon\Carbon::now();

        \DB::table('organization_user')->insert([
            [
                'user_id' => 1,
                'organization_id' => 1,
                'role' => 'member',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 1,
                'organization_id' => 2,
                'role' => 'member',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 1,
                'organization_id' => 3,
                'role' => 'owner',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 2,
                'organization_id' => 1,
                'role' => 'owner',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 2,
                'organization_id' => 2,
                'role' => 'member',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 2,
                'organization_id' => 3,
                'role' => 'member',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);

        foreach (Organization::all() as $organization) {

            $profile = \Myc\Domain\Profiles\Profile::create([
                'organization_id' => $organization->id
            ]);

            $now = \Carbon\Carbon::now();
            $fields = [
                [
                    'profile_id' => $profile->id,
                    'sort_order' => 1,
                    'field_type' => \Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_TEXT,
                    'label' => 'Building name',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'profile_id' => $profile->id,
                    'sort_order' => 2,
                    'field_type' => \Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_NUMERIC,
                    'label' => 'Date build',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'profile_id' => $profile->id,
                    'sort_order' => 3,
                    'field_type' => \Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_SELECT,
                    'label' => 'Condition',
                    'options' => json_encode(['good', 'poor', 'excellent']),
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'profile_id' => $profile->id,
                    'sort_order' => 4,
                    'field_type' => \Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_TEXTAREA,
                    'label' => 'Description',
                    'created_at' => $now,
                    'updated_at' => $now
                ]
            ];

            foreach ($fields as $field) {
                DB::table('profile_fields')->insert($field);
            }
        }


    }
}