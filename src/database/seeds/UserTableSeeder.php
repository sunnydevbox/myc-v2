<?php
use Illuminate\Database\Seeder;
use Myc\Domain\Users\User;

class UserTableSeeder extends Seeder {

    public function run() {
        User::create([
            'role_id' => \Myc\Domain\Users\Role::ROLE_ID_ADMIN,
            'first_name' => 'Jelmer',
            'last_name' => 'Wit',
            'last_name_prefix' => 'de',
            'email' => 'jelmer.dewit@gmail.com',
            'password' => bcrypt('jelmer'),
            'confirmed' => 1
        ]);

        User::create([
            'role_id' => \Myc\Domain\Users\Role::ROLE_ID_ADMIN,
            'first_name' => 'Frank',
            'last_name' => 'Eijking',
            'email' => 'frank@medianique.nl ',
            'password' => bcrypt('frank'),
            'confirmed' => 1
        ]);

        User::create([
            'role_id' => \Myc\Domain\Users\Role::ROLE_ID_ADMIN,
            'first_name' => 'Arne',
            'last_name'  => 'Hoorn',
            'last_name_prefix' => 'van',
            'email' => 'arne@vonkwerk.nl',
            'password' => bcrypt('arne'),
            'confirmed' => 1
        ]);
    }
}