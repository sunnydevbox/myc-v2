<?php
use Illuminate\Database\Seeder;
use Myc\Domain\Categories\Category;
use Myc\Domain\Categories\Tag;

class TagTableSeeder extends Seeder {

    public function run()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'Main'
            ],
            [
                'id' => 2,
                'name' => 'Advanced'
            ]
        ];

        foreach ($categories as $category) {
            $attributes = $category;
            Category::create($attributes);
        }

        $tags = [
            [
                'name' => 'Open Space',
                'category_id' => 1
            ],
            [
                'name' => 'Building',
                'category_id' => 1
            ],
            [
                'name' => 'Industrial Zone',
                'category_id' => 1
            ],
            [
                'name' => 'Commercial Zone',
                'category_id' => 1
            ],
            [
                'name' => 'Residential Zone',
                'category_id' => 1
            ],
            [
                'name' => 'Harbour Front',
                'category_id' => 2
            ],
            [
                'name' => 'Park',
                'category_id' => 2
            ],
            [
                'name' => 'Public Transit Access',
                'category_id' => 2
            ],
            [
                'name' => 'Brown Field',
                'category_id' => 2
            ],
            [
                'name' => 'Market',
                'category_id' => 2
            ],
            [
                'name' => 'Religious',
                'category_id' => 2
            ],
            [
                'name' => 'Cultural',
                'category_id' => 2
            ],
            [
                'name' => 'Farming',
                'category_id' => 2
            ],
            [
                'name' => 'Public School',
                'category_id' => 2
            ],
            [
                'name' => 'Sports',
                'category_id' => 2
            ],
            [
                'name' => 'Heritage',
                'category_id' => 2
            ]
        ];

        foreach ($tags as $tag) {
            $attributes = [
                'name' => $tag['name'],
                'category_id' => $tag['category_id']
            ];
            Tag::create($attributes);
        }

    }

}