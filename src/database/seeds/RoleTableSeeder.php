<?php
use Illuminate\Database\Seeder;
use Myc\Domain\Users\Role;

class RoleTableSeeder extends Seeder {

    public function run() {

        Role::create([
            'id' => 1,
            'name' => 'User',
            'slug' => 'user',
            'description' => 'The description of the User role'
        ]);

        Role::create([
            'id' => 2,
            'name' => 'Administrator',
            'slug' => 'admin',
            'description' => 'The description of the Administrator role'
        ]);

    }

}