<?php

namespace Myc\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Notifier
{
    private $username;
    private $password;
    private $apiKey;

    private $guzzleClient;

    private $channel = 'public';

    private $ttl = 86400;
    private $authenticated = false;

    private $loginUrl = 'users/login.json';
    private $notifyUrl = 'push_notification/notify_tokens.json';

    public function __construct($username, $password, $apiKey, Client $guzzleClient)
    {
        $this->username = $username;
        $this->password = $password;
        $this->apiKey = $apiKey;
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param int $ttl
     */
    public function setTtl($ttl)
    {
        $this->ttl = $ttl;
    }

    /**
     * @param string $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    public function sendMessage(array $payload, array $tokens)
    {
        // Add timestamp to payload
        $payload['send_uts'] = time();

        write_log(sprintf(
            'Pushing message % to device tokens %s',
            json_encode($payload),
            implode(',', $tokens)
        ));

        write_log(sprintf(
            'Payload: %s',
            json_encode($payload)
        ));

        if (!$this->authenticated) {
            $this->authenticate();
        }

        try {
            $response = $this->guzzleClient->post($this->notifyUrl,  [
                'body' => [
                    'channel' => $this->channel,
                    'payload' => json_encode($payload),
                    'to_tokens' => implode(',', $tokens),
                    'options' => json_encode(['expire_after_seconds' => $this->ttl]),
                ],
            ]);
        } catch (RequestException $e) {
            throw $e;
        }

        write_log(sprintf('response %s', $response->getBody()));


        return $response->getBody();
    }

    public function authenticate()
    {
        try {
            $response = $this->guzzleClient->post($this->loginUrl, [
                'body' => [
                    'login' => $this->username,
                    'password' => $this->password,
                ],
            ]);
        } catch (RequestException $e) {
            throw $e;
        }

        $this->authenticated = true;

        return $response->getBody();
    }
}
