<?php

namespace Myc\Commands;

use Geocoder\Geocoder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Bus\SelfHandling;
use Myc\Domain\Locations\Location;

class BatchProcessCsv extends Command implements SelfHandling
{
    use SerializesModels;

    /**
     * @var string Path to CSV file
     */
    private $csvFile;

    public function __construct($csvFile)
    {
        $this->csvFile = $csvFile;
    }

    public function handle(Location $location, Geocoder $geocoder)
    {
        // Force line ending detection on OSX
        if (!ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }

        try {
            if (!file_exists($this->csvFile)) {
                throw new \Exception('File does not exist.');
            }
            $file = fopen($this->csvFile, 'r');
        } catch (\Exception $e) {
            throw $e;
        }

        while (!feof($file)) {
            $line = fgetcsv($file, 0, ';');
            if (empty($line[0]) || empty($line[1])) {
                continue;
            }

            try {
                $geocode = $geocoder->geocode($line[1]);
                $address = $geocode->first();
            } catch (\Exception $e) {
                \Log::info('Error geocoding location: '.$e->getMessage());
                continue;
            }
            Location::unguard();
            dd($address);
            $location->create([
                'name' => utf8_encode($line[0]),
                //'address' => $line[1],
                'lat' => $address->getLatitude(),
                'lon' => $address->getLongitude(),
                'street_number' => $address->getStreetNumber(),
                'street_name' => $address->getStreetName(),
                'locality' => $address->getLocality(),
                'sub_locality' => $address->getSubLocality(),
                'postal_code' => $address->getPostalCode(),
                'country' => $address->getCountry()->getName(),
                'country_code' => $address->getCountry()->getCode(),
                'timezone' => $address->getTimezone(),
            ]);
        }
        fclose($file);
        $this->delete();
    }
}
