<?php

namespace Myc\Commands;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Myc\Domain\Categories\Tag;
use Myc\Domain\Images\ImageService;
use Myc\Domain\Locations\Location;
use Myc\Domain\Locations\LocationService;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Exceptions\ValidationException;

class ImportCsvFile extends Command implements SelfHandling, ShouldBeQueued
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var string
     */
    private $pathToFile;
    /**
     * @var Organization
     */
    private $organization;
    /**
     * @var User
     */
    private $creator;

    private $reportLines = [];
    private $errors = false;
    /**
     * @var
     */
    private $originalFilename;

    public function __construct($pathToFile, $originalFilename, User $creator, Organization $organization = null)
    {
        $this->pathToFile = $pathToFile;
        $this->organization = $organization;
        $this->creator = $creator;
        $this->originalFilename = $originalFilename;
    }

    public function handle(LocationService $locationService, ImageService $imageService)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        // Force line ending detection on OSX
        if (!ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }

        try {
            if (!file_exists($this->pathToFile)) {
                throw new \Exception('File does not exist.');
            }
            $file = fopen($this->pathToFile, 'r');
        } catch (\Exception $e) {
            throw $e;
        }

        $lineCount = 0;
        $this->addReportLine('Importing locations');
        while (!feof($file)) {
            $line = fgetcsv($file, 0, ';', '"');
            $lineCount++;

            if (!isset($line[0]) || !isset($line[1]) || empty($line[0]) || empty($line[1])) {
                continue;
            }

            $line = array_map('utf8_encode', array_map('trim', $line));

            $input = [
                'lat' => $line[0],
                'lon' => $line[1],
                'name' => isset($line[2]) ? $line[2] : '',
            ];

            try {
                $location = $locationService->createLocation($input, $this->creator, Location::STATUS_PUBLISHED);
                if (!is_null($this->organization)) {
                    $location->organizations()->sync([$this->organization->id], false);
                }
            } catch (ValidationException $e) {
                $this->addReportLine(sprintf(
                    'Line %s: latitude and longtitude are required. Location was not imported.',
                    $lineCount
                ));
                $this->errors = true;
                continue;
            } catch (\InvalidArgumentException $e) {
                $this->addReportLine(sprintf(
                    'Line %s: %s. Location was not imported.',
                    $lineCount,
                    $e->getMessage()
                ));
                $this->errors = true;
                continue;
            } catch (\Exception $e) {
                $this->addReportLine(sprintf(
                    'Line %s: %s. Location was not imported.',
                    $lineCount,
                    $e->getMessage()
                ));
                $this->errors = true;
                continue;
            }

            if (isset($line[3]) && !empty($line[3])) {
                try {
                    $imageService->remoteImageForLocation($line[3], $location);
                } catch (\InvalidArgumentException $e) {
                    $this->addReportLine(sprintf(
                        'Line %s: Image was not found. No image added to location.',
                        $lineCount
                    ));
                    $this->errors = true;
                }
            }

            if (isset($line[4]) && !empty($line[4])) {
                try {
                    $ids = array_map('intval', explode(',', $line[4]));
                    if (is_array($ids) && count($ids) > 0);
                    $tagIds = Tag::whereIn('id', $ids)->lists('id');
                    if (count($tagIds) > 0) {
                        $location->tags()->sync($tagIds);
                    }
                } catch (\Exception $e) {
                    $this->addReportLine(sprintf(
                        'Line %s: something went wrong when trying to add tags. Check format.',
                        $lineCount
                    ));
                    $this->errors = true;
                }
            }

            $locationService->updateIndex($location);
        }
        fclose($file);
        @unlink($this->pathToFile);
        $this->writeReport();

        // Delete job
        $this->delete();
    }

    private function addReportLine($message)
    {
        $this->reportLines[] = $message.PHP_EOL;
    }

    private function writeReport()
    {
        $message = $this->errors ? 'Done with errors' : 'Done (no errors)';
        $this->addReportLine($message);
        $filename = (new \DateTime())->format('Y_m_d_H_i_s').'_'.$this->originalFilename.'.log';
        \File::put(storage_path('app/import_logs/').$filename, implode('', $this->reportLines));
    }
}
