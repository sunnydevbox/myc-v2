<?php

namespace Myc\Commands;

use Geocoder\Geocoder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Bus\SelfHandling;
use Myc\Domain\Images\ImageService;
use Myc\Domain\Locations\Location;

class BatchProcessCsvAlkmaar extends Command implements SelfHandling
{
    use SerializesModels;

    /**
     * @var string Path to CSV file
     */
    private $csvFile;

    const IMAGE_URI = 'http://www.alkmaar.nl/etl-upload/giga/monumenten/foto/';

    public function __construct($csvFile)
    {
        $this->csvFile = $csvFile;
    }

    public function handle(
        Location $location,
        Geocoder $geocoder,
        ImageService $imageService
    ) {
        // Force line ending detection on OSX
        if (!ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }

        try {
            if (!file_exists($this->csvFile)) {
                throw new \Exception('File does not exist.');
            }
            $file = fopen($this->csvFile, 'r');
        } catch (\Exception $e) {
            throw $e;
        }

        while (!feof($file)) {
            $line = fgetcsv($file, 0, ',', '"');
            $line = array_map('utf8_encode', array_map('trim', $line));

            $street = $line[1];
            $number = ltrim($line[2], '0');

            if (empty($street) || empty($number)) {
                continue;
            }

            $type = $line[4]; // store as term
            $pathToImage = rawurlencode(ltrim($line[5], 'foto/'));

            try {
                $image = file_get_contents(static::IMAGE_URI.$pathToImage);
            } catch (\Exception $e) {
                $image = false;
                // Only process locations with images
                continue;
            }

            $addressString = implode(' ', [
                $street,
                $number,
                'Alkmaar',
            ]);

            try {
                $geocode = $geocoder->geocode($addressString.' Netherlands');
                $address = $geocode->first();
            } catch (\Exception $e) {
                write_log('Error geocoding location: '.$e->getMessage());
                continue;
            }
            Location::unguard();
            $theLocation = $location->create([
                'user_id' => 1,
                'name' => $addressString,
                'lat' => $address->getLatitude(),
                'lon' => $address->getLongitude(),
                'street_number' => $address->getStreetNumber(),
                'street_name' => $address->getStreetName(),
                'locality' => $address->getLocality(),
                'sub_locality' => $address->getSubLocality(),
                'postal_code' => $address->getPostalCode(),
                'country' => $address->getCountry()->getName(),
                'country_code' => $address->getCountry()->getCode(),
                'timezone' => $address->getTimezone(),
            ]);

            // Assign 3 random tags
            /*$randomTags = array_rand(Tag::all()->lists('id'), 3);
            $theLocation->tags()->sync([
                $randomTags[0],
                $randomTags[1],
                $randomTags[2]
            ]);*/

            if ($image) {
                $fullPath = static::IMAGE_URI.$pathToImage;
                $imageService->remoteImageForLocation($fullPath, $theLocation);
            }
        }

        fclose($file);
        $this->delete();
    }
}
