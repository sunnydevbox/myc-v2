<?php

namespace Myc\Handlers\Events;

use Illuminate\Contracts\Mail\Mailer;
use Myc\Events\UserWasRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendAccountConfirmationRequest implements ShouldBeQueued
{
    use InteractsWithQueue;

    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param UserWasRegistered $event
     */
    public function handle(UserWasRegistered $event)
    {
        //write_log('sending confirmation mail to '.$event->getUser()->name);
        // Send a confirmation mail
        $user = $event->getUser();
        $this->mailer->send('emails.confirm_account', ['user' => $user], function ($mail) use ($user) {
            $mail->to($user->email)->subject('Thanks for joining Map Your City');
        });
    }
}
