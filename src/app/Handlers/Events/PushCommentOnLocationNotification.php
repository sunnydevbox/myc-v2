<?php

namespace Myc\Handlers\Events;

use Illuminate\Support\Collection;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Followers\Follower;
use Myc\Domain\Followers\FollowerRepository;
use Myc\Domain\Locations\Location;
use Myc\Domain\Messages\MessageRepository;
use Myc\Domain\Organizations\OrganizationRepository;
use Myc\Domain\Users\User;
use Myc\Events\CommentOnLocationWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Myc\Services\Notifier;

class PushCommentOnLocationNotification implements ShouldBeQueued
{
    use InteractsWithQueue;

    /**
     * @var FavoriteRepository
     */
    private $favorites;
    /**
     * @var MessageRepository
     */
    private $messages;
    /**
     * @var Notifier
     */
    private $notifier;
    /**
     * @var FollowerRepository
     */
    private $followers;
    /**
     * @var OrganizationRepository
     */
    private $organizations;

    public function __construct(
        MessageRepository $messages,
        FavoriteRepository $favorites,
        FollowerRepository $followers,
        OrganizationRepository $organizations,
        Notifier $notifier
    ) {
        $this->messages = $messages;
        $this->favorites = $favorites;
        $this->chunkSize = 100;
        $this->notifier = $notifier;
        $this->followers = $followers;
        $this->organizations = $organizations;
    }

    /**
     * Handle the event.
     *
     * @param CommentOnLocationWasCreated $event
     */
    public function handle(CommentOnLocationWasCreated $event)
    {
        $comment = $event->getComment();
        $author = $comment->user;
        $location = $event->getLocation();

        $title = sprintf(
            '%s placed a comment on the wall of a location',
            $comment->user->display_name
        );

         $titleHtml = sprintf(
            '<b>%s</b> placed a comment on the wall of a location',
            $comment->user->display_name
        );

        $eventDescription = [
            'event' => 'comment_on_location_was_created',
            'location_id' => to_hashid($comment->location_id),
            'comment_id' => to_hashid($comment->id),
        ];

        $pushPayload = [
            'title' => $title,
            'alert' => $title,
            'event' => $eventDescription,
            'vibrate' => true,
            'sound' => 'default',
        ];


        $usersFollowingOrganization = $this->getUsersFollowingOrganizationsWhereTheLocationBelongs( $location );
        $usersWithFavorite = $this->getUsersFavoritedLocation( $location );
        $usersToNotify = $usersFollowingOrganization->merge( $usersWithFavorite );

        // Remove duplicates and exclude the user that initiated the event!
        $usersToNotify = $usersToNotify->unique( function( User $user ) {
            return $user->id;
        })/*->filter( function( User $user ) use ( $author ) {
            return $user->id != $author->id;
        } )*/;

        // Bulk insert a message for each user
        $ids = $usersToNotify->lists('id');
        $chunks = array_chunk($ids, $this->chunkSize);
        foreach ($chunks as $userIdsChunk) {
            $this->messages->addMessageForUserIds($eventDescription, $titleHtml, $userIdsChunk, $author->id);
        }

        // Send out a push notification to each user that has a device token registered (filter out null values)
        $deviceTokens = array_filter($usersToNotify->lists('device_token'), function ($token) {
            return !is_null($token);
        });

        $chunks = array_chunk($deviceTokens, $this->chunkSize);
        foreach ($chunks as $chunk) {
            $this->notifier->sendMessage($pushPayload, $chunk);
        }
    }

    /**
     * @param Location $location
     * @return Collection
     */
    private function getUsersFollowingOrganizationsWhereTheLocationBelongs( Location $location ) {
        $organizations = $this->organizations->filterByLocation( $location );
        $usersFollowingOrganization = new Collection();
        foreach ( $organizations->all() as $organization ) {
            $usersFollowingOrganization = $usersFollowingOrganization->merge( $this->followers->getByOrganization( $organization ) );
        }

        return $usersFollowingOrganization->map( function( Follower $follower ) {
            return $follower->user;
        });
    }

    /**
     * @param Location $location
     * @return static
     */
    private function getUsersFavoritedLocation( Location $location ) {
        return $this->favorites->getUsersWhoFavoriteLocation( $location );
    }
}
