<?php

namespace Myc\Handlers\Events;

use Myc\Domain\Messages\MessageRepository;
use Myc\Events\CommentWasFavorited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Myc\Services\Notifier;

class PushCommentFavoritedNotification implements ShouldBeQueued
{
    use InteractsWithQueue;

    public function __construct(
        MessageRepository $messages,
        Notifier $notifier
    ) {
        $this->messages = $messages;
        $this->notifier = $notifier;
    }

    /**
     * Handle the event.
     *
     * @param CommentWasFavorited $event
     */
    public function handle(CommentWasFavorited $event)
    {
        // When a Comment was favorited; we need to send a message
        // to the user that created this comment

        $comment = $event->getComment();
        $user = $event->getUser();

        $title = sprintf(
            '%s likes your comment',
            $user->display_name
        );

        $titleHtml = sprintf(
            '<b>%s</b> likes your comment',
            $user->display_name
        );

        // If this is a comment on a comment, the location ID
        // must be resolved from the parent comment
        if (!is_null($comment->parent_id)) {
            $locationId = $comment->parent->location_id;
        } else {
            $locationId = $comment->location_id;
        }

        if (empty($locationId)) {
            throw new \InvalidArgumentException(sprintf(
                'No location resolved from comment'
            ));
        }

        $eventDescription = [
            'event' => 'comment_was_favorited',
            'parent_id' => is_null($comment->parent_id) ? null : to_hashid($comment->parent_id),
            'comment_id' => to_hashid($comment->id),
            'location_id' => to_hashid($locationId),
        ];

        $pushPayload = [
            'title' => $title,
            'alert' => $title,
            'event' => $eventDescription,
            'vibrate' => true,
            'sound' => 'default',
        ];

        $creator = $comment->user;

        // If the user favorites a location he added himself, don't send a message
        if ($creator->id != $user->id) {
            $this->messages->addMessageForUserIds($eventDescription, $titleHtml, [$creator->id], $user->id);
            $deviceToken = $creator->device_token;
            if (!is_null($deviceToken) && !empty($deviceToken)) {
                $this->notifier->sendMessage($pushPayload, [$deviceToken]);
            }
        }
    }
}
