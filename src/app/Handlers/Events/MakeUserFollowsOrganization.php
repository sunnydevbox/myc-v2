<?php

namespace Myc\Handlers\Events;

use Myc\Domain\Followers\FollowerRepository;
use Myc\Events\UserEnrolledToOrganization;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class MakeUserFollowsOrganization implements ShouldBeQueued
{

    /**
     * @var FollowerRepository
     */
    private $followers;

    public function __construct(FollowerRepository $followers)
    {
        $this->followers = $followers;
    }

    /**
     * Handle the event.
     *
     * @param UserEnrolledToOrganization $event
     */
    public function handle(UserEnrolledToOrganization $event) {
        if ( $event->getRole() == 'member' ) {
            $this->followers->addOrganizationFollower( $event->getOrganization(), $event->getUser() );
        }
    }
}
