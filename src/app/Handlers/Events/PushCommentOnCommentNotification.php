<?php

namespace Myc\Handlers\Events;

use Myc\Domain\Messages\MessageRepository;
use Myc\Events\CommentOnCommentWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Myc\Services\Notifier;

class PushCommentOnCommentNotification implements ShouldBeQueued
{
    use InteractsWithQueue;

    /**
     * @var MessageRepository
     */
    private $messages;
    /**
     * @var Notifier
     */
    private $notifier;

    public function __construct(
        MessageRepository $messages,
        Notifier $notifier
    ) {
        $this->messages = $messages;
        $this->notifier = $notifier;
    }

    /**
     * Handle the event.
     *
     * @param CommentOnCommentWasCreated $event
     */
    public function handle(CommentOnCommentWasCreated $event)
    {
        $comment = $event->getComment();
        $user = $comment->user;

        $parent = $event->getParent();
        $commentOwner = $parent->user;

        $title = sprintf(
            '%s placed a comment on your comment',
            $user->display_name
        );

        $titleHtml = sprintf(
            '<b>%s</b> placed a comment on your comment',
            $user->display_name
        );

        $eventDescription = [
            'event' => 'comment_on_comment_was_created',
            'parent_id' => to_hashid($parent->id),
            'comment_id' => to_hashid($comment->id),
        ];

        $pushPayload = [
            'title' => $title,
            'alert' => $title,
            'event' => $eventDescription,
            'vibrate' => true,
            'sound' => 'default',
        ];

        // If the user favorites a location he added himself, don't send a message
        if ($commentOwner->id != $user->id) {
            $this->messages->addMessageForUserIds($eventDescription, $titleHtml, [$commentOwner->id], $user->id);
            $deviceToken = $commentOwner->device_token;
            if (!is_null($deviceToken) && !empty($deviceToken)) {
                $this->notifier->sendMessage($pushPayload, [$deviceToken]);
            }
        }
    }
}
