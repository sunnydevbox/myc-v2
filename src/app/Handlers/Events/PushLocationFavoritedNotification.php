<?php

namespace Myc\Handlers\Events;

use Carbon\Carbon;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Messages\MessageRepository;
use Myc\Events\LocationWasFavorited;
use Myc\Services\Notifier;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class PushLocationFavoritedNotification implements ShouldBeQueued
{
    use InteractsWithQueue;

    /**
     * @var \Myc\Domain\Favorites\FavoriteRepository
     */
    private $favorites;

    /**
     * @var \Myc\Services\Notifier
     */
    private $notifier;

    /**
     * @var \Myc\Domain\Messages\MessageRepository
     */
    private $messages;

    private $chunkSize = 800;

    public function __construct(
        FavoriteRepository $favorites,
        Notifier $notifier,
        MessageRepository $messages
    ) {
        $this->favorites = $favorites;
        $this->notifier = $notifier;
        $this->messages = $messages;
    }

    /**
     * Handle the event.
     *
     * @param LocationWasFavorited $event
     */
    public function handle(LocationWasFavorited $event)
    {
        // When a Location is favorited; we need to send a message
        // to the user that created this location

        $location = $event->getLocation();
        $user = $event->getUser();

        $title = sprintf(
            '%s likes a location you added',
            $user->display_name
        );

        $titleHtml = sprintf(
            '<b>%s</b> likes a location you added',
            $user->display_name
        );

        $eventDescription = [
            'event' => 'location_was_favorited',
            'location_id' => to_hashid($location->id),
        ];

        $pushPayload = [
            'title' => $title,
            'alert' => $title,
            'event' => $eventDescription,
            'vibrate' => true,
            'sound' => 'default',
        ];

        $creator = $location->user;

        // If the user favorites a location he added himself, don't send a message
        if ($creator->id != $user->id) {
            $this->messages->addMessageForUserIds($eventDescription, $titleHtml, [$creator->id], $user->id);
            $deviceToken = $creator->device_token;
            if (!is_null($deviceToken) && !empty($deviceToken)) {
                $this->notifier->sendMessage($pushPayload, [$deviceToken]);
            }
        }
    }
}
