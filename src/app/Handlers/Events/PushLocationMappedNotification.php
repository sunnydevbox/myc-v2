<?php

namespace Myc\Handlers\Events;

use App;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Myc\Domain\Followers\Follower;
use Myc\Domain\Followers\FollowerRepository;
use Myc\Domain\Messages\MessageRepository;
use Myc\Domain\Organizations\Organization;
use Myc\Events\LocationWasMappedToAnOrganization;
use Myc\Services\Notifier;

class PushLocationMappedNotification implements ShouldBeQueued
{
    use InteractsWithQueue;

    /**
     * @var FollowerRepository
     */
    private $followers;

    /**
     * @var MessageRepository
     */
    private $messages;

    /**
     * @var Notifier
     */
    private $notifier;

    public function __construct(MessageRepository $messages, FollowerRepository $followers, Notifier $notifier ) {
        $this->followers = $followers;
        $this->messages = $messages;
        $this->notifier = $notifier;
    }

    /**
     * Handle the event.
     *
     * @param LocationWasMappedToAnOrganization $event
     */
    public function handle(LocationWasMappedToAnOrganization $event) {

        $organization = $event->getOrganization();
        $location = $event->getLocation();
        $author = $event->getCreator();

        $followers = $this->followers->getByOrganization( $organization )->all();
        $followers = $this->removeAuthor( $followers, $author );

        $allFollowersIds = $this->getUserIds( $followers );

        $eventDescription = [
            'event' => 'location_was_mapped_to_an_organization',
            'organization_id' => to_hashid($organization->id),
            'location_id' => to_hashid( $location->id )
        ];

        $pushPayload = [
            'title' => $this->getTitle( $organization ),
            'alert' => $this->getTitle( $organization ),
            'event' => $eventDescription,
            'vibrate' => true,
            'sound' => 'default',
        ];

        $this->messages->addMessageForUserIds($eventDescription, $this->getTitleHtml( $organization ), $allFollowersIds, $author->id );

        $followersWithDeviceToken = $this->removeUsersWithoutDevices( $followers );
        if ( count( $followersWithDeviceToken ) == 0 ) {
            return;
        }

        $deviceTokens = $this->getDeviceTokens( $followersWithDeviceToken );

        if ( App::environment( 'production' ) ) {
            $this->notifier->sendMessage($pushPayload, $deviceTokens) ;
        }
        else {
            write_log( 'Skipped to notify devices because environment is not production' );
        }
    }

    private function getTitleHtml( Organization $organization ) {
        return sprintf( 'Check it out! <b>%s</b> has added a new location for you to explore', $organization->name );
    }

    private function getTitle( Organization $organization ) {
        return sprintf( 'Check it out! %s has added a new location for you to explore', $organization->name );
    }

    /**
     * @param $followers
     * @return array
     */
    private function removeUsersWithoutDevices( $followers ) {
        $followersWithDeviceToken = array_filter( $followers, function ( Follower $item ) {
            $user = $item->user;
            $deviceToken = $user->device_token;
            return !is_null( $deviceToken ) && !empty( $deviceToken );
        } );
        return $followersWithDeviceToken;
    }

    /**
     * @param $followers
     * @return array
     */
    private function getUserIds( $followers ) {
        return array_map( function ( Follower $follower ) {
            return $follower->user_id;
        }, $followers );
    }

    /**
     * @param $followersWithDeviceToken
     * @return array
     */
    private function getDeviceTokens( $followersWithDeviceToken ) {
        return array_map( function ( Follower $follower ) {
            return $follower->user->device_token;
        }, $followersWithDeviceToken );
    }

    /**
     * @param $followers
     * @param $author
     * @return array
     */
    private function removeAuthor( $followers, $author ) {
        return array_filter( $followers, function ( Follower $follower ) use ( $author ) {
            return $follower->user_id != $author->id;
        } );
    }
}
