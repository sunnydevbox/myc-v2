<?php

namespace Myc\Auth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Token\AccessToken;
use Myc\Exceptions\InvalidExchangeTokenException;

class FacebookClient
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzle;

    private $url = 'https://graph.facebook.com';

    /**
     * @var \League\OAuth2\Client\Token\AccessToken
     */
    private $token;

    public function __construct(
        Client $guzzle,
        Facebook $provider,
        $clientId,
        $clientSecret
    ) {
        $this->guzzle = $guzzle;
        $this->provider = $provider;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function getLongLivedToken($shortLivedToken = null)
    {
        $query = [
            'grant_type' => 'fb_exchange_token',
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'fb_exchange_token' => $shortLivedToken,
        ];

        try {
            $response = $this->guzzle->get($this->url.'/oauth/access_token', ['query' => $query]);
        } catch (RequestException $e) {
            if ('400' == $e->getResponse()->getStatusCode()) {
                throw new InvalidExchangeTokenException('Exchange Token Invalid');
            }
            throw $e;
        }

        parse_str($response->getBody(), $data);

        if (!isset($data['access_token'])) {
            throw new \Exception('Invalid response returned by Facebook');
        }

        return new AccessToken($data);
    }

    /**
     * @return AccessToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param AccessToken $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Get Facebook account details.
     *
     * @return \League\OAuth2\Client\Entity\User
     *
     * @throws \Exception
     */
    public function getUserDetails()
    {
        if (is_null($this->token)) {
            throw new \InvalidArgumentException('No access token available');
        }

        try {
            $user = $this->provider->getUserDetails($this->token);
        } catch (\Exception $e) {
            throw $e;
        }

        return $user;
    }
}
