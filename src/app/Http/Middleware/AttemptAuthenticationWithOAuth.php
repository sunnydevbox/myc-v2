<?php

namespace Myc\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use League\OAuth2\Server\Exception\AccessDeniedException;
use League\OAuth2\Server\Exception\InvalidRequestException;
use LucaDegasperi\OAuth2Server\Authorizer;
use Myc\Domain\Users\User;

class AttemptAuthenticationWithOAuth
{
    /**
     * @var Guard
     */
    private $auth;
    /**
     * @var Authorizer
     */
    private $oauth;

    public function __construct(Guard $auth, Authorizer $oauth)
    {
        $this->auth = $auth;
        $this->oauth = $oauth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->headers->get('Authorization');
        if (!empty($token)) {
            try {
                $this->oauth->validateAccessToken(true);
                // Verify the user ID registered with the access code exists
                $user = User::find((int) $this->oauth->getResourceOwnerId());

                // Catch exceptions thrown by OAuth2 server but don't take any
                // action since authentication is optional
            } catch (AccessDeniedException $e) {
            } catch (InvalidRequestException $e) {
            }

            if (isset($user) && !is_null($user)) {
                // If user is found as a result of validation of the
                // Bearer token, login user without sessions or cookies
                $this->auth->onceUsingId($user->id);
            }
        }

        return $next($request);
    }
}
