<?php namespace Myc\Http\Middleware;

use Closure;
use Illuminate\Support\ViewErrorBag;

class ShareErrorsFromSession extends \Illuminate\View\Middleware\ShareErrorsFromSession {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if ($request->hasSession() && $request->session()->has('errors')) {
            $this->view->share('errors', $request->session()->get('errors'));
        } else {
            $this->view->share('errors', new ViewErrorBag());
        }

        return $next($request);
	}

}
