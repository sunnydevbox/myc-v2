<?php

namespace Myc\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class IsAdminOrOwner
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($request->user()->isAdmin() || $request->user()->ownsSomeOrganizations())) {
            throw new AccessDeniedHttpException(sprintf(
                'You must either be an administrator or an organization owner to access this resource.'
            ));
        }

        return $next($request);
    }
}
