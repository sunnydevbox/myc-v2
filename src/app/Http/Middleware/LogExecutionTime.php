<?php

namespace Myc\Http\Middleware;

use Closure;

class LogExecutionTime
{
    private $slowRequest = 300;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $startTime = microtime(true);

        $response = $next($request);

        $executionTime = round((microtime(true) - $startTime) * 1000);

        if ($executionTime > $this->slowRequest) {
            $message = sprintf(
                '"%s","%s","%s","%s"',
                (new \DateTime())->format('Y-m-d H:i:m:s'),
                isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'local',
                $request->fullUrl(),
                $executionTime
            );
            @file_put_contents(storage_path('logs/').'execution_times.csv', $message."\n", FILE_APPEND);
        }

        return $response;
    }
}
