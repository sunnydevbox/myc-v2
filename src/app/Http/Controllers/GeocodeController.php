<?php

namespace Myc\Http\Controllers;

use Geocoder\Geocoder;
use Myc\Domain\Locations\Address;
use Myc\Domain\ValueObjects\LatLon;
use Illuminate\Http\Request;
use League\Fractal;

class GeocodeController extends ApiController
{
    /**
     * @var \Geocoder\Geocoder
     */
    private $geocoder;

    /**
     * @var Fractal\Manager
     */
    private $fractal;

    public function __construct(
        Fractal\Manager $fractal,
        Geocoder $geocoder
    ) {
        $this->fractal = $fractal;
        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
        $this->geocoder = $geocoder;
    }

    public function geocode(Request $request)
    {
        try {
            $latlon = LatLon::createFromQueryParam($request->input('latlon'));
        } catch (\InvalidArgumentException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }

        $geocoderAddress = $this->geocoder
            ->limit(1)
            ->reverse($latlon->getLat(), $latlon->getLon())
            ->first();

        if (is_null($geocoderAddress)) {
            return $this->errorNotFound('No Results From Geocode Providers.');
        }

        $address = Address::createFromGeocoderAddress($geocoderAddress);

        return $this->respondWithItem($this->fractal, $address, function (Address $address) {
            return array_merge(['type' => 'addresses'], $address->toArray());
        });
    }
}
