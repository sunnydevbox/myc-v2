<?php

namespace Myc\Http\Controllers;

use Geocoder\Geocoder;
use Illuminate\Auth\Guard;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Images\ImageTransformer;
use Myc\Domain\Locations\Location;
use Myc\Domain\Locations\LocationRepository;
use Myc\Domain\Locations\LocationService;
use Myc\Domain\Locations\LocationTransformer;
use Myc\Domain\Organizations\OrganizationRepository;
use Myc\Domain\Profiles\ProfileRepository;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\BoundingBox;
use Myc\Domain\ValueObjects\Distance;
use Myc\Domain\ValueObjects\LatLon;
use Myc\Exceptions\ValidationException;
use League\Fractal;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Myc\Domain\Pins\PinRepository;
use Intervention\Image\Image;

class LocationsController extends ApiController
{
    /**
     * @var LocationRepository
     */
    private $locations;
    /**
     * @var Fractal\Manager
     */
    private $fractal;
    /**
     * @var Geocoder
     */
    private $geocoder;
    /**
     * @var LocationService
     */
    private $locationService;
    /**
     * @var FavoriteRepository
     */
    private $favorites;
    /**
     * @var ProfileRepository
     */
    private $profiles;
    /**
     * @var OrganizationRepository
     */
    private $organizations;

    /**
     * @var PinRepository
     */
    private $pins;

    /**
     * @param Fractal\Manager $fractal
     * @param Geocoder $geocoder
     * @param Guard $auth
     * @param LocationService $locationService
     * @param FavoriteRepository $favorites
     * @param ProfileRepository $profiles
     * @param OrganizationRepository $organizations
     * @param PinRepository $pins
     */
    public function __construct(
        Fractal\Manager $fractal,
        Geocoder $geocoder,
        Guard $auth,
        LocationService $locationService,
        FavoriteRepository $favorites,
        ProfileRepository $profiles,
        OrganizationRepository $organizations,
        PinRepository $pins
    ) {
        $this->fractal = $fractal;
        $this->geocoder = $geocoder;
        $this->auth = $auth;
        $this->locationService = $locationService;
        $this->favorites = $favorites;
        $this->profiles = $profiles;
        $this->organizations = $organizations;
        $this->pins = $pins;

        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
        $this->locations = app()->make('Myc\Domain\Locations\ElasticSearchLocationRepository');
    }

    public function index(Request $request)
    {
        try {
            if ($request->has('bounding_box')) {
                $boundingBox = BoundingBox::createFromQueryParam($request->input('bounding_box'));
                $this->locations->filterByBoundingBox($boundingBox);
            }

            if ($request->has('latlon')) {
                $latLon = LatLon::createFromQueryParam($request->get('latlon'));
                $distance = new Distance($request->has('distance') ? $request->input('distance') : '100m');
                $this->locations->filterByDistanceFrom($latLon, $distance);
            }

            $applyUserFilter = $request->has( 'filter_by_user' ) && $request->input('filter_by_user') == '1';
            if ( $applyUserFilter && $this->auth->getUser() ) {
                /** @var User $user */
                $user = $this->auth->getUser();
                $this->locations->filterByUser( $user );
            }

            if ($request->has('tags')) {
                $tagIds = array_map('intval', explode(',', $request->input('tags')));
                $this->locations->filterByTagIds($tagIds);
            }

            if ($request->has('favorited') && to_bool($request->input('favorited'))) {
                if (!$this->auth->check()) {
                    // You need to be logged in to filter on your favorites
                    return $this->errorUnauthorized();
                }
                $favoriteIds = $this->favorites->getLocationFavoriteIdsByUser($this->auth->user());
                // What is user has no favorites?
                $this->locations->filterByFavoriteIds($favoriteIds);
            }

            if ($request->has('q')) {
                $this->locations->queryByString($request->get('q'));
            }

            if ($request->has('organization_id')) {
                $organization = $this->organizations->getById(from_hashid($request->input('organization_id')));
                if (!$organization) {
                    return $this->errorNotFound('Organization Not Found');
                }
                $this->locations->filterByOrganization($organization);
            }

            $locations = $this->locations->getAllPaginated($request->input('limit', 40), $request->input('page', 1));
        } catch (\InvalidArgumentException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }

        if (!isset($favoriteIds)) {
            // Favorite IDs may be set already when filtering on favorites
            $favoriteIds = $this->auth->check() ? $this->favorites->getLocationFavoriteIdsByUser($this->auth->user()) : [];
        }

        return $this->respondWithCollection(
            $this->fractal,
            $locations,
            new LocationTransformer($this->auth->check() ? $this->auth->user() : null, $favoriteIds)
        );
    }

    public function create(Request $request)
    {
        if (is_null($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $location = $this->locationService->createLocation($request->json('data'), $request->user());
        } catch (\InvalidArgumentException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }

        $favoriteIds = $this->favorites->getLocationFavoriteIdsByUser($request->user());

        return $this->setStatusCode(201)->respondWithItem(
            $this->fractal,
            $location,
            new LocationTransformer($request->user(),$favoriteIds)
        );
    }

    public function show($id)
    {
        // A detailed view contains the following resources
        $this->fractal->parseIncludes([
            'images',
            'tags',
            'user',
            'organizations',
            'organizations.profile',
            'comments',
        ]);

        $location = Location::find($id);
        if (!$location) {
            return $this->errorNotFound('The building you are looking for is removed or unpublished :(.');
        }

        $favoriteIds = $this->auth->check() ? $this->favorites->getLocationFavoriteIdsByUser($this->auth->user()) : [];
        $commentFavoriteIds = $this->auth->check() ? $this->favorites->getCommentFavoriteIdsByUser($this->auth->user()) : [];

        return $this->respondWithItem($this->fractal, $location, new LocationTransformer($this->auth->check() ? $this->auth->user() : null, $favoriteIds, $commentFavoriteIds, true));
    }

    public function update(Request $request, $id)
    {
        if (is_null($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $location = Location::find($id);
            if (!$location) {
                throw new ModelNotFoundException();
            }
            // Include user as argument because favorites can be added directly on location update
            $location = $this->locationService->updateLocation($location, $request->json('data'), $request->user());
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        }

        $includes = ['images', 'tags', 'organizations', 'organizations.profile', 'comments'];
        $location->load($includes);
        $this->fractal->parseIncludes($includes);
        $favoriteIds = $this->favorites->getLocationFavoriteIdsByUser($request->user());

        return $this->respondWithItem($this->fractal, $location, new LocationTransformer($request->user(), $favoriteIds));
    }

    public function destroy($id)
    {
        try {
            $location = Location::find($id);
            if (!$location) {
                throw new ModelNotFoundException();
            }
            $this->locationService->deleteLocation($location);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function getImages($id)
    {
        $location = Location::find($id);
        if (!$location) {
            return $this->errorNotFound();
        }

        return $this->respondWithCollection($this->fractal, $location->images, new ImageTransformer());
    }

    public function createFavorite(Request $request, $id)
    {
        try {
            $location = Location::find($id);
            if (!$location) {
                throw new ModelNotFoundException();
            }
            $this->favorites->addLocationFavoriteForUser($location, $request->user());
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function deleteFavorite($id)
    {
        try {
            $location = Location::find($id);
            if (!$location) {
                throw new ModelNotFoundException();
            }
            $this->favorites->deleteLocationFavoriteForUser($location, $this->auth->user());
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function showHtml($id)
    {
        $location = Location::find($id);
        if (!$location || !$location->isPublished()) {
            throw new NotFoundHttpException();
        }

        $location->load([
            'images',
            'tags',
            'user',
            'organizations',
            'organizations.profile',
            'comments',
        ]);

        return view('locations.show', ['location' => $location]);
    }

    public function showAll()
    {
        $pins = $this->pins
            ->includeBounds()
            ->search()
            ->getAll();

        $resource = new Fractal\Resource\Collection($pins, function ($pin) { return $pin; });
        $root = $this->fractal->createData($resource)->toArray();
        $root['bounding_box'] = $this->pins->getBoundingBox()->toArray();

        return view('locations.show_all', [
            'pins' => $root
        ]);
    }
}
