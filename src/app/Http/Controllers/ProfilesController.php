<?php

namespace Myc\Http\Controllers;

use Event;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Profiles\Fields\FieldType;
use Myc\Domain\Profiles\ProfileRepository;
use Myc\Domain\Profiles\ProfileTransformer;
use Myc\Events\LocationWasMappedToAnOrganization;
use Myc\Exceptions\ValidationException;
use League\Fractal;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProfilesController extends ApiController
{
    public function __construct(
        Fractal\Manager $fractal,
        ProfileRepository $profiles
    ) {
        $this->fractal = $fractal;
        $this->profiles = $profiles;

        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
    }

    public function show($locationId, $organizationId)
    {
        $location = Location::find(from_hashid($locationId));
        $organization = Organization::find(from_hashid($organizationId));

        if (!$location || !$organization) {
            return $this->errorNotFound();
        }

        try {
            $profile = $this->profiles->getProfileForOrganizationAndLocation($organization, $location);
        } catch (\InvalidArgumentException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }

        return $this->respondWithItem($this->fractal, $profile, new ProfileTransformer());
    }

    public function update(Request $request, $locationId, $organizationId)
    {
        $locationId = from_hashid($locationId);
        $organizationId = from_hashid($organizationId);

        if (is_null($request->json('data')) || !is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            /** @var Location $location */
            $location = Location::find($locationId);
            /** @var Organization $organization */
            $organization = Organization::find($organizationId);
            if (!$location || !$organization) {
                return $this->errorNotFound();
            }

            $isLocationNewInTheOrganization = !$organization->hasLocation( $location );

            if (!$request->user()->isAdmin() && !$request->user()->isMember($organization)) {
                throw new \InvalidArgumentException(sprintf(
                    "You can't edit this profile because you are not a member of %s.",
                    $organization->name
                ));
            }

            if ($organization->locationProfileLimitIsReached() && !$organization->hasLocation($location)) {
                throw new AccessDeniedHttpException(sprintf(
                    'The number of locations this organization can submit profile information for has been reached (limit is %s). Please upgrade the plan.',
                    $organization->building_number
                ));
            }

            $profile = $this->profiles->getProfileForOrganizationAndLocation($organization, $location);
            if (!$profile) {
                return $this->errorNotFound();
            }

            $input = array_reduce($request->json('data'), function ($memo, $item) {
                if (isset($item['link_href'])) {
                    $item['entry'] = sprintf(
                        '[%s](%s)',
                        isset($item['link_text']) ? $item['link_text'] : $item['link_href'],
                        $item['link_href']
                    );
                }
                if (!isset($item['id']) || !isset($item['entry'])) {
                    //throw new \InvalidArgumentException('ID and entry value must be set');
                } else {
                    $memo[(string) from_hashid($item['id'])] = $item['entry'];
                }

                return $memo;
            });

            foreach ($profile->getFields() as $field) {
                if ($field->isRequired() && !array_key_exists($field->id, $input)) {
                    // Field is required but is not part of POST body
                    throw new ValidationException(new MessageBag([
                        $field->id => 'The '.$field->label.' field is required.',
                    ]));
                }

                if (array_key_exists($field->id, $input)) {
                    if ($field->getIdentifier() === FieldType::FIELD_TYPE_MULTI_SELECT) {
                        $this->profiles->syncFieldOptions($field, $location, $input[$field->id]);
                    } else {
                        $field->setEntry($input[$field->id]);
                        $field->validate();
                        $this->profiles->upsertEntry($field, $location);
                    }
                }
            }

            // Attach the organization to the location (if not exists)
            $location->organizations()->sync([$organization->id], false);
        } catch (\InvalidArgumentException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        } catch (AccessDeniedHttpException $e) {
            return $this->errorForbidden($e->getMessage());
        }

        if ( $isLocationNewInTheOrganization ) {
            Event::fire( new LocationWasMappedToAnOrganization( $location, $organization, $request->user() ) );
        }

        return $this->respondWithItem($this->fractal, $profile, new ProfileTransformer());
    }
}
