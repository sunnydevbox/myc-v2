<?php

namespace Myc\Http\Controllers;

use Hashids\Hashids;
use Myc\Domain\Categories\Category;
use Myc\Domain\Categories\CategoryTransformer;
use League\Fractal;

class CategoriesController extends ApiController
{
    public function __construct(Fractal\Manager $fractal, Hashids $hashids)
    {
        $this->fractal = $fractal;
        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
        $this->hashids = $hashids;
    }

    public function index()
    {
        $categories = Category::all();

        return $this->respondWithCollection($this->fractal, $categories, new CategoryTransformer());
    }
}
