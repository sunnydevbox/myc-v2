<?php

namespace Myc\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Auth\Guard;
use League\Fractal\Manager;
use Myc\Domain\Followers\FollowerRepository;
use Myc\Domain\Organizations\OrganizationRepository;
use Myc\Domain\Organizations\OrganizationTransformer;
use Myc\Domain\Pins\PinRepository;
use Myc\Domain\Profiles\ProfileRepository;
use League\Fractal;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrganizationsController extends ApiController
{
    /**
     * @var OrganizationRepository
     */
    private $organizations;
    /**
     * @var PinRepository
     */
    private $pins;
    /**
     * @var Manager
     */
    private $fractal;
    /**
     * @var ProfileRepository
     */
    private $profiles;
    /**
     * @var FollowerRepository
     */
    private $followers;

    public function __construct(
        OrganizationRepository $organizations,
        ProfileRepository $profiles,
        PinRepository $pins,
        Guard $auth,
        Fractal\Manager $fractal,
        FollowerRepository $followers
    ) {
        $this->organizations = $organizations;
        $this->pins = $pins;
        $this->auth = $auth;
        $this->fractal = $fractal;
        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
        $this->profiles = $profiles;
        $this->followers = $followers;
    }

    public function show(Request $request, $id)
    {
        // A detailed view contains the following resources
        // $this->fractal->parseIncludes([
        //     'images',
        //     'tags',
        //     'user',
        //     'organizations',
        //     'organizations.profile',
        //     'comments',
        // ]);

        $organization = $this->organizations->getById($id);
        if (!$organization) {
            return $this->errorNotFound();
        }

        // $favoriteIds = $this->auth->check() ? $this->favorites->getLocationFavoriteIdsByUser($this->auth->user()) : [];
        // $commentFavoriteIds = $this->auth->check() ? $this->favorites->getCommentFavoriteIdsByUser($this->auth->user()) : [];

        $user = $this->auth->user(); //If the request is anonymous it just will return null
        return $this->respondWithItem($this->fractal, $organization, new OrganizationTransformer( null, $user ) );
    }

    public function showHtml(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);

        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $profile = $this->profiles->getProfileForOrganizationAndLocation($organization, null);

        if (!$this->organizations->hasLocations($organization)) {
            //TODO add ability to set bounding_box in organization setup
            $root = [
                    'data' => false,
                    'bounding_box' => [
                        'top_left' => [
                            "lat" => 52.423167,
                            "lon" => 4.762311
                        ],
                        'bottom_right' => [
                            "lat" => 52.294471,
                            "lon" => 5.006827
                        ]
                    ]];
        } else {
            // We need to know the precision for the grid to display all
            // pins associated with the organization, need to do a search first

            $preFetch = $this->pins
                ->filterByOrganization($organization)
                ->includeBounds()
                ->search();

            // Do another search to get the correct buckets
            $pins = $this->pins
                ->filterByBoundingBox($preFetch->getBoundingBox())
                ->setPrecision($preFetch->getBoundingBox()->getPrecision())
                ->search()
                ->getAll();

            //$resource = new Fractal\Resource\Collection($pins, function ($pin) { return $pin; });
            //$root = $this->fractal->createData($resource)->toArray();
        }

        return view('organizations.show', [
            'boundingBox' => $this->pins->getBoundingBox()->toArray(),
            'profile' => $profile,
            'organization' => $organization,
        ]);
    }

    public function follow(Request $request, $id) {
        $organization = $this->organizations->getById($id);

        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $this->followers->addOrganizationFollower( $organization, $request->user() );

        return $this->setStatusCode(204)->emptyResponse();

    }

    public function unfollow(Request $request, $id) {
        $organization = $this->organizations->getById($id);

        if (!$organization) {
            throw new NotFoundHttpException();
        }

        if ( !$this->followers->deleteOrganizationFollower( $organization, $request->user() ) ) {
            return $this->errorNotFound();
        }
        else {
            return $this->setStatusCode( 204 )->emptyResponse();
        }
    }
}
