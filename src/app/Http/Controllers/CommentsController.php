<?php

namespace Myc\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Fractal;
use Myc\Domain\Comments\CommentRepository;
use Myc\Domain\Comments\CommentService;
use Myc\Domain\Comments\CommentTransformer;
use Myc\Domain\Disapproval;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Locations\Location;
use Illuminate\Http\Request;
use Myc\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CommentsController extends ApiController
{
    /**
     * @var CommentRepository
     */
    private $comments;
    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    /**
     * @var CommentService
     */
    private $commentService;

    /**
     * @var FavoriteRepository
     */
    private $favorites;

    public function __construct(
        Fractal\Manager $fractal,
        CommentRepository $comments,
        CommentService $commentService,
        FavoriteRepository $favorites
    ) {
        $this->fractal = $fractal;
        $this->comments = $comments;
        $this->commentService = $commentService;
        $this->favorites = $favorites;
    }

    public function indexForLocation($hashid)
    {
        $location = Location::find($hashid);
        if (!$location) {
            return $this->errorNotFound();
        }

        return $this->respondWithCollection($this->fractal, $location->comments, new CommentTransformer());
    }

    public function indexForComment($hashid)
    {
        try {
            $comment = $this->comments->getById($hashid);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->respondWithCollection($this->fractal, $comment->comments, new CommentTransformer());
    }

    public function createOnLocation(Request $request, $hashid)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $location = Location::find($hashid);
            if (!$location) {
                throw new ModelNotFoundException();
            }
            $comment = $this->commentService->createCommentOnLocation($request->user(), $location, $request->json('data'));
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        }

        return $this->respondWithItem($this->fractal, $comment, new CommentTransformer());
    }

    public function createOnComment(Request $request, $hashid)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $parent = $this->comments->getById($hashid);
            $comment = $this->commentService->createCommentOnComment($request->user(), $parent, $request->json('data'));
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        }

        return $this->respondWithItem($this->fractal, $comment, new CommentTransformer());
    }

    public function createFavorite(Request $request, $id)
    {
        try {
            $comment = $this->comments->getById($id);
            $this->favorites->addCommentFavoriteForUser($comment, $request->user());
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function deleteFavorite(Request $request, $id)
    {
        try {
            $comment = $this->comments->getById($id);
            $this->favorites->deleteCommentFavoriteForUser($comment, $request->user());
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function show($id)
    {
        try {
            $comment = $this->comments->getById($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->respondWithItem($this->fractal, $comment, new CommentTransformer());
    }

    public function update(Request $request, $id)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $comment = $this->comments->getById($id);
            $this->commentService->updateComment($request->user(), $comment, $request->json('data'));
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        } catch (AccessDeniedHttpException $e) {
            return $this->errorForbidden();
        }

        return $this->respondWithItem($this->fractal, $comment, new CommentTransformer());
    }

    public function createDisapproval(Request $request, $id)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        $input = $request->json('data');

        try {
            $comment = $this->comments->getById($id);
            Disapproval::unguard();
            $disapproval = Disapproval::create([
                'user_id' => (is_null($request->user())) ? null : $request->user()->id,
                'comment_id' => $comment->id,
                'body' => $input['body'],
            ]);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->respondWithItem($this->fractal, $disapproval, function ($item) {
            return [
                'id' => to_hashid($item->id),
                'body' => $item->body,
            ];
        });
    }

    public function destroy($id)
    {
        // Any child comments have a ON DELETE CASCADE constraint so will be removed automatically
    }
}
