<?php

namespace Myc\Http\Controllers;

use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Search\Client as Search;
use League\Fractal;
use Illuminate\Http\Request;

class SearchController extends ApiController
{
    /**
     * @var Search
     */
    private $search;
    /**
     * @var Fractal\Manager
     */
    private $fractal;
    /**
     * @var FavoriteRepository
     */
    private $favorites;

    public function __construct(
        Fractal\Manager $fractal,
        Search $search,
        FavoriteRepository $favorites
    ) {
        $this->search = $search;
        $this->fractal = $fractal;
        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
        $this->favorites = $favorites;
    }

    public function search(Request $request)
    {
        if (!$request->has('q')) {
            return $this->errorInvalidArguments();
        }
        // Search locations as well as organizations and paginate

        $limit = 40;
        if ($request->has('limit')) {
            $limit = (int) $request->input('limit');
        }

        $params['index'] = Search::INDEX;
        $params['body']['size'] = $limit; // only return buckets
        $params['type'] = implode(',', [Search::LOCATION_TYPE, Search::ORGANIZATION_TYPE]);
        $params['body']['size'] = ($request->has('limit')) ? (int) $request->input('limit') : 40;
        $params['body']['query'] = [
            'match' => [
                '_all' => [
                    'query' => $request->input('q'),
                    'operator' => 'and',
                ],
            ],
        ];

        $this->search->search($params);

        $favoriteIds = [];
        if ($request->user()) {
            $favoriteIds = $this->favorites->getLocationFavoriteIdsByUser($request->user());
        }

        // TODO, set real relevance by type in ElasticSearch query
        $locations = [];
        $organizations = [];
        foreach ($this->search->getResults() as $result) {
            if (Search::LOCATION_TYPE === $result['_type']) {
                $locations[] = $this->search->transformResultToLocation($result['_source'], $request->user(), $favoriteIds);
            }
            if (Search::ORGANIZATION_TYPE === $result['_type']) {
                $organizations[] = $this->search->transformResultToOrganization($result['_source']);
            }
        }

        // Fake sorting
        $resource = array_merge($organizations, $locations);

        return $this->respondWithCollection($this->fractal, $resource, function ($item) {
            return $item;
        });
    }
}
