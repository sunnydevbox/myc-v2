<?php

namespace Myc\Http\Controllers;

use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\MessageBag;
use League\Fractal;

abstract class ApiController extends Controller
{
    private $statusCode = 200;

    protected function respondWithArray(array $data)
    {
        return response()->json($data, $this->statusCode);
    }

    protected function respondWithView(array $data)
    {
        return view('admin.locations.index', [
            'locations' => $locations,
            'categories' => Category::all(),
            'organizations' => Organization::all(),
            'perPage' => $perPage,
            'perPageSelected' => $perPageSelected
        ]);
    }

    protected function respondWithCollection(Fractal\Manager $fractal, $items, $callback)
    {
        if ($items instanceof LengthAwarePaginator) {
            $collection = $items->getCollection();
        } else {
            $collection = $items;
        }

        $resource = new Fractal\Resource\Collection($collection, $callback);

        if ($items instanceof LengthAwarePaginator) {
            $resource->setPaginator(new Fractal\Pagination\IlluminatePaginatorAdapter($items));
        }

        $root = $fractal->createData($resource);

        return $this->respondWithArray($root->toArray());
    }

    protected function respondWithItem(Fractal\Manager $fractal, $item, $callback)
    {
        $resource = new Fractal\Resource\Item($item, $callback);
        $root = $fractal->createData($resource);

        return $this->respondWithArray($root->toArray());
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    protected function emptyResponse()
    {
        return response('', $this->statusCode);
    }

    protected function respondWithError($message, $view = false)
    {
        if ($this->statusCode === 200) {
            trigger_error(
                'Error returned while status code is 200?',
                E_USER_WARNING
            );
        }

        if($view) {
            return $this->respondWithView([
                'error' => [
                    'http_code' => $this->getStatusCode(),
                    'message' => $message,
                ],
                'view' => $view
            ]);
        } else {
            return $this->respondWithArray([
                'error' => [
                    'http_code' => $this->getStatusCode(),
                    'message' => $message,
                ],
            ]);
        }
    }

    public function errorLocationNotFound($message = 'Resource Not Found')
    {
        return $this->setStatusCode(404)->respondWithError($message, 'locations.404');
    }

    public function errorNotFound($message = 'Resource Not Found')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function errorInvalidArguments($message = 'Invalid Arguments')
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    public function errorUnauthorized($message = 'Unauthorized')
    {
        return $this->setStatusCode(401)->respondWithError($message);
    }

    public function errorForbidden($message = 'Forbidden')
    {
        return $this->setStatusCode(403)->respondWithError($message);
    }

    public function errorInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    public function errorUnprocessableEntity(MessageBag $errors, $message = 'Unprocessable Entity')
    {
        return $this->setStatusCode(422)->respondWithArray([
            'error' => [
                'http_code' => $this->getStatusCode(),
                'message' => $message,
                'errors' => $errors->toArray(),
            ],
        ]);
    }
}
