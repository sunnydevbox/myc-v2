<?php

namespace Myc\Http\Controllers;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    public function getDocs()
    {
        $file = base_path().'/docs/api.html';
        header('Content-Type: text/html; charset=utf-8');
        header('Content-Length: '.filesize($file));
        readfile($file);
    }
}
