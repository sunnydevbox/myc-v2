<?php

namespace Myc\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use League\Glide;
use Myc\Domain\Disapproval;
use Myc\Domain\Images\Image;
use Myc\Domain\Images\ImageRepository;
use Myc\Domain\Images\ImageService;
use League\Fractal;
use Myc\Domain\Images\ImageTransformer;
use Myc\Domain\Locations\Location;
use Myc\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ImagesController extends ApiController
{
    /**
     * @var \League\Glide\Server
     */
    private $server;
    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var Fractal\Manager
     */
    private $fractal;
    /**
     * @var ImageRepository
     */
    private $images;

    public function __construct(
        Glide\Server $server,
        ImageRepository $images,
        ImageService $imageService,
        Fractal\Manager $fractal)
    {
        $this->server = $server;
        $this->images = $images;
        $this->imageService = $imageService;
        $this->fractal = $fractal;

        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
    }

    public function show($locationHashid, $imageHashid)
    {
        $imageId = from_hashid($imageHashid);
        $locationId = from_hashid($locationHashid);

        $image = Image::where('id', '=', $imageId)->where('location_id', '=', $locationId)->first();

        return $this->respondWithItem($this->fractal, $image, new ImageTransformer());
    }

    public function create(Request $request, $id)
    {
        if (!$request->hasFile('image')) {
            return $this->errorInvalidArguments('No file provided');
        }

        $file = $request->file('image');

        try {
            $image = $this->imageService->createImageForLocation(
                $file,
                $id,
                $request->user()
            );
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        }

        return $this->setStatusCode(201)->respondWithItem($this->fractal, $image, new ImageTransformer());
    }

    public function putSortOrder(Request $request, $id)
    {
        if (is_null($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $location = Location::find($id);
            if (!$location) {
                throw new ModelNotFoundException();
            }
            $this->imageService->setImageSortOrder($request->json('data'), $location);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function destroy(Request $request, $id)
    {
        try {
            $image = $this->images->getById($id);
            if (!$request->user()->isAdmin() && !$image->isOwnedBy($request->user())) {
                throw new AccessDeniedHttpException(sprintf(
                    'You must have administrator rights or you must have uploaded the image in order to delete it.'
                ));
            }
            $this->imageService->delete($image);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        } catch (AccessDeniedHttpException $e) {
            return $this->errorForbidden();
        }

        return $this->setStatusCode(204)->emptyResponse();
    }

    public function getAsset(Request $request)
    {
        try {
            return $this->server->outputImage($request);
        } catch (Glide\Http\NotFoundException $e) {
            return $this->errorNotFound();
        }
    }

    public function getAssetFromPath($path)
    {
        try {
            return $this->server->outputImage($path);
        } catch (Glide\Http\NotFoundException $e) {
            return $this->errorNotFound();
        }
    }

    public function createDisapproval(Request $request, $id)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }
        $input = $request->json('data');

        try {
            $image = Image::find($id);
            if (!$image) {
                throw new ModelNotFoundException();
            }
            Disapproval::unguard();
            $disapproval = Disapproval::create([
                'user_id' => (is_null($request->user())) ? null : $request->user()->id,
                'image_id' => $id,
                'body' => $input['body'],
            ]);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->respondWithItem($this->fractal, $disapproval, function ($item) {
            return [
                'id' => to_hashid($item->id),
                'body' => $item->body,
            ];
        });
    }

    public function showImageForLocation($id)
    {
        $location = Location::find($id);
        if (!$location) {
            return $this->errorNotFound('The building you are looking for is removed or unpublished :(.');
        }

        $image = $location->images->first();

        if($image !== null) {
            // $path = storage_path('app/images').DIRECTORY_SEPARATOR.$image->path . '?w=1200';
            $path = 'http://mapyour.city.dev/images/'.$image->path . '?w=1200';

            $path = asset('images/'.$image->path . '?w=1200');
            return $this->getAssetFromPath($path);
        }
    }
}
