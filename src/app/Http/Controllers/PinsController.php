<?php

namespace Myc\Http\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Locations\LocationTransformer;
use Myc\Domain\Organizations\OrganizationRepository;
use Myc\Domain\Pins\PinRepository;
use Myc\Domain\Profiles\ProfileRepository;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\BoundingBox;
use League\Fractal;

class PinsController extends ApiController
{
    /**
     * @var \Myc\Domain\Pins\PinRepository
     */
    private $pins;

    /**
     * @var Fractal\Manager
     */
    private $fractal;

    /**
     * @var Guard
     */
    private $auth;

    /**
     * @var \Myc\Domain\Locations\LocationRepository
     */
    private $locations;
    /**
     * @var FavoriteRepository
     */
    private $favorites;
    /**
     * @var OrganizationRepository
     */
    private $organizations;
    /**
     * @var ProfileRepository
     */
    private $profiles;

    public function __construct(
        PinRepository $pins,
        OrganizationRepository $organizations,
        ProfileRepository $profiles,
        Guard $auth,
        Fractal\Manager $fractal,
        FavoriteRepository $favorites
    ) {
        $this->pins = $pins;
        $this->auth = $auth;
        $this->fractal = $fractal;

        $this->locations = app()->make('Myc\Domain\Locations\ElasticSearchLocationRepository');

        $this->fractal->setSerializer(new Fractal\Serializer\DataArraySerializer());
        $this->favorites = $favorites;
        $this->organizations = $organizations;
        $this->profiles = $profiles;
    }

    /**
     * Query parameters.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        try {
            $boundingBox = BoundingBox::createFromQueryParam($request->input('bounding_box'));
            $precision = $boundingBox->getPrecision();
        } catch (\InvalidArgumentException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }

        $favoriteIds = $this->auth->check() ? $this->favorites->getLocationFavoriteIdsByUser($request->user()) : [];

        if ($request->has('tags')) {
            $tagIds = array_map('intval', explode(',', $request->input('tags')));
        }

        if ($request->has('q')) {
            $q = $request->get('q');
        }

        if ($request->has('organization_id')) {
            $organization = $this->organizations->getById(from_hashid($request->input('organization_id')));
        }

        if ($request->has('profile_fields')) {
            $locationIds = $this->profiles->getLocationsIdsMatchingProfileFieldEntries($request->input('profile_fields'));
        }

        $applyUserFilter = $request->has( 'filter_by_user' ) && in_array( $request->input('filter_by_user'), ['1', 'true'] );
        if ( $applyUserFilter && $this->auth->getUser() ) {
            $user = $this->auth->getUser();
        }

        $filterByFavorites = false;
        if ($request->has('favorited') && to_bool($request->get('favorited'))) {
            if (!$this->auth->check()) {
                // You need to be logged in to filter on your favorites
                return $this->errorUnauthorized();
            }
            if (!isset($favoriteIds)) {
                $favoriteIds = $this->favorites->getLocationFavoriteIdsByUser($request->user());
            }
            $filterByFavorites = true;
        }

        try {
            if ($precision <= 7) {
                $this->pins->setPrecision($precision)->filterByBoundingBox($boundingBox);

                if (isset($tagIds) && count($tagIds)) {
                    $this->pins->filterByTagIds($tagIds);
                }

                if (isset($q)) {
                    $this->pins->queryByString($q);
                }

                if ($filterByFavorites && isset($favoriteIds)) {
                    $this->pins->filterByFavoriteIds($favoriteIds);
                }

                if (isset($organization) && !is_null($organization)) {
                    $this->pins->filterByOrganization($organization);
                }

                if ($request->has('profile_fields') || isset($q)) {
                    $this->pins->includeBounds();
                }

                if (isset($locationIds) && !is_null($locationIds)) {
                    if (!count($locationIds)) {
                        $locationIds = [-1];
                    }
                    $this->pins->filterByIds($locationIds);
                }

                if ( isset( $user ) ) {
                    $this->pins->filterByUser( $user );
                }

                $pins = $this->pins->search()->getAll();

                $resource = new Fractal\Resource\Collection($pins, function ($pin) use ($favoriteIds) {
                    if ($pin['type'] === 'locations') {
                        $id = from_hashid($pin['id']);
                        if ($this->auth->check()) {
                            $pin['favorited'] = in_array($id, $favoriteIds);
                        }
                    }

                    return $pin;
                });
                $root = $this->fractal->createData($resource)->toArray();
                if ($this->pins->isBoundsIncluded()) {
                    $root['bounding_box'] = $this->pins->getBoundingBox()->toArray();
                }

                return $this->respondWithArray($root);

                // TODO cleanup
                /*return $this->respondWithCollection($this->fractal, $pins, function ($pin) use ($favoriteIds) {
                    if ($pin['type'] === 'locations') {
                        $id = from_hashid($pin['id']);
                        if ($this->auth->check()) {
                            $pin['favorited'] = in_array($id, $favoriteIds);
                        }
                    }
                    return $pin;
                });*/
            } else {
                // Precision 8 has a cell width of 38,2m. Don't aggregate but use the LocationRepo instead
                $this->locations->filterByBoundingBox($boundingBox);

                if (isset($tagIds) && count($tagIds)) {
                    $this->locations->filterByTagIds($tagIds);
                }

                if (isset($q)) {
                    $this->locations->queryByString($q);
                }

                if ($filterByFavorites && isset($favoriteIds)) {
                    $this->locations->filterByFavoriteIds($favoriteIds);
                }

                /*if ($request->has('profile_fields') || isset($q)) {
                    $this->locations->includeBounds();
                }*/

                if (isset($organization) && !is_null($organization)) {
                    $this->locations->filterByOrganization($organization);
                }

                if (isset($locationIds) && !is_null($locationIds)) {
                    if (!count($locationIds)) {
                        $locationIds = [-1];
                    }
                    $this->locations->filterByIds($locationIds);
                }
                
                if ( isset( $user ) ) {
                    $this->locations->filterByUser( $user );
                }

                $locations = $this->locations->getAllPaginated(1000);

                /*$resource = new Fractal\Resource\Collection($locations, new LocationTransformer($request->user(), $favoriteIds));
                $root = $this->fractal->createData($resource)->toArray();
                if ($this->locations->includeBounds()) {
                    $root['bounding_box'] = $this->locations->getBoundingBox()->toArray();
                }*/

                return $this->respondWithCollection($this->fractal, $locations, new LocationTransformer($request->user(), $favoriteIds));
            }
        } catch (\Exception $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }
    }
}
