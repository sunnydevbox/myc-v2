<?php

namespace Myc\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use League\Fractal;
use Myc\Domain\Messages\MessageRepository;
use Myc\Domain\Messages\MessageTransformer;

class MessagesController extends ApiController
{
    /**
     * @var Fractal\Manager
     */
    private $fractal;
    /**
     * @var \Myc\Domain\Messages\MessageRepository
     */
    private $messages;

    public function __construct(Fractal\Manager $fractal, MessageRepository $messages)
    {
        $this->fractal = $fractal;
        $this->messages = $messages;
    }

    public function index(Request $request)
    {
        $messages = $this->messages->getAllForUser($request->user());

        return $this->respondWithCollection($this->fractal, $messages, new MessageTransformer());
    }

    public function show(Request $request, $id)
    {
        try {
            $message = $this->messages->getById($id);
            if ($request->user()->id != $message->user_id) {
                return $this->errorForbidden('This is not your message');
            }
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return $this->respondWithItem($this->fractal, $message, new MessageTransformer());
    }

    public function updateReadOn(Request $request, $id)
    {
        try {
            $message = $this->messages->getById($id);
            if ($request->user()->id != $message->user_id) {
                return $this->errorForbidden('This is not your message');
            }
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }
        $message = $this->messages->updateReadOn($message);

        return $this->respondWithItem($this->fractal, $message, new MessageTransformer());
    }
}
