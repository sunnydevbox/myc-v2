<?php

namespace Myc\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Http\Controllers\ApiController;
use Myc\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Myc\Domain\Users\User;

class PasswordController extends ApiController
{
    use ResetsPasswords;

    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(
        Guard $auth,
        PasswordBroker $passwords,
        FlashNotifier $flash
    ) {
        $this->auth = $auth;
        $this->passwords = $passwords;
        $this->flash = $flash;
    }

    public function getEmail()
    {
        return view('auth.password');
    }

    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = $this->passwords->sendResetLink($request->only('email'), function ($m) {
            $m->subject('Reset your password');
        });

        if (PasswordBroker::RESET_LINK_SENT === $response) {
            if ($request->ajax() || $request->wantsJson()) {
                return $this->respondWithArray(['message' => trans($response)]);
            } else {
                return redirect()->back()->with('status', trans($response));
            }
        }

        if (PasswordBroker::INVALID_USER === $response) {
            if ($request->ajax() || $request->wantsJson()) {
                return $this->errorUnprocessableEntity(new MessageBag(['email' => [trans($response)]]));
            } else {
                return redirect()->back()->withErrors(['email' => trans($response)]);
            }
        }

        throw new \Exception('Unknown response from password broker');
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = $this->passwords->reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        $user = User::where('email', '=', $request->email)->first();

        if (PasswordBroker::PASSWORD_RESET === $response) {
            if ($request->ajax() || $request->wantsJson()) {
                return $this->respondWithArray(['message' => trans($response),]);
            } else {
                $this->flash->success(trans($response));
                if ($user->isAdmin()) {
                    return redirect()->route('auth.get_login');
                } else {
                    return redirect()->route('password.changed');
                }
            }
        }

        // Something failed
        if (PasswordBroker::INVALID_PASSWORD === $response) {
            $attribute = 'password';
        } else  if (PasswordBroker::INVALID_TOKEN === $response) {
            $attribute = 'token';
        } else {
            $attribute = 'email';
        }

        if ($request->ajax() || $request->wantsJson()) {
            return $this->errorUnprocessableEntity(new MessageBag([$attribute => [trans($response)]]));
        } else {
            return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors([$attribute => trans($response)]);
        }
    }

    public function getReset(Request $request)
    {
        if (!$request->has('token')) {
            throw new \InvalidArgumentException('No reset token set');
        }

        return view('auth.reset', [
            'token' => $request->input('token'),
            'email' => $request->input('email')
        ]);
    }

    public function passwordChanged()
    {
        return view('auth.password_changed');
    }
}
