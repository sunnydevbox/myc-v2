<?php

namespace Myc\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\Factory;
use League\Fractal\Manager;
use LucaDegasperi\OAuth2Server\Authorizer;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Auth\FacebookClient;
use Myc\Domain\Images\ImageService;
use Myc\Domain\Images\ImageTransformer;
use Myc\Domain\Users\User;
use Myc\Domain\Users\UserProfileTransformer;
use Myc\Domain\Users\UserRepository;
use Myc\Domain\Users\UserService;
use Myc\Exceptions\InvalidExchangeTokenException;
use Myc\Exceptions\ValidationException;
use Myc\Http\Controllers\ApiController;
use Illuminate\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthController extends ApiController
{
    // Strip this out
    use AuthenticatesAndRegistersUsers;

    private $redirectTo = '/admin';

    /**
     * @var Factory
     */
    private $validator;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var Authorizer
     */
    private $oauth;
    /**
     * @var Manager
     */
    private $fractal;
    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var FlashNotifier
     */
    private $flash;
    /**
     * @var UserRepository
     */
    private $users;

    public function __construct(
        Manager $fractal,
        Factory $validator,
        FacebookClient $facebookClient,
        UserRepository $users,
        UserService $userService,
        ImageService $imageService,
        Authorizer $oauth,
        Guard $auth,
        Registrar $registrar,
        FlashNotifier $flash
    ) {
        $this->fractal = $fractal;
        $this->validator = $validator;
        $this->facebook = $facebookClient;
        $this->userService = $userService;
        $this->imageService = $imageService;
        $this->oauth = $oauth;
        $this->auth = $auth;
        $this->auth = $auth;
        $this->registrar = $registrar;
        $this->flash = $flash;
        $this->users = $users;
    }

    public function getUser(Request $request)
    {
        $this->fractal->parseIncludes(['organizations', 'organizations.profile']);

        return $this->respondWithItem($this->fractal, $request->user(), new UserProfileTransformer());
    }

    public function createUser(Request $request)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $user = $this->userService->createUser($request->json('data'));
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        }

        $this->fractal->parseIncludes(['organizations', 'organizations.profile']);

        return $this->setStatusCode(201)->respondWithItem($this->fractal, $user, new UserProfileTransformer());
    }

    public function updateUser(Request $request)
    {
        if (!is_array($request->json('data'))) {
            return $this->errorInvalidArguments();
        }

        try {
            $user = $this->userService->updateUser($request->user(), $request->json('data'));
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        } catch (AccessDeniedHttpException $e) {
            return $this->errorForbidden($e->getMessage());
        }

        $this->fractal->parseIncludes(['organizations', 'organizations.profile']);

        return $this->respondWithItem($this->fractal, $user, new UserProfileTransformer());
    }

    public function updateUserImage(Request $request)
    {
        if (!$request->hasFile('image')) {
            return $this->errorInvalidArguments('No file provided');
        }

        $file = $request->file('image');

        try {
            $image = $this->imageService->updateImageForUser($file, $request->user());
        } catch (ValidationException $e) {
            return $this->errorUnprocessableEntity($e->getErrors());
        }

        return $this->setStatusCode(201)->respondWithArray([
                'data' => ['url' => route('images.get_asset', ['path' => $image->image])],
            ]);

        // >respondWithItem($this->fractal, $image, new ImageTransformer());
    }

    public function getAccessToken()
    {
        $token = $this->oauth->issueAccessToken();

        return $this->setStatusCode(201)->respondWithArray(['data' => $token]);
    }

    public function getFacebookAccessToken(Request $request)
    {
        $validation = $this->validator->make($request->only([
            'client_id',
            'client_secret',
            'grant_type',
            'fb_exchange_token',
        ]), [
            'client_id' => 'required',
            'client_secret' => 'required',
            'grant_type' => 'required|in:facebook',
            'fb_exchange_token' => 'required',
        ]);

        if ($validation->fails()) {
            return $this->errorUnprocessableEntity($validation->errors());
        }

        try {
            // Exchange the short-lived token for a long-lived token
            $accessToken = $this->facebook->getLongLivedToken($request->input('fb_exchange_token'));
            $this->facebook->setToken($accessToken);

            // Get the FB account details
            $facebookUser = $this->facebook->getUserDetails();

            // TODO, figure out the policy for merging accounts

            // Check if user already exists, if not, create an account with FB details
            $user = $this->users->getByEmail($facebookUser->email);
            if (!$user) {
                $user = $this->userService->createFromFacebook($facebookUser->getArrayCopy());
            } else {
                $user = $this->userService->mergeWithFacebookProfile($user, $facebookUser->getArrayCopy());
            }
            // When do we need to update data? Look at timestamp? Always?
        } catch (InvalidExchangeTokenException $e) {
            return $this->errorInvalidArguments($e->getMessage());
        }

        // We already know the access token is valid, if the exchange token was not valid
        // an error was already thrown. FacebookGrant only checks email and provides the
        // access token.
        $issuer = $this->oauth->getIssuer();
        $facebookGrant = $issuer->getGrantType($request->input('grant_type'));
        $facebookGrant->setParams([
            'client_id' => $request->input('client_id'),
            'client_secret' => $request->input('client_secret'),
            'email' => $user->email,
        ]);

        $token = $facebookGrant->completeFlow();

        return $this->setStatusCode(201)->respondWithArray(['data' => $token]);
    }

    public function getConfirmEmail(Request $request)
    {
        if (!$request->has('confirmation_code')) {
            throw new \InvalidArgumentException('No confirmation code set');
        }

        return view('auth.confirm')->with([
            'confirmationCode' => $request->get('confirmation_code'),
            'email' => $request->get('email'),
        ]);
    }

    public function postConfirmEmail(Request $request)
    {
        $validation = $this->validator->make($request->only([
            'confirmation_code',
            'email',
        ]), [
            'confirmation_code' => 'required',
            'email' => 'required',
        ]);

        if ($validation->fails()) {
            return $this->errorUnprocessableEntity($validation->errors());
        }

        $jsonResponse = $request->ajax() || $request->wantsJson();

        try {
            $user = User::where('email', '=', $request->input('email'))->first();

            if (!$user) {
                throw new \InvalidArgumentException('No account associated with this email found');
            }

            if ($user->isConfirmed()) {
                throw new \InvalidArgumentException('This account is already confirmed');
            }

            if ($user->confirmation_code != $request->input('confirmation_code')) {
                throw new \InvalidArgumentException('Invalid confirmation code');
            }

            $user->confirmed = true;
            $user->confirmation_code = null;
            $user->save();
        } catch (\InvalidArgumentException $e) {
            if ($jsonResponse) {
                return $this->errorInvalidArguments($e->getMessage());
            } else {
                return redirect()->back()->withErrors(['confirmation_code' => $e->getMessage()]);
            }
        }

        if ($jsonResponse) {
            return $this->setStatusCode(200)->respondWithArray([
                'message' => 'Account confirmed successfully',
            ]);
        }

        return view('auth.confirmed');
    }

    public function getRegister(Request $request)
    {
        return view('auth.register', [
            'email' => $request->has('email') ? $request->input('email') : '',
            'token' => $request->has('token') ? $request->input('token') : '',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        try {
            $user = $this->userService->createUser($request->all());
            $user->confirmed = true;
            $user->confirmation_code = null;
            $user->save();
        } catch (ValidationException $e) {
            return redirect()->route('auth.get_register')->withInput()->with(['errors' => $e->getErrors()]);
        }

        $this->auth->login($user);

        return redirect()->route('auth.get_registered');
    }

    public function getRegistered(Request $request)
    {
        return view('auth.registered', ['user' => $request->user()]);
    }

    public function getLogout()
    {
        $this->auth->logout();

        return redirect()->route('auth.get_login');
    }
}
