<?php

get('/', function () {
    return redirect()->to('admin');
});

/*get('/upload_test/{id}', function ($id) {
   return view('upload_test', ['locationId' => $id]);
});*/

get('docs', [
    'middleware' => ['auth', 'is_admin'],
    'as' => 'docs',
    'uses' => 'HomeController@getDocs',
]);

get('mobile-warning', function () {
    return View::make('mobile_warning');
});

// Get details of authenticated user
get('auth/user', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'auth.get_user',
    'uses' => 'Auth\AuthController@getUser',
]);

// Create User
post('users', [
    'as' => 'auth.create_user',
    'uses' => 'Auth\AuthController@createUser',
]);

// Update User settings/profile
put('auth/user', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'auth.update_user',
    'uses' => 'Auth\AuthController@updateUser',
]);

post('auth/user/image', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'auth.update_user_image',
    'uses' => 'Auth\AuthController@updateUserImage',
]);

get('auth/passwords/email', [
    'as' => 'password.get_email',
    'uses' => 'Auth\PasswordController@getEmail',
]);

post('auth/passwords/email', [
    'as' => 'password.post_email',
    'uses' => 'Auth\PasswordController@postEmail',
]);

get('auth/passwords/reset', [
    'as' => 'password.get_reset',
    'uses' => 'Auth\PasswordController@getReset',
]);

get('auth/passwords/changed', [
    'as' => 'password.changed',
    'uses' => 'Auth\PasswordController@passwordChanged',
]);

post('auth/passwords/reset', [
    'as' => 'password.post_reset',
    'uses' => 'Auth\PasswordController@postReset',
]);

get('auth/confirm', [
    'as' => 'auth.confirm',
    'uses' => 'Auth\AuthController@getConfirmEmail',
]);

post('auth/confirm', [
    'as' => 'auth.confirm',
    'uses' => 'Auth\AuthController@postConfirmEmail',
]);

// Create Access Token
post('oauth/access_token', [
    'as' => 'attempt_oauth.access_token',
    'uses' => 'Auth\AuthController@getAccessToken',
]);

// Create Access Token for Facebook Login
// Account will be created/merged with existing account
post('oauth/facebook', [
    'as' => 'auth.facebook_user',
    'uses' => 'Auth\AuthController@getFacebookAccessToken',
]);

// Web login
get('auth/login', [
    'as' => 'auth.get_login',
    'uses' => 'Auth\AuthController@getLogin',
]);

post('auth/login', [
    'as' => 'auth.post_login',
    'uses' => 'Auth\AuthController@postLogin',
]);

get('auth/register', [
    'as' => 'auth.get_register',
    'uses' => 'Auth\AuthController@getRegister',
]);

post('auth/register', [
    'as' => 'auth.post_register',
    'uses' => 'Auth\AuthController@postRegister',
]);

$router->group(['middleware' => 'auth'], function($router)
{
    get('auth/registered', [
        'as' => 'auth.get_registered',
        'uses' => 'Auth\AuthController@getRegistered',
    ]);

    get('auth/logout', [
        'as' => 'auth.logout',
        'uses' => 'Auth\AuthController@getLogout',
    ]);
});

// The pins controller is the entry point of the API.
// Used to display all points of interest on a map.
// Precision of grid is determined based on zoom level.
get('pins', [
    'middleware' => ['attempt_oauth'],
    'as' => 'pins.index',
    'uses' => 'PinsController@index',
]);

// List Locations
get('locations', [
    'middleware' => ['attempt_oauth'],
    'as' => 'locations.index',
    'uses' => 'LocationsController@index',
]);

// Show Location
get('locations/{hashid}', [
    'middleware' => ['attempt_oauth'],
    'as' => 'locations.show',
    'uses' => 'LocationsController@show',
]);

// Show HTML version of location
get('web/locations/{hashid}', [
    'as' => 'locations.show_public_profile',
    'uses' => 'LocationsController@showHtml',
]);

get('organizations/{hashid}', [
    'middleware' => ['attempt_oauth'],
    'as' => 'organizations.show',
    'uses' => 'OrganizationsController@show',
]);

post( 'organizations/{hashid}/follow', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'organizations.follow',
    'uses' => 'OrganizationsController@follow',
]);

delete( 'organizations/{hashid}/unfollow', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'organizations.unfollow',
    'uses' => 'OrganizationsController@unfollow',
]);

// Show HTML version of Organization ("iFrame")
get('web/organizations/{hashid}', [
    'as' => 'organizations.show_public_profile',
    'uses' => 'OrganizationsController@showHtml',
]);

get('web/all', [
    'as' => 'organizations.show_all',
    'uses' => 'LocationsController@showAll',
]);

// Create Location
post('locations', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'locations.create',
    'uses' => 'LocationsController@create',
]);

// Update Location
put('locations/{hashid}', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'locations.update',
    'uses' => 'LocationsController@update',
]);

// Delete Location
delete('locations/{hashid}', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'locations.delete',
    'uses' => 'LocationsController@destroy',
]);

// Favorite Location
post('locations/{hashid}/favorite', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'locations.create_favorite',
    'uses' => 'LocationsController@createFavorite',
]);

// Unfavorite Location
delete('locations/{hashid}/favorite', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'locations.delete_favorite',
    'uses' => 'LocationsController@deleteFavorite',
]);

// List Location Images
get('locations/{hashid}/images', [
    'as' => 'locations.get_images',
    'uses' => 'LocationsController@getImages',
]);

// Show Image
get('locations/{locationId}/images/{imageId}', [
    'as' => 'images.show',
    'uses' => 'ImagesController@show',
]);

// Create Location Image
post('locations/{hashid}/images', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'images.create',
    'uses' => 'ImagesController@create',
]);

delete('images/{hashid}', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'images.delete',
    'uses' => 'ImagesController@destroy',
]);

// Serve Image contents
// Differs from GET image in that this only returns the file contents not the metadata etc.
get('images/{path}', [
    'as' => 'images.get_asset',
    'uses' => 'ImagesController@getAsset',
])->where('path', '.+');

post('images/{hashid}/disapprovals', [
    'middleware' => ['attempt_oauth'],
    'as' => 'images.create_disapproval',
    'uses' => 'ImagesController@createDisapproval',
]);

// Set Image sort order
put('locations/{hashid}/images/sort_order', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'images.put_sort_order',
    'uses' => 'ImagesController@putSortOrder',
]);

// Create Comment on Location
post('locations/{hashid}/comments', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'comments.create',
    'uses' => 'CommentsController@createOnLocation',
]);

// Create Comment on Comment
post('comments/{hashid}/comments', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'comments.create',
    'uses' => 'CommentsController@createOnComment',
]);

// Update Comment
put('comments/{hashid}', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'comments.update',
    'uses' => 'CommentsController@update',
]);

post('comments/{hashid}/disapprovals', [
    'middleware' => ['attempt_oauth'],
    'as' => 'comments.create_disapproval',
    'uses' => 'CommentsController@createDisapproval',
]);

// Delete Comment
delete('comments/{hashid}', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'comments.delete',
    'uses' => 'CommentsController@destroy',
]);

// Favorite Comment
post('comments/{hashid}/favorite', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'comments.create_favorite',
    'uses' => 'CommentsController@createFavorite',
]);

// Unfavorite Comment
delete('comments/{hashid}/favorite', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'comments.delete_favorite',
    'uses' => 'CommentsController@deleteFavorite',
]);

// List Location Organizations
get('locations/{hashid}/organizations', [
    'as' => 'locations.get_organizations',
    'uses' => 'LocationsController@getOrganizations',
]);

// Custom profiles
get('locations/{locationHashid}/organizations/{organizationHashid}/profile', [
    'as' => 'profiles.show',
    'uses' => 'ProfilesController@show',
]);

put('locations/{locationHashid}/organizations/{organizationHashid}/profile', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'profiles.update',
    'uses' => 'ProfilesController@update',
]);

get('messages', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'messages.index',
    'uses' => 'MessagesController@index',
]);

get('messages/{hashid}', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'messages.show',
    'uses' => 'MessagesController@show',
]);

put('messages/{hashid}/read', [
    'middleware' => ['attempt_oauth', 'auth'],
    'as' => 'messages.update_read_on',
    'uses' => 'MessagesController@updateReadOn',
]);

get('search', [
    'middleware' => ['attempt_oauth'],
    'as' => 'search',
    'uses' => 'SearchController@search',
]);

// List Categories
get('categories', [
    'as' => 'categories.index',
    'uses' => 'CategoriesController@index',
]);

// Geocode utility
get('geocode', [
    'as' => 'geocode',
    'uses' => 'GeocodeController@geocode',
]);

// Image rewriter
get('i/{hashid}.jpg', [
    'as' => 'short_image',
    'uses' => 'ImagesController@showImageForLocation',
]);

// Url redirecter
$router->get('/redirect', [
    'as' => 'redirect.url',
    function(\Illuminate\Http\Request $request) {
        $scheme = config('app.scheme');

        switch($request->get('action')) {
            case 'confirm_account':
                $payload = 'auth/confirm?confirmation_code='.$request->get('confirmation_code').'&email='.$request->get('email');
                $app_redirect_ios = $scheme . '://' . $payload;
                $app_redirect_android = 'intent://' . $payload . '#Intent;package=' . config('app.android_package_name') . ';scheme=' . $scheme . ';launchFlags=268435456;end;';

                $redirect = route('auth.confirm', ['confirmation_code' => $request->get('confirmation_code'), 'email' => $request->get('email')]);
                return View::make('redirect', ['redirect' => $redirect, 'app_redirect_ios' => $app_redirect_ios, 'app_redirect_android' => $app_redirect_android]);
                break;
            case 'password_reset':
                $payload = 'auth/passwords/reset?token='.$request->get('token').'&email='.$request->get('email');
                $app_redirect_ios = $scheme . '://' . $payload;
                $app_redirect_android = 'intent://' . $payload . '#Intent;package=' . config('app.android_package_name') . ';scheme=' . $scheme . ';launchFlags=268435456;end;';
                $redirect = route('password.get_reset', ['token' => $request->get('token'), 'email' => $request->get('email')]);
                return View::make('redirect', ['redirect' => $redirect, 'app_redirect_ios' => $app_redirect_ios, 'app_redirect_android' => $app_redirect_android]);
                break;
        }
    }
]);