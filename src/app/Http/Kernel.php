<?php

namespace Myc\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'Myc\Http\Middleware\LogExecutionTime',
        'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
        'Illuminate\Cookie\Middleware\EncryptCookies',
        'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
        'Illuminate\Session\Middleware\StartSession',
        'Myc\Http\Middleware\ShareErrorsFromSession',
        //'Illuminate\View\Middleware\ShareErrorsFromSession',
        'LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware',
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'session' => 'Illuminate\Session\Middleware\StartSession',
        'auth' => 'Myc\Http\Middleware\Authenticate',
        'guest' => 'Myc\Http\Middleware\RedirectIfAuthenticated',
        'csrf' => 'Myc\Http\Middleware\VerifyCsrfToken',
        'attempt_oauth' => 'Myc\Http\Middleware\AttemptAuthenticationWithOAuth',
        // RBAC middleware
        'is_admin' => 'Myc\Http\Middleware\IsAdmin',
        'is_admin_or_owner' => 'Myc\Http\Middleware\IsAdminOrOwner',
    ];
}
