<?php

if (!function_exists('write_log')) {
    function write_log($message, $level = 'debug')
    {
        if (('debug' === $level) && !config('app.debug', false)) {
            return;
        }
        $logger = app()->make('Illuminate\Contracts\Logging\Log');
        $logger->log($level, $message);
    }
}

if (!function_exists('to_hashid')) {
    function to_hashid($id)
    {
        $hashids = app()->make('Hashids\Hashids');

        return $hashids->encode($id);
    }
}

if (!function_exists('from_hashid')) {
    function from_hashid($hashid)
    {
        $hashids = app()->make('Hashids\Hashids');
        $id = $hashids->decode($hashid);
        if (is_array($id)) {
            if (!isset($id[0])) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
            }
            $id = $id[0];
        }
        if (empty($id)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        return $id;
    }
}

if (!function_exists('build_array')) {
    function build_array($parts)
    {
        return array_filter($parts, function ($part) {
            return !empty($part);
        });
    }
}

if (!function_exists('to_bool')) {
    function to_bool($value)
    {
        if ('true' === $value || 'yes' === $value || 'on' === $value || intval($value)) {
            return true;
        }

        return false;
    }
}

if (!function_exists('image_asset')) {
    function image_asset($path)
    {
        if (is_null($path)) {
            return '';
        }

        return route('images.get_asset', ['path' => $path]);
    }
}

if (!function_exists('pagination')) {
    function pagination($resource, $appends = null)
    {
        $appends = is_null($appends) ? \Request::except(['page']) : $appends;

        return with(new \Myc\Admin\Pagination\PaginationPresenter($resource->appends($appends)))->render();
    }
}

if (!function_exists('links_to_html_tags')) {
    // https://gist.github.com/AlexPashley/5861213
    function links_to_html_tags($text)
    {
        $text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
        $ret = ' '.$text;
        // Replace Links with http://
        $ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
        // Replace Links without http://
        $ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
        // Replace Email Addresses
        $ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
        $ret = substr($ret, 1);

        return $ret;
    }
}

if (!function_exists('null_if_empty')) {
    function null_if_empty($var)
    {
        return empty($var) ? null : $var;
    }
}

if (!function_exists('array_strip_empty')) {
    function array_strip_empty(array $array)
    {
        return array_filter($array, function($item) {
            $item = trim($item); // Make sure items contain no empty spaces
            return (!empty($item) && ($item !== 0));
        });
    }
}
