<?php

namespace Myc\Providers;

use GuzzleHttp;
use Hashids\Hashids;
use Illuminate\Support\ServiceProvider;
use League\Glide;
use Myc\Services\Notifier;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     */
    public function register()
    {
        $this->app->bind(
            'Illuminate\Contracts\Pagination\Paginator',
            'Myc\Admin\Pagination\Paginator'
        );

        $this->app->bind(
            'Myc\Domain\Organizations\OrganizationRepository',
            'Myc\Domain\Organizations\EloquentOrganizationRepository'
        );

        $this->app->bind(
            'Illuminate\Contracts\Auth\Registrar',
            'Myc\Services\Registrar'
        );

        $this->app->when('Myc\Http\Controllers\LocationsController')
                  ->needs('Myc\Domain\Locations\LocationRepository')
                  ->give('Myc\Domain\Locations\ElasticSearchLocationRepository');

        $this->app->when('Myc\Admin\Http\Controllers\LocationsController')
                  ->needs('Myc\Domain\Locations\LocationRepository')
                  ->give('Myc\Domain\Locations\EloquentLocationRepository');

        $this->app->when('Myc\Admin\Http\Controllers\OrganizationsController')
                  ->needs('Myc\Domain\Locations\LocationRepository')
                  ->give('Myc\Domain\Locations\EloquentLocationRepository');

        $this->app->bind(
            'Myc\Domain\Categories\TagRepository',
            'Myc\Domain\Categories\EloquentTagRepository'
        );

        $this->app->bind(
            'Myc\Domain\Disapprovals\DisapprovalRepository',
            'Myc\Domain\Disapprovals\EloquentDisapprovalRepository'
        );

        $this->app->bind(
            'Myc\Domain\Images\ImageRepository',
            'Myc\Domain\Images\EloquentImageRepository'
        );

        $this->app->singleton('League\Glide\Server', function ($app) {
            $filesystem = $app->make('Illuminate\Contracts\Filesystem\Filesystem');

            return Glide\ServerFactory::create([
                'source' => $filesystem->getDriver(),
                'cache' => $filesystem->getDriver(),
                'source_path_prefix' => 'images',
                'cache_path_prefix' => 'images/.cache',
                'base_url' => 'images',
            ]);
        });

        $this->app->bind('Hashids\Hashids', function ($app) {
            return new Hashids(
                $app['config']->get('app.key'),
                8,
                'abcdefghijklmnopqrstuvwxyz1234567890'
            );
        });

        $this->app->bind('Myc\Auth\FacebookClient', function ($app) {
            $clientId = $app['config']->get('services.facebook.client_id');
            $clientSecret = $app['config']->get('services.facebook.client_secret');

            $provider = new \League\OAuth2\Client\Provider\Facebook([
                'clientId' => $clientId,
                'clietnSecret' => $clientSecret,
                'redirectUri' => null,
                'scopes' => [],
            ]);

            return new \Myc\Auth\FacebookClient(
                new GuzzleHttp\Client(),
                $provider,
                $clientId,
                $clientSecret
            );

        });

        $this->app->bind('Myc\Services\Notifier', function ($app) {
            return new Notifier(
                config('services.acs.username'),
                config('services.acs.password'),
                config('services.acs.api_key'),
                new GuzzleHttp\Client([
                    'base_url' => 'https://api.cloud.appcelerator.com/v1/',
                    'defaults' => [
                        'cookies' => true,
                        'timeout' => 60,
                        'allow_redirects' => true,
                        'query' => ['key' => config('services.acs.api_key')],
                    ],
                ])
            );
        });
    }
}
