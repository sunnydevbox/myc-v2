<?php

namespace Myc\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Myc\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param \Illuminate\Routing\Router $router
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $router->bind('hashid', function ($value) {
            $id = from_hashid($value);
            if (empty($id)) {
                throw new NotFoundHttpException();
            }

            return $id;
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });

        $router->group([
            'namespace' => 'Myc\Admin\Http\Controllers',
            'middleware' => [
                //'session',
                'auth',
                'is_admin_or_owner'
            ],
            'prefix' => 'admin',
        ], function ($router) {
            require app_path('Admin/Http/routes.php');
        });
    }
}
