<?php

namespace Myc\Providers;

use Illuminate\Support\ServiceProvider;

class GeocoderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind('Geocoder\Geocoder', function ($app) {
            $locale = null;
            $region = null;
            $useSsl = false;
            $apiKey = null;

            $bingApiKey = 'AoVYki2hovvx957gKUZAlVZztvBiiUqBbWl7TanTZ30qZ6LWJyMMGVo4RFRyNjaw';

            $geocoder = new \Geocoder\ProviderAggregator();
            $adapter = new \Ivory\HttpAdapter\CurlHttpAdapter();

            $chain = new \Geocoder\Provider\Chain([
                new \Geocoder\Provider\GoogleMaps($adapter, $locale, $region, $useSsl, $apiKey),
                new \Geocoder\Provider\OpenStreetMap($adapter, $locale),
                //new \Geocoder\Provider\ArcGISOnline($adapter, $locale, $useSsl),
                new \Geocoder\Provider\BingMaps($adapter, $bingApiKey, $locale),
            ]);

            $geocoder->registerProvider($chain);

            return $geocoder;
        });
    }
}
