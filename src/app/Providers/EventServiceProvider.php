<?php

namespace Myc\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Myc\Events\UserWasRegistered' => [
            'Myc\Handlers\Events\SendAccountConfirmationRequest',
        ],
        'Myc\Events\LocationWasFavorited' => [
            'Myc\Handlers\Events\PushLocationFavoritedNotification',
        ],
        'Myc\Events\CommentOnLocationWasCreated' => [
            'Myc\Handlers\Events\PushCommentOnLocationNotification',
        ],
        'Myc\Events\CommentOnCommentWasCreated' => [
            'Myc\Handlers\Events\PushCommentOnCommentNotification',
        ],
        'Myc\Events\CommentWasFavorited' => [
            'Myc\Handlers\Events\PushCommentFavoritedNotification',
        ],
        'Myc\Events\UserEnrolledToOrganization' => [
            'Myc\Handlers\Events\MakeUserFollowsOrganization',
        ],
        'Myc\Events\LocationWasMappedToAnOrganization' => [
            'Myc\Handlers\Events\PushLocationMappedNotification',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
