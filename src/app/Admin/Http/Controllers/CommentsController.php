<?php

namespace Myc\Admin\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Admin\Forms\FormValidationException;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Domain\Comments\CommentRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentsController extends Controller
{
    /**
     * @var CommentRepository
     */
    private $comments;

    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(
        CommentRepository $comments,
        FlashNotifier $flash
    ) {
        $this->comments = $comments;
        $this->flash = $flash;
    }

    public function index(Request $request)
    {
        $comments = $this->comments
            ->orderBy($request->input('order_by'), $request->input('order'))
            ->filterByString($request->input('q'))
            ->getAllPaginated(10);

        return view('admin.comments.index', compact('comments'));
    }

    public function destroy($id)
    {
        try {
            $this->comments->deleteById($id);
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException();
        }

        $this->flash->success('Comment deleted');

        return redirect()->back();
        // return redirect()->route('admin.comments.index');
    }

    public function edit($id)
    {
        $comment = $this->comments->getById($id);

        return view('admin.comments.edit', compact('comment'));
    }

    public function update($id, Request $request)
    {
        try {
            $this->tagValidator->validate($request->input());
            $tag = $this->tags->getById($id);

            $tag->fill($request->input());
            $tag->save();
        } catch (FormValidationException $e) {
            return redirect()->route('admin.tags.edit', ['id' => $id])
                ->withInput()
                ->with('errors', $e->getErrors());
        }

        $this->flash->success('Tag saved');

        return redirect()->route('admin.tags.index');
    }
}
