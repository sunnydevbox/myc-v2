<?php 
namespace Myc\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Myc\Domain\Locations\Location;
use DB;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExportController extends Controller {

    public function index()
    {
        return view('admin.export.index');
    }

    public function getFile(Request $request)
    {
        config(['database.fetch' => \PDO::FETCH_ASSOC]);
        if ('locations' === $request->input('type')) {
            $filename = 'locations';
            $headers = [
                [
                    'user_id',
                    'user_signed_up_at',
                    'user_first_name',
                    'user_last_name_prefix',
                    'user_last_name',
                    'user_email',
                    'location_id',
                    'location_created_at',
                    'location_name',
                    'location_street_name',
                    'location_street_number',
                    'location_postal_code',
                    'location_locality'
                ]
            ];
            $results = DB::table('locations')
                ->join('users', 'locations.user_id', '=', 'users.id')
                ->select([
                    'users.id AS user_id',
                    'users.created_at AS user_signed_up_at',
                    'users.first_name AS user_first_name',
                    'users.last_name_prefix AS last_name_prefix',
                    'users.last_name AS last_name',
                    'users.email AS email',
                    'locations.id AS location_id',
                    'locations.created_at AS location_created_at',
                    'locations.name AS location_name',
                    'locations.street_name AS location_street_name',
                    'locations.street_number AS location_street_number',
                    'locations.postal_code AS location_postal_code',
                    'locations.locality AS location_locality'
                ])
                ->get();
        } else if ('favorites' === $request->input('type')) {
            $filename = 'favorites';
            $headers = [
                [
                    'location_id',
                    'location_created_at',
                    'location_name',
                    'location_street_name',
                    'location_street_number',
                    'location_postal_code',
                    'location_locality',
                    'favorited_at',
                    'user_id',
                    'user_first_name',
                    'user_last_name_prefix',
                    'user_last_name',
                    'user_email'
                ]
            ];
            $results = DB::table('favorites')
                ->join('users', 'favorites.user_id', '=', 'users.id')
                ->join('locations', 'favorites.location_id', '=', 'locations.id')
                ->select([
                    'locations.id AS location_id',
                    'locations.created_at AS location_created_at',
                    'locations.name AS location_name',
                    'locations.street_name AS location_street_name',
                    'locations.street_number AS location_street_number',
                    'locations.postal_code AS location_postal_code',
                    'locations.locality AS location_locality',
                    'favorites.created_at AS favorited_at',
                    'users.id AS user_id',
                    'users.first_name AS user_first_name',
                    'users.last_name_prefix AS last_name_prefix',
                    'users.last_name AS last_name',
                    'users.email AS email'
                ])
                ->get();
        } else if ('users' === $request->input('type')) {
            $filename = 'users';
            $headers = [
                [
                    'user_id',
                    'user_signed_up_at',
                    'user_first_name',
                    'user_last_name_prefix',
                    'user_last_name',
                    'user_email'
                ]
            ];
            $results = DB::table('users')
                ->select([
                    'users.id AS user_id',
                    'users.created_at AS user_signed_up_at',
                    'users.first_name AS user_first_name',
                    'users.last_name_prefix AS last_name_prefix',
                    'users.last_name AS last_name',
                    'users.email AS email',
                ])
                ->get();
        } else {
            throw new \InvalidArgumentException(sprintf('Type must be locations|favorites|users'));
        }

        // Format the rows
        $rows = array_merge($headers, array_map(function($item) {
            return array_values((array) $item);
        }, $results));

        // Created streamed response;

        $date = (new \DateTime())->format('Y-m-d_H-i-s');

        $response = new StreamedResponse(function() use ($rows) {
            $handle = fopen('php://output', 'w');
            foreach ($rows as $row) {
                fputcsv($handle, $row);
            }
            fclose($handle);
        }, 200, ['Content-Type' => 'text/csv', 'Content-Disposition' => 'attachment; filename="'.$filename.'_'.$date.'.csv"']);

        return $response;
    }

}