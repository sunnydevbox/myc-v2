<?php

namespace Myc\Admin\Http\Controllers;

use Event;
use Illuminate\Contracts\Mail\Mailer;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Domain\Images\ImageService;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Organizations\OrganizationRepository;
use Myc\Domain\Users\Invite;
use Myc\Domain\Users\UserRepository;
use Myc\Domain\Users\UserService;
use Illuminate\Http\Request;
use Myc\Events\UserEnrolledToOrganization;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Myc\Exceptions\ValidationException;

class UsersController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var FlashNotifier
     */
    private $flash;

    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var OrganizationRepository
     */
    private $organizations;

    /**
     * @param UserRepository         $users
     * @param UserService            $userService
     * @param OrganizationRepository $organizations
     * @param ImageService           $imageService
     * @param FlashNotifier          $flash
     * @param Mailer                 $mailer
     */
    public function __construct(
        UserRepository $users,
        UserService $userService,
        OrganizationRepository $organizations,
        ImageService $imageService,
        FlashNotifier $flash,
        Mailer $mailer
    ) {
        $this->users = $users;
        $this->userService = $userService;
        $this->imageService = $imageService;
        $this->flash = $flash;
        $this->mailer = $mailer;
        $this->organizations = $organizations;
    }

    public function index(Request $request)
    {

        if($request->has('trash')) {
            $this->users->displayTrashed(true);
        }

        $users = $this->users
            ->orderBy($request->input('order_by'), $request->input('order'))
            ->filterByString($request->input('q'))
            ->getAllPaginated(10);

        return view('admin.users.index', ['users' => $users]);
    }

    public function edit($id)
    {
        $user = $this->users->getById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }

        return view('admin.users.edit', ['user' => $user]);
    }

    public function update($id, Request $request)
    {
        $user = $this->users->getById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }

        $input = $request->only('role_id');
        $user->fill($input);
        $user->save();

        $this->flash->success('User updated');

        return view('admin.users.edit', ['user' => $user]);
    }

    public function profile(Request $request)
    {
        return view('admin.users.profile', ['user' => $request->user()]);
    }

    public function updateProfileImage(Request $request)
    {
        $file = $request->file('image');
        if ($file) {
            try {
                $this->imageService->updateImageForUser($file, \Auth::user());
            } catch (ValidationException $e) {
                throw $e;
            } catch (\InvalidArgumentException $e) {
                // redirect back
                throw $e;
            }
        }

        $this->flash->success('Profile image added');

        return redirect()->route('admin.users.profile');
    }

    public function updateProfile(Request $request)
    {
        try {
            $input = $request->input();
            unset($input['_method'], $input['_token']);

            if (empty($input['password'])) {
                unset($input['password'], $input['password_confirmation']);
            }
            $this->userService->updateUser(\Auth::user(), $input);

            $this->updateProfileImage($request);
        } catch (ValidationException $e) {
            $this->flash->error('Could not update profile', $e->getErrors());

            return redirect()->route('admin.users.profile')->withInput()->with('errors', $e->getErrors());
        } catch (AccessDeniedHttpException $e) {
            return redirect()->route('admin.users.profile')->withInput()->with('errors', $e->getErrors());
        }

        $this->flash->success('Profile updated');

        return redirect()->route('admin.users.profile');
    }

    public function getInvite(Request $request)
    {
        return view('admin.users.invite', [
            'organizations' => Organization::all(),
            'roles' => ['owner', 'member'],
            'invites' => Invite::orderBy('created_at', 'DESC')->paginate(10),
        ]);
    }

    public function postInvite(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'organization_id' => 'required|exists:organizations,id',
            'role' => 'required|in:member,owner',
        ];

        $validation = \Validator::make($request->only(['email', 'organization_id', 'role']), $rules);
        if ($validation->fails()) {
            if ($request->input('redirect_to') == 'organization_profile') {
                $redirect = redirect()->route('admin.organizations.get_invite', ['id' => to_hashid($request->input('organization_id'))]);
            } else {
                $redirect = redirect()->route('admin.users.get_invite');
            }
            return $redirect->withInput()->with('errors', $validation->errors());
        }

        $email = trim($request->input('email'));
        $organization = $this->organizations->getById($request->input('organization_id'));

        if (!($request->user()->isAdmin() || $request->user()->isOwner($organization))) {
            throw new AccessDeniedHttpException('You can only invite users as an administrator or an organization owner');
        }

        try {
            if ($organization->userLimitIsReached()) {
                throw new AccessDeniedHttpException(sprintf(
                    'The number of members for %s is limited to %s. Please upgrade the plan.',
                    $organization->name,
                    $organization->user_number
                ));
            }
        } catch (AccessDeniedHttpException $e) {
            $this->flash->error($e->getMessage());
            return redirect()->route('admin.users.get_invite')->withInput();
        }

        $user = $this->users->getByEmail($email);

        if (!is_null($user)) {
            $this->organizations->addUserToOrganizationWithRole($user, $organization, $request->input('role'));
            Event::fire( new UserEnrolledToOrganization( $organization, $user, $request->input('role') ) );

            $this->flash->success(sprintf(
                'User with email address %s is already registered and is now linked to organization %s as %s',
                $email,
                $organization->name,
                $request->input('role')
            ));

            if($request->input('role') == 'owner') {
                $mail_template = 'emails.invite_owner';
            } else {
                $mail_template = 'emails.invite_member';
            }

            $this->mailer->send($mail_template, [
                'email' => $email,
                'token' => false,
                'organization' => $organization,
                'role' => $request->input('role'),
                'inviter' => \Auth::user()->getDisplayNameAttribute(),
                'register' => false
            ], function ($m) use ($email, $organization) {
                $m->subject('Invitation to join ' . $organization->name);
                $m->to($email);
            });

        } else {
            $token = hash_hmac('sha256', uniqid('', true), config('app.key'));

            Invite::unguard();
            $invite = Invite::create([
                'email' => $email,
                'token' => $token,
                'organization_id' => $request->input('organization_id'),
                'role' => $request->input('role'),
            ]);

            if($request->input('role') == 'owner') {
                $mail_template = 'emails.invite_owner';
            } else {
                $mail_template = 'emails.invite_member';
            }

            $this->mailer->send($mail_template, [
                'email' => $email,
                'token' => $token,
                'organization' => $organization,
                'role' => $request->input('role'),
                'inviter' => \Auth::user()->getDisplayNameAttribute(),
                'register' => true
            ], function ($m) use ($email, $organization) {
                $m->subject('Invitation to join ' . $organization->name);
                $m->to($email);
            });

            $this->flash->success(sprintf(
                'An email was sent to %s and user will be linked to %s as %s upon registration',
                $email,
                $organization->name,
                $request->input('role')
            ));
        }

        if ($request->input('redirect_to') == 'organization_profile') {
            return redirect()->route('admin.organizations.get_invite', ['id' => to_hashid($organization->id)]);
        }

        return redirect()->route('admin.users.get_invite');
    }

    public function getOrganizations(Request $request, $id)
    {
        $user = $this->users->getById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }

        $organizations = $user->organizations;
        $repo = app()->make('Myc\Domain\Organizations\OrganizationRepository');
        $organizationNotAttachedToUser = $repo->getAllNotAssociatedWithUser($user);

        return view('admin.users.organizations', [
            'user' => $user,
            'organizations' => $organizations,
            'organizationNotAttachedToUser' => $organizationNotAttachedToUser,
        ]);
    }

    public function patchOrganizations(Request $request, $id)
    {
        $user = $this->users->getById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }

        $input = $request->only(['organization_id', 'role']);

        $validation = \Validator::make($input, [
            'organization_id' => 'required|exists:organizations,id',
            'role' => 'required|in:member,owner',
        ]);

        if ($validation->fails()) {
            return redirect()->route('admin.users.get_organizations', ['id' => to_hashid($user->id)])->withInput()->withErrors($validation->errors());
        }

        $organization = $this->organizations->getById($input['organization_id']);

        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $this->organizations->addUserToOrganizationWithRole($user, $organization, $input['role']);
        Event::fire( new UserEnrolledToOrganization( $organization, $user, $input['role'] ) );

        $this->flash->success(sprintf(
            'User linked to organization %s as %s',
            $organization->name,
            $input['role']
        ));

        return redirect()->route('admin.users.get_organizations', ['id' => to_hashid($user->id)]);
    }

    public function deleteOrganization(Request $request, $id)
    {
        $user = $this->users->getById($id);
        $organization = $this->organizations->getById($request->input('organization_id'));

        if (!$user || !$organization) {
            throw new NotFoundHttpException();
        }

        $this->organizations->removeUserFromOrganization($user, $organization);

        $this->flash->success(sprintf(
            'User %s has been removed from organization %s',
            $user->display_name,
            $organization->name
        ));

        return redirect()->route('admin.users.get_organizations', ['id' => to_hashid($user->id)]);
    }

    public function destroyMultiple(Request $request)
    {
        if (is_array($request->input('users'))) {
            foreach ($request->input('users') as $id) {
                $this->userService->deleteById($id);
            }

            $this->flash->success(sprintf(
                'User%s deleted',
                (count($request->input('users')) > 1) ? 's' : ''
            ));

            return redirect()->route('admin.users.index');
        }
    }

    public function recoverMultiple(Request $request)
    {
        if (is_array($request->input('users'))) {
            foreach ($request->input('users') as $id) {
                $this->userService->recoverById($id);
            }

            $this->flash->success(sprintf(
                'User%s recovered',
                (count($request->input('users')) > 1) ? 's' : ''
            ));

            return redirect()->route('admin.users.index');
        }
    }
}
