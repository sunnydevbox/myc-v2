<?php

namespace Myc\Admin\Http\Controllers;

use Myc\Admin\Notifications\FlashNotifier;
use Myc\Domain\Disapprovals\DisapprovalRepository;
use Illuminate\Http\Request;

class DisapprovalsController extends Controller
{
    /**
     * @var DisapprovalRepository
     */
    private $disapprovals;

    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(
        DisapprovalRepository $disapprovals,
        FlashNotifier $flash
    ) {
        $this->disapprovals = $disapprovals;
        $this->flash = $flash;
    }

    public function index(Request $request)
    {
        $this->disapprovals->queryByString($request->input('q'));

        $disapprovals = $this->disapprovals->getAllPaginated(10);

        return view('admin.disapprovals.index', compact('disapprovals'));
    }

    public function destroyMultiple(Request $request)
    {
        if (is_array($request->input('disapprovals'))) {
            foreach ($request->input('disapprovals') as $id) {
                $this->disapprovals->deleteById($id);
            }

            if (count($request->input('disapprovals')) > 1) {
                $this->flash->success('Alerts deleted');
            } else {
                $this->flash->success('Alert deleted');
            }

            return redirect()->route('admin.disapprovals.index');
        }
    }
}
