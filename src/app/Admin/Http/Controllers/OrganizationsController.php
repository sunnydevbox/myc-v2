<?php

namespace Myc\Admin\Http\Controllers;

use Event;
use Geocoder\Exception\ChainNoResult;
use Geocoder\Exception\CollectionIsEmpty;
use Geocoder\Geocoder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use Myc\Domain\Categories\Tag;
use Myc\Domain\Locations\Location;
use Myc\Domain\Locations\LocationRepository;
use Myc\Domain\Locations\LocationService;
use Myc\Domain\Messages\Message;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Organizations\OrganizationRepository;
use Myc\Admin\Forms\OrganizationValidator;
use Myc\Domain\Images\ImageService;
use Myc\Admin\Forms\FormValidationException;
use Myc\Admin\Notifications\FlashNotifier;
use Illuminate\Http\Request;
use Myc\Domain\Profiles\Fields\AbstractField;
use Myc\Domain\Profiles\Fields\FieldOption;
use Myc\Domain\Profiles\Fields\FieldType;
use Myc\Domain\Users\UserRepository;
use Myc\Domain\ValueObjects\LatLon;
use Myc\Events\UserEnrolledToOrganization;
use Myc\Exceptions\ValidationException;
use Myc\Search\Client;
use Myc\Search\OrganizationDocument;
use Myc\Domain\Users\Invite;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Contracts\Mail\Mailer;


class OrganizationsController extends Controller
{
    protected $organizationValidator;
    /**
     * @var OrganizationRepository
     */
    private $organizations;
    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var LocationRepository
     */
    private $locations;
    /**
     * @var Client
     */
    private $search;

    /**
     * @var Mailer
     */
    private $mailer;

    const IMAGE_PATH = 'app/images/';
    /**
     * @var Geocoder
     */
    private $geocoder;
    /**
     * @var LocationService
     */
    private $locationService;

    public function __construct(
        OrganizationRepository $organizations,
        OrganizationValidator $organizationValidator,
        LocationRepository $locations,
        ImageService $imageService,
        FlashNotifier $flash,
        UserRepository $users,
        Client $search,
        Mailer $mailer,
        Geocoder $geocoder,
        LocationService $locationService
    ) {
        $this->organizations = $organizations;
        $this->organizationValidator = $organizationValidator;
        $this->locations = $locations;
        $this->imageService = $imageService;
        $this->flash = $flash;
        $this->users = $users;
        $this->search = $search;
        $this->mailer = $mailer;
        $this->geocoder = $geocoder;
        $this->locationService = $locationService;
    }

    public function index(Request $request)
    {
        $this->organizations
            ->orderBy($request->input('order_by'), $request->input('order'))
            ->queryByString($request->input('q'));

        if (!$request->user()->isAdmin()) {
            $this->organizations->filterByUser($request->user());
        }

        $organizations = $this->organizations->getAllPaginated(10);

        return view('admin.organizations.index', ['organizations' => $organizations]);
    }

    /**
     * Show the form for creating a new organization.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.organizations.create', ['organization' => new Organization()]);
    }

    /**
     * Show the form for editing the specified organization.
     *
     * @param int $id
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        return view('admin.organizations.edit', ['organization' => $organization]);
    }

    /**
     * Store a newly created organization in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $this->organizationValidator->validate($request->input());
            $organization = new Organization();
            $organization->fill($request->input())->save();

            $this->updateImage($request, $organization->id, 'image');
            $this->updateImage($request, $organization->id, 'header_image');

            $email = trim($request->input('email_address'));

            // Update search index -- only if organization is active
            if ($organization->status === Organization::STATUS_ACTIVE) {
                $this->search->indexDocument(OrganizationDocument::createFromOrganization($organization));

                $this->mailer->send('emails.organization_created', [
                    'organization' => $organization,
                ], function ($m) use ($email) {
                    $m->subject('Welcome on board');
                    $m->to($email);
                });

                $organization->welcome_mail_sent = 1;
                $organization->save();
            }
        } catch (FormValidationException $e) {
            return redirect()->route('admin.organizations.create')->withInput()->with('errors', $e->getErrors());
        }

        $this->flash->success('Organization added');

        return redirect()->route('admin.organizations.edit', ['id' => to_hashid($organization->id)]);
    }

    public function update(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        try {
            $this->organizationValidator->validate($request->input());
            $organization->fill($request->input())->save();

            $this->updateImage($request, $organization->id, 'image');
            $this->updateImage($request, $organization->id, 'header_image');

            // Update search index -- only if organization is active, remove otherwise
            if ($organization->status === Organization::STATUS_ACTIVE) {
                $this->search->indexDocument(OrganizationDocument::createFromOrganization($organization));
            } else {
                $this->search->deleteOrganization($organization->id);
            }
        } catch (FormValidationException $e) {
            return redirect()->route('admin.organizations.edit', ['id' => to_hashid($organization->id)])->withInput()->with('errors', $e->getErrors());
        } catch (ValidationException $e) {
            $this->flash->error(sprintf(
                'We are sorry but your image may not exceed %s kilobytes :(',
                ImageService::MAX_FILE_SIZE
            ));
            return redirect()->route('admin.organizations.edit', ['id' => to_hashid($organization->id)])->withInput()->with('errors', $e->getErrors());
        }

        $this->flash->success('Organization saved');

        return redirect()->route('admin.organizations.edit', ['id' => to_hashid($organization->id)]);
    }

    public function updateImage(Request $request, $id, $type)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $file = $request->file($type);

        if ($file) {
            try {
                $this->imageService->updateImageForOrganization($file, $organization, $type);
                // Update search index -- only if organization is active, remove otherwise
                if ($organization->status === Organization::STATUS_ACTIVE) {
                    $this->search->indexDocument(OrganizationDocument::createFromOrganization($organization));
                } else {
                    $this->search->deleteOrganization($organization->id);
                }
            } catch (ValidationException $e) {
                throw $e;
            } catch (\InvalidArgumentException $e) {
                throw $e;
            }
        }
    }

    public function getFormBuilder(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $profileRepo = app()->make('Myc\Domain\Profiles\ProfileRepository');

        $profile = $profileRepo
            ->includeInactiveFields()
            ->getProfileForOrganizationAndLocation($organization, null);

        return view('admin.organizations.form_builder', [
            'organization' => $organization,
            'fieldTypes' => FieldType::getTypes(),
            'profile' => $profile,
        ]);
    }

    public function putFormBuilder(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $profileService = app()->make('Myc\Domain\Profiles\ProfileService');

        try {
            $profileService->addFieldForOrganization($organization, $request->all());
        } catch (ValidationException $e) {
            return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($id)])->withInput()->with('errors', $e->getErrors());
        }

        $this->flash->success('Profile field added');

        return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($id)]);
    }

    public function destroyMultiple(Request $request)
    {
        if (is_array($request->input()['organizations'])) {
            foreach ($request->input()['organizations'] as $id) {
                try {
                    $this->organizations->deleteById($id);
                    $this->search->deleteOrganization($id);
                } catch (ModelNotFoundException $e) {
                    throw new NotFoundHttpException();
                }
            }

            if (count($request->input()['organizations']) > 1) {
                $this->flash->success('Organisations deleted');
            } else {
                $this->flash->success('Organisation deleted');
            }

            return redirect()->route('admin.organizations.index');
        }
    }

    public function getMembers(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $this->users->filterByOrganization($organization);
        $users = $this->users->getAllPaginated();

        return view('admin.organizations.members', [
            'organization' => $organization,
            'users' => $users,
        ]);
    }

    public function patchMembers(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $input = $request->only(['user_id', 'role']);

        $validation = \Validator::make($input, [
            'user_id' => 'required|exists:users,id',
            'role' => 'required|in:member,owner',
        ]);

        if ($validation->fails()) {
            return redirect()->route('admin.organizations.get_members', ['id' => to_hashid($organization->id)])->withInput()->withErrors($validation->errors());
        }

        $user = $this->users->getById($request->input('user_id'));

        if (!$user) {
            throw new NotFoundHttpException();
        }

        if (!$user->isAdmin() && $request->user()->id == $user->id) {
            $this->flash->error("You can't modify your own role");

            return redirect()->route('admin.organizations.get_members', ['id' => to_hashid($organization->id)]);
        }

        if ('member' === $request->input('role') && $this->organizations->getOwnerCount($organization) === 1) {
            $this->flash->error("You can't change the role to member. There must be at least one owner for the organization");

            return redirect()->route('admin.organizations.get_members', ['id' => to_hashid($organization->id)]);
        }

        $this->organizations->addUserToOrganizationWithRole($user, $organization, $input['role']);
        Event::fire( new UserEnrolledToOrganization( $organization, $user, $input['role'] ) );

        $this->flash->success(sprintf(
            'Role of %s changed to %s',
            $user->display_name,
            $input['role']
        ));

        return redirect()->route('admin.organizations.get_members', ['id' => to_hashid($organization->id)]);
    }

    public function deleteMembers(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        $user = $this->users->getById($request->input('user_id'));

        if (!$organization || !$user) {
            throw new NotFoundHttpException();
        }

        if ($this->organizations->getUserCount($organization) === 1) {
            $this->flash->error("You can't remove this member. There must be at least one member for an organization.");
        } else {
            $this->organizations->removeUserFromOrganization($user, $organization);

            $email = $user->email;
            $this->mailer->send('emails.detach_member', [
                'organization' => $organization,
                'user' => $user,
                'inviter' => \Auth::user()->getDisplayNameAttribute(),
            ], function ($m) use ($email, $organization) {
                $m->subject('You are no longer able to map with ' . $organization->name);
                $m->to($email);
            });

            $this->flash->success(sprintf(
                'User %s has been removed from organization %s',
                $user->display_name,
                $organization->name
            ));
        }

        return redirect()->route('admin.organizations.get_members', ['id' => to_hashid($organization->id)]);
    }

    public function getSubscription(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        return view('admin.organizations.subscription', ['organization' => $organization]);
    }

    public function putSubscription(Request $request, $id)
    {
        // TODO define specific validation rules for Organization subscriptions

        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        try {
            $this->organizationValidator->validateSubscription($request->input());
            $organization->fill($request->input())->save();

            if ($organization->status === Organization::STATUS_ACTIVE) {
                $this->search->indexDocument(OrganizationDocument::createFromOrganization($organization));

                $email = $organization->email_address;
                if(!$organization->welcome_mail_sent) {
                    $this->mailer->send('emails.organization_created', [
                        'organization' => $organization,
                    ], function ($m) use ($email) {
                        $m->subject('Organization added');
                        $m->to($email);
                    });

                    $organization->welcome_mail_sent = 1;
                    $organization->save();
                }
            } else {
                $this->search->deleteOrganization($organization->id);
            }
        } catch (FormValidationException $e) {
            return redirect()->route('admin.organizations.get_subscription', ['id' => to_hashid($organization->id)])->withInput()->with('errors', $e->getErrors());
        }

        $this->flash->success('Subscription data saved');

        return redirect()->route('admin.organizations.get_subscription', ['id' => to_hashid($organization->id)]);
    }

    public function getLocations(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $this->locations->filterByOrganization($organization);
        $locations = $this->locations->getAllPaginated();

        return view('admin.organizations.locations', [
            'organization' => $organization,
            'locations' => $locations,
        ]);
    }

    public function getInvite(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $invites = Invite::orderBy('created_at', 'DESC')->where('organization_id', $id)->paginate(10);

        return view('admin.organizations.invite', compact('organization', 'invites'));
    }


    public function destroyProfileImage(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $this->imageService->deleteImageForOrganization($organization, 'image');

        $this->flash->success('Profile image deleted');

        return redirect()->route('admin.organizations.edit', ['id' => to_hashid($organization->id)]);
    }

    public function destroyHeaderImage(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $this->imageService->deleteImageForOrganization($organization, 'header_image');

        $this->flash->success('Header image deleted');

        return redirect()->route('admin.organizations.edit', ['id' => to_hashid($organization->id)]);
    }

    public function getBulkImport(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $profileRepo = app()->make('Myc\Domain\Profiles\ProfileRepository');
        $profile = $profileRepo
            //->includeInactiveFields()
            ->getProfileForOrganizationAndLocation($organization, null);

        return view('admin.organizations.bulk_import', [
            'profile' => $profile,
            'tags' => Tag::all(),
            'organization' => $organization
        ]);
    }

    public function postBulkImport(Request $request, $id)
    {
        if (!$request->hasFile('file')) {
            $this->flash->error('No file provided');
            return redirect()->route('admin.organizations.get_bulk_import', ['id' => to_hashid($id)]);
        }

        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $excel = \PHPExcel_IOFactory::load($request->file('file')->getRealPath());
        $rows = $excel->getActiveSheet()->toArray(null, false, false, true);
        $errors = new MessageBag();

        $profileRepo = app()->make('Myc\Domain\Profiles\ProfileRepository');

        $ln = 0;
        $colCount = 0;
        $profileFields = [];
        foreach ($rows as $row) {
            $ln++;
            $row = array_values($row);

            if (empty($row[0]) && empty($row[1])) {
                // Empty line, skip
                continue;
            }

            // Convert "A" "B" etc cols

            try {
                if (1 === $ln) { // Header column
                    for ($j = 0; $j < count($row); $j++) {
                        if ($j > 8 && !empty($row[$j])) { // First 9 cols are standard and row must have a value
                            $regExp = preg_match('/\[(.*)\]\[(.*)\]\[([0-9]*)\]/', $row[$j], $matches);
                            if ($regExp === false || !isset($matches[3])) {
                                throw new \Exception(sprintf(
                                    'Profile field header is not formatted correctly: no ID found: %s',
                                    $row[$j]
                                ));
                            }
                            $fieldId = $matches[3];
                            $profileFields[$fieldId] = $j;
                        }
                        $colCount = $j + 1;
                    }
                    // Continue to next row
                    continue;
                }
            } catch (\Exception $e) {
                $errors->add($ln, $e->getMessage());
                break;
            }

            // Standard fields
            $fields = [
                'lat' => null_if_empty($row[0]),
                'lon' => null_if_empty($row[1]),
                'name' => null_if_empty($row[2]),
                'street_name' => null_if_empty($row[3]),
                'street_number' => null_if_empty($row[4]),
                'image1' => null_if_empty($row[5]),
                'image2' => null_if_empty($row[6]),
                'image3' => null_if_empty($row[7]),
                'tag_ids' => array_strip_empty(empty($row[8]) ? [] : array_unique(explode(',', $row[8])))
            ];

            try {
                $latlon = new LatLon($fields['lat'], $fields['lon']);
            } catch (\Exception $e) {
                $errors->add($ln, sprintf('Line %s: invalid format of latitude and/or longtitude.', $ln));
                continue;
            }

            try {
                foreach ($fields['tag_ids'] as $tagId) {
                    if (is_null($tag = Tag::find($tagId))) {
                        throw new \Exception(sprintf('Line %s: invalid tag ID: %s. Field Tag IDs must contain a comma seperated list of integers and integer must correspond with exisiting tag ID (see list).', $ln, $tagId));
                    }
                }
            } catch (\Exception $e) {
                $errors->add($ln, $e->getMessage());
                continue;
            }

            if (empty($fields['name']) && empty($fields['street_name'])) {
                $errors->add($ln, sprintf('Line %s: name or street name is required', $ln));
                continue;
            }

            try {
                $address = $this->geocoder->reverse($latlon->getLat(), $latlon->getLon())->first();
            } catch (CollectionIsEmpty $e) {
                $errors->add($ln, sprintf(
                    'Line %s: no address found for coordinate %s.',
                    $ln,
                    (string) $latlon
                ));
                continue;
            } catch (ChainNoResult $e) {
                $errors->add($ln, sprintf(
                    'Line %s: no Geo provider could find location on %s. Please check format.',
                    $ln,
                    (string) $latlon
                ));
            }

            $imageUrls = array_filter([
                $fields['image1'],
                $fields['image2'],
                $fields['image3']
            ], function($field) {
                return !empty($field);
            });

            foreach ($imageUrls as $imageUrl) {
                try {
                    $file = file_get_contents($imageUrl);
                    $data = getimagesizefromstring($file);
                    if (false === $data) {
                        throw new \InvalidArgumentException(sprintf('%s does not contain a readable image', $imageUrl));
                    }
                } catch (\ErrorException $e) {
                    $errors->add($ln, sprintf('Line %s: error adding image: %s', $ln, $imageUrl));
                    continue;
                } catch (\InvalidArgumentException $e) {
                    $errors->add($ln, sprintf('Line %s: error adding image: %s', $ln, $e->getMessage()));
                    continue;
                }
            }

            $profile = $profileRepo->getProfileForOrganizationAndLocation($organization, null);
            try {
                $profile->getFields()->each(function(AbstractField $field) use (
                    $profileFields,
                    $ln,
                    $row,
                    $errors
                ) {
                    if (!isset($profileFields[$field->getId()])) {
                        throw new \Exception(sprintf(
                            'Line: %s: current organization profile does not match header row. Label: %s',
                            $ln,
                            $field->getLabel()
                        ));
                    }

                    if (!isset($row[$profileFields[$field->getId()]])) {
                        // When Excel sheet does not have a value for the cell, just fill in an empty value
                        $value = '';
                    } else {
                        $value = $row[$profileFields[$field->getId()]];
                    }

                    if (FieldType::FIELD_TYPE_MULTI_SELECT === $field->getIdentifier()) {

                        $values = array_strip_empty(empty($value) ? [] : array_unique(explode(',', $value)));

                        if (!count($values) && $field->isRequired()) {
                            throw new ValidationException(new MessageBag([
                                $field->getIdentifier() => sprintf('Field %s is required but contains no values', $field->getLabel())
                            ]));
                        }

                        foreach ($values as $selectValue) {
                            if (is_null($option = FieldOption::where('profile_field_id', $field->getId())->where('id', $selectValue)->first())) {
                                throw new ValidationException(new MessageBag([$field->getIdentifier() => sprintf('"%s" is not a valid option for %s (see reference list)', $selectValue, $field->getLabel())]));
                            }
                        }

                        // Compare the array with options
                    } else {
                        $field->setEntry($value);
                        $field->validate();
                    }
                });
            } catch (ValidationException $e) {
                $errors->add($ln, sprintf(
                    'Validation error for custom field on line %s: %s',
                    $ln,
                    implode(' ', array_flatten($e->getErrors()->toArray()))
                ));
            } catch (\Exception $e) {
                $errors->add($ln, $e->getMessage());
            }
        }

        if ($errors->count() > 0) {
            $this->flash->error('The file has errors: ' . implode(' ', array_map(function($item) { return '<p>'.implode(', ', $item).'</p>'; }, $errors->toArray())));
        } else {
            write_log('No errors in file: creating locations.');
            $userThatCreatedLocation = $request->user();
            $profile = $profileRepo->getProfileForOrganizationAndLocation($organization, null);
            $ln = 0;
            $importCnt = 0;
            foreach ($rows as $row) {
                $ln++;
                if (1 === $ln) {
                    continue; // Don't import header row
                }
                $row = array_values($row);
                if (empty($row[0]) && empty($row[1])) {
                    // Empty line, skip
                    continue;
                }

                $fields = [
                    'lat' => null_if_empty($row[0]),
                    'lon' => null_if_empty($row[1]),
                    'name' => null_if_empty($row[2]),
                    'street_name' => null_if_empty($row[3]),
                    'street_number' => null_if_empty($row[4]),
                    'image1' => null_if_empty($row[5]),
                    'image2' => null_if_empty($row[6]),
                    'image3' => null_if_empty($row[7]),
                    'tag_ids' => array_strip_empty(empty($row[8]) ? [] : array_unique(array_map('intval', explode(',', $row[8]))))
                ];

                $location = $this->locationService->createLocation(
                    [
                        'lat' => $fields['lat'],
                        'lon' => $fields['lon']
                    ],
                    $userThatCreatedLocation
                );
                $importCnt++;

                if (count($fields['tag_ids'])) {
                    $location->tags()->sync($fields['tag_ids']);
                }

                $fieldsToUpdate = [
                    'name' => $fields['name'],
                    'status' => Location::STATUS_PUBLISHED
                ];

                if (!is_null($fields['street_name']) || (!is_null($fields['street_number']) && !is_null($fields['street_name']))) {
                    $fieldsToUpdate = array_merge($fieldsToUpdate, [
                        'street_name' => $fields['street_name'],
                        'street_number' => $fields['street_number']
                    ]);
                }

                $this->locationService->updateLocation($location, $fieldsToUpdate);

                // Add the images
                $imageUrls = array_filter([
                    $fields['image1'],
                    $fields['image2'],
                    $fields['image3']
                ], function($field) {
                    return !empty($field);
                });

                foreach ($imageUrls as $imageUrl) {
                    try {
                        $this->imageService->remoteImageForLocation($imageUrl, $location);
                    } catch (\InvalidArgumentException $e) {
                        write_log(sprintf(
                            'The URL to the image is not found: %s. Skipping.',
                            $imageUrl
                        ));
                        continue;
                    }
                }

                // Write the Profile Fields
                $profile->getFields()->each(function(AbstractField $field) use (
                    $profileRepo,
                    $profileFields,
                    $row,
                    $location
                ) {
                    // Below is already checked in validation run
                    $value = $row[$profileFields[$field->getId()]];
                    // Don't accept null values insert empty strings
                    $value = is_null($value) ? '' : $value;

                    $field->setEntry($value);

                    if ($field->getIdentifier() === FieldType::FIELD_TYPE_MULTI_SELECT) {
                        // Convert to array of ints
                        $value = array_strip_empty(!is_array($value) ? array_unique(array_map('intval', explode(',', $value))) : $value);
                        
                        $profileRepo->syncFieldOptions($field, $location, $value);
                    } else {
                        $profileRepo->upsertEntry($field, $location);
                    }
                });

                // Attach the organization to the location (if not exists)
                $location->organizations()->sync([$organization->id], false);

                $this->locationService->updateIndex($location);
            }; // Each row
            $this->flash->success(sprintf('%s locations imported.', $importCnt));
        }

        return redirect()->route('admin.organizations.get_bulk_import', ['id' => to_hashid($id)]);
    }

    public function getBulkImportCsvTemplate(Request $request, $id)
    {
        $organization = $this->organizations->getById($id);
        if (!$organization) {
            throw new NotFoundHttpException();
        }

        $headers = [
            'Latitude',
            'Longtitude',
            'Name',
            'Street name',
            'Street number',
            'ImageUrl1',
            'ImageUrl2',
            'ImageUrl3',
            'Tag IDs'
        ];

        $profileRepo = app()->make('Myc\Domain\Profiles\ProfileRepository');
        $profile = $profileRepo
            //->includeInactiveFields()
            ->getProfileForOrganizationAndLocation($organization, null);

        $profile->getFields()->each(function(AbstractField $field) use (&$headers) {
            $headers[] = sprintf(
                '[%s][%s][%s]',
                $field->getLabel(),
                $field->getIdentifier(),
                $field->getId()
            );
        });

        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);

        $rowNum = 1;
        for ($i = 0; $i < count($headers); $i++) {
            $cell = idx_to_letter($i).$rowNum;

            $excel->getActiveSheet()
                ->getStyle(idx_to_letter($i))
                ->getNumberFormat()
                ->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $excel->getActiveSheet()->setCellValue($cell, $headers[$i]);
        }

        $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $filename = str_slug($organization->name).'_import_template.xlsx';

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$filename.'"');

        $writer->save('php://output');
    }

}
