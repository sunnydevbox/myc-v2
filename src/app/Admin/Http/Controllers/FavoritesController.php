<?php

namespace Myc\Admin\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Domain\Favorites\FavoriteRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FavoritesController extends Controller
{
    /**
     * @var FavoriteRepository
     */
    private $favorites;

    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(
        FavoriteRepository $favorites,
        FlashNotifier $flash
    ) {
        $this->favorites = $favorites;
        $this->flash = $flash;
    }

    public function index(Request $request)
    {
        $favorites = $this->favorites
            ->filterByString($request->input('q'))
            ->getAllPaginated(10);

        return view('admin.favorites.index', compact('favorites'));
    }

    public function destroy(Request $request, $locationHash, $userHash)
    {
        $location = $this->favorites->getLocationByHash($locationHash);
        $user = $this->favorites->getUserByHash($userHash);
        try {
            $this->favorites->deleteLocationFavoriteForUser($location, $user);
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException();
        }

        $this->flash->success('Favorite deleted');
    }
}
