<?php

namespace Myc\Admin\Http\Controllers;

use Auth;
use Event;
use Geocoder\Geocoder;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Domain\Categories\Category;
use Myc\Domain\Comments\CommentRepository;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Images\ImageService;
use Myc\Domain\Images\ImageRepository;
use Myc\Domain\Locations\Location;
use Myc\Domain\Locations\LocationRepository;
use Illuminate\Http\Request;
use Myc\Domain\Locations\LocationService;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Profiles\Fields\FieldType;
use Myc\Domain\Users\User;
use Myc\Events\LocationWasMappedToAnOrganization;
use Myc\Exceptions\ValidationException;
use File;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LocationsController extends Controller
{
    /**
     * @var LocationRepository
     */
    private $locations;

    /**
     * @var FlashNotifier
     */
    private $flash;
    /**
     * @var Client
     */
    private $search;
    /**
     * @var Geocoder
     */
    private $geocoder;
    /**
     * @var LocationService
     */
    private $locationService;
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @var CommentRepository
     */
    private $comments;

    /**
     * @var FavoriteRepository
     */
    private $favorites;

    /**
     * @var ImageRepository
     */
    private $images;

    public function __construct(
        LocationRepository $locations,
        LocationService $locationService,
        CommentRepository $comments,
        ImageService $imageService,
        FlashNotifier $flash,
        ImageRepository $images,
        FavoriteRepository $favorites
    ) {
        $this->locations = $locations;
        $this->locationService = $locationService;
        $this->imageService = $imageService;
        $this->flash = $flash;
        $this->comments = $comments;
        $this->images = $images;
        $this->favorites = $favorites;
    }

    public function index(Request $request)
    {
        $this->locations
            ->queryByString($request->input('q'))
            ->orderBy($request->input('order_by'), $request->input('order'));

        // TODO cleanup
        if ($request->has('tags') && is_array($request->input('tags'))) {
            $tags = array_reduce($request->input('tags'), function ($memo, $item) {
                if (!empty($item)) {
                    $memo[] = (int) $item;
                }

                return $memo;
            }, []);
            if (!empty($tags)) {
                $this->locations->filterByTagIds($request->input('tags'));
            }
        }

        if ($request->has('organizations') && is_array($request->input('organizations'))) {
            $organizations = array_reduce($request->input('organizations'), function ($memo, $item) {
                if (!empty($item)) {
                    $memo[] = (int) $item;
                }

                return $memo;
            }, []);
            if (!empty($organizations)) {
                $this->locations->filterByOrganizationIds($request->input('organizations'));
            }
        }

        $perPage = [10, 50, 100, 500];
        $perPageSelected = 10;
        if($request->has('per-page') && in_array($request->input('per-page'), $perPage)) {
            $perPageSelected = $request->input('per-page');
        }
        $this->locations->itemsPerPage($perPageSelected);

        if($request->has('trash')) {
            $this->locations->displayTrashed(true);
        }

        $locations = $this->locations->getAllPaginated();

        return view('admin.locations.index', [
            'locations' => $locations,
            'categories' => Category::all(),
            'organizations' => Organization::all(),
            'perPage' => $perPage,
            'perPageSelected' => $perPageSelected
        ]);
        // return view('emails.password', ['token' => 'token']);
    }

    public function dashboard()
    {
        $this->locations->itemsPerPage(5);
        $locations = $this->locations
                        ->orderBy('created_at', 'desc')
                        ->getAllPaginated(5);

        $disapprovalsRepo = app()->make('Myc\Domain\Disapprovals\DisapprovalRepository');
        $disapprovals = $disapprovalsRepo->orderBy('created_at', 'desc')->getAllPaginated(5);

        $usersRepo = app()->make('Myc\Domain\Users\UserRepository');
        $users = $usersRepo->orderBy('created_at', 'desc')->getAllPaginated(5);

        return view('admin.locations.dashboard', compact('disapprovals', 'locations', 'users'));
    }

    public function create()
    {
        $categories = Category::all();

        return view('admin.locations.create', [
            'location' => new Location(),
            'categories' => $categories,
        ]);
    }

    public function store(Request $request)
    {
        try {
            $location = $this->locationService->createLocation($request->only(['lat', 'lon']), $request->user(), Location::STATUS_DRAFT);
            if ($request->input('tags')) {
                $location->tags()->sync($request->input('tags'));
            }
            $imageUrl = 'https://maps.googleapis.com/maps/api/streetview?size=640x640&location='.$location->lat.','.$location->lon;
            $this->imageService->remoteImageForLocation($imageUrl, $location);

            // Update index again because image/tags may be updated
            $this->locationService->updateIndex($location);
        } catch (ValidationException $e) {
            return redirect()->route('admin.locations.create')->withInput()->with('errors', $e->getErrors());
        } catch (\InvalidArgumentException $e) {
            $this->flash->error($e->getMessage());

            return redirect()->route('admin.locations.create')->withInput();
        }

        $this->flash->success('Location added');

        return redirect()->route('admin.locations.edit', ['id' => to_hashid($location->id)]);
    }

    public function edit(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }

        $this->checkIfAuthorized($request->user(), $location);

        return view('admin.locations.edit', [
            'location' => $location,
            'categories' => Category::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }

        $this->checkIfAuthorized($request->user(), $location);

        try {
            // Sync tag first because ES index will refresh after updating the location
            if ($request->input('tags')) {
                $location->tags()->sync($request->input('tags'));
            };
            // ES will refresh after this
            $this->locationService->updateLocation($location, $request->all(), $request->user());
        } catch (ValidationException $e) {
            return redirect()->route('admin.locations.edit', ['id' => to_hashid($id)])->withInput()->with('errors', $e->getErrors());
        } catch (\InvalidArgumentException $e) {
            $this->flash->error($e->getMessage());

            return redirect()->route('admin.locations.create')->withInput();
        }

        $this->flash->success('Location saved');

        return redirect()->route('admin.locations.edit', ['id' => to_hashid($id)]);
    }

    public function destroy(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }

        $this->checkIfAuthorized($request->user(), $location);

        $this->locationService->deleteLocation($location);

        $this->flash->success('Location deleted');

        return redirect()->route('admin.locations.index');
    }

    public function destroyMultiple(Request $request)
    {
        if (is_array($request->input('locations'))) {
            foreach ($request->input('locations') as $id) {
                $this->locationService->deleteById($id);
            }

            $this->flash->success(sprintf(
                'Location%s deleted',
                (count($request->input('locations')) > 1) ? 's' : ''
            ));

            return redirect()->route('admin.locations.index');
        }
    }

    public function recoverMultiple(Request $request)
    {
        if (is_array($request->input('locations'))) {
            foreach ($request->input('locations') as $id) {
                $this->locationService->recoverById($id);
            }

            $this->flash->success(sprintf(
                'Location%s recovered',
                (count($request->input('locations')) > 1) ? 's' : ''
            ));

            return redirect()->route('admin.locations.index');
        }
    }

    public function createImage(Request $request, $id)
    {
        $file = $request->file('image');
        try {
            $this->imageService->createImageForLocation(
                $file,
                $id,
                $request->user()
            );
        } catch (ValidationException $e) {
            $this->flash->error(sprintf(
                'We are sorry but your image may not exceed %s kilobytes :(',
                ImageService::MAX_FILE_SIZE
            ));
            return redirect()->route('admin.locations.get_images', ['id' => to_hashid($id)]);
        } catch (\InvalidArgumentException $e) {
            // redirect back
            throw $e;
        }

        $this->flash->success('Image added');

        return redirect()->route('admin.locations.get_images', ['id' => to_hashid($id)]);
    }

    public function destroyImage(Request $request, $locationHashid, $imageHashid)
    {
        /** @var Location $location */
        $location = $this->locations->getById(from_hashid($locationHashid));
        if (!$location) {
            throw new NotFoundHttpException();
        }

        $this->checkIfAuthorized($request->user(), $location);

        $image = $this->images->getById(from_hashid($imageHashid));

        if ( !$this->areLocationImagesCorrect( $location ) ) {
            $this->flash->error('A published location must have at least one image');
        }
        // Must be owner or admin to delete  an image
        else if (!$request->user()->isAdmin() && !$image->isOwnedBy($request->user())) {
            /*throw new AccessDeniedHttpException(sprintf(
                'You must have administrator rights or you must have uploaded the image in order to delete it.'
            ));*/
            $this->flash->error('You must have administrator rights or you must have uploaded the image in order to delete it.');
        } else {
            $imageService = app()->make('Myc\Domain\Images\ImageService');
            $imageService->destroy(from_hashid($imageHashid));
            $this->flash->success('Image deleted');
        }

        return redirect()->route('admin.locations.get_images', $locationHashid);
    }

    public function imagesSortOrder(Request $request)
    {
        $this->imageService->sortOrder($request->input('image-container'));

        return response()->json([], 200);
    }

    public function getComments(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }
        $comments = $this->comments->getAllForLocation($location, $request);
        return view('admin.locations.comments', [
            'location' => $location,
            'comments' => $comments,
        ]);
    }

    public function getFavorites(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }
        $favorites = $this->favorites->getUsersWhoFavoriteLocation($location);

        return view('admin.locations.favorites', [
            'location' => $location,
            'favorites' => $favorites,
        ]);
    }

    public function getImages(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }
        $images = $this->images->getAllForLocation($location);

        return view('admin.locations.images', [
            'location' => $location,
            'images' => $images,
        ]);
    }

    public function getOrganizationProfiles(Request $request, $id)
    {
        $location = $this->locations->getById($id);
        if (!$location) {
            throw new NotFoundHttpException();
        }

        $profileRepo = app()->make('Myc\Domain\Profiles\ProfileRepository');
        $organizations = $request->user()->organizations;
        $profiles = new Collection();

        $organizations = $organizations->filter( function( Organization $organization ) {
            return $organization->isActive();
        } );

        /** @var Organization $organization */
        foreach ($organizations as $organization) {

            $profile = $profileRepo->getProfileForOrganizationAndLocation($organization, $location);
            if ($profile) {
                $profiles[] = $profile;
            }
        }

        return view('admin.locations.organization_profiles', [
            'location' => $location,
            'profiles' => $profiles,
        ]);
    }

    public function putOrganizationProfile(Request $request, $locationId, $organizationId)
    {
        $locationId = from_hashid($locationId);
        $organizationId = from_hashid($organizationId);
        /** @var Location $location */
        $location = Location::find($locationId);
        /** @var Organization $organization */
        $organization = Organization::find($organizationId);
        if (!$location || !$organization) {
            throw new NotFoundHttpException();
        }

        $isLocationNewInTheOrganization = !$organization->hasLocation( $location );

        try {
            if (!$request->user()->isAdmin() && !$request->user()->isMember($organization)) {
                throw new \InvalidArgumentException(sprintf(
                    "You can't edit this profile because you are not a member of %s.",
                    $organization->name
                ));
            }

            $profileRepo = app()->make('Myc\Domain\Profiles\ProfileRepository');

            $profile = $profileRepo->getProfileForOrganizationAndLocation($organization, $location);
            if (!$profile) {
                throw new NotFoundHttpException();
            }

            $input = [];

            foreach ($request->all() as $key => $value) {
                if (in_array($key, ['_method', '_token', 'remove_profile'])) {
                    continue;
                }
                $input[(string) $key] = $value;
            }

            $hasInput = false;
            foreach ($input as $value) {
                if (is_array($value)) {
                    $value = array_reduce($value, function($memo, $item) {
                        if (!empty($item) && $item != -1) {
                            $memo[] = $item;
                        }
                        return $memo;
                    }, []);
                }

                if ($value && !empty($value)) {
                    $hasInput = true;
                    break;
                }
            }

            //only save when profile fields are posted, otherwise remove profile fields
            if($hasInput === false || 1 == (int) $request->input('remove_profile')) {
                $profileRepo->deleteProfileForOrganizationAndLocation($organization, $location);
                // Detach location from organization
                $location->organizations()->detach($organization->id);
            } else {
                if ($organization->locationProfileLimitIsReached() && !$organization->hasLocation($location)) {
                    throw new AccessDeniedHttpException(sprintf(
                        'The number of locations this organization can submit profile information for has been reached (limit is %s). Please upgrade the plan.',
                        $organization->building_number
                    ));
                }

                foreach ($profile->getFields() as $field) {
                    if ($field->isRequired() && !array_key_exists($field->id, $input)) {
                        // Field is required but is not part of POST body
                        throw new ValidationException(new MessageBag([
                            $field->id => 'The '.$field->label.' field is required.',
                        ]));
                    }

                    if (array_key_exists($field->id, $input)) {
                        if ($field->getIdentifier() === FieldType::FIELD_TYPE_MULTI_SELECT) {
                            // -1 value to clear out values
                            $profileRepo->syncFieldOptions($field, $location, $input[$field->id]);
                        } else {
                            $field->setEntry($input[$field->id]);
                            $field->validate();
                            $profileRepo->upsertEntry($field, $location);
                        }
                    }
                }

                // Attach the organization to the location (if not exists)
                $location->organizations()->sync([$organization->id], false);
            }

        } catch (\InvalidArgumentException $e) {
            $this->flash->error($e->getMessage());
            return redirect()->route('admin.locations.get_organization_profiles', ['id' => to_hashid($location->id)])->withInput();
        } catch (ValidationException $e) {
            return redirect()->route('admin.locations.get_organization_profiles', ['id' => to_hashid($location->id)])->withInput()->with('errors', $e->getErrors());
        } catch (AccessDeniedHttpException $e) {
            $this->flash->error($e->getMessage());
            return redirect()->route('admin.locations.get_organization_profiles', ['id' => to_hashid($location->id)])->withInput();
        }

        if ( !( (int) $request->input('remove_profile') ) && $isLocationNewInTheOrganization ) {
            Event::fire( new LocationWasMappedToAnOrganization( $location, $organization, Auth::user() ) );
        }

        $this->flash->success(sprintf(
            'Profile of %s updated',
            $organization->name
        ));

        return redirect()->route('admin.locations.get_organization_profiles', ['id' => to_hashid($location->id)]);
    }

    private function checkIfAuthorized(User $user, Location $location)
    {
        // Disable restrictions on Location CRUD for now
        /*if (!$user->isAdmin() && !$user->hasCreated($location)) {
            throw new AccessDeniedHttpException();
        }*/
    }

    /**
     * @param Location $location
     * @return bool
     *
     * This method checks the date because https://podio.com/andrsnu/map-your-city-dev-team/apps/development/items/494
     */
    private function areLocationImagesCorrect( Location $location ) {
        if ( !LocationService::isLocationCreatedAfterNewValidationRules( $location ) ) {
            return true;
        }

        return !($location->images->count() == 1 && $location->isPublished());
    }
}
