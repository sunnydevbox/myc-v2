<?php

namespace Myc\Admin\Http\Controllers;

use Geocoder\Geocoder;
use Illuminate\Http\Request;

class GeocodeController extends Controller
{
    /**
     * @var Geocoder
     */
    private $geocoder;

    public function __construct(Geocoder $geocoder)
    {
        $this->geocoder = $geocoder;
    }

    public function getForm()
    {
        return view('admin.geocode.form');
    }

    public function postForm(Request $request)
    {
        $addresses = [];

        if ($request->has('address_string')) {
            $address_string = $request->get('address_string');
            if (!empty($address_string)) {
                $addresses = $this->geocoder->geocode($request->input('address_string'));
            }
        }

        if ($request->has('lat_lon')) {
            $parts = explode(',', $request->input('lat_lon'));
            if ($parts) {
                $lat = (float) $parts[0];
                $lon = (float) $parts[1];
                $addresses = $this->geocoder->reverse($lat, $lon);
            }
        }

        return redirect()->route('admin.geocode.get_form')->withInput(['addresses' => $addresses]);
    }
}
