<?php

namespace Myc\Admin\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Admin\Forms\FormValidationException;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Admin\Forms\TagValidator;
use Myc\Domain\Categories\Category;
use Myc\Domain\Categories\Tag;
use Myc\Domain\Categories\TagRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TagsController extends Controller
{
    protected $tagValidator;

    /**
     * @var TagRepository
     */
    private $tags;

    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(
        TagRepository $tags,
        TagValidator $tagValidator,
        FlashNotifier $flash
    ) {
        $this->tags = $tags;
        $this->tagValidator = $tagValidator;
        $this->flash = $flash;
    }

    public function index(Request $request)
    {
        $tags = $this->tags
            ->orderBy($request->input('order_by'), $request->input('order'))
            ->filterByString($request->input('q'))
            ->getAllPaginated(10);

        return view('admin.tags.index', compact('tags'));
    }

    public function create()
    {
        $categories = Category::lists('name', 'id');

        return view('admin.tags.create', [
            'tag' => new Tag(),
            'categories' => $categories,
        ]);
    }

    public function edit($id)
    {
        $tag = $this->tags->getById($id);
        $categories = Category::lists('name', 'id');

        return view('admin.tags.edit', [
            'tag' => $tag,
            'categories' => $categories,
        ]);
    }

    public function store(Request $request)
    {
        try {
            $this->tagValidator->validate($request->input());
            $tag = new Tag();
            $tag->fill($request->input())->save();
        } catch (FormValidationException $e) {
            return redirect()->route('admin.tags.create')->withInput()->with('errors', $e->getErrors());
        }

        $this->flash->success('Tag added');

        return redirect()->route('admin.tags.index');
    }

    public function update($id, Request $request)
    {
        try {
            $this->tagValidator->validate($request->input());
            $tag = $this->tags->getById($id);

            $tag->fill($request->input());
            $tag->save();
        } catch (FormValidationException $e) {
            return redirect()->route('admin.tags.edit', ['id' => $id])
                ->withInput()
                ->with('errors', $e->getErrors());
        }

        $this->flash->success('Tag saved');

        return redirect()->route('admin.tags.index');
    }

    public function destroy($id)
    {
        try {
            $this->tags->deleteById($id);
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException();
        }

        $this->flash->success('Tag deleted');
    }

    public function destroyMultiple(Request $request)
    {
        if (is_array($request->input('tags'))) {
            foreach ($request->input('tags') as $id) {
                $this->tags->deleteById($id);
            }

            if (count($request->input('tags')) > 1) {
                $this->flash->success('Tags deleted');
            } else {
                $this->flash->success('Tag deleted');
            }

            return redirect()->route('admin.tags.index');
        }
    }
}
