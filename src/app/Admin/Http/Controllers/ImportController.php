<?php

namespace Myc\Admin\Http\Controllers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Commands\ImportCsvFile;
use Myc\Domain\Organizations\Organization;
use Ramsey\Uuid\Uuid;

class ImportController extends Controller
{
    /**
     * @var Filesystem
     */
    private $files;
    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(Filesystem $files, FlashNotifier $flash)
    {
        $this->files = $files;
        $this->flash = $flash;
    }

    public function create()
    {
        $logFiles = $this->files->allFiles(storage_path('app/import_logs'));
        arsort($logFiles);

        return view('admin.import.create', [
            'organizations' => Organization::all(),
            'logFiles' => $logFiles,
        ]);
    }

    public function store(Request $request)
    {
        $validation = \Validator::make($request->only(['file', 'organization_id']), [
            'file' => 'required',
            'organization_id' => 'exists:organizations,id',
        ]);

        if ($validation->fails()) {
            return redirect()->route('admin.import.create')->withInput()->withErrors($validation->errors());
        }

        if (!$request->hasFile('file')) {
            $this->flash->error('No file provided');

            return redirect()->route('admin.import.create');
        }

        $creator = $request->user();

        if ($request->has('organization_id')) {
            // Use the first created owner of the organization as the locations creator
            $userId = \DB::table('organization_user')
                ->where('organization_id', '=', $request->input('organization_id'))
                ->where('role', '=', 'owner')
                ->orderBy('created_at', 'ASC')
                ->pluck('user_id');
            $creator = \User::find($userId);
            if (!$creator) {
                $this->flash->error(sprintf(
                    'Selected organization has no owner, locations cannot be imported'
                ));

                return redirect()->route('admin.import.create')->withInput();
            }
            $organization = Organization::find($request->input('organization_id'));
        } else {
            $organization = null;
        }

        $filename = Uuid::uuid4();
        $request->file('file')->move(storage_path('app/import_tmp/'), $filename);

        $this->dispatch(new ImportCsvFile(storage_path('app/import_tmp/').$filename, $request->file('file')->getClientOriginalName(), $creator, $organization));

        $this->flash->success('Import job added to queue. When job is finished the log will be available in the list below.');

        return redirect()->route('admin.import.create');
    }

    public function getLogFile($path)
    {
        return response()->download(storage_path('app/import_logs/').$path);
    }

    public function deleteLogFile($path)
    {
        if ($this->files->delete(storage_path('app/import_logs/').$path)) {
            $this->flash->success('Log file deleted');
        } else {
            $this->flash->error('Error removing file');
        }

        return redirect()->route('admin.import.create');
    }
}
