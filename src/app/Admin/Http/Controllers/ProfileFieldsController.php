<?php

namespace Myc\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Myc\Admin\Notifications\FlashNotifier;
use Myc\Domain\Profiles\ProfileRepository;
use Myc\Domain\Profiles\ProfileService;
use Myc\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProfileFieldsController extends Controller
{
    /**
     * @var ProfileRepository
     */
    private $profiles;
    /**
     * @var ProfileService
     */
    private $profileService;
    /**
     * @var FlashNotifier
     */
    private $flash;

    public function __construct(ProfileRepository $profiles, ProfileService $profileService, FlashNotifier $flash)
    {
        $this->profiles = $profiles;
        $this->profileService = $profileService;
        $this->flash = $flash;
    }

    public function destroy($id)
    {
        $profileField = $this->profiles->getProfileFieldById($id);

        if (!$profileField) {
            throw new NotFoundHttpException();
        }

        $profile = $this->profiles->getByProfileField($profileField);

        if ($this->profiles->profileFieldHasEntries($profileField)) {
            $this->flash->error('This field already contains entries and cannot be removed');

            return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($profile->organization_id)]);
        }

        $this->profileService->deleteProfileField($profileField);
        $this->flash->success('Field deleted');

        return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($profile->organization_id)]);
    }

    public function putHide($id)
    {
        $profileField = $this->profiles->getProfileFieldById($id);

        if (!$profileField) {
            throw new NotFoundHttpException();
        }

        $profile = $this->profiles->getByProfileField($profileField);

        $this->profileService->hideProfileField($profileField);
        $this->flash->success('Field hidden');

        return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($profile->organization_id)]);
    }

    public function putShow($id)
    {
        $profileField = $this->profiles->getProfileFieldById($id);

        if (!$profileField) {
            throw new NotFoundHttpException();
        }

        $profile = $this->profiles->getByProfileField($profileField);

        $this->profileService->showProfileField($profileField);
        $this->flash->success('Field is active');

        return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($profile->organization_id)]);
    }

    public function postFieldOrder(Request $request)
    {
        if (is_null($request->json('data')) || !is_array($request->json('data'))) {
            dd('bad request');
        }

        $input = array_map(function ($item) {
            return [
                'id' => from_hashid($item['id']),
                'sort_order' => (int) $item['sort_order'],
            ];
        }, $request->json('data'));

        $this->profileService->updateProfileFieldOrder($input);

        return response()->json([], 200);
    }

    public function update(Request $request) {

        $profileField = $this->profiles->getProfileFieldById(from_hashid($request->input('hashid')));

        try {
            $this->profileService->updateFieldForOrganization($profileField, $request->input());
        } catch (ValidationException $e) {
            $this->flash->error('An error ocurred.', $e->getErrors());
            return redirect()->back();
        }

        $profile = $this->profiles->getByProfileField($profileField);

        $this->flash->success('Field updated');
        return redirect()->route('admin.organizations.get_form_builder', ['id' => to_hashid($profile->organization_id)]);
    }

}