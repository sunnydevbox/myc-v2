<?php

$router->get('/', function (\Illuminate\Http\Request $request) {
    if ($request->user()->isAdmin()) {
        return redirect()->route('admin.dashboard');
    }
    return redirect()->route('admin.locations.index');
});

// Locations
$router->group(['prefix' => 'locations'], function ($router) {
    $router->get('/', [
        'as' => 'admin.locations.index',
        'uses' => 'LocationsController@index',
    ]);

    $router->get('create', [
        'as' => 'admin.locations.create',
        'uses' => 'LocationsController@create',
    ]);

    $router->post('/', [
        'as' => 'admin.locations.store',
        'uses' => 'LocationsController@store',
    ]);

    $router->get('{hashid}/edit', [
        'as' => 'admin.locations.edit',
        'uses' => 'LocationsController@edit',
    ]);

    $router->get('{hashid}/comments', [
        'as' => 'admin.locations.get_comments',
        'uses' => 'LocationsController@getComments',
    ]);

    $router->get('{hashid}/favorites', [
        'as' => 'admin.locations.get_favorites',
        'uses' => 'LocationsController@getFavorites',
    ]);

    $router->get('{hashid}/images', [
        'as' => 'admin.locations.get_images',
        'uses' => 'LocationsController@getImages',
    ]);

    $router->put('{hashid}', [
        'as' => 'admin.locations.update',
        'uses' => 'LocationsController@update',
    ]);

    $router->get('{hashid}/organization_profiles', [
        'as' => 'admin.locations.get_organization_profiles',
        'uses' => 'LocationsController@getOrganizationProfiles',
    ]);

    $router->put('{locationHashid}/organizations/{organizationHashid}/profile', [
        'as' => 'admin.locations.put_organization_profile',
        'uses' => 'LocationsController@putOrganizationProfile',
    ]);

    $router->post('{hashid}/images', [
        'as' => 'admin.images.create',
        'uses' => 'LocationsController@createImage',
    ]);

    $router->delete('{locationHashid}/images/{imageHashid}', [
        'as' => 'admin.images.delete',
        'uses' => 'LocationsController@destroyImage',
    ]);

    $router->post('delete', [
        'as' => 'admin.locations.delete_multiple',
        'uses' => 'LocationsController@destroyMultiple',
    ]);

    $router->post('recover', [
        'as' => 'admin.locations.recover_multiple',
        'uses' => 'LocationsController@recoverMultiple',
    ]);

    $router->post('images/sort-order', [
        'as' => 'admin.images.sort-order',
        'uses' => 'LocationsController@imagesSortOrder',
    ]);
});

// Organizations
$router->group(['prefix' => 'organizations'], function ($router) {
    $router->group(['middleware' => 'is_admin'], function ($router) {
        // C D of organizations done by admin
        $router->get('create', [
            'as' => 'admin.organizations.create',
            'uses' => 'OrganizationsController@create',
        ]);

        $router->post('delete', [
            'as' => 'admin.organizations.delete_multiple',
            'uses' => 'OrganizationsController@destroyMultiple',
        ]);

        // Subscription mgmt is done by admin
        $router->get('{hashid}/subscription', [
            'as' => 'admin.organizations.get_subscription',
            'uses' => 'OrganizationsController@getSubscription',
        ]);

        $router->put('{hashid}/subscription', [
            'as' => 'admin.organizations.put_subscription',
            'uses' => 'OrganizationsController@putSubscription',
        ]);
    });

    $router->get('{hashid}/bulk_import', [
        'as' => 'admin.organizations.get_bulk_import',
        'uses' => 'OrganizationsController@getBulkImport',
    ]);

    $router->post('{hashid}/bulk_import', [
        'as' => 'admin.organizations.post_bulk_import',
        'uses' => 'OrganizationsController@postBulkImport',
    ]);

    $router->get('{hashid}/bulk_import_template', [
        'as' => 'admin.organizations.get_bulk_import_template',
        'uses' => 'OrganizationsController@getBulkImportCsvTemplate',
    ]);

    $router->get('/', [
        'as' => 'admin.organizations.index',
        'uses' => 'OrganizationsController@index',
    ]);

    $router->post('/', [
        'as' => 'admin.organizations.store',
        'uses' => 'OrganizationsController@store',
    ]);

    $router->put('/{hashid}', [
        'as' => 'admin.organizations.update',
        'uses' => 'OrganizationsController@update',
    ]);

    $router->get('{hashid}/edit', [
        'as' => 'admin.organizations.edit',
        'uses' => 'OrganizationsController@edit',
    ]);

    $router->get('{hashid}/form_builder', [
        'as' => 'admin.organizations.get_form_builder',
        'uses' => 'OrganizationsController@getFormBuilder',
    ]);

    $router->put('{hashid}/form_builder', [
        'as' => 'admin.organizations.put_form_builder',
        'uses' => 'OrganizationsController@putFormBuilder',
    ]);

    // List all Organization's members (Users associated with Organization)
    $router->get('{hashid}/members', [
        'as' => 'admin.organizations.get_members',
        'uses' => 'OrganizationsController@getMembers',
    ]);

    $router->patch('{hashid}/members', [
        'as' => 'admin.organizations.patch_members',
        'uses' => 'OrganizationsController@patchMembers',
    ]);

    $router->delete('{hashid}/members', [
        'as' => 'admin.organizations.delete_members',
        'uses' => 'OrganizationsController@deleteMembers',
    ]);

    $router->delete('{hashid}/profile_image', [
        'as' => 'admin.organizations.delete_profile_image',
        'uses' => 'OrganizationsController@destroyProfileImage',
    ]);

    $router->delete('{hashid}/header_image', [
        'as' => 'admin.organizations.delete_header_image',
        'uses' => 'OrganizationsController@destroyHeaderImage',
    ]);

    $router->get('{hashid}/invite', [
        'as' => 'admin.organizations.get_invite',
        'uses' => 'OrganizationsController@getInvite',
    ]);

    // List all Organization's Locations
    $router->get('{hashid}/locations', [
        'as' => 'admin.organizations.get_locations',
        'uses' => 'OrganizationsController@getLocations',
    ]);
});

// Users
$router->get('users', [
    'as' => 'admin.users.index',
    'uses' => 'UsersController@index',
]);

$router->get('users/{hashid}/edit', [
    'as' => 'admin.users.edit',
    'uses' => 'UsersController@edit',
]);

$router->get('users/{hashid}/organizations', [
    'as' => 'admin.users.get_organizations',
    'uses' => 'UsersController@getOrganizations',
]);

$router->patch('users/{hashid}/organizations', [
    'middleware' => ['is_admin'],
    'as' => 'admin.users.patch_organizations',
    'uses' => 'UsersController@patchOrganizations',
]);

$router->delete('users/{hashid}/organizations', [
    'middleware' => ['is_admin'],
    'as' => 'admin.users.delete_organization',
    'uses' => 'UsersController@deleteOrganization',
]);

$router->patch('users/{hashid}', [
    'as' => 'admin.users.update',
    'uses' => 'UsersController@update',
]);

$router->post('delete', [
    'as' => 'admin.users.delete_multiple',
    'uses' => 'UsersController@destroyMultiple',
]);

$router->post('recover', [
        'as' => 'admin.users.recover_multiple',
        'uses' => 'UsersController@recoverMultiple',
    ]);

// Fill in a form to send an invite for a person to signup
$router->get('users/invite', [
    'as' => 'admin.users.get_invite',
    'uses' => 'UsersController@getInvite',
]);

$router->post('users/invite', [
    'as' => 'admin.users.post_invite',
    'uses' => 'UsersController@postInvite',
]);

$router->get('profile', [
    'as' => 'admin.users.profile',
    'uses' => 'UsersController@profile',
]);

// User Profile
$router->put('profile', [
    'as' => 'admin.users.update_profile',
    'uses' => 'UsersController@updateProfile',
]);

$router->post('profile/image', [
    'as' => 'admin.users.profile_image',
    'uses' => 'UsersController@updateProfileImage',
]);

// Favorites
$router->group(['prefix' => 'favorites'], function ($router) {
    $router->delete('{locationHashid}/{userHashid}', [
        'as' => 'admin.favorites.delete',
        'uses' => 'FavoritesController@destroy',
    ]);
});

/*
 * Routes for disapprovals
 */
$router->get('disapprovals', [
    'as' => 'admin.disapprovals.index',
    'uses' => 'DisapprovalsController@index',
]);
$router->post('disapprovals/delete', [
    'as' => 'admin.disapprovals.delete_multiple',
    'uses' => 'DisapprovalsController@destroyMultiple',
]);

/*
 * Routes for profile_fields
 */
$router->group(['prefix' => 'profile_fields'], function($router)
{
    $router->delete('{hashid}', [
        'as' => 'admin.profile_fields.delete',
        'uses' => 'ProfileFieldsController@destroy'
    ]);

    $router->put('{hashid}/hide', [
        'as' => 'admin.profile_fields.hide',
        'uses' => 'ProfileFieldsController@putHide'
    ]);

    $router->put('{hashid}/show', [
        'as' => 'admin.profile_fields.show',
        'uses' => 'ProfileFieldsController@putShow'
    ]);

    $router->put('{hashid}', [
        'as' => 'admin.profile_fields.update',
        'uses' => 'ProfileFieldsController@update'
    ]);

    $router->post('order', [
        'as' => 'admin.profile_fields.order',
        'uses' => 'ProfileFieldsController@postFieldOrder'
    ]);
});


$router->group(['prefix' => 'comments'], function($router)
{
    $router->get('/', [
        'as' => 'admin.comments.index',
        'uses' => 'CommentsController@index',
    ]);

    $router->get('{hashid}/edit', [
        'as' => 'admin.comments.edit',
        'uses' => 'CommentsController@edit',
    ]);

    $router->delete('{hashid}', [
        'as' => 'admin.comments.delete',
        'uses' => 'CommentsController@destroy',
    ]);

    $router->patch('{hashid}', [
        'as' => 'admin.comments.update',
        'uses' => 'CommentsController@update',
    ]);

});

// Dashboard, Tags, Import and Approvals are for admin only
$router->group(['middleware' => 'is_admin'], function ($router) {
    // Dashboard
    $router->get('/dashboard', [
        'middleware' => ['is_admin'],
        'as' => 'admin.dashboard',
        'uses' => 'LocationsController@dashboard',
    ]);

    // Tags
    $router->get('tags', [
        'as' => 'admin.tags.index',
        'uses' => 'TagsController@index',
    ]);
    $router->get('tags/create', [
        'as' => 'admin.tags.create',
        'uses' => 'TagsController@create',
    ]);

    $router->get('tags/{hashid}/edit', [
        'as' => 'admin.tags.edit',
        'uses' => 'TagsController@edit',
    ]);

    $router->post('tags/', [
        'as' => 'admin.tags.store',
        'uses' => 'TagsController@store',
    ]);

    $router->patch('tags/{id}', [
        'as' => 'admin.tags.update',
        'uses' => 'TagsController@update',
    ]);

    $router->delete('tags/{id}', [
        'as' => 'admin.tags.delete',
        'uses' => 'TagsController@destroy',
    ]);

    $router->post('tags/delete', [
        'as' => 'admin.tags.delete_multiple',
        'uses' => 'TagsController@destroyMultiple',
    ]);

    // Import
    $router->get('import', [
        'as' => 'admin.import.create',
        'uses' => 'ImportController@create',
    ]);

    $router->get('import/log_file/{path}', [
        'as' => 'admin.import.get_log_file',
        'uses' => 'ImportController@getLogFile',
    ]);

    $router->delete('import/log_file/{path}', [
        'as' => 'admin.import.delete_log_file',
        'uses' => 'ImportController@deleteLogFile',
    ]);

    $router->post('import', [
        'as' => 'admin.import.store',
        'uses' => 'ImportController@store',
    ]);

    // Export
    $router->get('export', [
        'as' => 'admin.export.index',
        'uses' => 'ExportController@index'
    ]);

    $router->get('export/file', [
        'as' => 'admin.export.get_file',
        'uses' => 'ExportController@getFile'
    ]);

    // Disapprovals/Alerts
    $router->get('disapprovals', [
        'as' => 'admin.disapprovals.index',
        'uses' => 'DisapprovalsController@index',
    ]);

    $router->post('disapprovals/delete', [
        'as' => 'admin.disapprovals.delete_multiple',
        'uses' => 'DisapprovalsController@destroyMultiple',
    ]);
});

// Gecode debugger
$router->get('geocode', [
    'as' => 'admin.geocode.get_form',
    'uses' => 'GeocodeController@getForm',
]);

$router->post('geocode', [
    'as' => 'admin.geocode.post_form',
    'uses' => 'GeocodeController@postForm',
]);