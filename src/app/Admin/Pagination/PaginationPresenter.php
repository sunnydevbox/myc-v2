<?php

namespace Myc\Admin\Pagination;

use Illuminate\Pagination\BootstrapThreePresenter;

class PaginationPresenter extends BootstrapThreePresenter
{
    public function render()
    {
        if ($this->hasPages()) {
            return sprintf(
                '<ul class="pagination-plain">%s %s %s</ul>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            );
        }

        return '';
    }

    protected function getPreviousButton($text = '')
    {
        $disabled = $this->paginator->currentPage() <= 1;
        $url = $this->paginator->url($this->paginator->currentPage() - 1);

        return sprintf(
            '<li class="previous %s"><a href="%s">&larr; Previous</a></li>',
            $disabled ? 'disabled' : '',
            $disabled ? 'javascript:void(0)' : $url
        );
    }

    protected function getNextButton($text = '')
    {
        $disabled = !$this->paginator->hasMorePages();
        $url = $this->paginator->url($this->paginator->currentPage() + 1);

        return sprintf(
            '<li class="next %s"><a href="%s">Newer &rarr;</a></li>',
            $disabled ? 'disabled' : '',
            $disabled ? 'javascript:void(0)' : $url
        );
    }
}
