<?php

namespace Myc\Admin\Forms;

class TagValidator extends FormValidator
{
    /**
     * Validation rules for tags form.
     *
     * @var array
     */
    protected $rules = [
        'name' => 'required',
    ];
}
