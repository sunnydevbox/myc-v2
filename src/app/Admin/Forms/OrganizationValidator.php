<?php

namespace Myc\Admin\Forms;

class OrganizationValidator extends FormValidator
{
    /**
     * Validation rules for organization form.
     *
     * @var array
     */
    protected $rules = [
        'name' => 'required',
        'email_address' => 'required',
    ];

    public function validateSubscription(array $input)
    {
        $this->validation = $this->validator->make($input, $this->getRulesForSubscription());
        if ($this->validation->fails()) {
            throw new FormValidationException('Validation failed', $this->getValidationErrors());
        }

        return true;
    }

    private function getRulesForSubscription()
    {
        return [];
    }
}
