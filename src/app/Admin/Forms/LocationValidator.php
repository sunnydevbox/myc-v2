<?php

namespace Myc\Admin\Forms;

class LocationValidator extends FormValidator
{
    /**
     * Validation rules for locaqtions form.
     *
     * @var array
     */
    protected $rules = [
        'lon' => 'required',
        'lat' => 'required',
    ];
}
