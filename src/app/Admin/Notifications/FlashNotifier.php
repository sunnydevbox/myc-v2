<?php

namespace Myc\Admin\Notifications;

use Illuminate\Session\Store;

class FlashNotifier
{
    /**
     * @var \Illuminate\Session\Store
     */
    private $session;
    /**
     * @param Store $session
     */
    public function __construct(Store $session)
    {
        $this->session = $session;
    }
    /**
     * @param $message
     */
    public function success($message)
    {
        $this->message($message, 'success');
    }
    /**
     * @param $message
     */
    public function error($message, $errors = false)
    {
        $this->message($message, 'danger', $errors);
    }
    /**
     * @param $message
     */
    public function overlay($message)
    {
        $this->message($message);
        $this->session->flash('flash_notification.overlay', true);
    }
    /**
     * @param $message
     * @param string $level
     */
    public function message($message, $level = 'info', $errors = false)
    {
        $this->session->flash('flash_notification.message', $message);
        $this->session->flash('flash_notification.level', $level);
        $this->session->flash('flash_notification.errors', $errors);
    }
}
