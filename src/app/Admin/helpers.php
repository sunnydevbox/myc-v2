<?php

if (!function_exists('active_state')) {
    function active_state($route)
    {
        return strpos(\Request::url(), route($route)) !== false ? 'class="active"' : '';
    }
}

if (!function_exists('has_role')) {
    function has_role($role)
    {
        return app('auth')->user()->hasRole($role);
    }
}

if (!function_exists('user_avatar')) {
    function user_avatar($image)
    {
        if (is_null($image)) {
            $src = asset('assets/img/profile.png');
        } else {
            $src = route('images.get_asset', ['path' => $image]).'?w=56&h=56&fit=crop';
        }

        return sprintf(
            '<div class="avatar" style="background-image: url(%s)"></div>',
            $src
        );
    }
}

if (!function_exists('organization_thumbnail')) {
    function organization_thumbnail($image)
    {
        if (empty($image)) {
            return '';
        } else {
            return sprintf(
                '<div class="organization-thumb"><img src="%s"></div>',
                route('images.get_asset', ['path' => $image]).'?w=44&h=44&fit=crop'
            );
        }
    }
}

if (!function_exists('status_label')) {
    function status_label($status)
    {
        if (\Myc\Domain\Locations\Location::STATUS_PUBLISHED === $status) {
            return 'Published';
        }
        if (\Myc\Domain\Locations\Location::STATUS_DRAFT === $status || \Myc\Domain\Locations\Location::STATUS_DRAFT_MAP === $status) {
            return 'Draft';
        }
        throw new \InvalidArgumentException('Unknown Location Status: '.$status);
    }
}

if (!function_exists('organization_status_label')) {
    function organization_status_label($status)
    {
        if (\Myc\Domain\Organizations\Organization::STATUS_ACTIVE === $status) {
            return 'Active';
        }
        if (\Myc\Domain\Organizations\Organization::STATUS_DISABLED === $status) {
            return 'Disabled';
        }
        throw new \InvalidArgumentException('Unknown Organization Status: '.$status);
    }
}

if (!function_exists('error_class')) {
    function error_class($errors, $field)
    {
        return $errors->has($field) ? ' has-error' : '';
    }
}

if (!function_exists('location_status')) {
    function location_status($status)
    {
        if (\Myc\Domain\Locations\Location::STATUS_DRAFT === $status || \Myc\Domain\Locations\Location::STATUS_DRAFT_MAP === $status) {
            $label = [
                'className' => 'label-inverse',
                'text' => status_label($status),
            ];
        }
        if (\Myc\Domain\Locations\Location::STATUS_PUBLISHED === $status) {
            $label = [
                'className' => 'label-success',
                'text' => status_label($status),
            ];
        }
        if (!isset($label)) {
            throw new \InvalidArgumentException('Unknown Location Status: '.$status);
        }

        return '<span class="label '.$label['className'].'">'.$label['text'].'</span>';
    }
}

if (!function_exists('organization_status')) {
    function organization_status($status)
    {
        if (\Myc\Domain\Organizations\Organization::STATUS_DISABLED === $status) {
            $label = [
                'className' => 'label-danger',
                'text' => organization_status_label($status),
            ];
        }
        if (\Myc\Domain\Organizations\Organization::STATUS_ACTIVE === $status) {
            $label = [
                'className' => 'label-success',
                'text' => organization_status_label($status),
            ];
        }
        if (!isset($label)) {
            throw new \InvalidArgumentException('Unknown Organization Status: '.$status);
        }

        return '<span class="label '.$label['className'].'">'.$label['text'].'</span';
    }
}

if (!function_exists('option_selected')) {
    // Generic function to print out the selected attribute for <option> tags
    // when the id is in the the array of selected IDs
    function option_selected($id, $selected)
    {
        if (!is_array($selected)) {
            $selected = [$selected];
        }

        return in_array($id, $selected) ? 'selected="selected"' : '';
    }
}

if (!function_exists('checkbox_checked')) {
    // Generic function to print out the selected attribute for <option> tags
    // when the id is in the the array of selected IDs
    function checkbox_checked($condition)
    {
        if ($condition) {
            return 'checked';
        }
        return '';
    }
}

if (!function_exists('to_link')) {
    // Generic function to print out the selected attribute for <option> tags
    // when the id is in the the array of selected IDs
    function to_link($url, $label = null)
    {
        if (is_null($label)) {
            $label = $url;
        }

        return sprintf(
            '<a href="%s">%s</a>',
            $url,
            $label
        );
    }
}

if (!function_exists('field_type_label')) {
    function field_type_label($type)
    {
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_NUMERIC === $type) {
            return 'Number';
        }
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_YEAR === $type) {
            return 'Year';
        }
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_SELECT === $type) {
            return 'Dropdown';
        }
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_TEXTAREA === $type) {
            return 'Paragraph text';
        }
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_TEXT === $type) {
            return 'Single line text';
        }
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_URL === $type) {
            return 'Link';
        }
        if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_MULTI_SELECT === $type) {
            return 'Multi-select';
        }

        throw new \InvalidArgumentException('Unknown field type: '.$type);
    }
}

if (!function_exists('order_link')) {
    function order_link($field, $label, $default = false)
    {
        // Strip page and order from query string
        $params = \Request::except(['page', 'order_by', 'order']);
        $url = \Request::url();
        $order = (!\Request::has('order') || ((\Request::input('order_by') === $field) && \Request::input('order') === 'desc')) ? 'asc' : 'desc';

        $query = http_build_query(array_merge($params, [
            'order_by' => $field,
            'order' => $order,
        ]));

        return sprintf(
            '<a href="%s">%s %s</a>',
            $url.'?'.$query,
            $label,
            (\Request::input('order_by') === $field || (!\Request::has('order_by') && $default)) ? ($order === 'asc') ? '<i class="fa fa-caret-down"></i>' : '<i class="fa fa-caret-up"></i>' : ''
        );
    }
}

if (!function_exists('options_to_html_table')) {
    function options_to_html_table($options)
    {
        $parts = [];

        foreach ($options as $option) {
            $parts[] = sprintf('<strong>%s</strong>: %s', $option['id'], $option['value']);
        }

        return implode(', ', $parts);
    }
}

if (!function_exists('idx_to_letter')) {
    function idx_to_letter($idx)
    {
        $l = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        ];

        if (!isset($l[$idx])) {
            throw new \Exception('Excel col count Z is max');
        }

        return $l[$idx];
    }
}


function user_roles_description($user)
{
    $roles = [];

    if (\Myc\Domain\Users\Role::ROLE_ID_ADMIN === $user->role_id) {
        $roles[] = 'admin';
    } else {
        $roles[] = 'user';
    }

    if ($user->ownsSomeOrganizations()) {
        $roles[] = 'organization owner';
    }
    if ($user->isMemberOfSomeOrganizations()) {
        $roles[] = 'organization member';
    }

    return implode(', ', $roles);
}

function markdown_url_to_text($markdown) {
    return preg_replace('/\[([^\]]+)\]\(([^\)]+)\)/', '$1', $markdown);
}

function markdown_url_to_href($markdown) {
    return preg_replace('/\[([^\]]+)\]\(([^\)]+)\)/', '$2', $markdown);
}
