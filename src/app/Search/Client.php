<?php

namespace Myc\Search;

use Myc\Domain\Users\User;

class Client
{
    const INDEX = 'myc';
    const LOCATION_TYPE = 'location';
    const ORGANIZATION_TYPE = 'organization';

    private $results;

    public function __construct()
    {
        $this->client = new \Elasticsearch\Client([
            'hosts' => [
                env('ELASTICSEARCH_URL', 'http://localhost:9200' ),
            ]
        ]);
    }

    public function createMapping()
    {
        $params['index'] = static::INDEX;

        if ($this->client->indices()->exists($params)) {
            $this->client->indices()->delete($params);
        }

        $params['body']['settings'] = $this->getSettings();

        $params['body']['mappings'][ static::LOCATION_TYPE ]     = $this->getLocationMapping();
        $params['body']['mappings'][ static::ORGANIZATION_TYPE ] = $this->getOrganizationMapping();

        $this->client->indices()->create($params);
    }

    private function getSettings()
    {
        return [
            'number_of_shards' => 5,
            'analysis' => [
                'filter' => [
                    'autocomplete_filter' => [
                        'type' => 'edge_ngram',
                        'min_gram' => 1,
                        'max_gram' => 30,
                    ],
                ],
                'analyzer' => [
                    'autocomplete' => [
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => [
                            'lowercase',
                            'autocomplete_filter',
                        ],
                    ],
                ],
            ],
        ];
    }

    private function getOrganizationMapping()
    {
        $mapping = [
            '_all' => [
                'analyzer' => 'autocomplete',
                'search_analyzer' => 'standard',
            ],
            '_source' => [
                'includes' => ['id', 'name', 'pay_off', 'image'],
                'excludes' => ['website', 'email_adress', 'description'],
            ],
            'properties' => [
                'id' => [
                    'type' => 'integer',
                ],
                'name' => [
                    'type' => 'string',
                    'analyzer' => 'autocomplete',
                    'include_in_all' => true,
                ],
            ],
        ];

        return $mapping;
    }

    private function getLocationMapping()
    {
        $mapping = [
            '_source' => ['enabled' => true],
            '_all' => [
                'analyzer' => 'autocomplete',
                'search_analyzer' => 'standard',
            ],
            'properties' => [
                'id' => [
                    'type' => 'integer',
                    'include_in_all' => false,
                ],
                'name' => [
                    'type' => 'string',
                    'analyzer' => 'autocomplete',
                ],
                'address' => [
                    'type' => 'string',
                    'analyzer' => 'autocomplete',
                ],
                'street_name' => [
                    'type' => 'string',
                    'include_in_all' => false,
                ],
                'street_number' => [
                    'type' => 'string',
                    'include_in_all' => false,
                ],
                'postal_code' => [
                    'type' => 'string',
                    // No autocomplete filter on postal code: match complete postal code only
                ],
                'city' => [
                    'type' => 'string',
                    'analyzer' => 'autocomplete',
                ],
                'country' => [
                    'type' => 'string',
                    'analyzer' => 'autocomplete',
                ],
                'tag' => [
                    'type' => 'integer',
                    'index' => 'not_analyzed',
                    'include_in_all' => false,
                ],
                'location' => [
                    'type' => 'geo_point',
                    'include_in_all' => false,
                ],
            ],
        ];

        return $mapping;
    }

    public function search($params)
    {
        $this->results = $this->client->search($params);

        return $this;
    }

    public function getIds()
    {
        $ids = array_map(function ($result) {
            return (int) $result['_id'];
        }, $this->results['hits']['hits']);

        return $ids;
    }

    public function getResults()
    {
        return $this->results['hits']['hits'];
    }

    public function getHits()
    {
        return (int) $this->results['hits']['total'];
    }

    public function getRawResults()
    {
        return $this->results;
    }

    public function indexDocument(array $params)
    {
        $params['refresh'] = true;
        $response          = $this->client->index($params);

        return $response;
    }

    private function deleteDocument($id, $type)
    {
        $params = [
            'index' => static::INDEX,
            'type' => $type,
            'id' => $id,
            'refresh' => true,
        ];

        return $this->client->delete($params);
    }

    public function deleteLocation($id)
    {
        if ($this->locationExists($id)) {
            return $this->deleteDocument($id, static::LOCATION_TYPE);
        }

        return false;
    }

    public function deleteOrganization($id)
    {
        if ($this->organizationExists($id)) {
            return $this->deleteDocument($id, static::ORGANIZATION_TYPE);
        }

        return false;
    }

    private function documentExists($id, $type)
    {
        return $this->client->exists([
            'id' => $id,
            'index' => static::INDEX,
            'type' => $type,
        ]);
    }

    public function locationExists($id)
    {
        return $this->documentExists($id, static::LOCATION_TYPE);
    }

    public function organizationExists($id)
    {
        return $this->documentExists($id, static::ORGANIZATION_TYPE);
    }

    public function transformResultToLocation(array $source, User $user = null, $favoriteIds = [])
    {
        $hashid = to_hashid($source['id']);

        $resource = [
            'type' => 'locations',
            'id' => $hashid,
            'icon' => (string) $source['icon'],
            'lat' => (float) $source['location']['lat'],
            'lon' => (float) $source['location']['lon'],
            'name' => (string) $source['name'],
            'street_name' => (string) $source['street_name'],
            'street_number' => (string) $source['street_number'],
            'postal_code' => (string) $source['postal_code'],
            'city' => (string) $source['city'],
            'country' => (string) $source['country'],
            'links' => [
                'self' => route('locations.show', ['hashid' => $hashid]),
                'images' => route('locations.get_images', ['hashid' => $hashid]),
            ],
        ];

        if ($user) {
            $resource['favorited'] = in_array($source['id'], $favoriteIds);
        }

        return $resource;
    }

    public function transformResultToOrganization(array $source)
    {
        return [
            'type' => 'organizations',
            'id' => to_hashid($source['id']),
            'name' => (string) $source['name'],
            'pay_off' =>  (string) $source['pay_off'],
            'image' =>  (string) image_asset($source['image']),
        ];

    }
}
