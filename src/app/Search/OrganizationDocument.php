<?php

namespace Myc\Search;

use Myc\Domain\Organizations\Organization;

class OrganizationDocument
{
    public static function createFromOrganization(Organization $organization)
    {
        $doc = [];
        $doc['index'] = Client::INDEX;
        $doc['type'] = Client::ORGANIZATION_TYPE;
        $doc['id'] = intval($organization->id);
        $doc['body'] = [
            'id' => (int) $organization->id,
            'name' => (string) $organization->name,
            'pay_off' => (string) $organization->pay_off,
            'image' => (string) $organization->image,
            'website' => (string) $organization->website,
            'email_address' => (string) $organization->email_address,
            'description' => (string) $organization->description,
        ];

        return $doc;
    }
}
