<?php

namespace Myc\Search;

use Myc\Domain\Locations\Location;

class LocationDocument
{
    /**
     * @param Location $location
     *
     * @return array
     */
    public static function createFromLocation(Location $location)
    {
        $parts = build_array([
            $location->street_name,
            $location->street_number,
        ]);

        $address = (count($parts)) ? implode(' ', $parts) : null;

        $doc = [];
        $doc['index'] = Client::INDEX;
        $doc['type'] = Client::LOCATION_TYPE;
        $doc['id'] = intval($location->id);
        $doc['body'] = [
            'id' => (int) $location->id,
            'icon' => $location->getIconType(),
            'location' => [
                'lat' => (float) $location->lat,
                'lon' => (float) $location->lon,
            ],
            'name' => (string) $location->name,
            'address' => (string) $address,
            'street_name' => (string) $location->street_name,
            'street_number' => (string) $location->street_number,
            'postal_code' => (string) $location->postal_code,
            'city' => (string) is_null($location->locality) ? (string) $location->sub_locality : (string) $location->locality,
            'country' => (string) $location->country,
            'tag' => $location->tags->lists('id'),
        ];

        return $doc;
    }
}
