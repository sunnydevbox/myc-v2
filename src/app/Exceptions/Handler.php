<?php

namespace Myc\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        'Symfony\Component\HttpKernel\Exception\HttpException',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $e
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     *
     * @return Response
     */
    public function render($request, Exception $e)
    {
        if ($this->isHttpException($e)) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'error' => [
                        'http_code' => $e->getStatusCode(),
                        'message' => !is_null($e->getMessage()) ? $e->getMessage() : '',
                    ],
                ], $e->getStatusCode());
            }

            return $this->renderHttpException($e);
        }

        if (config('app.debug')) {
            return $this->renderExceptionWithWhoops($request, $e);
        }

        return parent::render($request, $e);
    }

    /**
     * Render an exception using Whoops.
     *
     * @param Request    $request
     * @param \Exception $e
     *
     * @return Response
     */
    protected function renderExceptionWithWhoops(Request $request, Exception $e)
    {
        $whoops = new \Whoops\Run();
        if ($request->ajax() || $request->wantsJson()) {
            $whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler());
        } else {
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
        }

        return new Response(
            $whoops->handleException($e),
            $e->getStatusCode(),
            $e->getHeaders()
        );
    }
}
