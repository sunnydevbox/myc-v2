<?php

namespace Myc\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'Myc\Console\Commands\CsvProcessCommand',
        'Myc\Console\Commands\GeocodeAddressCommand',
        'Myc\Console\Commands\SearchIndex',
        'Myc\Console\Commands\LocationCleanupDrafts',
        'Myc\Console\Commands\UserNotifyUnconfirmed',
        'Myc\Console\Commands\UserNotify2Unconfirmed',
        'Myc\Console\Commands\OAuth2CreateClientCommand',
        'Myc\Console\Commands\FixLocationEncodingCommand',
        'Myc\Console\Commands\MigrateImageOwnershipCommand',
        'Myc\Console\Commands\UpdateMinAndMaxValuesForNumericProfileFields'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('location:cleanup-drafts')->hourly();
        $schedule->command('user:notify-unconfirmed')->hourly();
        $schedule->command('user:notify2-unconfirmed')->hourly();

        // Every minute
        $schedule->command('profile-fields:update-min-max-values')->withoutOverlapping();
    }
}
