<?php

namespace Myc\Console\Commands;

use Geocoder\Geocoder;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class GeocodeAddressCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'geocode:address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Geocode an address.';

    public function __construct(Geocoder $geocoder)
    {
        $this->geocoder = $geocoder;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $collection = $this->geocoder->geocode($this->argument('address'));
        $this->info(json_encode($collection->first()->toArray()));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['address', InputArgument::REQUIRED, 'The address to geocode.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
