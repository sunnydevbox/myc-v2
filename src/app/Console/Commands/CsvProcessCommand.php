<?php

namespace Myc\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Myc\Commands\BatchProcessCsv;
use Myc\Commands\BatchProcessCsvAlkmaar;
use Symfony\Component\Console\Input\InputArgument;

class CsvProcessCommand extends Command
{
    use DispatchesCommands;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process a CSV file. See docs for required file format';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = $this->argument('file');
        if ($this->argument('format') == 'alkmaar') {
            $command = new BatchProcessCsvAlkmaar($file);
        } else {
            $command = new BatchProcessCsv($file);
        }
        $this->dispatch($command);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['file', InputArgument::REQUIRED, 'Path to CSV file.'],
            ['format', InputArgument::REQUIRED, 'Format'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
