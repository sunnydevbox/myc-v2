<?php namespace Myc\Console\Commands;

use Illuminate\Console\Command;
use Myc\Domain\Images\Image;
use Myc\Domain\Users\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MigrateImageOwnershipCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'migrate:image-ownership';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set all image records with no ownership to specific user ID.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
        $user = User::where('email', $this->argument('email'))->first();
        if (!$user) {
            $this->error(sprintf(
                'No user with email %s found. Aborting.',
                $this->argument('email')
            ));
            return;
        }

        Image::withTrashed()->whereNull('user_id')->chunk(100, function($images) use ($user) {
            foreach ($images as $image) {
                $this->info(sprintf(
                    'Setting image %s with ownership user ID %s',
                    $image->id,
                    $user->id
                ));
                if ($this->option('dryrun') === 'false') {
                    $this->info('Saving record.');
                    // Do not touch timestamps
                    $image->timestamps = false;
                    $image->user_id = $user->id;
                    $image->save();
                }
            }
        });
	}

	protected function getArguments()
	{
		return [
			['email', InputArgument::REQUIRED, 'The user\'s email.']
		];
	}

    protected function getOptions() {
        return [
            ['dryrun', null, InputOption::VALUE_OPTIONAL, 'Perform dryrun', 'true']
        ];
    }

}
