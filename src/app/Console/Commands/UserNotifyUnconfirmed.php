<?php

namespace Myc\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Myc\Domain\Users\User;
use Illuminate\Contracts\Mail\Mailer;

class UserNotifyUnconfirmed extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:notify-unconfirmed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mail unconfirmed users after 7 days.';

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create a new command instance.
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $unconfirmedUsers = User::where('confirmed', '=', 0)
            ->where('created_at', '<', Carbon::now()->subWeek()->toIso8601String())
            ->whereNull('remember_confirm_sent')
            ->get();

        $this->info(sprintf(
            "%s users notified who didn't confirm account",
            $unconfirmedUsers->count()
        ));

        foreach ($unconfirmedUsers as $user) {
            $email = $user->email;
            $this->mailer->send('emails.remember_confirm_account', [
                'email' => $email,
                'user' => $user
            ], function ($m) use ($email) {
                $m->subject('Don\'t forget to confirm your MYC account');
                $m->to($email);
            });
            $user->remember_confirm_sent = Carbon::now();

            $user->save();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
