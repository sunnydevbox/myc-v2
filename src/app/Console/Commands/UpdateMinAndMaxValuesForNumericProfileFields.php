<?php namespace Myc\Console\Commands;

use Illuminate\Console\Command;
use Myc\Domain\Profiles\ProfileFieldRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateMinAndMaxValuesForNumericProfileFields extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'profile-fields:update-min-max-values';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update min and max values for profile fields based on current entries.';
    /**
     * @var ProfileFieldRepository
     */
    private $profileFields;

    /**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ProfileFieldRepository $profileFields)
	{
		parent::__construct();
        $this->profileFields = $profileFields;
    }

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->profileFields->chunkAllNumericFields(10, function($fields)
        {
            foreach ($fields as $field) {

                $entries = \DB::table('profile_field_entries')->where('profile_field_id', $field->id)->get();

                $min = 0;
                $max = 0;
                foreach ($entries as $entry) {
                    if ($entry->value > $max) {
                        $max = $entry->value;
                    }
                    if ($entry->value < $min) {
                        $min = $entry->value;
                    }
                }
                $changed = false;
                if ($max != $field->max_value || $min != $field->min_value) {
                    $changed = true;
                }
                if ($changed) {
                    \DB::table('profile_fields')
                        ->where('id', $field->id)
                        ->update(['max_value' => $max, 'min_value' => $min]);
                    $this->info(sprintf(
                        'Profile field %s (%s) changed: max value %s, min value %s.',
                        $field->id,
                        $field->label,
                        $max,
                        $min
                    ));
                }
            }
        });
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
