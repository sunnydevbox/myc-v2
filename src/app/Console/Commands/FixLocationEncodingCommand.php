<?php namespace Myc\Console\Commands;

use Illuminate\Console\Command;
use Myc\Domain\Organizations\Organization;
use Myc\Search\Client;
use Myc\Search\LocationDocument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixLocationEncodingCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'location:encoding';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fix UTF-8 encoding of imported locations.';
    /**
     * @var Client
     */
    private $client;

	public function __construct(Client $client)
	{
		parent::__construct();
        $this->client = $client;
    }

	public function fire()
	{
		$organization = Organization::find($this->argument('organization_id'));
        $organization->locations()->chunk(100, function($locations) {
            foreach ($locations as $location) {
                $location->name = utf8_encode($location->name);
                $location->save();
                $doc = LocationDocument::createFromLocation($location);
                $this->client->indexDocument($doc);
                $this->info(sprintf('Location %s updated', $location->name));
            }
        });
	}

	protected function getArguments()
	{
		return [
			['organization_id', InputArgument::REQUIRED, 'The organization ID the locations are linked to.'],
		];
	}

	protected function getOptions()
	{
		return [];
	}
}
