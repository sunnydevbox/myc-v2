<?php

namespace Myc\Console\Commands;

use Illuminate\Console\Command;
use LucaDegasperi\OAuth2Server\Storage\FluentClient;
use Symfony\Component\Console\Input\InputArgument;

class OAuth2CreateClientCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'oauth2:create-client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a client for the OAuth2 Server.';
    /**
     * @var FluentClient
     */
    private $clientRepo;

    public function __construct(FluentClient $clientRepo)
    {
        parent::__construct();
        $this->clientRepo = $clientRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $clientName = $this->argument('clientName');
        $clientId = $this->argument('clientId');
        $clientSecret = $this->argument('clientSecret');

        $this->clientRepo->create($clientName, $clientId, $clientSecret);
        $this->info('Client created successfully');
        $this->info('Client Name: '.$clientName);
        $this->info('Client ID: '.$clientId);
        $this->info('Client Secret: '.$clientSecret);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['clientName', InputArgument::REQUIRED, 'The Client\'s name'],
            ['clientId', InputArgument::OPTIONAL, 'Client ID to use. A random one will be generated if none is provided.', str_random()],
            ['clientSecret', InputArgument::OPTIONAL, 'Client Secret to use. A random one will be generated if none is provided.', str_random(32)],
        ];
    }
}
