<?php

namespace Myc\Console\Commands;

use Illuminate\Console\Command;
use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Myc\Search\Client;
use Myc\Search\LocationDocument;
use Myc\Search\OrganizationDocument;

class SearchIndex extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'search:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index all locations and organizations.';

    public function __construct()
    {
        parent::__construct();

        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Creating index and mappings');

        $this->client->createMapping();
        $this->info('done');

        $this->info('Indexing locations');
        Location::chunk(100, function ($locations) {
            foreach ($locations as $location) {
                if ($location->status === Location::STATUS_PUBLISHED) {
                    $this->client->indexDocument(LocationDocument::createFromLocation($location));
                } else {
                    $this->client->deleteLocation($location->id);
                }
            }
        });
        $this->info('done');

        $this->info('Indexing organizations');
        $organizations = Organization::all();
        foreach ($organizations as $organization) {
            if ($organization->status === Organization::STATUS_ACTIVE) {
                $this->client->indexDocument(OrganizationDocument::createFromOrganization($organization));
            } else {
                $this->client->deleteOrganization($organization->id);
            }
        }
        $this->info('done');
    }
}
