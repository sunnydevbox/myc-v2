<?php

namespace Myc\Events;

use Myc\Domain\Comments\Comment;
use Myc\Domain\Locations\Location;
use Illuminate\Queue\SerializesModels;

class CommentOnLocationWasCreated extends Event
{
    use SerializesModels;
    /**
     * @var Comment
     */
    private $comment;
    /**
     * @var Location
     */
    private $location;

    public function __construct(Comment $comment, Location $location)
    {
        $this->comment = $comment;
        $this->location = $location;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }
}
