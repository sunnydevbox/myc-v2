<?php

namespace Myc\Events;

use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Events\Event;

use Illuminate\Queue\SerializesModels;

class LocationWasMappedToAnOrganization extends Event {

	use SerializesModels;

	/**
	 * @var Location
	 */
	private $location;

	/**
	 * @var Organization
	 */
	private $organization;
	/**
	 * @var User
	 */
	private $creator;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct( Location $location, Organization $organization, User $creator )
	{
		$this->location = $location;
		$this->organization = $organization;
		$this->creator = $creator;
	}

	/**
	 * @return User
	 */
	public function getCreator() {
		return $this->creator;
	}

	/**
	 * @return Location
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * @return Organization
	 */
	public function getOrganization() {
		return $this->organization;
	}

}
