<?php

namespace Myc\Events;

use Myc\Domain\Comments\Comment;
use Illuminate\Queue\SerializesModels;

class CommentOnCommentWasCreated extends Event
{
    use SerializesModels;
    /**
     * @var Comment
     */
    private $comment;
    /**
     * @var Comment
     */
    private $parent;

    public function __construct(Comment $comment, Comment $parent)
    {
        $this->comment = $comment;
        $this->parent = $parent;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return Comment
     */
    public function getParent()
    {
        return $this->parent;
    }
}
