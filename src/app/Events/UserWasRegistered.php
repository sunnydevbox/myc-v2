<?php

namespace Myc\Events;

use Myc\Domain\Users\User;
use Illuminate\Queue\SerializesModels;

class UserWasRegistered extends Event
{
    use SerializesModels;

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return \Myc\Domain\Users\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
