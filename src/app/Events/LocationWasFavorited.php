<?php

namespace Myc\Events;

use Myc\Domain\Locations\Location;
use Illuminate\Queue\SerializesModels;
use Myc\Domain\Users\User;

class LocationWasFavorited extends Event
{
    use SerializesModels;

    /**
     * @var Location
     */
    private $location;

    /**
     * @var User
     */
    private $user;

    public function __construct(Location $location, User $user)
    {
        $this->location = $location;
        $this->user = $user;
    }

    /**
     * @return \Myc\Domain\Locations\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
