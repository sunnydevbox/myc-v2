<?php

namespace Myc\Events;

use Myc\Domain\Comments\Comment;
use Myc\Domain\Users\User;
use Illuminate\Queue\SerializesModels;

class CommentWasFavorited extends Event
{
    use SerializesModels;
    /**
     * @var Comment
     */
    private $comment;
    /**
     * @var User
     */
    private $user;

    public function __construct(Comment $comment, User $user)
    {
        $this->comment = $comment;
        $this->user = $user;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
