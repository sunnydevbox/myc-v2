<?php
namespace Myc\Events;

use Illuminate\Queue\SerializesModels;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;

class UserEnrolledToOrganization extends Event {

	use SerializesModels;

	/**
	 * @var Organization
	 */
	private $organization;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var string
	 */
	private $role;

	public function __construct( Organization $organization, User $user, $role ) {
		$this->organization = $organization;
		$this->user = $user;
		$this->role = $role;
	}

	/**
	 * @return Organization
	 */
	public function getOrganization() {
		return $this->organization;
	}

	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @return string
	 */
	public function getRole() {
		return $this->role;
	}

}