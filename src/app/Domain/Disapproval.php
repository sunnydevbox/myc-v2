<?php

namespace Myc\Domain;

use Illuminate\Database\Eloquent\Model;

class Disapproval extends Model
{
    protected $table = 'disapprovals';

    public $timestamps = true;

    public function image()
    {
        return $this->belongsTo('Myc\Domain\Images\Image', 'image_id');
    }

    public function comment()
    {
        return $this->belongsTo('Myc\Domain\Comments\Comment', 'comment_id');
    }

    public function location()
    {
        return $this->belongsTo('Myc\Domain\Locations\Location', 'location_id');
    }

    public function user()
    {
        return $this->belongsTo('Myc\Domain\Users\User', 'user_id');
    }

    public function getTypeAttribute()
    {
        $type = 'Unknown';
        if ($this->location) {
            $type = 'Location';
        } elseif ($this->comment) {
            $type = 'Comment';
        } elseif ($this->image) {
            $type = 'Image';
        }

        return $type;
    }

    public function getAddressAttribute()
    {
        // @Arne, het kan voorkomen dat een disapproval over een comment gaat (!is_null($this->comment)) maar dat
        // deze geen locatie heeft, aangezien het een comment op een comment kan zijn, ik heb een check op de location
        // toegevoegd

        // Not Applicable lijkt me een betere benaming; in sommige gevallen is het adres niet van toepassing
        $address = 'N/A';
        if (!is_null($this->location)) {
            $address = $this->location->getAddressLineAttribute();
        } elseif (!is_null($this->comment) && !is_null($this->comment->location)) {
            $address = $this->comment->location->getAddressLineAttribute();
        } elseif (!is_null($this->image) && !is_null($this->image->location)) {
            $address = $this->image->location->getAddressLineAttribute();
        }

        return $address;
    }

    public function getLinkAttribute()
    {
        $link = false;
        if (!is_null($this->location)) {
            $link = route('admin.locations.edit', to_hashid($this->location->id));
        } elseif (!is_null($this->comment)) {
            $link = route('admin.comments.edit', to_hashid($this->comment->id));
        } elseif (!is_null($this->image) && !is_null($this->image->location)) {
            $link = route('admin.locations.get_images', to_hashid($this->image->location->id));
        }

        return $link;
    }
}
