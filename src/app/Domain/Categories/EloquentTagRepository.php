<?php

namespace Myc\Domain\Categories;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class EloquentTagRepository implements TagRepository
{
    /**
     * @var \Myc\Domain\Categories\Tag
     */
    private $model;

    public function __construct(Tag $model)
    {
        $this->model = $model;
    }

    public function orderBy($field, $order)
    {
        // Default order values
        $field = is_null($field) ? 'category' : $field;
        $order = is_null($order) ? 'asc' : $order;

        if ($field === 'category') {
            $this->model = $this->model->join('categories', 'tags.category_id', '=', 'categories.id')
                ->orderBy('categories.name', $order)
                ->select('tags.*');
        } else {
            $this->model = $this->model->orderBy('tags.'.$field, $order);
        }

        return $this;
    }

    public function filterByString($q)
    {
        if (empty($q)) {
            return $this;
        }
        $this->model = $this->model->where('tags.name', 'LIKE', trim($q).'%');

        return $this;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getAllPaginated($perPage)
    {
        return $this->model->paginate($perPage);
    }

    public function getById($id)
    {
        $tag = $this->model->find($id);
        if (!$tag) {
            throw new ModelNotFoundException();
        }

        return $tag;
    }

    public function getByIds(array $ids)
    {
        if (!count($ids)) {
            throw new \InvalidArgumentException('IDs cannot be empty');
        }

        return $this->model->whereIn('id', $ids)->get();
    }

    public function deleteById($id)
    {
        $tag = $this->model->find($id);
        if ($tag) {
            $tag->delete();
        }
    }
}
