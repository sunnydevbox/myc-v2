<?php

namespace Myc\Domain\Categories;

use League\Fractal\TransformerAbstract;

class TagTransformer extends TransformerAbstract
{
    public function transform(Tag $tag)
    {
        return [
            'id' => (int) $tag->id,
            'name' => (string) $tag->name,
        ];
    }
}
