<?php

namespace Myc\Domain\Categories;

interface TagRepository
{
    /**
     * @param $q
     *
     * @return \Myc\Domain\Categories\TagRepository
     */
    public function filterByString($q);
    public function getAll();
    public function getAllPaginated($perPage);
    public function getById($id);
    public function getByIds(array $ids);
    public function deleteById($id);
}
