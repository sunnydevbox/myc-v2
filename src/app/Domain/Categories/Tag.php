<?php

namespace Myc\Domain\Categories;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    const OPEN_SPACE_TAG_ID = 1;

    protected $fillable = [
        'name',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo('Myc\Domain\Categories\Category');
    }
}
