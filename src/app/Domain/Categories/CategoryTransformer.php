<?php

namespace Myc\Domain\Categories;

use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'tags',
    ];

    public function transform(Category $category)
    {
        return [
            'id' => (int) $category->id,
            'name' => (string) $category->name,
        ];
    }

    public function includeTags(Category $category)
    {
        $tags = $category->tags;

        return $this->collection($tags, new TagTransformer());
    }
}
