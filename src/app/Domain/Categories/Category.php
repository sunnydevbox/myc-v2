<?php

namespace Myc\Domain\Categories;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function tags()
    {
        return $this->hasMany('Myc\Domain\Categories\Tag');
    }
}
