<?php

namespace Myc\Domain\Followers;

use Illuminate\Database\Eloquent\Model;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;

class Follower extends Model
{
    protected $table = 'followers';

    public function organization()
    {
        return $this->belongsTo('Myc\Domain\Organization');
    }

    public function user()
    {
        return $this->belongsTo('Myc\Domain\Users\User');
    }

    public function isOrganizationFollower( User $user, Organization $organization ) {
        return !is_null( $this->where('user_id', $user->id )
            ->where( 'organization_id', $organization->id )
            ->first() );
    }
}
