<?php

namespace Myc\Domain\Followers;

use Carbon\Carbon;
use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use DB;

class FollowerRepository
{
    /**
     * @var Location
     */
    private $organizations;

    /**
     * @var Follower
     */
    private $followers;

    /**
     * @var User
     */
    private $users;

    public function __construct(Organization $organizations, User $users, Follower $followers)
    {
        $this->organizations = $organizations;
        $this->users = $users;
        $this->followers = $followers;
    }

    public function addOrganizationFollower(Organization $organization, User $user) {
        $follower = $this->followers
            ->where('organization_id', $organization->id)
            ->where( 'user_id', $user->id )
            ->first();

        if ( !$follower ) {
            $now = Carbon::now();
            return $this->followers->insert([
                'user_id' => $user->id,
                'organization_id' => $organization->id,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
        return false;
    }

    public function deleteOrganizationFollower( Organization $organization, User $user) {
        return $this->followers
            ->where('organization_id', $organization->id)
            ->where( 'user_id', $user->id )
            ->delete();
    }

    public function getByOrganization( Organization $organization ) {
        return $this->followers
            ->where( 'organization_id', $organization->id )
            ->get();
    }
}
