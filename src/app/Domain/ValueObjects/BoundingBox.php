<?php

namespace Myc\Domain\ValueObjects;

use Illuminate\Contracts\Support\Arrayable;

class BoundingBox implements Arrayable
{
    const EARTH_RADIUS = 6371000.0;

    /**
     * @var LatLon
     */
    private $topLeft;

    /**
     * @var LatLon
     */
    private $topRight;

    /**
     * @var LatLon
     */
    private $bottomRight;

    /**
     * @var LatLon
     */
    private $bottomLeft;

    /**
     * @var LatLon
     */
    private $centerPoint;

    /**
     * @var float
     */
    private $widthInMeters;

    /**
     * @var float
     */
    private $heightInMeters;

    private $ellipsoid = [
        'name' => 'WGS 84',
        'a' => 6378136.0,
        'invF' => 298.257223563,
    ];

    private $precisions = [
        [
            'geohash_length' => 1,
            'width' => 5009400,
            'height' => 4992600,
        ],
        [
            'geohash_length' => 2,
            'width' => 1252300,
            'height' => 624100,
        ],
        [
            'geohash_length' => 3,
            'width' => 156500,
            'height' => 156500,
        ],
        [
            'geohash_length' => 4,
            'width' => 39100,
            'height' => 19500,
        ],
        [
            'geohash_length' => 5,
            'width' => 4900,
            'height' => 4900,
        ],
        [
            'geohash_length' => 6,
            'width' => 1200,
            'height' => 609.4,
        ],
        [
            'geohash_length' => 7,
            'width' => 152.9,
            'height' => 152.9,
        ],
        [
            'geohash_length' => 8,
            'width' => 38.2,
            'height' => 19,
        ],
        [
            'geohash_length' => 9,
            'width' => 4.8,
            'height' => 4.8,
        ],
        [
            'geohash_length' => 10,
            'width' => 1.2,
            'height' => 0.595,
        ],
        [
            'geohash_length' => 11,
            'width' => 0.149,
            'height' => 0.149,
        ],
        [
            'geohash_length' => 12,
            'width' => 0.037,
            'height' => 0.019,
        ],
    ];

    public function __construct(LatLon $topLeft, LatLon $bottomRight)
    {
        $this->topLeft = $topLeft;
        $this->bottomRight = $bottomRight;
    }

    /**
     * @return LatLon
     */
    public function getTopLeft()
    {
        return $this->topLeft;
    }

    /**
     * @return LatLon
     */
    public function getBottomRight()
    {
        return $this->bottomRight;
    }

    /**
     * @return LatLon
     */
    public function getCenterPoint()
    {
        if (is_null($this->centerPoint)) {
            // TODO verify this
            // https://github.com/pierrre/geohash/blob/master/geohash.go
            $lat = ($this->bottomRight->getLat() + $this->topLeft->getLat()) / 2;
            $lat = (min($this->bottomRight->getLat(), $this->topLeft->getLat()) + max($this->bottomRight->getLat(), $this->topLeft->getLat())) / 2;
            //$lon = ($this->bottomRight->getLon() + $this->topLeft->getLon()) / 2;
            $lon = (min($this->bottomRight->getLon(), $this->topLeft->getLon()) + max($this->bottomRight->getLon(), $this->topLeft->getLon())) / 2;
            $this->centerPoint = new LatLon($lat, $lon);
        }

        return $this->centerPoint;
    }

    /*
     * Compute the Haversine distance between the two coordinates.
     * http://en.wikipedia.org/wiki/Haversine_formula
     */
    private function getDistanceInMeters(LatLon $from, LatLon $to)
    {
        $latA = $from->getLat();
        $lonA = $from->getLon();
        $latB = $to->getLat();
        $lonB = $to->getLon();

        $deltaLat = deg2rad($latB - $latA);
        $deltaLon = deg2rad($lonB - $lonA);

        $a = sin($deltaLat / 2) * sin($deltaLat / 2) + cos(deg2rad($latA)) * cos(deg2rad($latB)) * sin($deltaLon / 2) * sin($deltaLon / 2);
        $c = 2 * asin(sqrt($a));

        return static::EARTH_RADIUS * $c;
    }

    public function getWidthInMeters()
    {
        if (is_null($this->widthInMeters)) {
            $center = $this->getCenterPoint();
            $boundsLat = ($this->bottomRight->getLat() + $this->topLeft->getLat()) / 2;
            $boundsLon = $this->topLeft->getLon();
            $centerToBounds = $this->getDistanceInMeters(
                new LatLon($boundsLat, $boundsLon),
                $center
            );
            $this->widthInMeters = $centerToBounds * 2;
        }

        return $this->widthInMeters;
    }

    public function getHeightInMeters()
    {
        if (is_null($this->heightInMeters)) {
            $center = $this->getCenterPoint();
            $boundsLat = $this->bottomRight->getLat();
            $boundsLon = ($this->topLeft->getLon() + $this->bottomRight->getLon()) / 2;
            $centerToBounds = $this->getDistanceInMeters(
                new LatLon($boundsLat, $boundsLon),
                $center
            );
            $this->heightInMeters = $centerToBounds * 2;
        }

        return $this->heightInMeters;
    }

    public function getSquareMeters()
    {
        return $this->getWidthInMeters() * $this->getHeightInMeters();
    }

    public function getPrecision()
    {
        // In all cases the cells have a larger width
        // Use the width of the mapview to determine what precision is appropriate

        $size = ($this->getWidthInMeters() > $this->getHeightInMeters()) ? $this->getWidthInMeters() : $this->getHeightInMeters();
        //$size = $this->getSquareMeters();
        $factor = 3;

        $idx = count($this->precisions) - 1;
        for ($i = $idx; $i >= 0; $i--) {
            if (($this->precisions[$i]['width'] * $factor) < $size) {
                $idx = $i;
            }
        }

        return $this->precisions[$idx]['geohash_length'];
    }

    public static function createFromQueryParam($param)
    {
        $parts = explode(',', $param);
        if (count($parts) < 4) {
            throw new \InvalidArgumentException('Lat and lon for top left and bottom right must be provided');
        }

        return new static(new LatLon($parts[0], $parts[1]), new LatLon($parts[2], $parts[3]));
    }

    public function toArray()
    {
        return [
            'top_left' => $this->topLeft->toArray(),
            'bottom_right' => $this->bottomRight->toArray(),
        ];
    }

    public function __toString()
    {
        return (string) $this->topLeft.' '.(string) $this->bottomRight;
    }

    public function toQueryParam()
    {
        $parts = [
            $this->topLeft->getLat(),
            $this->topLeft->getLon(),
            $this->bottomRight->getLat(),
            $this->bottomRight->getLon(),
        ];

        return implode(',', $parts);
    }
}
