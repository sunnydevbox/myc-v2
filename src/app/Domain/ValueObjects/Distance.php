<?php

namespace Myc\Domain\ValueObjects;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class Distance
{
    const UNIT_METERS = 'm';
    const UNIT_KILOMETERS = 'km';

    private $number;
    private $unit;

    public function __construct($distance)
    {
        if (!preg_match('/^(\d+)(k?m)$/', $distance, $matches)) {
            throw new InvalidArgumentException('Distance must be of format: integer(km|m)');
        }

        if (!filter_var($matches[1], FILTER_VALIDATE_INT)) {
            throw new InvalidArgumentException('Distance must be an integer');
        }

        $this->number = (int) $matches[1];
        $this->unit = strtolower($matches[2]);
    }

    public function __toString()
    {
        return $this->number.$this->unit;
    }

    public function getMeters()
    {
        if ($this->unit === static::UNIT_METERS) {
            return $this->number;
        }
        if ($this->unit === static::UNIT_KILOMETERS) {
            return round($this->number / 1000);
        }
        throw new \Exception('Unknown unit: '.$this->unit);
    }

    public function getKilometers()
    {
        if ($this->unit === static::UNIT_METERS) {
            return $this->number * 1000;
        }
        if ($this->unit === static::UNIT_KILOMETERS) {
            return $this->number;
        }
        throw new \Exception('Unknown unit: '.$this->unit);
    }
}
