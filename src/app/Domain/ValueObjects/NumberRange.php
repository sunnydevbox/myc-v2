<?php 
namespace Myc\Domain\ValueObjects;


use Illuminate\Contracts\Support\Arrayable;

class NumberRange implements Arrayable
{

    private $min;
    private $max;

    public function __construct($min, $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @return mixed
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @return mixed
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            $this->getMin(),
            $this->getMax()
        ];
    }

}