<?php

namespace Myc\Domain\ValueObjects;

use Illuminate\Contracts\Support\Arrayable;

class LatLon implements Arrayable
{
    private $lat;
    private $lon;

    public function __construct($lat, $lon)
    {
        /*if (!preg_match('/^-?([1-8]?[1-9]|[1-9]0)?(\.{1}\d+)$/', $lat)) {
            throw new \InvalidArgumentException('Not a valid latitude: ' . $lat);
        }
        if (!preg_match('/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])?(\.{1}\d+)$/', $lon)) {
            throw new \InvalidArgumentException('Not a valid longtitude: ' . $lon);
        }*/
        $this->setLat($this->normalizeLat($lat));
        $this->setLon($this->normalizeLon($lon));
    }

    public function toArray()
    {
        return [
            'lat' => $this->getLat(),
            'lon' => $this->getLon(),
        ];
    }

    public function normalizeLat($lat)
    {
        $lat = str_replace(',','.', $lat);

        return (double) max(-90, min(90, $lat));
    }

    public function normalizeLon($lon)
    {
        // Elasticsearch returns "Infinity" or "-Infinity" on earth bounds
        if ($lon === 'Infinity') {
            $lon = 180.0;
        }

        if ($lon === '-Infinity') {
            $lon = -180.0;
        }

        if (180 === $lon % 360) {
            return 180.0;
        }

        $lon = str_replace(',','.', $lon);

        if (!is_numeric($lon)) {
            throw new \Exception("$lon is a non well formed numeric value");
        }

        $mod = fmod($lon, 360);
        $lon = $mod < -180 ? $mod + 360 : ($mod > 180 ? $mod - 360 : $mod);

        // TODO figure this out
        if ($lon === -180.0) {
            $lon += 0.00000000001;
        }

        if ($lon === 180.0) {
            $lon -= 0.00000000001;
        }

        return (double) $lon;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function getLon()
    {
        return $this->lon;
    }

    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    public function __toString()
    {
        return "POINT($this->lat,$this->lon)";
    }

    public static function createFromQueryParam($param)
    {
        $parts = explode(',', $param);
        if (count($parts) !== 2) {
            throw new \InvalidArgumentException('Must provide lat and lon');
        }

        return new static($parts[0], $parts[1]);
    }
}
