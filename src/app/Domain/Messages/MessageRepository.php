<?php

namespace Myc\Domain\Messages;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Domain\Users\User;
use DB;

class MessageRepository
{
    private $query;

    public function __construct(Message $model)
    {
        $this->model = $model;
        $this->query = $model->newQuery();
    }

    public function addMessageForUserIds(array $payload, $title, array $userIds, $creatorId = null)
    {
        $now = Carbon::now();
        $rows = array_map(function ($userId) use ($payload, $title, $now, $creatorId) {
            return [
                'user_id' => (int) $userId,
                'creator_id' => (int) $creatorId,
                'title' => $title,
                'payload' => json_encode($payload),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }, $userIds);

        DB::table('messages')->insert($rows);
    }

    public function getById($id)
    {
        $message = $this->query->where('id', '=', $id)->first();
        if (!$message) {
            throw new ModelNotFoundException();
        }

        return $message;
    }

    public function updateReadOn(Message $message)
    {
        $message->read_on = Carbon::now();
        $message->save();

        return $message;
    }

    public function getAllForUser(User $user)
    {
        return $this->model
            ->with('user')
            ->where('user_id', '=', $user->id)
            ->get();
    }
}
