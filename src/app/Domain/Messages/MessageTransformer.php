<?php

namespace Myc\Domain\Messages;

use League\Fractal\TransformerAbstract;

class MessageTransformer extends TransformerAbstract
{
    public function transform(Message $message)
    {
        return [
            'id' => to_hashid($message->id),
            'title' => (string) $message->title,
            'payload' => json_decode($message->payload),
            'read_on' => is_null($message->read_on) ? null : $message->read_on->toIso8601String(),
            'created_at' => $message->created_at->toIso8601String(),
            'user_avatar' => !is_object($message->creator) ? null : (is_null($message->creator->image) ? null : route('images.get_asset', ['path' => $message->creator->image])),
        ];
    }
}
