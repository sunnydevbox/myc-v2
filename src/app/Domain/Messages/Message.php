<?php

namespace Myc\Domain\Messages;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $dates = ['read_on'];

    public function isRead()
    {
        return !is_null($this->read_on);
    }

    public function user()
    {
        return $this->belongsTo('Myc\Domain\Users\User', 'user_id');
    }

    public function creator()
    {
        return $this->belongsTo('Myc\Domain\Users\User', 'creator_id');
    }
}
