<?php

namespace Myc\Domain\Organizations;

use Illuminate\Database\Eloquent\Model;
use Myc\Domain\Locations\Location;

class Organization extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    protected $table = 'organizations';

    protected $fillable = [
        'name',
        'pay_off',
        'website',
        'email_address',
        'description',
        'company_name',
        'first_name',
        'last_name',
        'street_number',
        'street_name',
        'postal_code',
        'locality',
        'country',
        'phone_number',
        'email_address_billing',
        'plan',
        'started_at',
        'building_number',
        'user_number',
        'status',
    ];

    public function locations()
    {
        return $this->belongsToMany('Myc\Domain\Locations\Location', 'location_organization')
            ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('Myc\Domain\Users\User', 'organization_user')
            ->withPivot('role')
            ->withTimestamps();
    }

    public function profile()
    {
        return $this->hasOne('Myc\Domain\Profiles\Profile');
    }

    public function hasUserLimit()
    {
        return !is_null($this->user_number);
    }

    public function hasLocationProfileLimit()
    {
        return !is_null($this->building_number);
    }

    public function userLimitIsReached()
    {
        return $this->hasUserLimit() && ($this->users->count() >= $this->user_number);
    }

    public function locationProfileLimitIsReached()
    {
        return (
            $this->hasLocationProfileLimit() &&
            ($this->locations->count() >= $this->building_number)
        );
    }

    public function hasLocation(Location $location)
    {
        return !is_null($this->locations()->where('id', '=', $location->id)->first());
    }

    public function isActive() {
        return $this->status == self::STATUS_ACTIVE;
    }


}
