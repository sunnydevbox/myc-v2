<?php

namespace Myc\Domain\Organizations;

use Illuminate\Database\Eloquent\Collection;
use Myc\Domain\Locations\Location;
use Myc\Domain\Users\User;

interface OrganizationRepository
{
    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByUser(User $user);
    /**
     * @param string $q
     *
     * @return $this
     */
    public function queryByString($q);

    /**
     * @param string $field
     * @param string $order
     *
     * @return $this
     */
    public function orderBy($field, $order);
    public function getAll();
    public function getAllPaginated($perPage);
    public function getById($id);
    public function deleteById($id);

    /**
     * @param User $user
     *
     * @return Collection
     */
    public function getAllNotAssociatedWithUser(User $user);
    public function addUserToOrganizationWithRole(User $user, Organization $organization, $role);
    public function removeUserFromOrganization(User $user, Organization $organization);
    public function getOwnerCount(Organization $organization);
    public function getUserCount(Organization $organization);

    public function hasLocations(Organization $organization);

    public function filterByLocation( Location $location );
}
