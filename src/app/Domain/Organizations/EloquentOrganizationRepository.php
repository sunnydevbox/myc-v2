<?php

namespace Myc\Domain\Organizations;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Domain\Locations\Location;
use Myc\Domain\Users\User;
use DB;

class EloquentOrganizationRepository implements OrganizationRepository
{
    /**
     * @var \Myc\Domain\Organizations\Organization
     */
    private $model;

    public function __construct(Organization $model)
    {
        $this->model = $model;
    }

    public function orderBy($field, $order)
    {
        // Default order values
        $field = is_null($field) ? 'created_at' : $field;
        $order = is_null($order) ? 'desc' : $order;

        $this->model = $this->model->orderBy('organizations.'.$field, $order);

        return $this;
    }

    public function queryByString($q)
    {
        if (empty($q)) {
            return $this;
        }

        $this->model = $this->model->where('organizations.name', 'LIKE', '%'.$q.'%');

        return $this;
    }

    public function filterByUser(User $user)
    {
        $organizationIds = DB::table('organization_user')
            ->where('user_id', '=', $user->id)
            ->where('role', '=', 'owner')
            ->lists('organization_id');

        if (!count($organizationIds)) {
            $organizationIds = [-1];
        }

        $this->model = $this->model->whereIn('id', $organizationIds);

        return $this;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getAllPaginated($perPage)
    {
        return $this->model->paginate($perPage);
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function deleteById($id)
    {
        $organization = $this->model->find($id);
        if ($organization) {
            $organization->delete();
        }
    }

    /**
     * @param User $user
     *
     * @return Collection
     */
    public function getAllNotAssociatedWithUser(User $user)
    {
        $excludeIds = $user->organizations()->lists('id');
        if (count($excludeIds)) {
            $this->model = $this->model->whereNotIn('id', $excludeIds);
        }

        return $this->model->get();
    }

    public function addUserToOrganizationWithRole(User $user, Organization $organization, $role)
    {
        if (!in_array($role, ['owner', 'member'])) {
            throw new \InvalidArgumentException('User can either be a member or an owner.');
        }

        $record = DB::table('organization_user')
            ->where('user_id', '=', $user->id)
            ->where('organization_id', '=', $organization->id)
            ->first();

        $now = Carbon::now();

        if (!$record) {
            DB::table('organization_user')->insert([
                'user_id' => $user->id,
                'organization_id' => $organization->id,
                'role' => $role,
                'created_at' => $now,
            ]);
        } else {
            DB::table('organization_user')
                ->where('user_id', '=', $user->id)
                ->where('organization_id', '=', $organization->id)
                ->update(['role' => $role, 'updated_at' => $now]);
        }
    }

    public function removeUserFromOrganization(User $user, Organization $organization)
    {
        return DB::table('organization_user')
            ->where('user_id', '=', $user->id)
            ->where('organization_id', '=', $organization->id)
            ->delete();
    }

    public function getOwnerCount(Organization $organization)
    {
        return (int) DB::table('organization_user')
            ->where('organization_id', '=', $organization->id)
            ->where('role', '=', 'owner')
            ->count();
    }

    public function getUserCount(Organization $organization)
    {
        return (int) DB::table('organization_user')
            ->where('organization_id', '=', $organization->id)
            ->count();
    }

    public function hasLocations(Organization $organization)
    {
        $record = DB::table('location_organization')
            ->join('locations', 'location_organization.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->where('location_organization.organization_id', '=', (int) $organization->id)
            ->first();

        return !is_null($record);
    }

    public function filterByLocation( Location $location ) {
        return $this->model
            ->join('location_organization', 'location_organization.organization_id', '=', 'organizations.id')
            ->where('location_organization.location_id', '=', $location->id)
            ->get();
    }
}
