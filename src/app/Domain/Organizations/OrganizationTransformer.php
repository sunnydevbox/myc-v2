<?php

namespace Myc\Domain\Organizations;

use League\Fractal;
use Myc\Domain\Followers\Follower;
use Myc\Domain\Locations\Location;
use Myc\Domain\Profiles\ProfileTransformer;
use Myc\Domain\Users\User;

class OrganizationTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = ['profile'];

    private $location;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Follower
     */
    private $follower;

    public function __construct(Location $location = null, User $user = null, Follower $follower = null )
    {
        $this->location = $location;
        $this->user = $user;

        if ( empty( $this->follower ) ) {
            $this->follower = new Follower();
        }
    }

    public function transform(Organization $organization)
    {
        $resource = [
            'type' => 'organizations',
            'id' => to_hashid($organization->id),
            'name' => $organization->name,
            'pay_off' => empty($organization->pay_off) ? null : $organization->pay_off,
            'website' => empty($organization->website) ? null : $organization->website,
            'email_address' => empty($organization->email_address) ? null : $organization->email_address,
            'description' => empty($organization->description) ? null : $organization->description,
            'image' => empty($organization->image) ? null : route('images.get_asset', ['path' => $organization->image]),
            'header_image' => empty($organization->header_image) ? null : route('images.get_asset', ['path' => $organization->header_image]),
        ];

        if (!is_null($this->user)) {

            $resource['is_follower'] = $this->follower->isOrganizationFollower( $this->user, $organization );
            
            if ( $this->user->isMember($organization) ) {
                $resource['role'] = $this->user->getOrganizationRole( $organization );
            }
        }

        return $resource;
    }

    public function includeProfile(Organization $organization)
    {
        $profileRepository = app()->make('Myc\Domain\Profiles\ProfileRepository');
        $profile = $profileRepository->getProfileForOrganizationAndLocation($organization, $this->location);

        if (!$profile) {
            return;
        }

        return $this->item($profile, new ProfileTransformer());
    }
}
