<?php

namespace Myc\Domain\Comments;

use Illuminate\Validation\Factory;
use Myc\Domain\Locations\Location;
use Myc\Domain\Users\User;
use Myc\Events\CommentOnCommentWasCreated;
use Myc\Events\CommentOnLocationWasCreated;
use Myc\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CommentService
{
    private $comment;

    private $rules = [
        'body' => 'required|min:1|max:600',
    ];
    /**
     * @var Factory
     */
    private $validator;

    public function __construct(Comment $comment, Factory $validator)
    {
        $this->comment = $comment;
        $this->validator = $validator;
    }

    public function createCommentOnLocation(User $user, Location $location, array $input)
    {
        $rules = array_merge($this->rules, [
            'location_id' => 'exists:locations,id',
        ]);

        $input = array_merge($input, [
            'location_id' => $location->id,
        ]);

        $validation = $this->validator->make($input, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $this->comment->unguard();
        $comment = $this->comment->create([
            'user_id' => $user->id,
            'location_id' => $location->id,
            'parent_id' => null,
            'body' => $input['body'],
        ]);

        // Cache comment count
        $location->timestamps = false;
        $location->comment_count = $location->comment_count + 1;
        $location->save();

        event(new CommentOnLocationWasCreated($comment, $location));

        return $comment;
    }

    public function createCommentOnComment(User $user, Comment $comment, array $input)
    {
        $rules = array_merge($this->rules, [
            'comment_id' => 'exists:comments,id',
        ]);

        $input = array_merge($input, [
            'comment_id' => $comment->id,
        ]);

        $validation = $this->validator->make($input, $rules);

        $validation->after(function ($validator) use ($comment) {
            if (!is_null($comment->parent_id)) {
                $validator->errors()->add('comment_id', 'Comments are only allowed for 2 levels.');
            }
        });

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $this->comment->unguard();
        $childComment = $this->comment->create([
            'user_id' => $user->id,
            'location_id' => null,
            'parent_id' => $comment->id,
            'body' => $input['body'],
        ]);

        event(new CommentOnCommentWasCreated($childComment, $comment));

        return $comment;
    }

    public function updateComment(User $user, Comment $comment, array $input)
    {
        if ($user->id != $comment->user_id) {
            throw new AccessDeniedHttpException();
        }

        $validation = $this->validator->make($input, $this->rules);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $comment->body = $input['body'];
        $comment->save();

        return $comment;
    }
}
