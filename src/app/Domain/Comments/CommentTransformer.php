<?php

namespace Myc\Domain\Comments;

use League\Fractal\TransformerAbstract;
use Myc\Domain\Users\User;
use Myc\Domain\Users\UserTransformer;

class CommentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user', 'comments'];
    /**
     * @var \Myc\Domain\Users\User
     */
    private $user;
    /**
     * @var array
     */
    private $favoritedIds;
    /**
     * @var \Myc\Domain\Favorites\FavoriteRepository
     */
    private $favorites;

    public function __construct(User $user = null, array $favoritedIds = [])
    {
        $this->user = $user;
        $this->favoritedIds = $favoritedIds;

        $this->favorites = app()->make('Myc\Domain\Favorites\FavoriteRepository');
    }

    public function transform(Comment $comment)
    {
        $resource =  [
            'type' => 'comments',
            'id' => to_hashid($comment->id),
            'body' => $comment->body,
            'created_at' => $comment->created_at->toIso8601String(),
            'favorite_count' => (int) $comment->favorite_count,
        ];

        if ($this->user) {
            $resource['favorited'] = in_array($comment->id, $this->favoritedIds);
        }

        return $resource;
    }

    public function includeUser(Comment $comment)
    {
        return $this->item($comment->user, new UserTransformer());
    }

    public function includeComments(Comment $comment)
    {
        return $this->collection($comment->comments, new self($this->user, $this->favoritedIds));
    }
}
