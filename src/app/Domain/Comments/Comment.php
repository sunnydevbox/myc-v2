<?php

namespace Myc\Domain\Comments;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    private $favoriteCount;

    public function location()
    {
        return $this->belongsTo('Myc\Domain\Locations\Location');
    }

    public function user()
    {
        return $this->belongsTo('Myc\Domain\Users\User');
    }

    public function parent()
    {
        return $this->belongsTo('Myc\Domain\Comments\Comment', 'parent_id');
    }

    public function comments()
    {
        return $this->hasMany('Myc\Domain\Comments\Comment', 'parent_id');
    }

    public function disapprovals()
    {
        return $this->hasMany('Myc\Domain\Disapproval');
    }
}
