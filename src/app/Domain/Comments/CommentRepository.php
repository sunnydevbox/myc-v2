<?php

namespace Myc\Domain\Comments;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Locations\Location;
use Illuminate\Http\Request;

class CommentRepository
{
    /**
     * @var FavoriteRepository
     */
    private $favorites;

    public function __construct(Comment $comment, FavoriteRepository $favorites)
    {
        $this->model = $comment;
        $this->favorites = $favorites;
    }

    /**
     * @param $id
     *
     * @return \Myc\Domain\Comments\Comment
     */
    public function getById($id)
    {
        $comment = $this->model->find($id);

        if (!$comment) {
            throw new ModelNotFoundException();
        }

        return $comment;
    }

    public function deleteById($id)
    {
        $comment = $this->model->find($id);

        if ($comment) {
            $comment->delete();
        }
    }

    public function getAllForLocation(Location $location, Request $request)
    {
        // Default order values
        $field = is_null($request->input('order_by')) ? 'comments.created_at' : $request->input('order_by');
        $order = is_null($request->input('order')) ? 'desc' : $request->input('order');
        $q = $request->input('q');
        if(!empty($q)) {
            return $location
                ->comments()
                ->join('users', 'comments.user_id', '=', 'users.id')
                ->where('comments.body', 'LIKE', '%'.trim($request->input('q')).'%')
                ->orWhereRaw('CONCAT_WS(" ",first_name,last_name_prefix,last_name) LIKE "%'.trim($request->input('q')).'%"')
                ->orderBy($field, $order)
                ->get();
        } else {
            return $location
                ->comments()
                ->join('users', 'comments.user_id', '=', 'users.id')
                ->orderBy($field, $order)
                ->get();
            }

        // return $location->comments()->get();
    }

    public function filterByString($q)
    {
        if (empty($q)) {
            return $this;
        }
        $this->model = $this->model
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->where('comments.body', 'LIKE', '%'.trim($q).'%')
            ->orWhereRaw('CONCAT_WS(" ",first_name,last_name_prefix,last_name) LIKE "%'.trim($q).'%"');

        return $this;
    }

    public function getAllPaginated($perPage)
    {
        return $this->model->with('location')->whereNotNull('location_id')->paginate($perPage);
    }

    public function orderBy($field, $order)
    {
        // Default order values
        $field = is_null($field) ? 'comments.created_at' : $field;
        $order = is_null($order) ? 'desc' : $order;

        $this->model = $this->model->orderBy($field, $order);

        return $this;
    }
}
