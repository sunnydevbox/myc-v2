<?php

namespace Myc\Domain\Disapprovals;

interface DisapprovalRepository
{
    public function orderBy($field, $order);
    public function getAll();
    public function getAllPaginated($perPage);
    public function getById($id);
    public function getByIds($ids);
    public function queryByString($q);
}
