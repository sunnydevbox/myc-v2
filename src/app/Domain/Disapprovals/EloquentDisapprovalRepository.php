<?php

namespace Myc\Domain\Disapprovals;

use Myc\Domain\Disapproval;

class EloquentDisapprovalRepository implements DisapprovalRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $query;

    public function __construct(Disapproval $disapproval)
    {
        $this->query = $disapproval->newQuery();
        $this->model = $disapproval;
    }

    public function orderBy($field, $order)
    {
        $field = is_null($field) ? 'created_at' : $field;
        $order = is_null($order) ? 'desc' : $order;

        $this->model = $this->model->orderBy('disapprovals.'.$field, $order);

        return $this;
    }

    public function getAll()
    {
        // // Transformer
        $disapprovals = $this->query->get();

        return $disapprovals;
    }

    public function getAllPaginated($perPage)
    {
        $this->orderBy(null, null);
        return $this->model->with(['location', 'comment', 'image'])->paginate($perPage);
    }

    public function getById($id)
    {
        return $this->query->find($id);
    }

    public function getByIds($ids)
    {
        if (is_array($ids)) {
            return $this->query->whereIn('id', $ids)->get();
        }
    }

    public function deleteById($id)
    {
        $disapproval = $this->model->find($id);
        if ($disapproval) {
            $disapproval->delete();
        }
    }

    public function queryByString($q)
    {
        if (empty($q)) {
            return $this;
        }

        // $this->model = $this->model
        //     ->where('name', 'LIKE', '%'.$q.'%')
        //     ->orWhere('street_name', 'LIKE', '%'.$q.'%');

        return $this;
    }
}
