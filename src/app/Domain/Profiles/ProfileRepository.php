<?php

namespace Myc\Domain\Profiles;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Profiles\Fields\AbstractField;
use DB;
use Myc\Domain\Profiles\Fields\FieldType;

class ProfileRepository
{
    /**
     * @var Profile
     */
    private $model;

    private $includeInactiveFields = false;

    public function __construct(Profile $profiles)
    {
        $this->model = $profiles;
    }

    /**
     * @return $this
     */
    public function includeInactiveFields()
    {
        $this->includeInactiveFields = true;

        return $this;
    }

    /**
     * @param Organization $organization
     *
     * @return \Myc\Domain\Profiles\Profile
     */
    public function getByOrganizaton(Organization $organization)
    {
        return $this->model->where('organization_id', '=', $organization->id)->first();
    }

    /**
     * @param Organization $organization
     * @param Location     $location
     *
     * @return \Myc\Domain\Profiles\Profile
     */
    public function getProfileForOrganizationAndLocation(Organization $organization, Location $location = null)
    {
        $profile = $this->getByOrganizaton($organization);

        if (!$profile) {
            return;
        }

        // A location _may_ be set but is not required
        // LEFT JOIN entries only when a location object is required
        $query = DB::table('profile_fields')
            ->where('profile_fields.profile_id', '=', $profile->id)
            ->groupBy('profile_fields.id')
            ->orderBy('profile_fields.sort_order', 'ASC')
            ->orderBy('profile_fields.created_at', 'DESC');

        if (!$this->includeInactiveFields) {
            $query = $query->where('profile_fields.is_active', '=', true);
        }

        $select = ['profile_fields.*'];

        $hasEntries = false;
        if (!is_null($location)) {
            // Left join with profile field entries
            // The entry must match the location and the profile
            $hasEntries = true;
            $query->leftJoin('profile_field_entries', function ($join) use ($location) {
                $join
                    ->on('profile_fields.id', '=', 'profile_field_entries.profile_field_id')
                    ->where('profile_field_entries.location_id', '=', $location->id);
            });
            $select = array_merge($select, [
                'profile_field_entries.id AS entry_id',
                'profile_field_entries.value AS entry',
                'profile_field_entries.updated_at AS entry_updated_at',
            ]);
        }

        $rows = $query->select($select)->get();

        $fields = new Collection();
        foreach ($rows as $row) {
            $field = ProfileFieldFactory::make($row);
            $fields[] = $field;
        }

        $profile->setFields($fields);

        return $profile;
    }

    public function upsertEntry(AbstractField $field, Location $location)
    {
        $table = 'profile_field_entries';
        $record = DB::table($table)
            ->where('profile_field_id', '=', $field->getId())
            ->where('location_id', '=', $location->id)
            ->first();

        if (!$record) {
            // The record does not exist, insert the record
            $field->entryUpdatedAt = Carbon::now();
            DB::table($table)
                ->insert([
                    'profile_field_id' => $field->getId(),
                    'location_id' => $location->id,
                    'value' => $field->entry,
                    'created_at' => $field->entryUpdatedAt,
                    'updated_at' => $field->entryUpdatedAt,
                ]);
            $record = DB::table($table)
                ->where('profile_field_id', $field->getId())
                ->where('location_id', $location->id)
                ->first();
        } else {
            // The record exists, check if entry is dirty
            $dirty = ($record->value != $field->entry);
            if ($dirty) {
                // If yes, update the updated time
                $field->entryUpdatedAt = Carbon::now();
                DB::table($table)
                  ->where('profile_field_id', '=', $field->getId())
                  ->where('location_id', '=', $location->id)
                  ->update([
                      'value' => $field->entry,
                      'updated_at' => $field->entryUpdatedAt,
                  ]);
            }
        }

        return $record;
    }

    public function syncFieldOptions(AbstractField $field, Location $location, array $options)
    {
        $field->setEntry('');
        $record = $this->upsertEntry($field, $location);

        if (!$record) {
            return;
        }

        $entryId = $record->id;

        $optionsInDatabase = \DB::table('profile_field_entry_profile_field_option')
            ->where('profile_field_entry_id', $entryId)
            ->lists('profile_field_option_id');

        foreach ($optionsInDatabase as $optionInDatabase) {
            // The options in the database is not in the posted options,
            // so delete
            if (!in_array($optionInDatabase, $options)) {
                \DB::table('profile_field_entry_profile_field_option')
                    ->where('profile_field_entry_id', $entryId)
                    ->where('profile_field_option_id', $optionInDatabase)
                    ->delete();
            } else {
                $options = array_reduce($options, function($memo, $item) use ($optionInDatabase) {
                    if ($item != $optionInDatabase) {
                        $memo[] = $item;
                    }
                    return $memo;
                }, []);
            }
        }

        // Only insert the options not yet in database
        $options = array_except($options, $optionsInDatabase);

        foreach ($options as $option) {
            if (empty($option)) {
                continue;
            }
            if ($option < 0) {
                continue;
            }

            // If the option ID does not exist, skip
            if (!\DB::table('profile_field_options')->where('id', $option)->first()) {
                continue;
            }

            $createdAt = Carbon::now();
            \DB::table('profile_field_entry_profile_field_option')
                ->insert([
                    'profile_field_entry_id' => $entryId,
                    'profile_field_option_id' => $option,
                    'created_at' => $createdAt,
                    'updated_at' => $createdAt
                ]);
        }
    }

    public function getByProfileField(AbstractField $field)
    {
        $fieldRecord = DB::table('profile_fields')->where('id', '=', $field->id)->first();

        if (!$fieldRecord) {
            return;
        }

        return $this->model->where('id', '=', $fieldRecord->profile_id)->first();
    }

    /**
     * @param $id
     *
     * @return Fields\NumericAbstractField|Fields\TextareaAbstractField|Fields\TextAbstractField|Fields\YearAbstractField|null
     */
    public function getProfileFieldById($id)
    {
        $record = DB::table('profile_fields')->where('id', '=', $id)->first();
        if (!$record) {
            return null;
        }

        $field = ProfileFieldFactory::make($record);

        return $field;
    }

    /**
     * @param AbstractField $field
     *
     * @return bool
     */
    public function profileFieldHasEntries(AbstractField $field)
    {
        $entry = DB::table('profile_field_entries')->where('profile_field_id', '=', $field->id)->first();

        return !is_null($entry);
    }

    public function getLocationsIdsMatchingProfileFieldEntries(array $profileFields)
    {
        $items = [];
        foreach ($profileFields as $key => $value) {
            if (empty($value)) {
                continue;
            }
            // Convert hashid to id
            $items[] = ['id' => from_hashid($key), 'entry' => $value];
        }

        $ids = [];
        foreach ($items as $item) {
            $ids[] = $this->getLocationIdsForProfileFieldEntry($item);
        }

        if (!count($ids)) {
            return [];
        }

        if (1 === count($ids)) {
            return $ids[0];
        }

        // Return IDs that satisfy _all_ conditions
        return array_values(call_user_func_array('array_intersect', $ids));
    }

    private function getLocationIdsForProfileFieldEntry(array $item)
    {
        $profileField = $this->getProfileFieldById($item['id']);

        if (!$profileField || (FieldType::FIELD_TYPE_TEXTAREA === $profileField->getIdentifier())) {
            // Return an empty array of the profile field does not exist or type is textarea (no searching on textarea)
            return [];
        }

        $query = DB::table('profile_field_entries')
            ->where('profile_field_id', '=', $item['id'])
            ->groupBy('profile_field_entries.location_id')
            ->select('profile_field_entries.location_id');

        if (FieldType::FIELD_TYPE_TEXT === $profileField->getIdentifier()) {
            // Perform a poor man's full text on text field
            $query->where('profile_field_entries.value', 'LIKE', '%'.trim($item['entry']).'%');
        } else if (FieldType::FIELD_TYPE_SELECT === $profileField->getIdentifier()) {
            $query->where('profile_field_entries.value', '=', $item['entry']);
        } else if (FieldType::FIELD_TYPE_MULTI_SELECT === $profileField->getIdentifier()) {
            $values = explode(',', $item['entry']);
            if (count($values) > 0) {
                $query->join('profile_field_entry_profile_field_option', 'profile_field_entry_profile_field_option.profile_field_entry_id', '=', 'profile_field_entries.id')
                    ->whereIn('profile_field_entry_profile_field_option.profile_field_option_id', $values);
            }
        } else if (FieldType::FIELD_TYPE_NUMERIC === $profileField->getIdentifier() && (strpos($item['entry'], '[') === 0)) {
            preg_match('/\[\'(.*)\',\'(.*)\'\]/', $item['entry'], $matches);
            if (count($matches) < 3) {
                throw new \InvalidArgumentException(sprintf(
                    'When filtering by number range, range must have min and max value'
                ));
            }
            $min = floatval($matches[1]);
            $max = floatval($matches[2]);
            $query->where(function($q) use ($min, $max) {
                $q->where('profile_field_entries.value', '>=', $min)
                    ->where('profile_field_entries.value', '<=', $max);
            });
        } else {
            // On other fields require an exact match
            $query->where('profile_field_entries.value', '=', trim($item['entry']));
        }
        $locationIds = $query->lists('location_id');

        return array_unique($locationIds);
    }

    //TODO: move to ProfileService
    public function deleteProfileForOrganizationAndLocation(Organization $organization, Location $location)
    {
        $profile = $this->getProfileForOrganizationAndLocation($organization, $location);
        foreach ($profile->getFields() as $field) {
            $query = DB::table('profile_field_entries')
                ->where('profile_field_entries.profile_field_id', '=', $field->id)
                ->where('profile_field_entries.location_id', '=', $location->id)
                ->delete();
        }

        // Detach Location from organization


    }
}
