<?php

namespace Myc\Domain\Profiles;

use Myc\Domain\Profiles\Fields\FieldType;
use Myc\Domain\Profiles\Fields\MultiSelectField;
use Myc\Domain\Profiles\Fields\NumericField;
use Myc\Domain\Profiles\Fields\SelectField;
use Myc\Domain\Profiles\Fields\TextareaField;
use Myc\Domain\Profiles\Fields\TextField;
use Myc\Domain\Profiles\Fields\UrlField;
use Myc\Domain\Profiles\Fields\YearField;

class ProfileFieldFactory
{
    public static function make(\stdClass $row)
    {
        if (FieldType::FIELD_TYPE_SELECT === $row->field_type) {
            return new SelectField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null,
                json_decode($row->options)
            );
        }

        if (FieldType::FIELD_TYPE_MULTI_SELECT === $row->field_type) {
            return new MultiSelectField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null,
                isset($row->entry_id) ? $row->entry_id : null
            );
        }

        if (FieldType::FIELD_TYPE_NUMERIC === $row->field_type) {
            return new NumericField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null,
                [$row->min_value, $row->max_value]
            );
        }

        if (FieldType::FIELD_TYPE_TEXT === $row->field_type) {
            return new TextField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null
            );
        }

        if (FieldType::FIELD_TYPE_TEXTAREA === $row->field_type) {
            return new TextareaField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null
            );
        }

        if (FieldType::FIELD_TYPE_YEAR === $row->field_type) {
            return new YearField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null
            );
        }

        if (FieldType::FIELD_TYPE_URL === $row->field_type) {
            return new UrlField(
                $row->id,
                $row->label,
                $row->is_required,
                $row->is_active,
                isset($row->entry) ? $row->entry : null,
                isset($row->entry_updated_at) ? $row->entry_updated_at : null
            );
        }

        throw new \InvalidArgumentException('Unknown field type: '.$row->field_type);
    }
}
