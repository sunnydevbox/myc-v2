<?php

namespace Myc\Domain\Profiles;

use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract
{
    public function transform(Profile $profile)
    {
        $resource = [];
        foreach ($profile->getFields() as $field) {
            $resource[] = $field->toArray();
        }

        return $resource;
    }
}
