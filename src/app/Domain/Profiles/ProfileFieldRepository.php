<?php 
namespace Myc\Domain\Profiles;

use Myc\Domain\Profiles\Fields\FieldType;
use DB;

class ProfileFieldRepository {

    private $table = 'profile_fields';

    public function chunkAllNumericFields($chunkSize, \Closure $callable)
    {
        return DB::table($this->table)->where('field_type', FieldType::FIELD_TYPE_NUMERIC)->chunk($chunkSize, $callable);
    }

}