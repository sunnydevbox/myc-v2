<?php 
namespace Myc\Domain\Profiles\Fields;

use Carbon\Carbon;

class MultiSelectField extends AbstractField
{
    protected $identifier = FieldType::FIELD_TYPE_MULTI_SELECT;
    protected $view = 'profile_fields._multi_select';
    protected $viewForSearch = 'profile_fields._multi_select_search';

    public function __construct($id, $label, $required, $active, $entry, $entryUpdatedAt, $entryId)
    {
        if (false === filter_var($id, FILTER_VALIDATE_INT)) {
            throw new \InvalidArgumentException('Profile field ID must be an integer');
        }

        $this->id = (int) $id;
        $this->label = $label;
        $this->required = (bool) $required;
        $this->active = (bool) $active;
        $this->entry = $entry;
        $this->entryUpdatedAt = is_null($entryUpdatedAt) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $entryUpdatedAt);

        $this->entryId = $entryId;
    }

    public function getEntries()
    {
        return \DB::table('profile_field_entry_profile_field_option')
            ->where('profile_field_entry_id', $this->entryId)
            ->lists('profile_field_option_id');
    }

    public function getOptions()
    {
        $records = \DB::table('profile_field_options')->where('profile_field_id', '=', $this->id)
            ->get();

        return array_map(function($record) {
            return [
                'id' => $record->id,
                'value' => $record->value
            ];
        }, $records);
    }

    public function getEntryString()
    {
        $entries = $this->getEntries();
        if (!$entries) {
            return '';
        }

        return implode(', ', \DB::table('profile_field_options')->whereIn('id', $entries)->lists('value'));
    }

    public function toArray()
    {
        $resource = [
            'id' => to_hashid($this->id),
            'field_type' => $this->identifier,
            'label' => (string) $this->label,
            'options' => $this->getOptions(),
        ];

        if (!is_null($this->entry)) {
            $resource['entry'] = $this->getEntries();
        }

        if (!is_null($this->entryUpdatedAt)) {
            $resource['entry_updated_at'] = $this->entryUpdatedAt->toIso8601String();
        }

        return $resource;
    }


}