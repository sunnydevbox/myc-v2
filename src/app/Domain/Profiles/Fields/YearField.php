<?php

namespace Myc\Domain\Profiles\Fields;

class YearField extends AbstractField
{
    protected $identifier = FieldType::FIELD_TYPE_YEAR;
    protected $view = 'profile_fields._year';
    protected $viewForSearch = 'profile_fields._year_search';

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];
            if ($this->required) {
                $rules[] = 'required';
            }
            // Must be of format Y see http://php.net/manual/en/function.date-parse-from-format.php
            $rules[] = 'regex:/^[1-9]\d{3}$/';
            $this->rules[$this->id] = implode('|', $rules);
        }

        return $this->rules;
    }
}
