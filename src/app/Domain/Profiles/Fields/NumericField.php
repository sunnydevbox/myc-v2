<?php

namespace Myc\Domain\Profiles\Fields;

use Carbon\Carbon;
use Myc\Domain\ValueObjects\NumberRange;

class NumericField extends AbstractField
{
    protected $identifier = FieldType::FIELD_TYPE_NUMERIC;
    protected $view = 'profile_fields._numeric';
    protected $viewForSearch = 'profile_fields._numeric_search';

    /**
     * @var NumberRange
     */
    private $minMaxValues;

    public function __construct($id, $label, $required, $active, $entry, $entryUpdatedAt, array $minMaxValues)
    {
        if (false === filter_var($id, FILTER_VALIDATE_INT)) {
            throw new \InvalidArgumentException('Profile field ID must be an integer');
        }

        $this->id = (int) $id;
        $this->label = $label;
        $this->active = (bool) $active;
        $this->required = (bool) $required;
        $this->entry = $entry;
        $this->entryUpdatedAt = is_null($entryUpdatedAt) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $entryUpdatedAt);

        if (is_null($minMaxValues[0]) || is_null($minMaxValues[1])) {
            $this->minMaxValues = null;
        } else {
            $this->minMaxValues = new NumberRange(
                $minMaxValues[0],
                $minMaxValues[1]
            );
        }
    }

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];

            if ($this->required) {
                $rules[] = 'required';
            }

            // Must be a numeric value
            $rules[] = 'integer';

            $this->rules[$this->id] = implode('|', $rules);
        }

        return $this->rules;
    }

    public function getMessages()
    {
        $messages = parent::getMessages();
        $messages[$this->id.'.integer'] = 'The field "'.$this->label.'" must be a whole number.';
        return $messages;
    }

    public function hasNumberRange()
    {
        return !is_null($this->minMaxValues);
    }

    public function getMaxValue()
    {
        return $this->minMaxValues->getMax();
    }

    public function getMinValue()
    {
        return $this->minMaxValues->getMin();
    }
}
