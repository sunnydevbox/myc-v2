<?php

namespace Myc\Domain\Profiles\Fields;

class TextField extends AbstractField
{
    protected $identifier = FieldType::FIELD_TYPE_TEXT;
    protected $view = 'profile_fields._text';
    protected $viewForSearch = 'profile_fields._text_search';

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];
            if ($this->required) {
                $rules[] = 'required';
            }
            // Must be a string of length max 255
            $rules[] = 'max:255';
            $this->rules[$this->id] = implode('|', $rules);
        }

        return $this->rules;
    }
}
