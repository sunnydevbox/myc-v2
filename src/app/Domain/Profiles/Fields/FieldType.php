<?php

namespace Myc\Domain\Profiles\Fields;

class FieldType
{
    const FIELD_TYPE_SELECT = 'select';
    const FIELD_TYPE_MULTI_SELECT = 'multi_select';
    const FIELD_TYPE_NUMERIC = 'numeric';
    const FIELD_TYPE_TEXT = 'text';
    const FIELD_TYPE_TEXTAREA = 'textarea';
    const FIELD_TYPE_YEAR = 'year';
    const FIELD_TYPE_URL = 'url';

    public static function getTypes()
    {
        $class = new \ReflectionClass(__CLASS__);

        return array_values($class->getConstants());
    }
}
