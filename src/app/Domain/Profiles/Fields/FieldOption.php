<?php 
namespace Myc\Domain\Profiles\Fields;


use Illuminate\Database\Eloquent\Model;

class FieldOption extends Model {

    protected $table = 'profile_field_options';

    public function profileField()
    {
        return $this->belongsTo('Myc\Domain\Profiles\MultiSelectField');
    }
}