<?php

namespace Myc\Domain\Profiles\Fields;

class TextareaField extends AbstractField
{
    protected $identifier = FieldType::FIELD_TYPE_TEXTAREA;
    protected $view = 'profile_fields._textarea';

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];

            if ($this->required) {
                $rules[] = 'required';
            }

            $this->rules[$this->id] = implode('|', $rules);
        }

        return $this->rules;
    }
}
