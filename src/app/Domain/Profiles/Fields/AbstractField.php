<?php

namespace Myc\Domain\Profiles\Fields;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Myc\Exceptions\ValidationException;

abstract class AbstractField extends Model implements Arrayable
{
    protected $table = 'profile_fields';
    protected $primaryKey = 'id';

    protected $view;
    protected $viewForSearch;
    protected $identifier;

    /**
     * @var \Illuminate\Validation\Factory;
     */
    protected $validator;

    protected $rules;
    protected $required;
    protected $active;

    public $id;
    public $label;
    public $entry;

    /**
     * @var \Carbon\Carbon
     */
    public $entryUpdatedAt;

    public function __construct($id, $label, $required, $active, $entry, $entryUpdatedAt)
    {
        if (false === filter_var($id, FILTER_VALIDATE_INT)) {
            throw new \InvalidArgumentException('Profile field ID must be an integer');
        }

        $this->id = (int) $id;
        $this->label = $label;
        $this->active = (bool) $active;
        $this->required = (bool) $required;
        $this->entry = $entry;
        $this->entryUpdatedAt = is_null($entryUpdatedAt) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $entryUpdatedAt);
    }

    /*
    public function entries()
    {
        return $this->hasMany('Myc\Domain\Profiles\ProfileFieldEntry');
    }

    public function fieldOptions()
    {
        return $this->hasMany('Myc\Domain\Profiles\Fields\FieldOption');
    }
    */

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * @param mixed $entry
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    }

    public function getMessages()
    {
        $messages = [
            $this->id.'.required' => 'The field "'.$this->label.'" is required.',
            $this->id.'.numeric' => 'The field "'.$this->label.'" must have a numeric value.',
            $this->id.'.regex' => 'The field "'.$this->label.'" is not valid.',
            $this->id.'.in' => 'The field "'.$this->label.'" does not have a valid option selected.',
        ];

        return $messages;
    }

    public function getInput()
    {
        return [$this->id => $this->entry];
    }

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];

            if ($this->required) {
                $rules[] = 'required';
            }
        }

        return $this->rules;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    public function getViewForSearch()
    {
        return $this->viewForSearch;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function validate()
    {
        $this->validator = app()->make('Illuminate\Contracts\Validation\Factory');
        $validation = $this->validator->make($this->getInput(), $this->getRules(), $this->getMessages());

        // TODO format MessageBag

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }
    }

    public function toArray()
    {
        $resource = [
            'id' => to_hashid($this->id),
            'field_type' => $this->identifier,
            'label' => (string) $this->label,
        ];

        if (!is_null($this->entry)) {
            $resource['entry'] = $this->entry;
        }

        if (!is_null($this->entryUpdatedAt)) {
            $resource['entry_updated_at'] = $this->entryUpdatedAt->toIso8601String();
        }

        return $resource;
    }
}
