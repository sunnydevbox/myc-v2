<?php

namespace Myc\Domain\Profiles\Fields;

use Carbon\Carbon;

class SelectField extends AbstractField
{
    protected $identifier = FieldType::FIELD_TYPE_SELECT;
    protected $view = 'profile_fields._select';
    protected $viewForSearch = 'profile_fields._select_search';

    public $options;

    public function __construct($id, $label, $required, $active, $entry, $entryUpdatedAt, array $options)
    {
        if (false === filter_var($id, FILTER_VALIDATE_INT)) {
            throw new \InvalidArgumentException('Profile field ID must be an integer');
        }

        $this->id = (int) $id;
        $this->label = $label;
        $this->required = (bool) $required;
        $this->active = (bool) $active;
        $this->options = $options;
        $this->entry = $entry;
        $this->entryUpdatedAt = is_null($entryUpdatedAt) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $entryUpdatedAt);
    }

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];

            if ($this->required) {
                $rules[] = 'required';
            }

            // Entry must be one of the available options
            $rules[] = 'in:'.implode(',', $this->options);

            $this->rules[ $this->id ] = implode('|', $rules);
        }

        return $this->rules;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function toArray()
    {
        $resource = [
            'id' => to_hashid($this->id),
            'field_type' => $this->identifier,
            'label' => (string) $this->label,
            'options' => $this->options,
        ];

        if (!is_null($this->entry)) {
            $resource['entry'] = $this->entry;
        }

        if (!is_null($this->entryUpdatedAt)) {
            $resource['entry_updated_at'] = $this->entryUpdatedAt->toIso8601String();
        }

        return $resource;
    }
}
