<?php 
namespace Myc\Domain\Profiles\Fields;


use Myc\Exceptions\ValidationException;

class UrlField extends AbstractField {

    protected $identifier = FieldType::FIELD_TYPE_URL;
    protected $view = 'profile_fields._url';

    protected function getRules()
    {
        if (is_null($this->rules)) {
            $rules = [];
            if ($this->required) {
                $rules[] = 'required';
            }
            // Must be a string of length max 255
            $rules[] = 'max:255';
            $this->rules[$this->id] = implode('|', $rules);
        }

        return $this->rules;
    }

    public function validate()
    {
        $this->validator = app()->make('Illuminate\Contracts\Validation\Factory');
        $validation = $this->validator->make($this->getInput(), $this->getRules(), $this->getMessages());

        $validation->after(function($validation) {
            $url = $this->getLinkUrl();
            if ($url !== '' && filter_var($url, FILTER_VALIDATE_URL) === false) {
                $validation->errors()->add($this->id, 'This is not a valid URL.');
            }
        });

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }
    }

    public function getLinkUrl()
    {
        return markdown_url_to_href($this->entry);
    }

    public function getLinkText()
    {
        return markdown_url_to_text($this->entry);
    }

    public function getHtml()
    {
        return sprintf(
            '<a href="%s" target="_blank">%s</a>',
            $this->getLinkUrl(),
            $this->getLinkText()
        );
    }

    public function toArray()
    {
        // Replace 'entry' index with HTML code
        $resource = parent::toArray();

        // 15/2/2016 set empty fields when no entry
        if (!isset($resource['entry'])) {
            $resource['entry'] = '';
        }
        if (!isset($resource['entry_updated_at'])) {
            $resource['entry_updated_at'] = null;
        }

        if (isset($resource['entry'])) {
            $resource['link_href'] = ($resource['entry'] === '') ? '' : $this->getLinkUrl();
            $resource['link_text'] = ($resource['entry'] === '') ? '' : $this->getLinkText();
        }

        return $resource;
    }

}