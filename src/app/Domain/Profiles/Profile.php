<?php

namespace Myc\Domain\Profiles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Myc\Domain\Profiles\Fields\AbstractField;
use Myc\Domain\Profiles\Fields\FieldType;

class Profile extends Model
{
    protected $table = 'profiles';

    private $profileFieldsTable = 'profile_fields';
    private $profileFieldEntriesTable = 'profile_field_entries';

    // Wrong approach
    // Full text search can not be merged with name/address search at this point
    private $fullTextSearchFields = [
        //FieldType::FIELD_TYPE_TEXTAREA,
        //FieldType::FIELD_TYPE_TEXT
    ];

    private $excludeFromSearchControls = [
        FieldType::FIELD_TYPE_TEXTAREA,
        FieldType::FIELD_TYPE_URL
    ];

    protected $fillable = ['organization_id'];

    /**
     * @var \Illuminate\Support\Collection
     */
    private $fields;

    public function organization()
    {
        return $this->belongsTo('Myc\Domain\Organizations\Organization');
    }

    /**
     * @param \Illuminate\Support\Collection $fields
     */
    public function setFields(Collection $fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return AbstractField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFieldsForSearchControls()
    {
        $forSearch = new Collection();
        foreach ($this->fields as $field) {
            if (!in_array($field->getIdentifier(), $this->excludeFromSearchControls)) {
                $forSearch[] = $field;
            }
        }

        return $forSearch;
    }

    public function hasFullTextSearch()
    {
        $fullText = false;
        foreach ($this->fields as $field) {
            if (in_array($field->getIdentifier(), $this->fullTextSearchFields)) {
                $fullText = true;
                break;
            }
        }

        return $fullText;
    }

    /**
     * Does the field have any values?
     *
     * @return bool
     */
    public function isActive()
    {
        $isActive = false;

        foreach ($this->fields as $field) {
            if (FieldType::FIELD_TYPE_MULTI_SELECT === $field->getIdentifier()) {
                $entry = $field->getEntries();
            } else {
                $entry = $field->entry;
            }
            $oldValue = old((string) $field->id, $entry);

            if (is_array($entry)) {
                $entry = implode(', ', $entry);
            }
            if (is_array($oldValue)) {
                $oldValue = implode(', ', $oldValue);
            }

            if(!empty($entry) || !empty($oldValue)) {
                $isActive = true;
                break;
            }
        }

        return $isActive;
    }
}
