<?php 
namespace Myc\Domain\Profiles;


use Illuminate\Database\Eloquent\Model;

class ProfileFieldEntry extends Model {

    protected $table = 'profile_field_entries';

    public function location() {
        return $this->belongsTo('Myc\Domain\Locations\Location');
    }

    public function profileField()
    {
        return $this->belongsTo('Myc\Domain\Profiles\Fields\FieldAbstract');
    }

}