<?php

namespace Myc\Domain\Profiles;

use Carbon\Carbon;
use Illuminate\Validation\Factory;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Locations\Location;
use Myc\Domain\Profiles\Fields\AbstractField;
use Myc\Domain\Profiles\Fields\FieldType;
use DB;
use Myc\Exceptions\ValidationException;

class ProfileService
{
    /**
     * @var Factory
     */
    private $validator;

    public function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }

    public function fieldCreationRules()
    {
        return [
            'label' => 'required',
            'field_type' => 'required|in:'.implode(',', FieldType::getTypes()),
            'is_required' => 'boolean',
        ];
    }

    public function addFieldForOrganization(Organization $organization, $input)
    {
        $profile = Profile::firstOrCreate(['organization_id' => $organization->id]);

        $validation = $this->validator->make($input, $this->fieldCreationRules());

        if (FieldType::FIELD_TYPE_SELECT == $input['field_type'] ||
        FieldType::FIELD_TYPE_MULTI_SELECT == $input['field_type']) {
            // Strip out empty values
            if (isset($input['options']) && is_array($input['options'])) {
                $input['options'] = array_reduce($input['options'], function ($memo, $item) {
                    if (!empty($item)) {
                        $memo[] = $item;
                    }

                    return $memo;
                }, []);
            }

            $validation->after(function ($validator) use ($input) {
                if (!isset($input['options']) ||
                    empty($input['options']) ||
                    !is_array($input['options']) ||
                    count($input['options']) < 2) {
                    $validator->errors()->add('field_type', 'The field must have at least 2 options.');
                }
            });
        }

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $now = Carbon::now();
        $values = [
            'profile_id' => $profile->id,
            'field_type' => $input['field_type'],
            'label' => trim($input['label']),
            'options' => null,
            'sort_order' => 0,
            'is_required' => isset($input['is_required']) ? true : false,
            'is_active' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ];

        if (FieldType::FIELD_TYPE_SELECT == $input['field_type']) {
            $values['options'] = isset($input['options']) ? json_encode($input['options']) : null;
        }

        $profileFieldId = DB::table('profile_fields')->insertGetId($values);

        if (FieldType::FIELD_TYPE_MULTI_SELECT == $input['field_type']) {
            $options = isset($input['options']) ? $input['options'] : [];
            foreach ($options as $option) {
                $record = DB::table('profile_field_options')
                    ->where('profile_field_id', $profileFieldId)
                    ->where('value', $option)
                    ->first();

                if (!$record) {
                    $createdAt = Carbon::now();
                    DB::table('profile_field_options')
                        ->insert([
                            'profile_field_id' => $profileFieldId,
                            'value' => $option,
                            'created_at' => $createdAt,
                            'updated_at' => $createdAt
                        ]);
                }
            }
        }
    }

    public function updateFieldForOrganization(AbstractField $field, $input)
    {
        $validation = $this->validator->make($input, $this->fieldCreationRules());

        if (FieldType::FIELD_TYPE_SELECT == $input['field_type'] || FieldType::FIELD_TYPE_MULTI_SELECT == $input['field_type']) {
            // Strip out empty values
            if (isset($input['options']) && is_array($input['options'])) {
                $input['options'] = array_reduce($input['options'], function($memo, $item)
                {
                    if (!empty($item)) {
                        $memo[] = $item;
                    }
                    return $memo;
                }, []);
            }

            $validation->after(function($validator) use ($input)
            {
                if (!isset($input['options']) ||
                    empty($input['options']) ||
                    !is_array($input['options']) ||
                    count($input['options']) < 2) {
                    $validator->errors()->add('field_type', 'Dropdown field must have at least 2 options.');
                }
            });
        }

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        if (FieldType::FIELD_TYPE_SELECT == $input['field_type']) {
            $options = isset($input['options']) ? json_encode($input['options']) : null;
        } else {
            $options = null;
        }

        $now = Carbon::now();
        DB::table('profile_fields')->where('id', '=', $field->id)->update([
            'label' => trim($input['label']),
            'options' => $options,
            'is_required' => isset($input['is_required']) && $input['is_required'] == 1 ? true : false,
            'updated_at' => $now
        ]);

        if (FieldType::FIELD_TYPE_MULTI_SELECT == $input['field_type']) {
            $options = isset($input['options']) ? $input['options'] : [];

            $q = DB::table('profile_field_options')
                ->where('profile_field_id', $field->id);
            /*if (count($options)) {
                $q->whereIn('value', $options);
            }*/
            $values = $q->lists('value');
            $toDelete = array_diff($values, $options);

            \DB::table('profile_field_options')
                ->where('profile_field_id', $field->id)
                ->whereIn('value', $toDelete)
                ->delete();

            $toInsert = array_diff($options, $values);

            foreach ($toInsert as $option) {
                $record = DB::table('profile_field_options')
                    ->where('profile_field_id', $field->id)
                    ->where('value', $option)
                    ->first();

                if (!$record) {
                    $createdAt = Carbon::now();
                    DB::table('profile_field_options')
                        ->insert([
                            'profile_field_id' => $field->id,
                            'value' => $option,
                            'created_at' => $createdAt,
                            'updated_at' => $createdAt
                        ]);
                }
            }
        }

    }

    // TODO refactor to single PUT operation (no RPC but REST!)
    public function deleteProfileField(AbstractField $field)
    {
        DB::table('profile_fields')->where('id', '=', $field->id)->delete();
    }

    public function hideProfileField(AbstractField $field)
    {
        DB::table('profile_fields')->where('id', '=', $field->id)->update(['is_active' => false]);
    }

    public function showProfileField(AbstractField $field)
    {
        DB::table('profile_fields')->where('id', '=', $field->id)->update(['is_active' => true]);
    }

    public function updateProfileFieldOrder(array $input)
    {
        foreach ($input as $fieldRecord) {
            DB::table('profile_fields')->where('id', '=', $fieldRecord['id'])->update(['sort_order' => $fieldRecord['sort_order']]);
        }
    }
}
