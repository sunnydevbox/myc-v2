<?php

namespace Myc\Domain\Favorites;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Redis\Database;
use Myc\Domain\Comments\Comment;
use Myc\Domain\Locations\Location;
use Myc\Domain\Users\User;
use DB;
use Myc\Events\CommentWasFavorited;
use Myc\Events\LocationWasFavorited;

class FavoriteRepository
{
    /**
     * @var Database
     */
    private $redis;
    /**
     * @var Location
     */
    private $locations;

    public function __construct(Database $redis, Location $locations, User $users, Favorite $favorites)
    {
        // Store favorite IDs in Redis key/value store
        $this->redis = $redis;
        $this->locations = $locations;
        $this->users = $users;
        $this->favorites = $favorites;
    }

    public function getFavoritesCommentsKeyForUser(User $user)
    {
        return sha1(implode('_', ['favorites', 'comments', $user->id]));
    }

    public function getFavoriteCountKeyForComment(Comment $comment)
    {
        return sha1(implode('_', ['comment_favorite_count', $comment->id]));
    }

    public function getFavoritesLocationsKeyForUser(User $user)
    {
        return sha1(implode('_', ['favorites', 'locations', $user->id]));
    }

    public function getFavoriteCountKeyForLocation(Location $location)
    {
        return sha1(implode('_', ['location_favorite_count', $location->id]));
    }

    public function getLocationFavoriteIdsByUser(User $user)
    {
        return array_map('intval', $this->redis->command('SMEMBERS', [$this->getFavoritesLocationsKeyForUser($user)]));
    }

    public function getFavoriteCountForLocation(Location $location)
    {
        $count = $this->redis->command('GET', [$this->getFavoriteCountKeyForLocation($location)]);
        if (is_null($count)) {
            $count = 0;
        }

        return $count;
    }

    public function getCommentFavoriteIdsByUser(User $user)
    {
        return array_map('intval', $this->redis->command('SMEMBERS', [$this->getFavoritesCommentsKeyForUser($user)]));
    }

    public function getFavoriteCountForComment(Comment $comment)
    {
        $count = $this->redis->command('GET', [$this->getFavoriteCountKeyForComment($comment)]);
        if (is_null($count)) {
            $count = 0;
        }

        return $count;
    }

    public function addLocationFavoriteForUser(Location $location, User $user)
    {
        $result = $user->favorites()->sync([$location->id], false);
        $changed = count($result['attached']);

        if ($changed) {
            $this->redis->command('SADD', [$this->getFavoritesLocationsKeyForUser($user), $location->id]);

            $location->favorite_count = $location->favorite_count + 1;
            $location->timestamps = false; // Do not update timestamps when updating favorite count
            $location->save();
            $this->redis->command('INCR', [$this->getFavoriteCountKeyForLocation($location)]);

            event(new LocationWasFavorited($location, $user));
        }

        return $changed;
    }

    public function addCommentFavoriteForUser(Comment $comment, User $user)
    {
        $result = $user->commentFavorites()->sync([$comment->id], false);
        $changed = count($result['attached']);

        if ($changed) {
            $this->redis->command('SADD', [$this->getFavoritesCommentsKeyForUser($user), $comment->id]);

            $comment->favorite_count = $comment->favorite_count + 1;
            $comment->timestamps = false;
            $comment->save();
            $this->redis->command('INCR', [$this->getFavoriteCountKeyForComment($comment)]);

            event(new CommentWasFavorited($comment, $user));
        }

        return $changed;
    }

    public function deleteLocationFavoriteForUser(Location $location, User $user)
    {
        $changed = $user->favorites()->detach($location->id);

        if ($changed) {
            $this->redis->command('SREM', [$this->getFavoritesLocationsKeyForUser($user), $location->id]);

            $location->favorite_count = ($location->favorite_count) ? $location->favorite_count - 1 : 0;
            $location->timestamps = false;
            $location->save();
            $this->redis->command('DECR', [$this->getFavoriteCountKeyForLocation($location)]);
        }

        return $changed;
    }

    public function deleteCommentFavoriteForUser(Comment $comment, User $user)
    {
        $changed = $user->commentFavorites()->detach($comment->id);

        if ($changed) {
            $this->redis->command('SREM', [$this->getFavoritesCommentsKeyForUser($user), $comment->id]);

            $comment->favorite_count = ($comment->favorite_count) ? $comment->favorite_count - 1 : 0;
            $comment->timestamps = false;
            $comment->save();
            $this->redis->command('DECR', [$this->getFavoriteCountKeyForComment($comment)]);
        }

        return $changed;
    }

    /**
     * @param Location $location
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUsersWhoFavoriteLocation(Location $location)
    {
        $userIds = DB::table('favorites')
            ->whereNull('comment_id')
            ->where('location_id', $location->id)
            ->lists('user_id');

        if (!count($userIds)) {
            return new Collection();
        }

        $users = User::whereIn('id', $userIds)->get();

        return $users;
    }

    public function getLocationByHash($locationHash)
    {
        return $this->locations->find(from_hashid($locationHash));
    }

    public function getUserByHash($userHash)
    {
        return $this->users->find(from_hashid($userHash));
    }
}
