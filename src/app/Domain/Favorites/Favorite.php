<?php

namespace Myc\Domain\Favorites;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';

    public function location()
    {
        return $this->belongsTo('Myc\Domain\Location');
    }

    public function comments()
    {
        return $this->belongsTo('Myc\Domain\Comments\Comment');
    }

    public function user()
    {
        return $this->belongsTo('Myc\Domain\Users\User');
    }
}
