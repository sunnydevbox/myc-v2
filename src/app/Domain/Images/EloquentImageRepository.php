<?php

namespace Myc\Domain\Images;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Myc\Domain\Locations\Location;

class EloquentImageRepository implements ImageRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $query;

    public function __construct(Image $image)
    {
        $this->query = $image->newQuery();
    }
    public function getById($id)
    {
        $image = $this->query->find($id);
        if (!$image) {
            throw new ModelNotFoundException();
        }

        return $image;
    }

    public function getAllForLocation(Location $location)
    {
        return $location->images()->orderBy('updated_at', 'DESC')->get();
    }
}
