<?php

namespace Myc\Domain\Images;

use Myc\Domain\Locations\Location;

interface ImageRepository
{
    public function getById($id);
    public function getAllForLocation(Location $location);
}
