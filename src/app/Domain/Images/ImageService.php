<?php

namespace Myc\Domain\Images;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Validation\Factory;
use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Exceptions\ValidationException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class ImageService
{
    /**
     * @var Image
     */
    private $images;
    /**
     * @var Location
     */
    private $locations;
    /**
     * @var Factory
     */
    private $validator;

    const IMAGE_PATH = 'app/images/';

    const MAX_FILE_SIZE = 6000;

    /**
     * @var Filesystem
     */
    private $files;

    public function __construct(
        Image $images,
        Location $locations,
        Factory $validator
    ) {
        $this->images = $images;
        $this->locations = $locations;
        $this->validator = $validator;
        $this->query = $images->newQuery();
    }

    public function getDirectoryForFilename($filename)
    {
        return substr($filename, 0, 3);
    }

    public function createImageForLocation(UploadedFile $file, $locationId, User $user)
    {
        $validation = $this->validator->make([
            'location_id' => $locationId,
            'image' => $file,
        ], [
            'location_id' => 'required|exists:locations,id',
            'image' => 'image|max:'.ImageService::MAX_FILE_SIZE,
        ]);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $fileInfo = $this->getFileInfo($file->getPathname());
        $file->move(storage_path(static::IMAGE_PATH).DIRECTORY_SEPARATOR.$fileInfo['directory'], $fileInfo['filename']);

        $this->images->unguard();
        $image = $this->images->create([
            'location_id' => $locationId,
            'user_id' => $user->id,
            'path' => $fileInfo['path'],
            'mime_type' => $fileInfo['mime_type'],
            'original_width' => (int) $fileInfo['width'],
            'original_height' => (int) $fileInfo['height'],
            'sort_order' => 9999,
        ]);

        return $image;
    }

    public function updateImageForUser(UploadedFile $file, User $user)
    {
        $this->validateImage($file);

        $previousImage = $user->image;

        $fileInfo = $this->getFileInfo($file->getPathname());
        $file->move(storage_path(static::IMAGE_PATH).DIRECTORY_SEPARATOR.$fileInfo['directory'], $fileInfo['filename']);
        $user->image = $fileInfo['path'];
        $user->save();

        if (!is_null($previousImage)) {
            // Remove previous image from filesystem if any
            $this->deleteFile(storage_path(static::IMAGE_PATH).DIRECTORY_SEPARATOR.$previousImage);
        }

        return $user;
    }

    public function updateImageForOrganization(UploadedFile $file, Organization $organization, $type)
    {
        $this->validateImage($file);

        $previousImage = $organization->{$type};

        $fileInfo = $this->getFileInfo($file);
        $file->move(storage_path(static::IMAGE_PATH).DIRECTORY_SEPARATOR.$fileInfo['directory'], $fileInfo['filename']);
        $organization->{$type} = $fileInfo['path'];
        $organization->save();

        if (!is_null($previousImage)) {
            // Remove previous image from filesystem if any
            $this->deleteFile(storage_path(static::IMAGE_PATH).DIRECTORY_SEPARATOR.$previousImage);
        }

        return $organization;
    }

    public function deleteImageForOrganization(Organization $organization, $type) {
        $image = $organization->{$type};

        if (!is_null($image)) {
            // Remove image from filesystem if any
            $this->deleteFile(storage_path(static::IMAGE_PATH).DIRECTORY_SEPARATOR.$image);

            $organization->{$type} = null;
            $organization->save();
        }
    }

    public function storeRemoteImage($url)
    {
        try {
            $file = file_get_contents($url);
        } catch (\ErrorException $e) {
            throw new \InvalidArgumentException($e->getMessage());
        }

        $fileInfo = $this->getFileInfo($file, true);
        $path = storage_path(static::IMAGE_PATH).$fileInfo['directory'].DIRECTORY_SEPARATOR;
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777);
        }
        File::put($path.$fileInfo['filename'], $file);

        return $fileInfo;
    }

    public function setImageSortOrder($input, $location)
    {
        foreach ($input as $item) {
            if (!isset($item['sort_order']) || !isset($item['id'])) {
                throw new \InvalidArgumentException('Invalid Arguments: "id" and "sort_order" are required');
            }
            $image = $this->images->find(from_hashid($item['id']));
            if (!$image) {
                throw new \InvalidArgumentException('Image does not exist');
            }
            if ($image->location_id != $location->id) {
                throw new \InvalidArgumentException('Image does not belong to this location');
            }
            $image->sort_order = (int) $item['sort_order'];
            $image->save();
        }

        return $location;
    }

    public function remoteImageForLocation($url, Location $location)
    {
        $fileInfo = $this->storeRemoteImage($url);

        $this->images->unguard();
        $image = $this->images->create([
            'path' => $fileInfo['path'],
            'location_id' => $location->id,
            'mime_type' => $fileInfo['mime_type'],
            'original_width' => (int) $fileInfo['width'],
            'original_height' => (int) $fileInfo['height'],
        ]);

        return $image;
    }

    public function delete(Image $image)
    {
        $this->deleteFile(storage_path(static::IMAGE_PATH).$image->path);

        $image->delete();

        return;
    }

    private function validateImage(UploadedFile $file)
    {
        $validation = $this->validator->make(['image' => $file], ['image' => 'image|max:'.ImageService::MAX_FILE_SIZE]);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }
    }

    private function getFileInfo($file, $contents = false)
    {
        $data = $contents ? getimagesizefromstring($file) : getimagesize($file);

        if (false === $data) {
            throw new \InvalidArgumentException('Not a readable image');
        }

        list($width, $height, $type) = $data;
        $mimeType = image_type_to_mime_type($type);
        $extension = str_replace('jpeg', 'jpg', image_type_to_extension($type));

        // Generate UUID as a filename
        $filename = Uuid::uuid4()->toString().$extension;
        $directory = $this->getDirectoryForFilename($filename);

        return [
            'width' => $width,
            'height' => $height,
            'mime_type' => $mimeType,
            'filename' => $filename,
            'directory' => $directory,
            'path' => $directory.DIRECTORY_SEPARATOR.$filename,
        ];
    }

    private function deleteFile($pathToFile)
    {
        $success = true;
        if (false === @unlink($pathToFile)) {
            $success = false;
        }

        return $success;
    }

    // Strip out from here

    public function getById($id)
    {
        return $this->query->find($id);
    }

    public function destroy($id)
    {
        $image = $this->getById($id);
        $image->delete();

        return true;
    }

    public function sortOrder($id_array)
    {
        $sort_order = 1;

        foreach ($id_array as $id) {
            $image = $this->images->find($id);
            $image->sort_order = $sort_order;
            $image->save();

            $sort_order++;
        }
    // /* Return ------------------------------------- */
    //     return ['success' => true];
    }
}
