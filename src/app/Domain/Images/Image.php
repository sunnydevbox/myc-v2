<?php

namespace Myc\Domain\Images;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Myc\Domain\Users\User;

class Image extends Model
{
    use SoftDeletes;

    protected $table = 'images';
    protected $dates = ['deleted_at'];

    public function location()
    {
        return $this->belongsTo('Myc\Domain\Locations\Location');
    }

    public function disapprovals()
    {
        return $this->hasMany('Myc\Domain\Disapproval');
    }

    public function isOwnedBy(User $user)
    {
        return $this->user_id == $user->id;
    }

}
