<?php

namespace Myc\Domain\Images;

use League\Fractal;

class ImageTransformer extends Fractal\TransformerAbstract
{
    /**
     * @var \Hashids\Hashids
     */
    private $hashids;

    public function __construct()
    {
        $this->hashids = app()->make('Hashids\Hashids');
    }

    public function transform(Image $image, $user = null)
    {
        $editable = !is_null($user) ? ($user->isAdmin() || $image->isOwnedBy($user)) : false;

        return [
            'type' => 'images',
            'id' => (string) $this->hashids->encode($image->id),
            'url' => route('images.get_asset', ['path' => $image->path]),
            'mime_type' => (string) $image->mime_type,
            'original_width' => (int) $image->original_width,
            'original_height' => (int) $image->original_height,
            // TODO check for ownership
            'editable' => $editable
        ];
    }
}
