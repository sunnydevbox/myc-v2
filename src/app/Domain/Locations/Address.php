<?php

namespace Myc\Domain\Locations;

use Geocoder\Model\Address as GeocoderAddress;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Myc\Domain\ValueObjects\LatLon;

class Address implements Jsonable, Arrayable
{
/**
 * @var \Myc\Domain\ValueObjects\LatLon
 */
    //private $pin;
    private $lat;
    private $lon;
    private $streetName;
    private $streetNumber;
    private $postalCode;
    private $city;
    private $country;

    public static function createFromGeocoderAddress(GeocoderAddress $geocoderAddress)
    {
        $address = new static();
        //$address->pin = new LatLon($geocoderAddress->getLatitude(), $geocoderAddress->getLongitude());

        $address->lat = $geocoderAddress->getLatitude();
        $address->lon = $geocoderAddress->getLongitude();
        $address->streetName = (string) $geocoderAddress->getStreetName();
        $address->streetNumber = (string) $geocoderAddress->getStreetNumber();
        $address->postalCode = (string) $geocoderAddress->getPostalCode();
        $address->city = (string) $geocoderAddress->getLocality();
        $address->country = (string) $geocoderAddress->getCountry()->getName();

        return $address;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'lat' => $this->lat,
            'lon' => $this->lon,
            'street_name' => $this->streetName,
            'street_number' => $this->streetNumber,
            'postal_code' => $this->postalCode,
            'city' => $this->city,
            'country' => $this->country,
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options
     *
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }
}
