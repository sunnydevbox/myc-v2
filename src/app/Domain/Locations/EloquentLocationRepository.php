<?php

namespace Myc\Domain\Locations;

use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\BoundingBox;
use Myc\Domain\ValueObjects\Distance;
use Myc\Domain\ValueObjects\LatLon;
use DB;

class EloquentLocationRepository implements LocationRepository
{
    /**
     * @var \Myc\Domain\Locations\Location
     */
    private $model;

    private $itemsPerPage = 40;
    private $displayTrashed = false;

    public function __construct(Location $location)
    {
        $this->model = $location;

        // The Repository that gets the locations from the DB must exclude the
        // the records with status STATUS_DRAFT_MAP by default
        $this->excludeLocationsDraftedOnMap();
    }

    private function excludeLocationsDraftedOnMap()
    {
        $this->model = $this->model->where('status', '!=', Location::STATUS_DRAFT_MAP);

        return $this;
    }

    public function orderBy($field, $order)
    {
        // Defaults
        $field = is_null($field) ? 'created_at' : $field;
        $order = is_null($order) ? 'desc' : $order;

        $this->model = $this->model->orderBy('locations.'.$field, $order);

        return $this;
    }

    public function itemsPerPage($perPage) {
        $this->itemsPerPage = $perPage;
    }

    public function displayTrashed($trashed = false) {
        $this->displayTrashed = $trashed;
    }

    public function queryByString($q)
    {
        if (empty($q)) {
            return $this;
        }

        $this->model = $this->model
            ->where(function($query) use ($q) {
                $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('street_name', 'LIKE', '%'.$q.'%')
                    ->orWhere('locality', 'LIKE', '%'.$q.'%');
            });

        return $this;
    }

    public function filterByTagIds(array $tagIds)
    {
        $locationIds = DB::table('location_tag')
            ->whereIn('tag_id', $tagIds)
            ->lists('location_id');

        if (!count($locationIds)) {
            // Hack; when no location IDs are find, do WHERE IN with non-existing location ID
            $locationIds = [-1];
        }

        $this->model = $this->model->whereIn('id', $locationIds);

        return $this;
    }

    public function filterByOrganizationIds(array $organizationIds)
    {
        $locationIds = DB::table('location_organization')
            ->whereIn('organization_id', $organizationIds)
            ->lists('location_id');

        if (!count($locationIds)) {
            // Hack; when no location IDs are find, do WHERE IN with non-existing location ID
            $locationIds = [-1];
        }

        $this->model = $this->model->whereIn('id', $locationIds);

        return $this;
    }

    public function filterByOrganization(Organization $organization)
    {
        $locationIds = DB::table('location_organization')
            ->where('organization_id', '=', $organization->id)
            ->lists('location_id');

        if (!count($locationIds)) {
            $locationIds = [-1];
        }

        $this->model = $this->model->whereIn('id', $locationIds);

        return $this;
    }

    public function getAllPaginated($perPage = 40, $currentPage = 1)
    {
        if($this->displayTrashed) {
            return $this->model->onlyTrashed()->paginate($this->itemsPerPage);
        } else {
            return $this->model->paginate($this->itemsPerPage);
        }
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    // TODO this is never used when querying from DB, remove, seperate interfaces?
    public function filterByDistanceFrom(LatLon $location, Distance $distance)
    {
    }
    public function filterByBoundingBox(BoundingBox $boundingBox)
    {
    }

    public function filterByFavoriteIds(array $favoriteIds)
    {
    }

    public function filterByUser( User $user ) {
        $this->model->where( 'user_id', $user->id );
        return $this;
    }
}

