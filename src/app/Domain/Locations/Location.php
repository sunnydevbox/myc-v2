<?php

namespace Myc\Domain\Locations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Myc\Domain\Categories\Tag;

class Location extends Model
{
    use SoftDeletes;

    const ICON_BUILDING = 'building';
    const ICON_OPEN_SPACE = 'open_space';

    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAFT_MAP = 'draft_map';
    const STATUS_DRAFT = 'draft';

    public $icons = [
        self::ICON_BUILDING,
        self::ICON_OPEN_SPACE,
    ];

    protected $table = 'locations';

    protected $fillable = [
        'name',
        'slug',
        'lat',
        'lon',
        'street_name',
        'street_number',
        'postal_code',
        'city',
        'country',
    ];

    public function user()
    {
        return $this->belongsTo('Myc\Domain\Users\User', 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany('Myc\Domain\Categories\Tag', 'location_tag')
            ->withTimestamps();
    }

    public function organizations()
    {
        return $this->belongsToMany('Myc\Domain\Organizations\Organization', 'location_organization')
            ->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany('Myc\Domain\Comments\Comment');
            // ->orderBy('created_at', 'ASC');
    }

    public function disapprovals()
    {
        return $this->hasMany('Myc\Domain\Disapproval');
    }

    public function images()
    {
        return $this->hasMany('Myc\Domain\Images\Image')
            ->orderBy('sort_order', 'ASC');
    }

    // Determine icon type based on tag OPEN_SPACE_TAG_ID
    public function getIconType()
    {
        return (in_array(Tag::OPEN_SPACE_TAG_ID, $this->tags->lists('id'))) ? self::ICON_OPEN_SPACE : self::ICON_BUILDING;
    }

    public function getDisplayNameAttribute()
    {
        $fallback = 'No Name';
        // A name _or_ street name + number should be available
        // If somehow street number is missing, use street name only
        // If all is missing, name attribute will be null
        $displayName = $this->name;
        if (empty($displayName)) {
            if (!empty($this->street_name)) {
                $displayName = trim($this->street_name);
                if ('0' === $this->street_number || !empty($this->street_number)) {
                    $displayName .= ' '.trim($this->street_number);
                }
            }
        }

        if (empty($displayName)) {
            $displayName = $fallback;
        }

        return $displayName;
    }

    public function getCityAttribute()
    {
        return is_null($this->locality) ? (is_null($this->sub_locality) ? '' : $this->sub_locality) : $this->locality;
    }

    public function getAddressLineAttribute()
    {
        $parts = array_filter([
            trim($this->postal_code),
            trim($this->city),
            trim($this->country),
        ], function ($item) {
            return !empty($item);
        });

        return implode(' ', $parts);
    }

    public function getDisplayAddressAttribute()
    {
        $parts = build_array([
            $this->street_name,
            $this->street_number,
        ]);

        return implode(' ', $parts);
    }

    public function getDisplayNameAndAddressAttribute()
    {
        $displayName = $this->name;
        if (!empty($displayName)) {
            $displayName .= ', ';
        }

        $displayName .= $this->street_name.' '.$this->street_number;
        $displayName .= "\n".$this->getAddressLineAttribute();

        return $displayName;
    }

    public function getPrimaryImageUrl() {
        if ($this->images->isEmpty()) {
            return '';
        };
        return image_asset($this->images->first()->path);
    }

    public function isPublished()
    {
        return ($this->status === self::STATUS_PUBLISHED);
    }
}
