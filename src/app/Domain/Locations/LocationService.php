<?php

namespace Myc\Domain\Locations;

use Carbon\Carbon;
use Geocoder\Exception\CollectionIsEmpty;
use Geocoder\Geocoder;
use Illuminate\Validation\Factory;
use Myc\Domain\Favorites\FavoriteRepository;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\LatLon;
use Myc\Exceptions\ValidationException;
use Myc\Search\Client;
use Myc\Search\LocationDocument;

class LocationService
{
    /**
     * @var Location
     */
    private $location;

    /**
     * @var \Myc\Search\Client
     */
    private $search;

    /**
     * @var Geocoder
     */
    private $geocoder;
    /**
     * @var FavoriteRepository
     */
    private $favorites;
    /**
     * @var Factory
     */
    private $validator;

    public function __construct(
        Location $locations,
        Client $search,
        Geocoder $geocoder,
        FavoriteRepository $favorites,
        Factory $validator
    ) {
        $this->locations = $locations;
        $this->search = $search;
        $this->geocoder = $geocoder;
        $this->favorites = $favorites;
        $this->validator = $validator;
    }

    /**
     * @param array  $input
     * @param User   $user
     * @param string $status
     *
     * @return \Myc\Domain\Locations\Location
     *
     * @throws \Myc\Exceptions\ValidationException
     */
    public function createLocation(array $input, User $user, $status = Location::STATUS_DRAFT_MAP)
    {
        $rules = [
            'lat' => 'required',
            'lon' => 'required',
        ];


        $validation = $this->validator->make($input, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $latlon = new LatLon($input['lat'], $input['lon']);

        try {
            $address = $this->geocoder->reverse($latlon->getLat(), $latlon->getLon())->first();
            $fields = [
                'street_number' => $address->getStreetNumber(),
                'street_name' => $address->getStreetName(),
                'locality' => $address->getLocality(),
                'sub_locality' => $address->getSubLocality(),
                'postal_code' => $address->getPostalCode(),
                'country' => $address->getCountry()->getName(),
                'country_code' => $address->getCountry()->getCode(),
                'timezone' => $address->getTimezone(),
            ];
        } catch (CollectionIsEmpty $e) {
            //write_log('No address found for lat/lon');
            //if (!isset($input['name']) || empty($input['name'])) {
                throw new \InvalidArgumentException(sprintf(
                    'No address found for coordinate'
                ));
            //}
            /*$fields = [
                'street_number' => null,
                'street_name' => null,
                'locality' => null,
                'sub_locality' => null,
                'postal_code' => null,
                'country' => null,
                'country_code' => null,
                'timezone' => null
            ];*/
        }

        $fields = array_merge($fields, [
            'user_id' => $user->id, // Keep track of which user added to location
            'name' => (!isset($input['name']) || empty($input['name'])) ? null : $input['name'],
            'lat' => $latlon->getLat(), // Note: Use posted Lat/lon
            'lon' => $latlon->getLon(),
            'status' => $status,
        ]);

        $this->locations->unguard();
        $location = $this->locations->create($fields);

        return $location;
    }

    public function updateLocation(Location $location, array $input, $user = null)
    {
        $rules = [
            'name' => 'max:255',
            'street_name' => 'max:255',
            'street_number' => 'max:255',
            'status' => 'required|in:'.implode(',', [Location::STATUS_DRAFT, Location::STATUS_DRAFT_MAP, Location::STATUS_PUBLISHED]),
        ];

        $validation = $this->validator->make($input, $rules);

        // The form in the CMS provides a field with name locality
        // DB field is called locality TODO make consistent
        if (isset($input['locality']) && !empty($input['locality'])) {
            $input['city'] = $input['locality'];
            unset($input['locality']);
        }

        $status = $input['status'];

        // If a street number is set, a street name must be present
        /*$validation->after(function ($validator) use ($input) {
            if ((isset($input['street_number']) && !empty($input['street_number']))
                && (!isset($input['street_name'])) || empty($input['street_name'])) {
                $validator->errors()->add('street_name', 'When street number is set a street name is required');
            }
        });*/

        if (Location::STATUS_PUBLISHED === $status) {
            // If a Location needs to be published Name or Street Name is required
            $validation->after(function ($validator) use ($input) {
                if ((!isset($input['street_name']) || empty($input['street_name'])) &&
                    (!isset($input['name']) || empty($input['name']))) {
                    $validator->errors()->add('name', 'Name or street name is required');
                }
            });


            if ( $this->isLocationCreatedAfterNewValidationRules( $location ) ) {
                $this->addPicturesConstraint( $location, $validation );
            }
        }

        $location->status = $status;

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $fields = [
            'lat',
            'lon',
            'name',
            'street_name',
            'street_number',
            'postal_code',
            'city',
            'country',
            'favorited',
        ];

        foreach ($fields as $field) {
            if (isset($input[ $field ])) {
                // This should be done in a separate route
                if ('favorited' === $field && !is_null($user)) {
                    if (to_bool($input[ $field ])) {
                        $this->favorites->addLocationFavoriteForUser($location, $user);
                    } else {
                        $this->favorites->deleteLocationFavoriteForUser($location, $user);
                    }
                } elseif ('city' === $field) {
                    $location->locality     = $input[ $field ];
                    $location->sub_locality = null;
                } elseif (in_array($field, $location->getFillable())) {
                    $location->$field = (empty($input[ $field ])) ? null : $input[ $field ];
                }
            }
        }

        // This should be done in a seperate route
        if (isset($input['tags']['data']) && is_array($input['tags']['data'])) {
            $tags = $input['tags']['data'];
            $tagIds = array_map(function ($item) {
                if (!isset($item['id'])) {
                    throw new \InvalidArgumentException('ID attribute missing in tag');
                }

                return (int) $item['id'];
            }, $tags);
            $location->tags()->sync($tagIds);
        }
        $location->save();

        $this->updateIndex($location);

        return $location;
    }

    public function updateIndex(Location $location)
    {
        if ($location->status === Location::STATUS_PUBLISHED) {
            // Upsert Location in ES index
            $this->search->indexDocument(LocationDocument::createFromLocation($location));
        } else {
            // Remove from index if status is draft|draft_map
            $this->search->deleteLocation($location->id);
        }
    }

    public function deleteLocation($location)
    {
        if ($location->delete()) {
            $this->search->deleteLocation($location->id);
        }
    }

    public function deleteById($id)
    {
        $location = $this->locations->find($id);
        if ($location) {
            $this->deleteLocation($location);
        }
    }

    public function restoreLocation($location)
    {
        if ($location->restore()) {
            $this->updateIndex($location);
        }
    }

    public function recoverById($id)
    {
        $location = $this->locations->onlyTrashed()->find($id);
        if ($location) {
            $this->restoreLocation($location);
        }
    }

    /**
     * @param Location $location
     * @return bool
     *
     * More info see https://podio.com/andrsnu/map-your-city-dev-team/apps/development/items/494
     */
    public static function isLocationCreatedAfterNewValidationRules( Location $location ) {
        $boundaryDate = Carbon::create( 2016, 7, 18, 0, 0 );

        return $location->created_at >= $boundaryDate;
    }

    /**
     * @param Location $location
     * @param $validation
     */
    private function addPicturesConstraint( Location $location, $validation ) {
        $validation->after( function ( $validator ) use ( $location ) {
            if ( $location->images->count() == 0 ) {
                $validator->errors()->add( 'generic', 'A published location must have at least one image. Please upload an image in order to publish this location.' );
            }
        } );
    }
}
