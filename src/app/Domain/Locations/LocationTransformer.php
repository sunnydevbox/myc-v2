<?php

namespace Myc\Domain\Locations;

use Illuminate\Database\Eloquent\Collection;
use League\Fractal;
use Myc\Domain\Categories\TagTransformer;
use Myc\Domain\Comments\CommentTransformer;
use Myc\Domain\Images\ImageTransformer;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Organizations\OrganizationTransformer;
use Myc\Domain\Users\User;
use Myc\Domain\Users\UserTransformer;

class LocationTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'images',
        'tags',
        'user',
        'organizations',
        'comments',
    ];

    /**
     * @var array
     */
    private $favoriteIds;

    /**
     * @var \Myc\Domain\Users\User|null
     */
    private $user;
    /** @var bool */
    private $includeOnlyActiveOrganizations = false;

    public function __construct(User $user = null, array $favoriteIds = [], array $commentFavoriteIds = [], $includeOnlyActiveOrganizations = false )
    {
        $this->user = $user;
        $this->favoriteIds = $favoriteIds;
        $this->commentFavoriteIds = $commentFavoriteIds;
        $this->includeOnlyActiveOrganizations = $includeOnlyActiveOrganizations;
    }

    public function transform(Location $location)
    {
        $hashid = to_hashid($location->id);

        $links = [
            'self' => route('locations.show', ['hashid' => $hashid]),
        ];

        if (!$location->images->isEmpty()) {
            $links['images'] = route('locations.get_images', ['hashid' => $hashid]);
        }

        $resource = [
            'type' => 'locations',
            'id' => $hashid,
            'icon' => (string) $location->getIconType(),
            'lat' => (float) $location->lat,
            'lon' => (float) $location->lon,
            // Note: A name or street name + street number must be provided
            'name' => (string) $location->display_name,
            'street_name' => is_null($location->street_name) ? null : (string) $location->street_name,
            'street_number' => is_null($location->street_number) ? null : (string) $location->street_number,
            'postal_code' => (string) $location->postal_code,
            'city' => (string) is_null($location->locality) ? $location->sub_locality : $location->locality,
            'country' => (string) $location->country,
            'created_at' => $location->created_at->toIso8601String(),
            'favorite_count' => (int) $location->favorite_count,
            'status' => (string) $location->status,
            'links' => $links,
        ];

        if ($this->user) {
            $resource['favorited'] = in_array($location->id, $this->favoriteIds);
            //$resource['editable'] = ($this->user->hasCreated($location) || $this->user->isAdmin());
            $resource['editable'] = true;
        }

        return $resource;
    }

    public function includeImages(Location $location)
    {
        $images = $location->images;

        return $this->collection($images, new ImageTransformer());
    }

    public function includeTags(Location $location)
    {
        $images = $location->tags;

        return $this->collection($images, new TagTransformer());
    }

    public function includeUser(Location $location)
    {
        return $this->item($location->user, new UserTransformer());
    }

    public function includeOrganizations(Location $location)
    {
        /** @var Collection $organizations */
        $organizations = $location->organizations;

        if ( $this->includeOnlyActiveOrganizations ) {
            $organizations = $organizations->filter( function( Organization $item ) {
                return $item->isActive();
            } );
        }

        return $this->collection($organizations, new OrganizationTransformer($location, $this->user));
    }

    public function includeComments(Location $location)
    {
        $comments = $location->comments;

        return $this->collection($comments, new CommentTransformer($this->user, $this->commentFavoriteIds));
    }
}
