<?php

namespace Myc\Domain\Locations;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\BoundingBox;
use Myc\Domain\ValueObjects\Distance;
use Myc\Domain\ValueObjects\LatLon;
use Myc\Search\Client;
use DB;

class ElasticSearchLocationRepository implements LocationRepository
{
    /**
     * @var Client
     */
    private $client;

    private $perPage = 40;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->query = [];
        $this->filter = [];

        $this->params = [];
        $this->params['index'] = Client::INDEX;
        $this->params['type'] = Client::LOCATION_TYPE;
        $this->query = ['match_all' => new \stdClass()];
    }

    public function filterByBoundingBox(BoundingBox $boundingBox)
    {
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = [
            'geo_bounding_box' => [
                'location' => [
                    'top_left' => $boundingBox->getTopLeft()->toArray(),
                    'bottom_right' => $boundingBox->getBottomRight()->toArray(),
                ],
            ],
        ];

        return $this;
    }

    public function filterByTagIds(array $tagIds)
    {
        $terms = [];
        foreach ($tagIds as $tagId) {
            $terms[] = ['term' => ['tag' => $tagId]];
        }
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = $terms;

        return $this;
    }

    public function filterByOrganizationIds(array $organizationIds)
    {
        // $terms = [];
        // foreach ($tagIds as $tagId) {
        //     $terms[] = ['term' => ['tag' => $tagId]];
        // }
        // if (!isset($this->filter['bool']['must'])) {
        //     $this->filter['bool']['must'] = [];
        // }
        // $this->filter['bool']['must'][] = $terms;

        // return $this;
    }

    public function filterByDistanceFrom(LatLon $latlon, Distance $distance)
    {
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = [
            'geo_distance' => [
                'distance' => (string) $distance,
                'location' => [
                    'lat' => $latlon->getLat(),
                    'lon' => $latlon->getLon(),
                ],
            ],
        ];
    }

    public function filterByFavoriteIds(array $favoriteIds)
    {
        if (!count($favoriteIds)) {
            $favoriteIds = [-1];
        }

        return $this->filterByIds($favoriteIds);
    }

    public function filterByOrganization(Organization $organization)
    {
        $locationIds = DB::table('location_organization')
            ->where('organization_id', '=', $organization->id)
            ->lists('location_id');

        if (!count($locationIds)) {
            $locationIds = [-1];
        }

        return $this->filterByIds($locationIds);
    }

    public function filterByIds(array $ids)
    {
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = [
            'terms' => ['id' => $ids],
        ];

        return $this;
    }

    public function itemsPerPage($perPage) {

    }

    public function queryByString($q)
    {
        $this->query = [
            'match' => [
                '_all' => [
                    'query' => $q,
                    'operator' => 'and',
                ],
            ],
        ];

        return $this;
    }

    public function displayTrashed($trashed = false) {

    }

    public function getAllPaginated($perPage = 40, $currentPage = 1)
    {
        if (filter_var($perPage, FILTER_VALIDATE_INT) === false) {
            throw new \InvalidArgumentException('Parameter limit must be an integer.');
        }

        $this->params['body']['size'] = $perPage;
        $this->params['body']['from'] = ($currentPage - 1) * $perPage;

        if ($this->query) {
            $query = $this->query;
        } else {
            $query['match_all'] = new \stdClass();
        }

        if ($this->filter) {
            $this->params['body']['query']['filtered']['query'] = $query;
            $this->params['body']['query']['filtered']['filter'] = $this->filter;
        } else {
            $this->params['body']['query'] = $query;
        }

        //echo json_encode($this->params);
        //die();

        try {
            $this->client->search($this->params);
        } catch (\Exception $e) {
            throw $e;
        }

        $ids = $this->client->getIds();

        if (!$ids) {
            return new Collection();
        }

        $locations = Location::whereIn('id', $ids)->get();

        $paginator = new LengthAwarePaginator(
            $locations,
            $this->client->getHits(),
            $perPage,
            $currentPage
        );

        return $paginator;
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function filterByUser( User $user ) {
        $locationIds = DB::table('locations')
            ->where('user_id', $user->id)
            ->lists( 'id' );

        if (!count($locationIds)) {
            $locationIds = [-1];
        }

        return $this->filterByIds($locationIds);
    }
}
