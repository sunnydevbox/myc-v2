<?php

namespace Myc\Domain\Locations;

use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\BoundingBox;
use Myc\Domain\ValueObjects\Distance;
use Myc\Domain\ValueObjects\LatLon;

interface LocationRepository
{
    /**
     * @param BoundingBox $boundingBox
     *
     * @return $this
     */
    public function filterByBoundingBox(BoundingBox $boundingBox);
    public function filterByDistanceFrom(LatLon $location, Distance $distance);
    public function filterByTagIds(array $tagIds);
    public function filterByOrganizationIds(array $organizationIds);
    public function filterByFavoriteIds(array $favoriteIds);
    public function filterByOrganization(Organization $organization);
    public function filterByUser(User $user);
    public function itemsPerPage($perPage);
    public function displayTrashed($trashed);
    public function queryByString($q);
    public function getAllPaginated($perPage = 40, $currentPage = 1);
    public function getById($id);
}
