<?php

namespace Myc\Domain\Pins;

use Myc\Domain\Organizations\Organization;
use Myc\Domain\Users\User;
use Myc\Domain\ValueObjects\BoundingBox;
use Myc\Domain\ValueObjects\LatLon;
use Myc\Search\Client;
use DB;

class PinRepository
{
    /**
     * @var \Myc\Search\Client
     */
    private $search;

    public $filter = [];
    private $params = [];
    private $query = [];
    private $results;

    private $precision = 6;

    private $boundsIncluded = false;
    /**
     * @var \Myc\Domain\ValueObjects\BoundingBox
     */
    private $boundingBox;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->params['index'] = Client::INDEX;
        $this->params['type'] = Client::LOCATION_TYPE;
        $this->query = ['match_all' => new \stdClass()];
    }

    public function setPrecision($precision)
    {
        if (!filter_var($precision, FILTER_VALIDATE_INT) || ($precision > 12 || $precision < 1)) {
            throw new \InvalidArgumentException('Precision must be an integer between 1 and 12');
        }
        $this->precision = (int) $precision;

        return $this;
    }

    public function queryByString($q)
    {
        $this->query = [
            'match' => [
                '_all' => [
                    'query' => $q,
                    'operator' => 'and',
                ],
            ],
        ];

        return $this;
    }

    public function filterByFavoriteIds(array $favoriteIds)
    {
        if (!count($favoriteIds)) {
            $favoriteIds = [-1];
        }

        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = [
            'terms' => ['id' => $favoriteIds],
        ];

        return $this;
    }

    public function filterByBoundingBox(BoundingBox $boundingBox)
    {

        // Add the structure if not yet exists
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = [
            'geo_bounding_box' => [
                'location' => [
                    'top_left'     => $boundingBox->getTopLeft()->toArray(),
                    'bottom_right' => $boundingBox->getBottomRight()->toArray(),
                ],
            ],
        ];

        return $this;
    }

    public function filterByTagIds($tagIds)
    {
        if (is_string($tagIds)) {
            $tagIds = explode(',', $tagIds);
        }
        $tagIds = array_map('intval', $tagIds);

        $terms = [];
        foreach ($tagIds as $tagId) {
            $terms[] = ['term' => ['tag' => $tagId]];
        }
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }
        $this->filter['bool']['must'][] = $terms;

        return $this;
    }

    /**
     * @param Organization $organization
     *
     * @return $this
     */
    public function filterByOrganization(Organization $organization)
    {
        $locationIds = DB::table('location_organization')
            ->where('organization_id', '=', $organization->id)
            ->lists('location_id');

        if (!count($locationIds)) {
            $locationIds = [-1];
        }

        return $this->filterByIds($locationIds);
    }

    public function filterByUser( User $user ) {
        $locationIds = DB::table('locations')
            ->where('user_id', $user->id)
            ->lists( 'id' );

        if (!count($locationIds)) {
            $locationIds = [-1];
        }

        return $this->filterByIds($locationIds);
    }

    public function filterByIds(array $ids)
    {
        if (!isset($this->filter['bool']['must'])) {
            $this->filter['bool']['must'] = [];
        }

        $ids = array_values($ids);

        // If there are already IDs filtered, add only the nonexistent IDs to the condition
        if (isset($this->filter['bool']['must']['terms']['id'])) {
            $ids = array_unique(array_merge($this->filter['bool']['must']['terms']['id'], $ids));
        }

        $this->filter['bool']['must'][] = [
            'terms' => ['id' => $ids],
        ];

        return $this;
    }

    public function includeBounds()
    {
        if (!isset($this->params['body']['aggs'])) {
            $this->params['body']['aggs'] = [];
        }
        $this->params['body']['aggs']['viewport'] = ['geo_bounds' => ['field' => 'location']];

        $this->boundsIncluded = true;

        return $this;
    }

    /**
     * @return BoundingBox
     */
    public function getBoundingBox()
    {
        return $this->boundingBox;
    }

    public function search()
    {
        $this->params['body']['size'] = 0; // only return buckets
        $this->params['body']['query']['filtered']['query'] = $this->query;
        $this->params['body']['query']['filtered']['filter'] = $this->filter;

        // Depends on zoom level!
        // Don't aggregate on precision > 7?
        if (!isset($this->params['body']['aggs'])) {
            $this->params['body']['aggs'] = [];
        }

        $this->params['body']['aggs']['location_grid'] = [
            'geohash_grid' => ['field' => 'location', 'precision' => $this->precision],
            'aggs' => [
                'cell' => ['geo_bounds' => ['field' => 'location']], // Get bounds of bucket
                'top_location_hits' => ['top_hits' => ['size' => 1]], // Get top hit (if 1, return location)
            ],
        ];

        try {
            $this->results = $this->client->search($this->params)->getRawResults();
            if ($this->boundsIncluded) {
                if (isset($this->results['aggregations']['viewport']['bounds'])) {
                    $bounds = $this->results['aggregations']['viewport']['bounds'];
                    $this->boundingBox = new BoundingBox(
                        new LatLon($bounds['top_left']['lat'], $bounds['top_left']['lon']),
                        new LatLon($bounds['bottom_right']['lat'], $bounds['bottom_right']['lon'])
                    );
                } else {
                    $this->boundsIncluded = false;
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    public function getAll()
    {
        return $this->transformBuckets($this->results['aggregations']['location_grid']['buckets']);
    }

    private function transformBuckets(array $buckets)
    {
        $resource = [];
        foreach ($buckets as $bucket) {
            // If a buckets contains only 1 location: no zooming is required so
            // location details can be returned directly.
            $isSingleLocation = $bucket['top_location_hits']['hits']['total'] === 1;

            if ($isSingleLocation) {
                // No DB query, use source data from ES.
                $source = $bucket['top_location_hits']['hits']['hits'][0]['_source'];
                $resource[] = $this->client->transformResultToLocation($source);
            } else {
                // A bucket must provide the number of locations in it, the bounding box
                // which contains all locations and the center point of the bounding box.
                $bounds = $bucket['cell']['bounds'];
                $boundingBox = new BoundingBox(
                    new LatLon($bounds['top_left']['lat'], $bounds['top_left']['lon']),
                    new LatLon($bounds['bottom_right']['lat'], $bounds['bottom_right']['lon'])
                );
                $resource[] = [
                    'type' => 'buckets',
                    'id' => (string) $bucket['key'], // Use the geohash as ID
                    'location_count' => (int) $bucket['doc_count'],
                    'lat' => $boundingBox->getCenterPoint()->getLat(),
                    'lon' => $boundingBox->getCenterPoint()->getLon(),
                    'bounding_box' => $boundingBox->toArray(),
                    'links' => [
                        'self' => route('pins.index', ['bounding_box' => $boundingBox->toQueryParam()]),
                    ],
                ];
            }
        }

        return $resource;
    }

    /**
     * @return bool
     */
    public function isBoundsIncluded()
    {
        return $this->boundsIncluded;
    }
}
