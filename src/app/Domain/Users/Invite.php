<?php

namespace Myc\Domain\Users;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $table = 'invites';

    protected $dates = ['registered_at'];

    public function organization()
    {
        return $this->belongsTo('Myc\Domain\Organizations\Organization');
    }
}
