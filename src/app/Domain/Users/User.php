<?php

namespace Myc\Domain\Users;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Myc\Domain\Locations\Location;
use Myc\Domain\Organizations\Organization;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $dates = ['registered_for_organization_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'name',
        'email',
        'password',
        'confirmed',
        'confirmation_code',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $with = ['role'];

    private $displayName;

    /**
     * @return mixed
     */
    public function role()
    {
        return $this->belongsTo('Myc\Domain\Users\Role');
    }

    public function favorites()
    {
        return $this->belongsToMany('Myc\Domain\Locations\Location', 'favorites')->withTimestamps();
    }

    public function commentFavorites()
    {
        return $this->belongsToMany('Myc\Domain\Comments\Comment', 'favorites')->withTimestamps();
    }

    public function organizations()
    {
        return $this->belongsToMany('Myc\Domain\Organizations\Organization')
            ->withPivot('role')
            ->withTimestamps();
    }

    public function isConfirmed()
    {
        return (bool) $this->attributes['confirmed'];
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function hasRole($name)
    {
        return (!is_null($this->role) && ($name === $this->role->slug));
    }

    public function assignRole($role)
    {
        $this->role()->associate($role);
        $this->save();
    }

    // Is User either a member of owner of Organization?
    public function isMember(Organization $organization)
    {
        return !is_null(DB::table('organization_user')
            ->where('organization_id', '=', $organization->id)
            ->where('user_id', '=', $this->id)
            ->first());
    }

    public function isOwner(Organization $organization)
    {
        return !is_null(DB::table('organization_user')
            ->where('organization_id', '=', $organization->id)
            ->where('user_id', '=', $this->id)
            ->where('role', '=', 'owner')
            ->first());
    }

    public function hasSomeOrganizations()
    {
        return !is_null(DB::table('organization_user')
            ->where('user_id', '=', $this->id)
            ->first());
    }

    public function isMemberOfSomeOrganizations()
    {
        return !is_null(DB::table('organization_user')
            ->where('user_id', '=', $this->id)
            ->where('role', '=', 'member')
            ->first());
    }

    public function ownsSomeOrganizations()
    {
        return !is_null(DB::table('organization_user')
            ->where('user_id', '=', $this->id)
            ->where('role', '=', 'owner')
            ->first());
    }

    public function getOrganizationRole(Organization $organization)
    {
        return DB::table('organization_user')
          ->where('organization_id', '=', $organization->id)
          ->where('user_id', '=', $this->id)
          ->pluck('role');
    }

    public function hasCreated(Location $location)
    {
        return ($this->id == $location->user_id);
    }

    public function getDisplayNameAttribute()
    {
        if (is_null($this->displayName)) {
            $parts = [
                'first_name',
                'last_name_prefix',
                'last_name',
            ];

            $name = [];
            foreach ($parts as $part) {
                if (!empty($this->$part)) {
                    $name[] = trim($this->$part);
                }
            }

            if (count($name)) {
                $this->displayName = implode(' ', $name);
            } else {
                $this->displayName = null;
            }
        }

        return $this->displayName;
    }
}
