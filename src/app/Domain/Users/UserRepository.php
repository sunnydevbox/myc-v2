<?php

namespace Myc\Domain\Users;

use Myc\Domain\Organizations\Organization;

class UserRepository
{
    private $model;

    private $displayTrashed = false;


    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function orderBy($field, $order)
    {
        $field = is_null($field) ? 'created_at' : $field;
        $order = is_null($order) ? 'desc' : $order;

        $this->model = $this->model->orderBy('users.'.$field, $order);

        return $this;
    }

    public function getAllPaginated($perPage = 40)
    {
        if($this->displayTrashed) {
            return $this->model->onlyTrashed()->paginate($perPage);
        } else {
            return $this->model->paginate($perPage);
        }
        // return $this->model->paginate($perPage);
    }

    /**
     * @param int $id
     *
     * @return \Myc\Domain\Users\User
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param string $email
     *
     * @return \Myc\Domain\Users\User
     */
    public function getByEmail($email)
    {
        return $this->model->where('email', '=', $email)->first();
    }

    public function filterByString($q)
    {
        if (empty($q)) {
            return $this;
        }

        $this->model = $this->model
            ->where('users.first_name', 'LIKE', '%'.$q.'%')
            ->orWhere('users.last_name', 'LIKE', '%'.$q.'%')
            ->orWhere('users.email', 'LIKE', '%'.$q.'%');

        return $this;
    }

    public function displayTrashed($trashed = false) {
        $this->displayTrashed = $trashed;
    }

    public function filterByRole(Role $role)
    {
        $this->model = $this->model->where('users.role_id', '=', $role->id);

        return $this;
    }

    public function filterByOrganization(Organization $organization)
    {
        // With pivot
        $this->model = $this->model
            ->join('organization_user', 'users.id', '=', 'organization_user.user_id')
            ->where('organization_user.organization_id', '=', $organization->id)
            ->select([
                'users.*',
                'organization_user.role AS organization_role',
                'organization_user.created_at AS registered_for_organization_at',
            ]);

        return $this;
    }
}
