<?php

namespace Myc\Domain\Users;

use Myc\Domain\Organizations\OrganizationTransformer;

class UserProfileTransformer extends UserTransformer
{
    protected $availableIncludes = ['organizations'];

    public function transform(User $user)
    {
        return [
            //'id' => to_hashid($user->id),
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'last_name_prefix' => $user->last_name_prefix,
            'image' => is_null($user->image) ? null : route('images.get_asset', ['path' => $user->image]),
            'role' => $user->role->slug,
            'device_token' => $user->device_token,
        ];
    }

    public function includeOrganizations(User $user)
    {
        return $this->collection($user->organizations, new OrganizationTransformer(null, $user));
    }
}
