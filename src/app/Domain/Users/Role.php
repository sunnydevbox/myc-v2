<?php

namespace Myc\Domain\Users;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ROLE_ID_USER = 1;
    const ROLE_ID_ADMIN = 2;

    protected $fillable = ['name', 'label', 'description'];

    public function users()
    {
        return $this->hasMany('Myc\Domain\Users\User');
    }
}
