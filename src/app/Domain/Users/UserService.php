<?php

namespace Myc\Domain\Users;

use Event;
use Carbon\Carbon;
use Illuminate\Validation\Factory;
use Myc\Domain\Images\ImageService;
use Myc\Events\UserEnrolledToOrganization;
use Myc\Events\UserWasRegistered;
use Myc\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Contracts\Mail\Mailer;

class UserService
{
    private $model;
    private $validator;

    private $rules = [
        'email' => 'required|email|max:255|unique:users',
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'last_name_prefix' => 'max:255',
        'password' => 'required|confirmed|max:60|min:6',
    ];

    private function getRulesForUpdate(User $user)
    {
        $rules = $this->rules;

        $fields = ['email', 'first_name', 'last_name', 'password'];
        foreach ($fields as $field) {
            $rules[$field] = 'sometimes|'.$rules[$field];
        }
        // Exclude own ID in unique check
        $rules['email'] = $rules['email'].',email,'.$user->id;

        return $rules;
    }

    private $rulesForUpdate = [
        'email' => 'required|email|max:255|unique:users',
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'last_name_prefix' => 'max:255',
        'password' => 'required|confirmed|max:60|min:6',
    ];

    /**
     * @var \Myc\Domain\Images\ImageService
     */
    private $imageService;

    public function __construct(
        User $user,
        Factory $validator,
        ImageService $imageService,
        Mailer $mailer)
    {
        $this->model = $user;
        $this->validator = $validator;
        $this->imageService = $imageService;
        $this->mailer = $mailer;
    }

    /**
     * @param array $input
     *
     * @return \Myc\Domain\Users\User
     * @throws ValidationException
     */
    public function createUser(array $input)
    {
        $validation = $this->validator->make($input, $this->rules);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $confirmationCode = hash_hmac('sha256', str_random(40), config('app.key'));

        $this->model->unguard();
        $user = $this->model->create([
            'role_id' => Role::ROLE_ID_USER, // User role by default
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'last_name_prefix' => (isset($input['last_name_prefix']) && !empty($input['last_name_prefix'])) ? $input['last_name_prefix'] : null,
            'confirmed' => false,
            'confirmation_code' => $confirmationCode,
            'device_token' => (isset($input['device_token']) && !empty($input['device_token'])) ? $input['device_token'] : null,
        ]);

        // If a token is set, lookup the invite and register and associate the user with the organization
        if (isset($input['token']) && !empty($input['token'])) {
            $invite = Invite::where('token', '=', $input['token'])->orderBy('created_at', 'DESC')->first();
        } else {
            // If no token is set, check if there are any invites for this email address, and associate the
            // organization registered with the invite
            $invite = Invite::where('email', '=', $input['email'])->orderBy('created_at', 'DESC')->first();
        }
        // Note: in both cases use the newest invite available

        if (isset($invite) && !is_null($invite)) {
            $repo = app()->make('Myc\Domain\Organizations\OrganizationRepository');
            $organization = $repo->getById($invite->organization_id);
            if ($organization) {
                $repo->addUserToOrganizationWithRole($user, $organization, $invite->role);
                Event::fire( new UserEnrolledToOrganization( $organization, $user, $invite->role ) );

                $invite->registered_at = Carbon::now();
                $invite->save();

                //send mail & return $user
                $this->mailer->send('emails.organization_account_confirmed', ['user' => $user], function ($mail) use ($user) {
                    $mail->to($user->email)->subject('Thanks!');
                });
                return $user;
            }
        }

        event(new UserWasRegistered($user));

        return $user;
    }

    public function updateUser(User $user, array $input)
    {
        if (!$user->isConfirmed()) {
            throw new AccessDeniedHttpException('You need to confirm your email address before you can edit your profile');
        }

        $validation = $this->validator->make($input, $this->getRulesForUpdate($user));

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        $input['last_name_prefix'] = (isset($input['last_name_prefix']) && !empty($input['last_name_prefix'])) ? $input['last_name_prefix'] : null;
        $input['device_token'] = (isset($input['device_token']) && !empty($input['device_token'])) ? $input['device_token'] : null;

        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }

        if (isset($input['password'])) {
            unset($input['password_confirmation']);
        }

        $send_confirmation = false;
        if(isset($input['email']) && ($user->email != $input['email'])) {
            //email changed, user must reconfirm his account
            $send_confirmation = true;
            $confirmationCode = hash_hmac('sha256', str_random(40), config('app.key'));
            $input['confirmed'] = false;
            $input['confirmation_code'] = $confirmationCode;
        }

        $user->unguard();
        $user->fill($input);
        $user->save();

        if($send_confirmation) {
            //send email confirmation mail
            $this->mailer->send('emails.confirm_email_change', ['user' => $user], function ($mail) use ($user) {
                $mail->to($user->email)->subject('You changed your email address');
            });
        }

        return $user;
    }

    public function createFromFacebook(array $input)
    {
        try {
            $fileInfo = $this->imageService->storeRemoteImage($input['imageUrl']);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->model->unguard();
        $user = $this->model->create([
            'role_id' => Role::ROLE_ID_USER, // User role by default
            'email' => $input['email'],
            'first_name' => $input['firstName'],
            'last_name' => $input['lastName'],
            'password' => null,
            'image' => $fileInfo['path'],
            'confirmed' => true,
            'from_facebook' => true,
        ]);

        return $user;
    }

    public function mergeWithFacebookProfile(User $user, array $input)
    {
        try {
            $fileInfo = $this->imageService->storeRemoteImage($input['imageUrl']);
        } catch (\Exception $e) {
            throw $e;
        }

        $user->unguard();
        $user->fill([
            'first_name' => $input['firstName'],
            'last_name' => $input['lastName'],
            'last_name_prefix' => null,
            'image' => $fileInfo['path'],
            'confirmed' => true,
            'from_facebook' => true,
        ])->save();

        return $user;
    }

    public function deleteUser($user)
    {
        if ($user->delete()) {
        }
    }

    public function deleteById($id)
    {
        $user = $this->model->find($id);
        if ($user) {
            $this->deleteUser($user);
        }
    }

    public function restoreUser($user)
    {
        if ($user->restore()) {
        }
    }

    public function recoverById($id)
    {
        $user = $this->model->onlyTrashed()->find($id);
        if ($user) {
            $this->restoreUser($user);
        }
    }
}
