<?php

namespace Myc\Domain\Users;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => to_hashid($user->id),
            'display_name' => $user->display_name,
            'image' => is_null($user->image) ? null : route('images.get_asset', ['path' => $user->image]),
        ];
    }
}
