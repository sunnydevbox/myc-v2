<div class="form-group{{ $errors->has((string) $field->id) ? ' has-error' : '' }}">
    <label for="{{ $field->id }}">{{ $field->label }}</label>
    <textarea style="height: 200px" class="form-control" id="{{ $field->id }}" name="{{ $field->id }}">{{ old((string) $field->id, $field->entry) }}</textarea>
    {!! $errors->first((string) $field->id, '<p class="help-block">:message</p>') !!}
</div>
