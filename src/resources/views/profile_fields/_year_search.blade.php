<div class="form-group">
    <label for="{{ to_hashid($field->id) }}">{{ $field->label }}</label>
    <input class="form-control year" type="text" id="{{ to_hashid($field->id) }}" name="profile_fields[{{ to_hashid($field->id) }}]">
</div>
