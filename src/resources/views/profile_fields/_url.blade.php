<div class="form-group{{ $errors->has((string) $field->id) ? ' has-error' : '' }} form-group__url-field">
    <label for="{{ $field->id }}">{{ $field->label }}</label>
    <div class="form-group form-inline">
        <label for="{{ $field->id }}_text" style="width: 15%;">Link text</label>
        <input type="text" id="{{ $field->id }}_text" class="form-control link-text" value="{{ markdown_url_to_text(old((string) $field->id, $field->entry)) }}" style="width: 80%;">
    </div>
    <div class="form-group form-inline">
        <label for="{{ $field->id }}_href" style="width: 15%;">URL</label>
        <input type="text" id="{{ $field->id }}_href"  class="form-control link-href" value="{{ markdown_url_to_href(old((string) $field->id, $field->entry)) }}" style="width: 80%;">
    </div>
    <input type="hidden" id="{{ $field->id }}" name="{{ $field->id }}" value="{{ old((string) $field->id, $field->entry) }}">
    {!! $errors->first((string) $field->id, '<p class="help-block">:message</p>') !!}
</div>
