<div class="form-group{{ $errors->has((string) $field->id) ? ' has-error' : '' }}">
    <label for="{{ $field->id }}">{{ $field->label }}</label>
    <input class="form-control" id="{{ $field->id }}" name="{{ $field->id }}" value="{{ old((string) $field->id, $field->entry) }}">
    {!! $errors->first((string) $field->id, '<p class="help-block">:message</p>') !!}
</div>
