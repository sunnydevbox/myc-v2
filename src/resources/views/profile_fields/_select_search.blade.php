<div class="form-group">
    <label for="{{ to_hashid($field->id) }}">{{ $field->label }}</label>
    <select class="form-control" id="{{ to_hashid($field->id) }}" name="profile_fields[{{ to_hashid($field->id) }}]">
        <option value="">Choose an option</option>
        @foreach ($field->options as $option)
            <option value="{{ $option }}">{{ $option }}</option>
        @endforeach
    </select>
</div>
