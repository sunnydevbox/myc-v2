<div class="form-group">
    <label for="{{ to_hashid($field->id) }}">{{ $field->label }}</label>
    @if (false)
        <div class="range-slider" data-min="{!! $field->getMinValue() !!}" data-max="{!! $field->getMaxValue() !!}"></div>
        <input
                type="hidden"
                id="{{ to_hashid($field->id) }}"
                name="profile_fields[{{ to_hashid($field->id) }}]"
                value="['{!! $field->getMinValue() !!}','{!! $field->getMaxValue() !!}']"
        />
    @else
        <input class="form-control" type="text" id="{{ to_hashid($field->id) }}" name="profile_fields[{{ to_hashid($field->id) }}]">
    @endif
</div>
