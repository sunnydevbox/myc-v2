<div class="form-group{{ $errors->has((string) $field->id) ? ' has-error' : '' }}">
    <label for="{{ $field->id }}">{{ $field->label }}</label>
    <select class="form-control" id="{{ $field->id }}" name="{{ $field->id }}">
        <option value="">Choose an option</option>
        @foreach ($field->options as $option)
            <option value="{{ $option }}" {!! option_selected($option, old((string) $field->id, $field->entry)) !!}>{{ $option }}</option>
        @endforeach
    </select>
    {!! $errors->first((string) $field->id, '<p class="help-block">:message</p>') !!}
</div>