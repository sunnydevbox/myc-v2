<div class="form-group">
    <label class="checkbox-header" for="{{ to_hashid($field->id) }}">{{ $field->label }}</label>
    @foreach($field->getOptions() as $option)
        <label>
            <input type="checkbox" name="profile_fields[{{ to_hashid($field->id) }}][{!! $option['id'] !!}]" value="{!! $option['id'] !!}"/>
            {{ $option['value'] }}
        </label>
    @endforeach
</div>