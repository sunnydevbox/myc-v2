<div class="form-group{{ $errors->has((string) $field->id) ? ' has-error' : '' }}">
    <label for="{{ $field->id }}">{{ $field->label }}</label>
    <input type="hidden" name="{!! $field->id !!}[]" id="{!! $field->id !!}[]" value="-1">
    @foreach ($field->getOptions() as $option)
        <label style="display: block;">
            <input type="checkbox" name="{!! $field->id !!}[]" id="{!! $field->id !!}[]" value="{!! $option['id'] !!}" {{ checkbox_checked(in_array($option['id'], old((string) $field->id, $field->getEntries()))) }}>
            {{ $option['value'] }}
        </label>
    @endforeach
    {!! $errors->first((string) $field->id, '<p class="help-block">:message</p>') !!}
</div>