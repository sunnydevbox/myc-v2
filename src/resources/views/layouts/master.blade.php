<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('viewport')
    <title>Map Your City</title>
    {{--<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>--}}
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/flat-ui-pro.css" rel="stylesheet">
    <link href="/assets/css/app.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid main-container">
    @yield('content')
</div>
<script src="/flat-ui-pro/js/vendor/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/js/select2.min.js"></script>
<script src="/flat-ui-pro/js/flat-ui-pro.js"></script>
<script src="/assets/js/jquery.cookie.js"></script>
<script src="/assets/js/app.js"></script>
@yield('scripts')
</body>
</html>
