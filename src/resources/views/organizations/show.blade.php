<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $organization->name }} | Map Your City</title>
    <link rel="icon" type="image/png" href="/assets/img/icon_app.png">
    <meta name="description" content="{{ $organization->description }}">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="https://api.tiles.mapbox.com/mapbox.js/v2.1.8/mapbox.css" rel="stylesheet">
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.2.1/nouislider.min.css"/>
    <link rel="stylesheet" href="{!! asset('assets/css/normalize.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/map.css') !!}">
</head>
<body class="">
    <div id="myc-copyright" style="display:none;">&copy; {{ date('Y') }} All Rights Reserved. <a href="http://www.mapyour.city" target="_blank">MAP YOUR CITY&reg;</a></div>
    <div id="map"></div>
    <div class="filter">
        <div class="inner">
            <form>
                <div class="form-group" style="margin-top: 0;">
                    <label for="q">Name or address</label>
                    <input class="form-control" type="text" name="term" id="term" placeholder="Search">
                </div>
                @if ($profile)
                    @foreach($profile->getFieldsForSearchControls() as $field)
                        @include($field->getViewForSearch(), $field)
                    @endforeach
                @endif
                <a href="#" class="reset-filter">Reset all filters</a>
            </form>
        </div>
    </div>
    <div class="download-app">
        @include('locations._download_app')
    </div>
    <div class="details"></div>
    <script src="//api.tiles.mapbox.com/mapbox.js/v2.1.8/mapbox.js"></script>
    <script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.syphon/0.4.1/backbone.syphon.min.js"></script>

    <script src="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.2.1/nouislider.min.js"></script>

    <script src="{!! asset('assets/js/map.js') !!}"></script>
    <script>
         $('form').on('keyup keypress', function(evt) {
            // Prevent form being submitted on enter
            if (evt.which == 13 && evt.target.type !== 'textarea') {
                evt.preventDefault();
            }
        });
        L.mapbox.accessToken = '{{ config('services.mapbox.public_token') }}';

        $(function() {
            var pins = new PinCollection();
            var state = new State({}, {
                pins: pins,
                organization_id: '{{ to_hashid($organization->id) }}'
            });

            var filterView = new FilterView({state: state});

            var detailView = new DetailView({state: state});

            var mapView = new MapView({
                bounds: {!! json_encode($boundingBox) !!},
                state: state
            });

            state.fetchPins();
        });
    </script>
</body>
</html>