@extends('layouts.master')

@section('viewport')
<meta name="viewport" content="width=device-width, initial-scale=1">
@stop

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div id="logo" class="big"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>This application is optimized for use on desktop computers.</p>
                    <button class="btn btn-primary set-mobile-warning">Continue to login</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
(function($) {
	$('.set-mobile-warning').on('click', function() {
		$.cookie('myc-mobile-warning', true, { expires: 7, path: '/' });
		document.location.href = '{!! route('auth.get_login') !!}';
	});
})(jQuery);
</script>
@stop