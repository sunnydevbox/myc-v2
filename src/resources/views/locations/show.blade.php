<!DOCTYPE html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# place: http://ogp.me/ns/place#">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $location->getDisplayNameAndAddressAttribute() }} | Map Your City</title>
    <link rel="icon" type="image/png" href="/assets/img/icon_app.png">
    <meta property="fb:app_id" content="{{ config('services.facebook.app_id') }}">
    <meta property="og:type" content="place">
    <meta property="og:url" content="{{ route('locations.show_public_profile', ['id' => to_hashid($location->id)]) }}">
    {{--<meta property="og:url" content="http://mapyour.city/l/{{ to_hashid($location->id) }}">--}}
    <meta property="og:title" content="{{ $location->display_name }}">
    <meta property="og:image" content="{{ $location->getPrimaryImageUrl() }}">
    <meta property="place:location:latitude" content="{{ $location->lat }}">
    <meta property="place:location:longitude" content="{{ $location->lon }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@MapYour_City">
    <meta name="twitter:image" content="{{ $location->getPrimaryImageUrl() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-itunes-app" content="app-id={{ config('app.ios_app_id') }}">
    <link rel="stylesheet" href="{!! asset('assets/css/normalize.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/location.css') !!}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick-theme.css"/>
</head>
<body>

<header>
    <div class="header-inner">
        <a href="http://www.mapyour.city"><img class="logo" src="{!! asset('assets/img/logo.svg') !!}"></a>
        @include('locations._download_app')
    </div>
</header>

<div class="container" style="padding: 0;">
    <div class="location-header">
        <h1 class="location-title">{{ $location->display_name }}</h1>
        <h2 class="location-subtitle">{{ !empty($location->name) ? $location->display_address.' ' : '' }}{{ $location->address_line }}</h2>
    </div>
    @if (!$location->images->isEmpty())
        <div class="slider">
            @foreach ($location->images as $image)
                <div><img src="{!! route('images.get_asset', ['path' => $image->path])!!}?w=1272&h=636&fit=crop&crop=center"></div>
            @endforeach
        </div>
    @else
        <div style="line-height: 0;"><img style="width: 100%; height: auto;" src="{{ url('/assets/img/placeholder.jpg') }}" alt="Image not available" /></div>
    @endif
    @if (!$location->tags->isEmpty())
    <div class="tags-container">
        <ul class="tags">
            @foreach ($location->tags as $tag)
                <li>{{ $tag->name }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <p class="favorites"><i class="icon favorite"></i> Favorite of {{ $location->favorite_count }} people</p>

    @if (!$location->organizations->isEmpty())
        <div class="organizations">
            <h1 class="organizations-title">Organizations</h1>
            @foreach ($location->organizations as $organization)
                <div class="organization">
                    <div class="organization__header">
                        <div class="image-container" style="background-image: url({!! image_asset($organization->image).'?w=120&h=120&fit=crop' !!});"></div>
                        <h2>{{ $organization->name }}</h2>
                        <h3>{{ $organization->pay_off }}</h3>
                        <div class="toggle"><i class="fa fa-angle-down"></i></div>
                    </div>
                    <div class="organization__profile" style="display: none;">
                        <?php
                        $profileRepository = app()->make('Myc\Domain\Profiles\ProfileRepository');
                        $profile = $profileRepository->getProfileForOrganizationAndLocation($organization, $location);
                        ?>
                        @if ($profile)
                            <dl>
                                @foreach($profile->getFields() as $field)
                                    @if (!is_null($field->entry) && !empty(trim($field->entry)) && \Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_MULTI_SELECT != $field->getIdentifier())
                                        @if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_TEXTAREA === $field->getIdentifier())
                                            <p style="margin: 0;">{!! nl2br(links_to_html_tags($field->entry)) !!}</p>
                                        @else
                                            <dt>{{ $field->label }}:&nbsp;</dt>
                                            @if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_URL === $field->getIdentifier())
                                                <dd>{!! $field->getHtml() !!} &nbsp;</dd>
                                            @else
                                                <dd>{{ $field->entry }}&nbsp;</dd>
                                            @endif
                                        @endif
                                    @elseif (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_MULTI_SELECT === $field->getIdentifier() && !empty(trim($field->getEntryString())))
                                        <dt>{{ $field->label }}:&nbsp;</dt>
                                        <dd>{{ $field->getEntryString() }}&nbsp;</dd>
                                    @endif
                                @endforeach
                            </dl>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

<footer>
    <div class="footer-inner">
        <p class="credits">MAP YOUR CITY&reg;</p>
        @include('locations._download_app')
    </div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.min.js"></script>
<script>
(function($) {
    $('.slider').slick({dots: true});
    $('.organization__header').on('click', function() {
        $(this).toggleClass('open');
        $(this).next().slideToggle(300);
        $('html, body').animate({
            scrollTop: $(this).next().offset().top + $('window').height()
        }, 300);
    });
})(jQuery);
</script>
</body>
</html>