<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Map Your City</title>
    <link rel="icon" type="image/png" href="/assets/img/icon_app.png">
    <meta name="description" content="Map Your City">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="https://api.tiles.mapbox.com/mapbox.js/v2.1.8/mapbox.css" rel="stylesheet">
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick-theme.css"/>
    <link rel="stylesheet" href="{!! asset('assets/css/normalize.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/map.css') !!}">
</head>
<body class="">
    <div id="map"></div>
    <div class="filter">
        <div class="inner">
            <form>
                <div class="form-group" style="margin-top: 0;">
                    <label for="q">Name or address</label>
                    <input class="form-control" type="text" name="term" id="term" placeholder="Search">
                </div>
                <a href="#" class="reset-filter">Reset all filters</a>
            </form>
        </div>
    </div>
    <div class="details"></div>
    <script src="//api.tiles.mapbox.com/mapbox.js/v2.1.8/mapbox.js"></script>
    <script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.syphon/0.4.1/backbone.syphon.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.min.js"></script>

    <script src="{!! asset('assets/js/map.js') !!}"></script>
    <script>
        $('form').on('keyup keypress', function(evt) {
            // Prevent form being submitted on enter
            if (evt.which == 13 && evt.target.type !== 'textarea') {
                evt.preventDefault();
            }
        });

        L.mapbox.accessToken = '{{ config('services.mapbox.public_token') }}';
        $(function() {
            var pins = new PinCollection({!! json_encode($pins['data']) !!});
            var state = new State({}, {
                pins: pins
            });
            var filterView = new FilterView({state: state});
            var detailView = new DetailView({state: state});
            @if ($pins['bounding_box'])
                var mapView = new MapView({
                    bounds: {!! json_encode($pins['bounding_box']) !!},
                    state: state
                });
            @else
                var mapView = new MapView({
                    bounds: {!! json_encode($pins['bounding_box']) !!},
                    state: state
                });
            @endif
        });
    </script>
</body>
</html>