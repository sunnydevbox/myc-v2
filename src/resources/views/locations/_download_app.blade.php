<p class="download-app-container">
    Download the app now:
    <a href="{!! config('app.mobile_app_download_url.android') !!}" target="_blank" class="download-app ios"><i class="fa fa-android"></i></a>
    <a href="{!! config('app.mobile_app_download_url.ios') !!}" target="_blank" class="download-app android"><i class="fa fa-apple"></i></a>
</p>
