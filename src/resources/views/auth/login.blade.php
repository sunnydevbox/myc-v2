@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="logo" class="big"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if (Session::has('flash_notification'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">{{ Session::get('flash_notification.message') }}</div>
                            {{ Session::forget('flash_notification') }}
                        @endif
                        {!! Form::open(['route' => 'auth.post_login']) !!}
                            <div class="form-group">
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                            </div>
                            <div class="form-group margin-top">
                                {!! Form::button('<span class="fui-user"></span> Login', array('type' => 'submit', 'class' => 'btn btn-primary btn-wide')) !!}
                            </div>
                        {!! Form::close() !!}
                        <small>{!! link_to_route('password.get_email', 'Forgot my password'); !!}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
<script>
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    if(!$.cookie('myc-mobile-warning')) {
        document.location.href = '{!! url('mobile-warning') !!}';
    }
}
</script>
@stop