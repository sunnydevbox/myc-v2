@extends('layouts.master')

@section('viewport')
<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="logo" class="big"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Click the button below to confirm your account.</p>

                        <form class="form-horizontal" role="form" method="POST" action="{!! route('auth.confirm') !!}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="confirmation_code" value="{{ $confirmationCode }}">
                            <input type="hidden" name="email" value="{{ $email }}">

                            <button type="submit" class="btn btn-primary">Confirm Your Account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
