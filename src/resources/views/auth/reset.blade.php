@extends('layouts.master')

@section('viewport')
<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
@stop

@section('content')

<?php $old_email = old('email'); ?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div id="logo" class="big"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                	@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

                    @if ($errors && count($errors) > 0)
						<div class="alert alert-danger">

                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach

                        </div>
                    @endif

					<h4>Reset your password</h4>
                    {!! Form::open(['route' => 'password.post_reset']) !!}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            {!! Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                        </div>

						<div class="form-group">
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm password']) !!}
                        </div>


                        <div class="form-group margin-top">
                            {!! Form::button('Reset password', array('type' => 'submit', 'class' => 'btn btn-primary btn-wide')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
