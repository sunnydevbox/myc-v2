@extends('layouts.master')

@section('viewport')
<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
@stop

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div id="logo" class="big"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                	@if (session('status'))
						<div class="alert alert-success">{{ session('status') }}</div>
					@endif

                    @include('admin.partials._flash')
					<h3 style="margin-bottom: 20px; text-align: center;">Register</h3>

                    {!! Form::open(['route' => 'auth.post_register']) !!}
                        <input type="hidden" name="token" value="{{ old('token', $token) }}">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}"">
                            {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => 'First name']) !!}
                            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="row">
                            <div class="col-md-3 form-group{{ $errors->has('last_name_prefix') ? ' has-error' : '' }}"">
                                {!! Form::text('last_name_prefix', old('last_name_prefix'), ['class' => 'form-control', 'placeholder' => 'Prefix']) !!}
                                {!! $errors->first('last_name_prefix', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-md-9 form-group{{ $errors->has('last_name') ? ' has-error' : '' }}"">
                                {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => 'Last name']) !!}
                                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"">
                            {!! Form::email('email', old('email', $email), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"">
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}"">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm password']) !!}
                            {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group margin-top">
                            {!! Form::button('Register', array('type' => 'submit', 'class' => 'btn btn-primary btn-wide')) !!}
                        </div>
                    {!! Form::close() !!}
                    <small>{!! link_to_route('auth.get_login', 'Back to login'); !!}</small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
