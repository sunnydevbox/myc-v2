@extends('layouts.master')

@section('viewport')
<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="logo" class="big"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Hi there,</p>
                        <p>Your password has been changed.</p>
                        <p><a href="mapyourcity://">Open the app</a></p>

                        <p>Or download the app now:</p>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-4"><a href="{!! config('app.mobile_app_download_url.ios') !!}" target="_blank" class="download-app android">iOS</a></div>
                                <div class="col-xs-4"><a href="{!! config('app.mobile_app_download_url.android') !!}" target="_blank" class="download-app ios">Android</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop