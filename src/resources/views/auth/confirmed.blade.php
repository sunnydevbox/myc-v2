@extends('layouts.master')

@section('viewport')
<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="logo" class="big"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p>Hi there,</p>
                        <p>Thank you for confirming!</p>
                        <p>Download the app now:</p>
                        <p><a href="{!! config('app.mobile_app_download_url.android') !!}" target="_blank" class="download-app ios">Download app for Android</a></p>
                        <p><a href="{!! config('app.mobile_app_download_url.ios') !!}" target="_blank" class="download-app android">Download app for iOS</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
