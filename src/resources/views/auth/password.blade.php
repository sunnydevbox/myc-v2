@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div id="logo" class="big"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                	@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

                    @if ($errors && count($errors) > 0)
						<div class="alert alert-danger">

                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach

                        </div>
                    @endif
					<h4>Reset your password</h4>
                    {!! Form::open(['route' => 'password.post_email']) !!}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'value' => old('email')]) !!}
                        </div>
                        <div class="form-group margin-top">
                            {!! Form::button('<span class="fui-mail"></span> Send Password Reset Link', array('type' => 'submit', 'class' => 'btn btn-primary btn-wide')) !!}
                        </div>
                    {!! Form::close() !!}
                    <small>{!! link_to_route('auth.get_login', 'Back to login'); !!}</small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
