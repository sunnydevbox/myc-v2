@if(Session::has('flash_notification'))
	@if(Session::has('overlay'))
		@include('admin.partials._modal', ['modalClass' => 'flash-modal', 'title' => 'Notice', 'body' => Session::get('flash_notification.message')])
	@else
		<div class="alert alert-{{ Session::get('flash_notification.level') }}">
			<a href="#" class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
            {!! Session::get('flash_notification.message') !!}
		</div>
	@endif
	{{ Session::forget('flash_notification') }}
@endif