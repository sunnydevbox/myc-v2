<div id="logo"></div>
<ul class="nav nav-list">
	@if (has_role('admin'))
		<li {!! active_state('admin.dashboard') !!}>{!! link_to_route('admin.dashboard', 'Dashboard'); !!}</li>
	@endif
	<li {!! active_state('admin.locations.index') !!}>{!! link_to_route('admin.locations.index', 'Locations'); !!}</li>
	<li {!! active_state('admin.organizations.index') !!}>{!! link_to_route('admin.organizations.index', 'Organizations'); !!}</li>
	@if (has_role('admin'))
		<li {!! active_state('admin.comments.index') !!}>{!! link_to_route('admin.comments.index', 'Comments'); !!}</li>
		<li {!! active_state('admin.tags.index') !!}>{!! link_to_route('admin.tags.index', 'Tags'); !!}</li>
    	<li {!! active_state('admin.users.index') !!}>{!! link_to_route('admin.users.index', 'Users'); !!}</li>
    	<li {!! active_state('admin.disapprovals.index') !!}>{!! link_to_route('admin.disapprovals.index', 'Alerts'); !!}</li>
		<li {!! active_state('admin.import.create') !!}>{!! link_to_route('admin.import.create', 'Import'); !!}</li>
        <li {!! active_state('admin.export.index') !!}>{!! link_to_route('admin.export.index', 'Export'); !!}</li>
	@endif
</ul>

