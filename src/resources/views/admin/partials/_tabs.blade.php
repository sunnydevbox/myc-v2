<ul class="nav nav-tabs border-bottom">
    @foreach($tabs as $key => $value)
        <li {!! ($key === $active) ? 'class="active"' : '' !!}><a href="{!! route($baseRoute.'.'.$key, ['id' => to_hashid($user->id)]) !!}">{{ $value }}</a></li>
    @endforeach
</ul>
