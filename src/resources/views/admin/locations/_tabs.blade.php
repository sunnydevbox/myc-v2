<?php
// TODO move to view composer
$tabs = [
        'edit' => 'Info',
        'get_images' => 'Images',
        'get_organization_profiles' => 'Organization Profiles',
        'get_comments' => 'Comments',
        'get_favorites' => 'Likes',
];
?>

<div class="col-md-12">
    <h1 class="page-title">Edit {{ $location->display_name }}</h1>
    <ul class="nav nav-tabs border-bottom">
        @foreach($tabs as $key => $value)
            <li {!! ($key === $active) ? 'class="active"' : '' !!}><a href="{!! route('admin.locations.'.$key, ['id' => to_hashid($location->id)]) !!}">{{ $value }}</a></li>
        @endforeach
    </ul>
</div>
