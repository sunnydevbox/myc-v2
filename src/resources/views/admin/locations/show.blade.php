@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="inline-block"><h1 class="page-title">Manage locations</h1></div>
            <div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.locations.create') !!}">Add</a></div>
        </div>
        <div class="col-md-4 text-right">
            <div class="form-group">
                <div class="input-group input-group-sm">
                    <input class="form-control input-append" type="search" placeholder="Search">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><span class="fui-search"></span></button>
					</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <table class="table table-bordered table-rounded index-table text-left">
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Lat</th>
                    <th>Lng</th>
                    <th></th>
                </tr>
                @foreach ($locations as $location)
                    <tr>
                        <td>
                            <label class="checkbox" for="check{{ $location->id }}">
                                <input type="checkbox" value="1" id="check{{ $location->id }}" data-toggle="checkbox">
                            </label>
                        </td>
                        <td>{{ $location->name }}</td>
                        <td>{{ $location->address }}</td>
                        <td>{{ $location->lat }}</td>
                        <td>{{ $location->lng }}</td>
                        <td><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.locations.edit', ['id' => $location->id]) !!}">Edit</a></td>
                    </tr>
                @endforeach
            </table>
            <div class="pagination">
                {!! $locations->render() !!}
            </div>
        </div>
    </div>
@stop