@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.locations._tabs', ['location' => $location, 'active' => 'edit'])
    </div>
    @if( $errors->has('generic') )
    <div class="alert alert-danger">
        {!! $errors->first('generic') !!}
    </div>
    @endif
    {!! Form::model($location, ['route' => ['admin.locations.update', to_hashid($location->id)], 'method' => 'PUT']) !!}
    <div class="row">
        <div class="col-md-6">
            <div id="map"></div>
            <input id="address-input" type="text" placeholder="Enter a location">

            <div class="row">
                <div class="form-group col-md-4">
                    {!! Form::label('lat', 'Latitude') !!}
                    {!!	Form::text('lat', null, ['class' => 'form-control preview-address', 'readonly' => 'true']) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('lon', 'Longitude') !!}
                    {!!	Form::text('lon', null, ['class' => 'form-control preview-address', 'readonly' => 'true']) !!}
                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary btn-embossed pull-right geocode margin-top-40"><span class="fui-location"></span> Refresh data</button>
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('street_name') ? ' has-error' : '' }}">
                {!! Form::label('street_name', 'Street name') !!}
                {!! Form::text('street_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('street_name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="row">
                <div class="form-group col-md-6{{ $errors->has('street_number') ? ' has-error' : '' }}">
                    {!! Form::label('street_number', 'Street number') !!}
                    {!! Form::text('street_number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('street_number', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                    {!! Form::label('postal_code', 'Postal code') !!}
                    {!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('postal_code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                {!! Form::label('city', 'City') !!}
                {!! Form::text('city', null, ['class' => 'form-control']) !!}
                {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                {!! Form::label('country', 'Country') !!}
                {!! Form::text('country', null, ['class' => 'form-control']) !!}
                {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                {!! Form::label('status', 'Status') !!}
                <select id="status" name="status" class="form-control">
                    @foreach([Myc\Domain\Locations\Location::STATUS_DRAFT, Myc\Domain\Locations\Location::STATUS_PUBLISHED] as $status)
                        <option value="{{ $status }}" {!! option_selected($status, [$location->status, old('status')]) !!}>{{ status_label($status) }}</option>
                    @endforeach
                </select>
                @if ($location->isPublished())
                    <p class="help-block">Please note that changing the status to draft will make the public profile of this Location unavailable</p>
                @endif
            </div>

            @if ($location->isPublished())
                <a class="btn btn-default btn-sm btn-primary btn-embossed" href="{!! route('locations.show_public_profile', ['id' => to_hashid($location->id)]) !!}" target="_blank"><i class="fa fa-external-link"></i> Public profile</a>
            @endif

            <div class="form-group tags">
                <h3>Categories/Filters</h3>
                @foreach($categories as $category)
                    <h4>{{ $category->name }}</h4>
                    @foreach($category->tags as $tag)
                        <div class="tag">
                            @if (in_array($tag->id, $location->tags->lists('id')))
                                <span class="label label-default label-primary">{{ $tag->name }}</span>
                                <input type="checkbox" name="tags[]" value="{{ $tag->id }}" checked class="hide" />
                            @else
                                <span class="label label-default">{{ $tag->name }}</span>
                                <input type="checkbox" name="tags[]" value="{{ $tag->id }}" class="hide" />
                            @endif
                        </div>
                    @endforeach
                @endforeach
            </div>

            <div class="form-group margin-top">
                {!! link_to_route('admin.locations.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                {!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        window.lat = '{{ $location->lat }}';
        window.lng = '{{ $location->lon }}';
        // Values voor Google Maps API

        $(document).ready(function(){
            var $geocodeBtn = $('button.geocode'),
                $inputLat = $('input#lat'),
                $inputLon = $('input#lon'),
                $inputStreetName = $('input#street_name'),
                $inputStreetNumber = $('input#street_number'),
                $inputPostalCode = $('input#postal_code'),
                $inputLocality = $('input#city'),
                $inputCountry = $('input#country'),
                $removeImageModal = $('.remove-image-modal');


            // TODO fix styling for selectboxes
            //$('#status').select2({dropdownCssClass: 'dropdown-inverse'});
            $('form').on('keyup keypress', function(evt) {
                // Prevent form being submitted on enter
                if (evt.which == 13 && evt.target.type !== 'textarea') {
                    evt.preventDefault();
                }
            });

            $('form.delete-image-form').submit(function(evt){
                evt.preventDefault();
                $removeImageModal.data('form', $(this).attr('id'));
                $removeImageModal.modal('show');
            });

            $geocodeBtn.on('click', function(evt) {
                evt.preventDefault();
                confirm('Overwriting current address data, do you want to continue?');
                var xhr = $.ajax({
                    url: '/geocode',
                    type: 'GET',
                    data: {latlon: $inputLat.val() + ',' + $inputLon.val()}
                });

                xhr.done(function(resp, textStatus, jqXHR) {
                    // Set fields to values from /geocode route
                    var address = resp.data;
                    $inputStreetName.val(address.street_name);
                    $inputStreetNumber.val(address.street_number);
                    $inputPostalCode.val(address.postal_code);
                    $inputLocality.val(address.city);
                    $inputCountry.val(address.country);
                });
            });

            $('.delete-confirm').on('click', function() {
                form = $('#'+$('.remove-image-modal').data('form'));
                form.off('submit');
                form.submit();
            });

            $('.images-container').sortable({
                axis: 'y',
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: '{{ route('admin.images.sort-order') }}'
                    });
                }
            }).disableSelection();

        });
    </script>
@stop