@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="inline-block"><h1 class="page-title">Manage Locations</h1></div>
			<div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.locations.create') !!}">Add</a></div>
		</div>
	</div>
	<form method="get">
	<div class="row">
		<div class="col-md-3 form-group">
			<div class="input-group input-group-sm" style="width: 100%;">
				<select class="form-control" id="categories-filter" name="tags[]" onchange="this.form.submit()">
					<option value="">Filter by category</option>
					@foreach ($categories as $category)
						<optgroup label="{{ $category->name }}">
							@foreach ($category->tags as $tag)
								<option value="{{ $tag->id }}" {!! option_selected($tag->id, isset($_GET['tags']) ? $_GET['tags'] : []) !!}>{{ $tag->name }}</option>
							@endforeach
						</optgroup>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-3 form-group">
		@if(has_role('admin'))
			<div class="input-group input-group-sm" style="width: 100%;">
				<select class="form-control" id="categories-filter" name="organizations[]" onchange="this.form.submit()">
					<option value="">Filter by organization</option>
					@foreach ($organizations as $organization)
						<option value="{{ $organization->id }}" {!! option_selected($organization->id, isset($_GET['organizations']) ? $_GET['organizations'] : []) !!}>{{ $organization->name }}</option>
					@endforeach
				</select>
			</div>
		@endif
		</div>
		<div class="col-md-3 col-md-offset-3">
			<div class="input-group input-group-sm pull-right">
				<input class="form-control input-append" type="search" name="q" placeholder="Search" value="{{ !empty($_GET['q']) ? $_GET['q'] : '' }}">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit"><span class="fui-search"></span></button>
				</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		@if(!isset($_GET['trash']))
			<a href="#" class="trash disabled" style="float: left;"><span class="fui-trash"></span></a>
			{!! link_to_route('admin.locations.index', 'Show trash', ['trash' => true], ['class' => 'link-small']) !!}
		@else
			<a href="#" class="trash recover disabled" style="float: left;"><span class="fui-check"></span></a>
			{!! link_to_route('admin.locations.index', 'Cancel', false, ['class' => 'link-small']) !!}
		@endif
		</div>
		<div class="col-md-6 text-right per-page-container">
			<input type="hidden" name="per-page" value="{{ $perPageSelected }}" />

			<ul>
				<li>Items per page:</li>
			@foreach ($perPage as $item)
				<li><span class="per-page {{ $perPageSelected == $item ? 'active' : '' }}" data-perpage="{{ $item }}">{{ $item }}</span></li>
			@endforeach
			</ul>
		</div>
	</div>
	</form>

	<div class="row">
		@include('admin.locations._table', [
			'locations' => $locations,
			'allowDelete' => true
		])
	</div>
@stop

@section('scripts')
<script>
    $('.per-page').on('click', function(){
    	$('[name=per-page]').val($(this).data('perpage'));
    	$(this).closest('form').submit();
    });

    $('#check-all').on('change.radiocheck', function() {
    	if($(this).is(':checked')) {
    		$('.trash-checkbox').radiocheck('check');
    	} else {
    		$('.trash-checkbox').radiocheck('uncheck');
    	}
    });

    var recoverUrl = '{{ route('admin.locations.recover_multiple') }}';
</script>
@stop
