<div class="row">
    <div class="col-md-6 clearfix field-group">
        {!! Form::open(['route' => ['admin.locations.put_organization_profile', to_hashid($location->id), to_hashid($profile->organization->id)], 'method' => 'PUT', 'class' => 'organization-profile clearfix']) !!}
        <div class="row-fluid">
        	<div class="col-md-9">
        		<h3>{!! organization_thumbnail($profile->organization->image) !!} <span>{{ $profile->organization->name }}</span></h3>
        	</div>
        	<div class="col-md-3 text-right" style="padding:16px 0 0 0">
        		<input type="checkbox" {!! $profile->isActive() ? 'checked="checked"' : '' !!} data-toggle="switch" data-on-text="<i class='fui-check'></i>" data-off-text="<i class='fui-cross'></i>" />
        	</div>
        </div>
        <div class="clearfix"></div>
        <div class="fields {!! !$profile->isActive() ? 'hide' : '' !!}">
        @foreach($profile->getFields() as $field)
            @include($field->getView(), ['field' => $field])
        @endforeach
            <input type="hidden" id="remove_profile" name="remove_profile" value="0"/>
        </div>
        <button class="btn btn-embossed btn-primary pull-right {!! !$profile->isActive() ? 'hide' : '' !!}" type="submit">Save</button>
        {!! Form::close() !!}
    </div>
</div>
