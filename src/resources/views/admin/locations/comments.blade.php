@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.locations._tabs', ['location' => $location, 'active' => 'get_comments'])
    </div>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4 text-right">
            <form>
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <input class="form-control input-append" type="search" name="q" placeholder="Search" value="{{ !empty($_GET['q']) ? $_GET['q'] : '' }}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><span class="fui-search"></span></button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12 text-center">
            <table class="table table-bordered table-rounded text-left">
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>{!! order_link('users.email', 'Email') !!}</th>
                    <th>{!! order_link('comments.created_at', 'Date') !!}</th>
                    <th>Comment</th>
                </tr>
                @foreach ($comments as $comment)
                    <tr>
                        <td style="vertical-align: top;">
                            {!! Form::open(['route' => ['admin.comments.delete', to_hashid($comment->id)], 'method' => 'DELETE']) !!}
                            <button class="btn btn-xs btn-danger btn-embossed" type="submit" onclick="return confirm('Are you sure you want to delete this comment?')"><i class="fa fa-trash-o"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                        <td style="vertical-align: top; width: 200px;">{{ $comment->user->display_name }}</td>
                        <td style="vertical-align: top; width: 200px;">{!! to_link($comment->user->email) !!}</td>
                        <td style="vertical-align: top; width: 200px;">{{ $comment->created_at->format('d-m-Y H:i:s') }}</td>
                        <td style="vertical-align: top;">{{ $comment->body }}</td>
                    </tr>
                    @foreach ($comment->comments as $nested)
                        <tr>
                            <td style="vertical-align: top;">
                                {!! Form::open(['route' => ['admin.comments.delete', to_hashid($nested->id)], 'method' => 'DELETE']) !!}
                                <button class="btn btn-xs btn-danger btn-embossed" type="submit" onclick="return confirm('Are you sure you want to delete this comment?')"><i class="fa fa-trash-o"></i> Delete</button>
                                {!! Form::close() !!}
                            </td>
                            <td style="vertical-align: top;"><span class="comment-tree"></span> {{ $nested->user->display_name }}</td>
                            <td style="vertical-align: top;">{!! to_link($nested->user->email) !!}</td>
                            <td style="vertical-align: top;">{{ $nested->created_at->format('d-m-Y H:i:s') }}</td>
                            <td style="vertical-align: top; padding-left: 60px;">{{ $nested->body }}</td>
                        </tr>
                    @endforeach
                @endforeach
            </table>
        </div>
    </div>
@stop

@section('scripts')
@stop