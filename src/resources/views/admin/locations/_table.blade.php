<div class="col-md-12 text-center">
    @if ($allowDelete)
        {!! Form::open(['route' => ['admin.locations.delete_multiple'], 'method' => 'POST']) !!}
    @endif
    <table class="table table-bordered table-condensed table-rounded index-table text-left">
        <tr>
            @if ($allowDelete)
                <th>
                    <label class="checkbox" for="check-all">
                        <input type="checkbox" id="check-all" data-toggle="checkbox">
                    </label>
                </th>
            @endif
            <th>{!! order_link('name', 'Name') !!}</th>
            <th>{!! order_link('street_name', 'Address') !!}</th>
            <th>{!! order_link('locality', 'City') !!}</th>
            <th>{!! order_link('created_at', 'Date/time added', true) !!}</th>
            <th>{!! order_link('favorite_count', 'Favorites') !!}</th>
            <th>{!! order_link('comment_count', 'Comment count') !!}</th>
            <th>{!! order_link('status', 'Status') !!}</th>
            <th></th>
            <td></td>
        </tr>
        @foreach ($locations as $location)
            <tr>
                @if ($allowDelete)
                <td>
                    <label class="checkbox" for="check{{ $location->id }}">
                        <input type="checkbox" id="check{{ $location->id }}" name="locations[]" value="{{ $location->id }}" class="trash-checkbox" data-toggle="checkbox">
                    </label>
                </td>
                @endif
                <td>{{ $location->name }}</td>
                <td>{{ $location->street_name }} {{ $location->street_number }}</td>
                <td>{{ $location->locality }}</td>
                <td><span class="label label-default">{{ $location->created_at->format('d-m-Y H:i') }}</span></td>
                <td>{{ $location->favorite_count }}</td>
                <td>{{ $location->comment_count }}</td>
                <td>{!! location_status($location->status) !!}</td>
                <td>
                    @if (Myc\Domain\Locations\Location::STATUS_PUBLISHED === $location->status)
                        <a target="_blank" class="small" style="margin-right: 6px; text-decoration: underline;" href="{!! route('locations.show_public_profile', ['id' => to_hashid($location->id)]) !!}">Public profile</a><i class="small fa fa-external-link" style="color: #444;"></i>
                    @endif
                </td>
                <td>
                    <a class="btn btn-xs btn-primary btn-embossed" href="{!! route('admin.locations.edit', ['id' => to_hashid($location->id)]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                </td>
            </tr>
        @endforeach
    </table>
    @if ($allowDelete)
        <?php
        $delete_actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
        $delete_actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';

        $recover_actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
        $recover_actions .= '<button type="submit" class="btn btn-primary recover-confirm">Ok</button>';
        ?>
        @include('admin.partials._modal', ['modalClass' => 'remove-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to delete these locations?', 'actions' => $delete_actions])
        @include('admin.partials._modal', ['modalClass' => 'recover-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to recover these locations?', 'actions' => $recover_actions])
        {!! Form::close() !!}
    @endif


    {!! pagination($locations) !!}
</div>