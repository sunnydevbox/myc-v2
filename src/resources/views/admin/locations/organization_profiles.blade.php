@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.locations._tabs', ['location' => $location, 'active' => 'get_organization_profiles'])
    </div>

    {{--<div class="row">
        <div class="col-md-12">
            @if ($errors->has())
                <div class="alert alert-danger">
                    <h4>{{ implode(', ', $errors->all()) }}</h4>
                </div>
            @endif
        </div>
        ^[1-9]\d{3,}$
    </div>--}}

    @foreach ($profiles as $profile)
        @include('admin.locations._organization_profile')
    @endforeach
@stop

@section('scripts')
<script>
    $('[data-toggle=switch]').on('switchChange.bootstrapSwitch', function(event, state) {
        if(state) {
            $(this).closest('.field-group').find('.fields').removeClass('hide');
            $(this).closest('.field-group').find('button[type=submit]').removeClass('hide');
            $(this).closest('.field-group').find('input#remove_profile').val('0');
        } else {
            //$(this).closest('.field-group').find('.fields input, .fields textarea, select').val('');
            $(this).closest('.field-group').find('input#remove_profile').val('1');
            $(this).closest('.field-group').find('.fields').addClass('hide');
        }
    });

    $('.form-group__url-field .link-text, .form-group__url-field .link-href').on('keyup', function(evt) {
        var $container = $(this).closest('.form-group__url-field'),
                $text = $container.find('input.link-text'),
                $href = $container.find('input.link-href'),
                $hidden = $container.find('input[type="hidden"]');

        var value;
        if ($href.val() == '') {
            value = '';
        } else {
            var hrefValue = ($href.val().indexOf('http') !== 0) ? 'http://' + $href.val() : $href.val();
            var textValue = ($text.val() == '') ? $href.val() : $text.val();
            value = '[' + textValue + '](' + hrefValue + ')';
        }
        $hidden.val(value);
    });
</script>
@stop