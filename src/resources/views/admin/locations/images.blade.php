@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.locations._tabs', ['location' => $location, 'active' => 'get_images'])
    </div>
    <div class="row">
        <div class="col-md-3">
            <h4 class="no-margin-top">Images</h4>
            <div class="images-container">
                @foreach ($location->images as $image)
                    <div id="image-container-{{ $image->id }}">
                        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.images.delete', to_hashid($location->id), to_hashid($image->id)], 'class' => 'delete-image-form', 'id' => 'delete-' . $image->id]) !!}
                        <img class="img-responsive" src="{!! route('images.get_asset', ['path' => $image->path]) !!}">
                        <div class="delete">
                            {!! Form::submit('') !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                @endforeach
            </div>

            {!! Form::open(['route' => ['admin.images.create', 'id' => to_hashid($location->id)], 'files' => true, 'class' => 'add-image-form']) !!}
            <div class="form-group">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                    <div>
                        <span class="btn btn-sm btn-primary btn-embossed btn-file">
                            <span class="fileinput-new"><span class="fui-image"></span>  Select image</span>
                            <span class="fileinput-exists"><span class="fui-gear"></span>  Change</span>
                            <input type="file" name="image" id="image">
                        </span>
                        <a href="#" class="btn btn-sm btn-primary btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>  Remove</a>
                    </div>
                </div>
            </div>
            {!! Form::submit('Save image', ['class' => 'btn btn-primary btn-embossed', 'style' => 'display: none;']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    <?php
    $actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
    $actions .= '<button type="button" class="btn btn-primary delete-confirm">Ok</button>';
    ?>
    @include('admin.partials._modal', ['modalClass' => 'remove-image-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to delete this image?', 'actions' => $actions])
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            var $removeImageModal = $('.remove-image-modal');

            $('.add-image-form input#image').on('change', function(evt) {
                if ($(evt.currentTarget).val() && $(evt.currentTarget).val() != '') {
                    $('form.add-image-form').submit();
                }
            });

            $('form.delete-image-form').submit(function(evt){
                evt.preventDefault();
                $removeImageModal.data('form', $(this).attr('id'));
                $removeImageModal.modal('show');
            });

            $('.delete-confirm').on('click', function() {
                form = $('#'+$('.remove-image-modal').data('form'));
                form.off('submit');
                form.submit();
            });

            $('.images-container').sortable({
                axis: 'y',
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url: '{{ route('admin.images.sort-order') }}'
                    });
                }
            }).disableSelection();

        });
    </script>
@stop