@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8">
			<div class="inline-block"><h1 class="page-title">Dashboard</h1></div>
		</div>
	</div>
	<div class="row">
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="inline-block"><h2 class="page-title">Latest alerts</h2></div>
			<div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.disapprovals.index') !!}">View all</a></div>
			<table class="table table-bordered table-rounded index-table text-left">
				<tr>
					<th>Address</th>
					<th>Alert type</th>
					<th>Date</th>
					<th></th>
				</tr>
				@foreach ($disapprovals as $disapproval)
				<tr>
					<td>{{ $disapproval->getAddressAttribute() }}</td>
					<td>{{ $disapproval->getTypeAttribute() }}</td>
					<td>{{ $disapproval->created_at->format('j M') }}</td>
					<td><a class="btn btn-xs btn-primary mhm" href="{!! $disapproval->getLinkAttribute() !!}">View</a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
		<div class="col-md-6">
			<div class="inline-block"><h2 class="page-title">Latest locations</h2></div>
			<div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.locations.index') !!}">View all</a></div>
			<table class="table table-bordered table-rounded index-table text-left">
				<tr>
					<th>Address</th>
					<th>City</th>
					<th>Date</th>
					<th></th>
				</tr>
				@foreach ($locations as $location)
				<tr>
					<td>{{ $location->street_name }} {{ $location->street_number }}</td>
					<td>{{ $location->city }}</td>
					<td>{{ $location->created_at->format('j M') }}</td>
					<td><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.locations.edit', to_hashid($location->id)) !!}">View</a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="inline-block"><h2 class="page-title">Latest members</h2></div>
			<div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.users.index') !!}">View all</a></div>
			<table class="table table-bordered table-rounded index-table text-left">
				<tr>
					<th>Name</th>
					<th>E-mail address</th>
					<th>Date</th>
					<th></th>
				</tr>
				@foreach ($users as $user)
				<tr>
					<td>{{ $user->getDisplayNameAttribute() }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->created_at->format('j M') }}</td>
					<td><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.users.edit', to_hashid($user->id)) !!}">View</a>
					</td>

					{{-- <td><a class="btn btn-xs btn-primary btn-embossed" href="{!! route('admin.tags.edit', ['id' => to_hashid($tag->id)]) !!}"><i class="fa fa-pencil"></i> Edit</a></td> --}}
				</tr>
				@endforeach
			</table>
		</div>
	</div>
@stop