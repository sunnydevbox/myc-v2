@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.locations._tabs', ['location' => $location, 'active' => 'get_favorites'])
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <table class="table table-bordered table-rounded text-left">
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Date</th>
                </tr>
                @foreach ($favorites as $favorite)
                    <tr>
                        <td style="vertical-align: top;">
                            {!! Form::open(['route' => ['admin.favorites.delete', to_hashid($location->id), to_hashid($favorite->id)], 'method' => 'DELETE']) !!}
                            <button class="btn btn-xs btn-danger btn-embossed" type="submit" onclick="return confirm('Are you sure you want to delete this favorite?')"><i class="fa fa-trash-o"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                        <td>{{ $favorite->display_name }}</td>
                        <td>{{ $favorite->created_at->format('d-m-Y H:i:s') }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop

@section('scripts')
@stop