@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-title">Add location</h1>
		</div>
	</div>
	{!! Form::model($location, ['route' => ['admin.locations.store'], 'method' => 'POST']) !!}

		<div class="row">
			<div class="col-md-6">
				<div id="map"></div>
                <input id="address-input" type="text" placeholder="Enter a location">

                <div class="row">
                    <div class="form-group col-md-12">
                        {!! Form::label('address', 'Address') !!}
                        {!! Form::text('address', null, ['class' => 'form-control preview-address', 'readonly' => 'true']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('lat', 'Latitude') !!}
                        {!!	Form::text('lat', null, ['class' => 'form-control preview-address', 'readonly' => 'true']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('lon', 'Longitude') !!}
                        {!!	Form::text('lon', null, ['class' => 'form-control preview-address', 'readonly' => 'true']) !!}
                    </div>
                </div>

			</div>

            <div class="col-md-6">

                <div class="form-group text-right">
                    {!! link_to_route('admin.locations.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                    {!! Form::submit('Add location', ['class' => 'btn btn-primary btn-embossed']) !!}
                </div>

            </div>
		</div>

    {!! Form::close() !!}

    <script>
        window.lat = '{{ $location->lat }}';
        window.lng = '{{ $location->lon }}';
    </script>
@stop

@section('scripts')
    <script>
        // Values voor Google Maps API
        $('form').on('keyup keypress', function(evt) {
            // Prevent form being submitted on enter
            if (evt.which == 13 && evt.target.type !== 'textarea') {
                evt.preventDefault();
            }
        });
    </script>
@stop