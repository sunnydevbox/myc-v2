@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid" style="margin-top: 15px;">
        {!! Form::open(['route' => 'admin.geocode.post_form']) !!}
        <div class="form-group">
            <label for="address_string" style="width: 80px;">Adres</label>
            <input style="width: 400px;" type="text" id="address_string" name="address_string">
        </div>
        <div class="form-group">
            <label for="address_string" style="width: 80px;">Lat/Lon</label>
            <input style="width: 400px;" type="text" id="address_string" name="lat_lon">
        </div>
        <button class="btn btn-primary" type="submit">Ok</button>
        {!! Form::close() !!}

        @foreach (old('addresses', []) as $address)
        <h4>Geocoder Object</h4>
        <pre>
            {{ print_r($address->toArray(), true) }}
        </pre>

        <h4>KML</h4>
        <pre>
            {{ (new \Geocoder\Dumper\Kml())->dump($address) }}
        </pre>

        @endforeach


    </div>
@stop

