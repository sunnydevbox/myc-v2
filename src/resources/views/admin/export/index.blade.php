@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Import Data</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="help-block"><a href="{!! route('admin.export.get_file', ['type' => 'locations']) !!}" target="_blank">Locations dump</a></p>
            <p class="help-block"><a href="{!! route('admin.export.get_file', ['type' => 'favorites']) !!}" target="_blank">Favorites dump</a></p>
            <p class="help-block"><a href="{!! route('admin.export.get_file', ['type' => 'users']) !!}" target="_blank">Users dump</a></p>
        </div>
    </div>
@stop
