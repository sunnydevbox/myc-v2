@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-title">Edit comment</h1>
		</div>
	</div>
	{!! Form::model($comment, ['route' => ['admin.comments.update', $comment->id], 'method' => 'PATCH']) !!}
		<div class="row">
			<div class="col-md-12">
                <table class="table table-bordered table-rounded text-left">
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Location</th>
                        <th>Comment</th>
                    </tr>
                    <?php
                    $address = false;
                    if($comment->location) {
                        $address = $comment->location->getAddressLineAttribute();
                    }
                    ?>
                    <tr>
                        <td style="vertical-align: top;">
                            {!! Form::open(['route' => ['admin.comments.delete', to_hashid($comment->id)], 'method' => 'DELETE']) !!}
                            <button class="btn btn-xs btn-danger btn-embossed" type="submit" onclick="return confirm('Are you sure you want to delete this comment?')"><i class="fa fa-trash-o"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                        <td style="vertical-align: top; width: 200px;">{{ $comment->user->display_name }}</td>
                        <td style="vertical-align: top; width: 200px;">{{ $comment->created_at->format('d-m-Y H:i:s') }}</td>
                        <td style="vertical-align: top; width: 200px;">{{ $address }}</td>
                        <td style="vertical-align: top;">{{ $comment->body }}</td>
                    </tr>
                    @foreach ($comment->comments as $nested)
                        <?php
                        $nested_address = false;
                        if($nested->location) {
                            $address = $nested->location->getAddressLineAttribute();
                        }
                        ?>
                        <tr>
                            <td style="vertical-align: top;">
                                {!! Form::open(['route' => ['admin.comments.delete', to_hashid($nested->id)], 'method' => 'DELETE']) !!}
                                <button class="btn btn-xs btn-danger btn-embossed" type="submit" onclick="return confirm('Are you sure you want to delete this comment?')"><i class="fa fa-trash-o"></i> Delete</button>
                                {!! Form::close() !!}
                            </td>
                            <td style="vertical-align: top;"><span class="comment-tree"></span> {{ $nested->user->display_name }}</td>
                            <td style="vertical-align: top;">{{ $nested->created_at->format('d-m-Y H:i:s') }}</td>
                            <td style="vertical-align: top;">{{ $nested_address }}</td>
                            <td style="vertical-align: top; padding-left: 60px;">{{ $nested->body }}</td>
                        </tr>
                    @endforeach
                </table>
			</div>
        </div>
    {!! Form::close() !!}
@stop
