@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-title">Edit Tag "{{ $tag->name }}"</h1>
		</div>
	</div>
	{!! Form::model($tag, ['route' => ['admin.tags.update', $tag->id], 'method' => 'PATCH']) !!}
		<div class="row">
			<div class="col-md-6">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    {!! Form::label('category_id', 'Category') !!}
                    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group margin-top">
                    {!! link_to_route('admin.tags.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                    {!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
                </div>
			</div>
        </div>
    {!! Form::close() !!}
@stop
