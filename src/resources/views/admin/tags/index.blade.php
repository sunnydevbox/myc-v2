@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8">
			<div class="inline-block"><h1 class="page-title">Manage Tags</h1></div>
			<div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.tags.create') !!}">Add</a></div>
		</div>
		<div class="col-md-4 text-right">
			<form class="form-group">
				<div class="input-group input-group-sm">
					<input class="form-control input-append" type="search" name="q" placeholder="Search" value="{{ !empty($_GET['q']) ? $_GET['q'] : '' }}">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit"><span class="fui-search"></span></button>
				</span>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="#fakelink" class="trash disabled"><span class="fui-trash"></span></a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			{!! Form::open(['route' => ['admin.tags.delete_multiple'], 'method' => 'POST']) !!}
			<table class="table table-bordered table-rounded index-table text-left">
				<tr>
					<th></th>
					<th>ID</th>
					<th>{!! order_link('name', 'Name') !!}</th>
					<th>{!! order_link('category', 'Category', true) !!} </th>
					<th></th>
				</tr>
				@foreach ($tags as $tag)
				<tr>
					<td>
						<label class="checkbox" for="check{{ $tag->id }}">
							<input type="checkbox" name="tags[]" value="{{ $tag->id }}" class="trash-checkbox" data-toggle="checkbox">
						</label>
					</td>
					<td>{{ $tag->id }}</td>
					<td>{{ $tag->name }}</td>
					<td>{{ $tag->category->name }}</td>
					<td><a class="btn btn-xs btn-primary btn-embossed" href="{!! route('admin.tags.edit', ['id' => to_hashid($tag->id)]) !!}"><i class="fa fa-pencil"></i> Edit</a></td>
				</tr>
				@endforeach
			</table>
			<?php
			$actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
			$actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';
			?>
			@include('admin.partials._modal', ['modalClass' => 'remove-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to delete these tags?', 'actions' => $actions])
			{!! Form::close() !!}
			{!! pagination($tags) !!}
		</div>
	</div>
@stop