@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8">
			<div class="inline-block"><h1 class="page-title">Manage alerts</h1></div>
		</div>
		<form>
		<div class="col-md-4 text-right">
			<div class="form-group">
				<div class="input-group input-group-sm">
					<input class="form-control input-append" type="search" name="q" placeholder="Search" value="{{ !empty($_GET['q']) ? $_GET['q'] : '' }}">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><span class="fui-search"></span></button>
					</span>
				</div>
			</div>
		</div>
		</form>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="#" class="trash disabled"><span class="fui-trash"></span></a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			{!! Form::open(['route' => ['admin.disapprovals.delete_multiple'], 'method' => 'POST']) !!}
			<table class="table table-bordered table-rounded index-table text-left">
				<tr>
					<th></th>
					<th>Address</th>
					<th>Date added</th>
					<th>Alert type</th>
					<th></th>
				</tr>
				@foreach ($disapprovals as $disapproval)
				<tr>
					<td>
						<label class="checkbox" for="check{{ $disapproval->id }}">
							<input type="checkbox" name="disapprovals[]" value="{{ $disapproval->id }}" class="trash-checkbox" data-toggle="checkbox">
						</label>
					</td>
					<td>{{ $disapproval->getAddressAttribute() }}</td>
					<td>{{ $disapproval->created_at->format('Y-m-d') }}</td>
					<td>{{ $disapproval->getTypeAttribute() }}</td>
					<td>
						<a class="action btn btn-xs btn-primary mhm" href="{{ $disapproval->getLinkAttribute() }}">Edit</a>
					</td>
				</tr>
				@endforeach
			</table>
			<?php
			$actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
			$actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';
			?>
			@include('admin.partials._modal', ['modalClass' => 'remove-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to delete these alerts?', 'actions' => $actions])

			{!! Form::close() !!}
			{!! $disapprovals->render() !!}
		</div>
	</div>
@stop