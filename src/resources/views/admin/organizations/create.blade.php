@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-title">Add Organization</h1>
		</div>
	</div>
	{!! Form::model($organization, ['route' => ['admin.organizations.store'], 'method' => 'POST', 'files' => true]) !!}
		<div class="row">
			<div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('status', 'Status') !!}
                    <select id="status" name="status" class="form-control">
                        @foreach([Myc\Domain\Organizations\Organization::STATUS_DISABLED, Myc\Domain\Organizations\Organization::STATUS_ACTIVE] as $status)
                            <option value="{{ $status }}" {!! option_selected($status, old('status')) !!}>{{ organization_status_label($status) }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group {{ error_class($errors, 'name') }}">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group {{ error_class($errors, 'pay-off') }}">
                    {!! Form::label('pay_off', 'Pay off') !!}
                    {!! Form::text('pay_off', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('pay_off', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('website', 'Website') !!}
                    {!! Form::text('website', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group {{ error_class($errors, 'email_address') }}">
                    {!! Form::label('email_address', 'Email address') !!}
                    {!! Form::text('email_address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email_address', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group margin-top">
                    {!! link_to_route('admin.organizations.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                    {!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
                </div>
			</div>

            <div class="col-md-3">
                <h4 class="no-margin-top">Profile image</h4>
                <p class="small">For best results we recommend a square image of at least 512x512 pixels.</p>
                <div class="form-group">
                  <div class="fileinput fileinput-new profile-image-container" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="background-image:url({!! route('images.get_asset', ['path' => $organization->image]) !!});"></div>
                    <div>
                      <span class="hide">
                         <input type="file" name="image">
                      </span>
                    </div>
                  </div>
                </div>

                <h4 class="no-margin-top">Header image</h4>
                <p class="small">A high-resolution image in landscape orientation works best.</p>
                <div class="form-group">
                  <div class="fileinput fileinput-new image-container" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"><div style="background-image:url({!! route('images.get_asset', ['path' => $organization->header_image]) !!});"></div></div>
                    <div>
                      <span class="hide">
                         <input type="file" name="header_image">
                      </span>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            $('form').on('keyup keypress', function(evt) {
                // Prevent form being submitted on enter
                if (evt.which == 13 && evt.target.type !== 'textarea') {
                    evt.preventDefault();
                }
            });
        });
    </script>
@stop