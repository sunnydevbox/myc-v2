@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.organizations._tabs', ['organization' => $organization, 'active' => 'get_bulk_import'])
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <a href="{{ route('admin.organizations.get_bulk_import_template', ['id' => to_hashid($organization->id)]) }}">Download template</a>
            </div>
            <h4 style="margin-top: 20px;">Tag IDs</h4>
            <table style="font-size: 12px; width: 500px;">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                @foreach ($tags as $tag)
                    <tr>
                        <td><strong>{!! $tag->id !!}</strong></td>
                        <td>{!! $tag->name !!}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2">
                        <strong>Values must be formatted as a comma separated list of IDs (e.g. 1,2,3).</strong>
                    </td>
                </tr>
            </table>
            <h4 style="margin-top: 20px;">Profile fields</h4>
            @foreach($profile->getFields() as $field)
                <table style="font-size: 12px; width: 500px; margin-top: 16px;">
                    <tr>
                        <td style="width: 140px;">Label</td>
                        <td>{!! $field->getLabel() !!}</td>
                    </tr>
                    <tr>
                        <td>ID</td>
                        <td><strong>{!! $field->getId() !!}</strong></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>{!! $field->getIdentifier() !!}</td>
                    </tr>

                    @if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_MULTI_SELECT === $field->getIdentifier())
                    <tr>
                        <td>Allowed values</td>
                        <td>{!! options_to_html_table($field->getOptions()) !!}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <strong>Values must be formatted as a comma separated list of IDs (e.g. 1,2,3).</strong>
                        </td>
                    </tr>
                    @endif

                    @if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_SELECT === $field->getIdentifier())
                        <tr>
                            <td>Allowed values</td>
                            <td>{!! implode(', ', $field->getOptions()) !!}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Value must be exact match of one of the options (case sensitive!).</strong>
                            </td>
                        </tr>
                    @endif

                </table>
            @endforeach

            <p style="font-size: 14px; margin-top: 20px;">Fields of type <strong>url</strong> must be in Markdown format: [The name of the URL](http://link.to.website).</p>

            <p>All columns in Excel file <strong>must be formatted as text</strong> and file must be saved in Excel5 format.</p>

            {!! Form::open(['route' => ['admin.organizations.post_bulk_import', to_hashid($organization->id)], 'method' => 'POST', 'files' => true]) !!}
            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}" style="margin-top: 20px;">
                <h4 style="margin-top: 30px;">Select file</h4>
                <span class="btn btn-primary btn-file btn-embossed">
                    Select File
                    {!! Form::file('file', null, ['class' => 'form-control']) !!}
                </span>
                {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Start', ['class' => 'btn btn-primary btn-embossed']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $(document).ready( function() {
                $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                    $(this).parent().parent().find('.label.file').remove();
                    $(this).parent().after('<span class="file label label-default" style="display: inline-block; margin-left: 10px;">'+ label +'</span>');
                });
            });
        });
    </script
@stop
