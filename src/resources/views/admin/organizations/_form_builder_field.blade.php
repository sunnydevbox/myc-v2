<div class="profile-field-container clearfix {{ !$field->isActive() ? 'is-hidden' : '' }}" data-id="{{ to_hashid($field->id) }}">
    {!! Form::open(['route' => ['admin.profile_fields.update', to_hashid($field->id)], 'method' => 'PUT']) !!}
    <h4>
        <i class="fa fa-bars"></i>
        <input class="field-element" type="hidden" disabled="disabled" value="{{ to_hashid($field->id) }}" name="hashid" />
        <input class="field-element" type="hidden" disabled="disabled" value="{{ $field->getIdentifier() }}" name="field_type" />
        <input class="field-element" type="text" disabled="disabled" value="{{ $field->label }}" name="label" />
        <span class="value-field">{{ $field->label }}</span>
        <label class="disable"><input type="checkbox" disabled="disabled" value="1" class="field-element" name="is_required" {!! $field->isRequired() ? 'checked="checked"' : '' !!} />&nbsp;Required</label>
        <button class="inline-block btn btn-xs btn-primary btn-embossed btn-sm disable save-field">Save</button>
        {!! !$field->isActive() ? '<span class="label label-warning">Field is hidden</span>' : '' !!}
    </h4>
    <span class="label label-primary">{{ field_type_label($field->getIdentifier())}}</span>
    {!! $field->isRequired() ? '<span class="label value-field label-info">Is required</span>' : '' !!}

    @if (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_SELECT === $field->getIdentifier())
        <div class="select-field-options-container" style="margin: 10px 0;">
            <span class="options small">Available options</span>
            @foreach ($field->options as $option)
                <div class="option label label-primary value-field">{{ $option}}</div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-4">
                @foreach ($field->options as $option)
                    <div class="form-group">
                        <input type="text" name="options[]" class="form-control disable" value="{{ $option }}">
                    </div>
                @endforeach
                <button class="btn btn-xs btn-embossed btn-primary add-option disable" type="button">Add option</button>
            </div>
        </div>
    @elseif (\Myc\Domain\Profiles\Fields\FieldType::FIELD_TYPE_MULTI_SELECT === $field->getIdentifier())
        <div class="select-field-options-container" style="margin: 10px 0;">
            <span class="options small">Available options</span>
            @foreach ($field->getOptions() as $option)
                <div class="option label label-primary value-field">{{ $option['value'] }}</div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-4">
                @foreach ($field->getOptions() as $option)
                    <div class="form-group">
                        <input type="text" name="options[]" class="form-control disable" value="{{ $option['value'] }}">
                    </div>
                @endforeach
                <button class="btn btn-xs btn-embossed btn-primary add-option disable" type="button">Add option</button>
            </div>
        </div>
    @endif
    {!! Form::close() !!}

    <div class="actions" style="text-align: right;">
        @if ($field->isActive())
            {!! Form::open(['route' => ['admin.profile_fields.hide', to_hashid($field->id)], 'method' => 'PUT']) !!}
            <button class="inline-block btn btn-xs btn-default btn-embossed btn-sm" type="submit"><i class="fa fa-times"></i> Hide this field</button>
            {!! Form::close() !!}
        @else
            {!! Form::open(['route' => ['admin.profile_fields.show', to_hashid($field->id)], 'method' => 'PUT']) !!}
            <button class="inline-block btn btn-xs btn-success btn-embossed btn-sm" type="submit"><i class="fa fa-check"></i> Show this field</button>
            {!! Form::close() !!}
        @endif
        {!! Form::open(['route' => ['admin.profile_fields.delete', to_hashid($field->id)], 'method' => 'DELETE']) !!}
        <button class="inline-block btn btn-xs btn-danger btn-embossed btn-sm" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
        {!! Form::close() !!}
        <button class="inline-block btn btn-xs btn-primary btn-embossed btn-sm edit-field" type="button"><i class="fa fa-edit"></i> Edit</button>
    </div>
</div>
