@extends('admin.layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8">
			<div class="inline-block"><h1 class="page-title">Manage Organizations</h1></div>
			@if (has_role('admin'))
				<div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.organizations.create') !!}">Add</a></div>
			@endif
		</div>
		<div class="col-md-4 text-right">
			<form>
				<div class="form-group">
					<div class="input-group input-group-sm">
						<input class="form-control input-append" type="search" name="q" placeholder="Search" value="{{ !empty($_GET['q']) ? $_GET['q'] : '' }}">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button"><span class="fui-search"></span></button>
						</span>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		@if (has_role('admin'))<div class="col-md-1"><a href="#" class="trash disabled"><span class="fui-trash"></span></a></div>@endif
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			@if (has_role('admin'))
			{!! Form::open(['route' => ['admin.organizations.delete_multiple'], 'method' => 'POST']) !!}
			@endif
				<table class="table table-bordered table-rounded index-table text-left">
					<tr>
						@if (has_role('admin')) <th style="width: 44px;"></th> @endif
						<th style="width: 60px;"></th>
						<th>{!! order_link('name', 'Name') !!}</th>
						<th style="width: 200px;">{!! order_link('created_at', 'Date added', true) !!}</th>
						<th>{!! order_link('status', 'Status') !!}</th>
						<th></th>
						<th></th>
					</tr>
					@foreach ($organizations as $organization)
					<tr>
						@if (has_role('admin'))
						<td>
							<label class="checkbox" for="check{{ $organization->id }}">
								<input type="checkbox" id="check{{ $organization->id }}" name="organizations[]" value="{{ $organization->id }}" class="trash-checkbox" data-toggle="checkbox">
							</label>
						</td>
						@endif
						<td>{!! organization_thumbnail($organization->image) !!}</td>
						<td>{{ $organization->name }}</td>
						<td><span class="label label-default">{{ $organization->created_at->format('d-m-Y H:i:s') }}</span></td>
						<td>{!! organization_status($organization->status) !!}</td>
						<td>
							<a target="_blank" class="small" style="margin-right: 6px; text-decoration: underline;" href="{!! route('organizations.show_public_profile', ['id' => to_hashid($organization->id)]) !!}">Public profile</a><i class="small fa fa-external-link" style="color: #444;"></i>
						</td>
						<td><a class="action btn btn-xs btn-primary btn-embossed" href="{!! route('admin.organizations.edit', ['id' => to_hashid($organization->id)]) !!}"><i class="fa fa-pencil"></i> Edit</a></td>
					</tr>
					@endforeach
				</table>
			@if (has_role('admin'))
				<?php
				$actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
				$actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';
				?>
				@include('admin.partials._modal', ['modalClass' => 'remove-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to delete these organisations?', 'actions' => $actions])
				{!! Form::close() !!}
			@endif
			{!! pagination($organizations) !!}
		</div>
	</div>
@stop