@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Send Invite for {{ $organization->name }}</h1>
        </div>
    </div>
    {!! Form::open(['route' => ['admin.users.post_invite'], 'method' => 'POST']) !!}
    <input type="hidden" id="organization_id" name="organization_id" value="{{ $organization->id }}">
    <input type="hidden" id="redirect_to" name="redirect_to" value="organization_profile">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="row">
                <div class="col-md-4 form-group{{ $errors->has('role') ? ' has-error' : '' }}    ">
                    {!! Form::label('role', 'Role') !!}
                    {!! Form::select('role', ['member' => 'member', 'owner' => 'owner'], old('role'), ['class' => 'form-control']) !!}
                    {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::submit('Send', ['class' => 'btn btn-primary btn-embossed']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="row">
        <div class="col-md-12 text-center">
            <table class="table table-bordered table-condensed table-rounded index-table text-left">
                <tr>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Registered</th>
                </tr>
                @foreach ($invites as $invite)
                    <tr>
                        <td>{!! link_to('mailto:'.$invite->email, $invite->email) !!}</td>
                        <td>{{ $invite->role }}</td>
                        <td class="text-center">{!! is_null($invite->registered_at) ? '<span class="fui-cross-circle text-primary"></span>' : '<span class="label label-default">'.$invite->registered_at->format('d-m-Y H:i').'</span>' !!}</td>
                    </tr>
                @endforeach
            </table>
            {!! pagination($invites) !!}
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function(){
            //$('select').select2({dropdownCssClass: 'dropdown-inverse'});
        });
    </script>
@stop