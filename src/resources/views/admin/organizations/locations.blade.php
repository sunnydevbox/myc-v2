@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.organizations._tabs', ['organization' => $organization, 'active' => 'get_locations'])
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="embed-code">Embed code</label>
            <div class="form-group">
                <textarea id="embed-code" class="form-control"><iframe src="{!! route('organizations.show_public_profile', ['id' => to_hashid($organization->id)]) !!}" frameborder="0" width="100%" height="600" style="border:none; overflow:hidden;"></iframe></textarea>
            </div>
        </div>
        @include('admin.locations._table', [
			'locations' => $locations,
			'allowDelete' => false
		])
    </div>
@stop
@section('scripts')
    <script></script>
@stop
