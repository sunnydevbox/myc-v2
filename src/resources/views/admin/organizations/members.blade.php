@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.organizations._tabs', ['organization' => $organization, 'active' => 'get_members'])
    </div>
    <div class="row margin-bottom">
        <div class="col-md-12">
            <a class="btn btn-xs btn-primary" href="{{ route('admin.organizations.get_invite', ['id' => to_hashid($organization->id)]) }}">Invite member</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <table class="table table-bordered table-rounded index-table text-left">
                <tr>
                    <th></th>
                    <th>{!! order_link('last_name', 'Name') !!}</th>
                    <th>{!! order_link('email', 'Email') !!}</th>
                    <th>Registered at</th>
                    <th>Registered for organization at</th>
                    <th>Role</th>
                    <th></th>
                    <th></th>
                </tr>
                @foreach ($users as $user)
                    <tr>
                        <td style="width: 44px; text-align: center">{!! user_avatar($user->image) !!}</td>
                        <td>{{ $user->display_name }}</td>
                        <td>{!! link_to('mailto:'.$user->email, $user->email) !!}</td>
                        <td><span class="label label-default">{{ $user->created_at->format('d-m-Y H:i') }}</span></td>
                        <td><span class="label label-default">{{ $user->registered_for_organization_at->format('d-m-Y H:i') }}</span></td>
                        <td>{{ $user->organization_role }}</td>
                        <td>
                            {!! Form::open(['route' => ['admin.organizations.patch_members', to_hashid($organization->id)], 'method' => 'PATCH']) !!}
                            <input type="hidden" id="role" name="role" value="{{ $user->isOwner($organization) ? 'member' : 'owner' }}">
                            <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}">
                            <button class="btn btn-xs btn-embossed btn-default" type="submit"><i class="fa fa-pencil"></i> Change to {{ $user->isOwner($organization) ? 'member' : 'owner' }}</button>
                            {!! Form::close() !!}
                        </td>
                        <td>
                            @if(\Auth::user()->id != $user->id)
                                {!! Form::open(['route' => ['admin.organizations.delete_members', to_hashid($organization->id)], 'method' => 'DELETE']) !!}
                                <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}">
                                <button class="btn btn-xs btn-embossed btn-danger" type="submit"><i class="fa fa-remove"></i> Delete</button>
                                {!! Form::close() !!}
                            @else
                                <button class="btn btn-xs btn-embossed btn-danger disabled" type="submit"><i class="fa fa-remove"></i> Delete</button>
                            @endif

                        </td>
                    </tr>
                @endforeach
            </table>
            {!! pagination($users) !!}
        </div>
    </div>
@stop
@section('scripts')
    <script></script>
@stop
