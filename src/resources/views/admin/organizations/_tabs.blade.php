<?php
// TODO move to view composer
$tabs = [
    'edit' => 'Info',
    'get_form_builder' => 'Form Builder',
    'get_locations' => 'Locations',
    'get_members' => 'Members'
];

if (has_role('admin')) {
    $tabs['get_subscription'] = 'Subscription';
    $tabs['get_bulk_import'] = 'Bulk import';
}
?>

<div class="col-md-12">
    <h1 class="page-title">
        <div class="inline-block">Edit {{ $organization->name }}</div>
        <div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('organizations.show_public_profile', ['id' => to_hashid($organization->id)]) !!}" target="_blank"><i class="fa fa-external-link"></i> Public profile</a></div>
    </h1>
    <ul class="nav nav-tabs border-bottom">
        @foreach($tabs as $key => $value)
            <li {!! ($key === $active) ? 'class="active"' : '' !!}><a href="{!! route('admin.organizations.'.$key, ['id' => to_hashid($organization->id)]) !!}">{{ $value }}</a></li>
        @endforeach
    </ul>
</div>
