@extends('admin.layouts.master')

@section('content')
<div class="row">
	@include('admin.organizations._tabs', ['organization' => $organization, 'active' => 'edit'])
</div>
{!! Form::model($organization, ['route' => ['admin.organizations.update', to_hashid($organization->id)], 'method' => 'PUT', 'files' => true]) !!}
<div class="row">
	<div class="col-md-6">
		<div class="form-group {{ error_class($errors, 'name') }}">
			{!! Form::label('name', 'Name') !!}
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
			{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
		</div>

		<div class="form-group {{ error_class($errors, 'pay-off') }}">
			{!! Form::label('pay_off', 'Pay off') !!}
			{!! Form::text('pay_off', null, ['class' => 'form-control']) !!}
			{!! $errors->first('pay_off', '<p class="help-block">:message</p>') !!}
		</div>

		<div class="form-group">
			{!! Form::label('website', 'Website') !!}
			{!! Form::text('website', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group {{ error_class($errors, 'email_address') }}">
			{!! Form::label('email_address', 'Email address') !!}
			{!! Form::text('email_address', null, ['class' => 'form-control']) !!}
			{!! $errors->first('email_address', '<p class="help-block">:message</p>') !!}
		</div>

		<div class="form-group">
			{!! Form::label('description', 'Description') !!}
			{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group margin-top">
			{!! link_to_route('admin.organizations.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
			{!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
		</div>
	</div>
	{!! Form::close() !!}


	<div class="col-md-3 organization-images-container">
		<h4 class="no-margin-top">Profile image</h4>
		<p class="small">For best results we recommend a square image of at least 512x512 pixels.</p>
		<div class="form-group">
			<div class="fileinput fileinput-new profile-image-container" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="background-image:url({!! route('images.get_asset', ['path' => $organization->image]) !!});"></div>
				<span class="save-msg fileinput-exists">Please press save to update profile image</span>
				@if ($organization->image)
					<div class="delete"><button type="button" data-toggle="modal" data-target=".modal-profile-image"></button></div>
				@endif
				<div>
					<?php
					$actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
					$actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';
					?>
					{!! Form::open(['method' => 'DELETE', 'route' => ['admin.organizations.delete_profile_image', to_hashid($organization->id)], 'class' => 'delete-image-form']) !!}
					@include('admin.partials._modal', ['modalClass' => 'modal-profile-image', 'title' => 'Notice', 'body' => 'Are you sure you want to delete the profile image?', 'actions' => $actions])
					{!! Form::close() !!}

					<span class="hide"><input type="file" name="image"></span>
				</div>
			</div>
		</div>
		<br />

		<h4 class="no-margin-top">Header image</h4>
		<p class="small">A high-resolution image in landscape orientation works best.</p>

		<div class="form-group">
			<div class="fileinput fileinput-new header-image-container" data-provides="fileinput">
				<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="background-image:url({!! route('images.get_asset', ['path' => $organization->header_image]) !!});"></div>
				<span class="save-msg fileinput-exists">Please press save to update header image</span>
				@if ($organization->header_image)
					<div class="delete"><button type="button" data-toggle="modal" data-target=".modal-header-image"></button></div>
				@endif
				<div>
					<?php
					$actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
					$actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';
					?>
					{!! Form::open(['method' => 'DELETE', 'route' => ['admin.organizations.delete_header_image', to_hashid($organization->id)], 'class' => 'delete-image-form']) !!}
					@include('admin.partials._modal', ['modalClass' => 'modal-header-image', 'title' => 'Notice', 'body' => 'Are you sure you want to delete the header image?', 'actions' => $actions])
					{!! Form::close() !!}

					<span class="hide"><input type="file" name="header_image"></span>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('scripts')
<script>
	$(document).ready(function(){
		$('form').on('keyup keypress', function(evt) {
			// Prevent form being submitted on enter
			if (evt.which == 13 && evt.target.type !== 'textarea') {
				evt.preventDefault();
			}
		});
		var datepickerSelector = $('.datepicker');
		datepickerSelector.datepicker({
			showOtherMonths: true,
			selectOtherMonths: true,
			dateFormat: 'yy-mm-dd',
			yearRange: '-1:+1'
		}).prev('.input-group-btn').on('click', function (e) {
			e && e.preventDefault();
			datepickerSelector.focus();
		});
	});
</script>
@stop