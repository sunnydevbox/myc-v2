@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.organizations._tabs', ['organization' => $organization, 'active' => 'get_subscription'])
    </div>
    {!! Form::model($organization, ['route' => ['admin.organizations.put_subscription', to_hashid($organization->id)], 'method' => 'PUT']) !!}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('status', 'Status') !!}
                    <select id="status" name="status" class="form-control">
                        @foreach([Myc\Domain\Organizations\Organization::STATUS_DISABLED, Myc\Domain\Organizations\Organization::STATUS_ACTIVE] as $status)
                            <option value="{{ $status }}" {!! option_selected($status, $organization->status) !!}>{{ organization_status_label($status) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                Billing information

                <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                    {!! Form::label('company_name', 'Company name') !!}
                    {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    {!! Form::label('first_name', 'First name') !!}
                    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    {!! Form::label('last_name', 'Last name') !!}
                    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="row">
                    <div class="form-group col-md-9{{ $errors->has('street_name') ? ' has-error' : '' }}">
                        {!! Form::label('street_name', 'Street') !!}
                        {!! Form::text('street_name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('street_name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group col-md-3{{ $errors->has('street_number') ? ' has-error' : '' }}">
                        {!! Form::label('street_number', 'Number') !!}
                        {!! Form::text('street_number', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('street_number', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                        {!! Form::label('postal_code', 'Postal code') !!}
                        {!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('postal_code', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group col-md-6{{ $errors->has('locality') ? ' has-error' : '' }}">
                        {!! Form::label('locality', 'City') !!}
                        {!! Form::text('locality', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('locality', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                    {!! Form::label('country', 'Country') !!}
                    {!! Form::text('country', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                    {!! Form::label('phone_number', 'Phone number') !!}
                    {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('email_address_billing') ? ' has-error' : '' }}">
                    {!! Form::label('email_address_billing', 'Email address') !!}
                    {!! Form::text('email_address_billing', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email_address_billing', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group margin-top">
                    {!! link_to_route('admin.organizations.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                    {!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
                </div>

            </div>
            <div class="col-md-6">
                Payment user_number

                <div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
                    {!! Form::label('plan', 'Plan') !!}
                    {!! Form::text('plan', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('started_at') ? ' has-error' : '' }}">
                    {!! Form::label('started_at', 'Starting date') !!}
                    {!! Form::text('started_at', null, ['class' => 'form-control datepicker']) !!}
                    {!! $errors->first('started_at', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('building_number') ? ' has-error' : '' }}">
                    {!! Form::label('building_number', 'Number of buildings') !!}
                    {!! Form::text('building_number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('building_number', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
                    {!! Form::label('user_number', 'Number of users') !!}
                    {!! Form::text('user_number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('user_number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@stop
@section('scripts')
    <script>
        $(document).ready(function(){
            $('form').on('keyup keypress', function(evt) {
                // Prevent form being submitted on enter
                if (evt.which == 13 && evt.target.type !== 'textarea') {
                    evt.preventDefault();
                }
            });
            var datepickerSelector = $('.datepicker');
            datepickerSelector.datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'yy-mm-dd',
                yearRange: '-1:+1'
            }).prev('.input-group-btn').on('click', function (e) {
                e && e.preventDefault();
                datepickerSelector.focus();
            });
        });
    </script>
@stop
