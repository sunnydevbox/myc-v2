@extends('admin.layouts.master')

@section('content')
    <div class="row">
        @include('admin.organizations._tabs', ['organization' => $organization, 'active' => 'get_form_builder'])
    </div>

    <div class="row">
        <div class="col-md-7">
            @if ($profile)
                <div class="profile-fields">
                    @foreach ($profile->getFields() as $field)
                        @include('admin.organizations._form_builder_field', ['field' => $field, 'profile' => $profile])
                    @endforeach
                </div>
            @endif
        </div>
        <div class="col-md-5">
            {!! Form::model($organization, ['route' => ['admin.organizations.put_form_builder', to_hashid($organization->id)], 'method' => 'PUT', 'class' => 'add-field-form clearfix']) !!}
                <h4>Add Field</h4>
                <div class="add-field-container">
                    <div class="form-group {{ $errors->has('field_type') ? ' has-error' : '' }}">
                        <label for="label">Field type</label>
                        <select class="form-control" id="field_type" name="field_type">
                            <option value="">Select field type</option>
                            @foreach ($fieldTypes as $fieldType)
                                <option value="{{ $fieldType }}">{{ field_type_label($fieldType)}}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('field_type', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('label') ? ' has-error' : '' }}">
                        <label for="label">Label</label>
                        <input class="form-control" type="text" id="label" name="label">
                        {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group{{ $errors->has('is_required') ? ' has-error' : '' }}">
                        <label for="label">
                            This field is required
                            <input type="checkbox" id="is_required" name="is_required" value="1">
                        </label>
                        {!! $errors->first('is_required', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <button class="btn btn-primary btn-embossed pull-right" type="submit">Add field</button>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            var $addFieldContainer = $('.add-field-container');
            $('#field_type').on('change', function(evt) {
                var $el = $(evt.currentTarget);
                $addFieldContainer.find('.options-container').remove();
                if ($el.val() === 'select' || $el.val() === 'multi_select') {
                    var optionControl = '<div class="input-group input-group-sm"><input type="text" class="form-control" name="options[]"><span class="input-group-btn"><button class="btn btn-default remove-option" type="button"><span class="fui-trash"></span></button></span></div>';
                    var $optionsContainer = $('<div class="options-container"><label>Options</label><div class="options"><div class="option clearfix" style="margin-bottom: 10px;">' + optionControl + '</div></div><button class="btn btn-xs btn-embossed btn-primary add-option" type="button">Add option</button></div>');
                    $addFieldContainer.append($optionsContainer).find('.add-option').on('click', function(evt) {
                        evt.preventDefault();
                        $optionsContainer.find('.option').first().clone(true).appendTo($optionsContainer.find('.options')).find('input').val('').focus();
                    });
                    $('.option .remove-option', $addFieldContainer).on('click', function(evt) {
                        evt.preventDefault();
                        if ($('.option', $addFieldContainer).length > 1) {
                            $(this).closest('.option').remove();
                        }
                    });
                }
            });

            $('.profile-fields').sortable({
                axis: 'y',
                update: function(evt, ui) {
                    var idx = 1;
                    var items = $(this).sortable('toArray', {attribute: 'data-id'}).map(function(item) {
                        var data = {
                            'id': item,
                            'sort_order': idx
                        };
                        idx++;
                        return data;
                    });
                    // Namespace
                    items = {data: items};
                    $.ajax({
                        dataType: 'json',
                        contentType: 'application/json',
                        type: 'POST',
                        data: JSON.stringify(items),
                        url: '{{ route('admin.profile_fields.order') }}'
                    });
                }
            }).disableSelection();

            $('.edit-field').on('click', function() {
                $(this).closest('.profile-field-container').find(':disabled, .disable').prop('disabled', false).show();
                $(this).closest('.profile-field-container').find('.value-field').hide();
            });

            $('.profile-field-container .add-option').on('click', function(evt) {
                evt.preventDefault();
                $clone = $(this).parent().find('.form-group:last').clone(true);
                $clone.find('input[type=text]').val('');
                $(this).parent().find('.form-group:last').after($clone);
            });
        });
    </script>
@stop