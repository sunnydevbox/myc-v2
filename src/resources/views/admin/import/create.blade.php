@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Import Data</h1>
        </div>
    </div>
    {!! Form::open(['route' => ['admin.import.store'], 'method' => 'POST', 'files' => true]) !!}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                <span class="btn btn-primary btn-file btn-embossed">
                    Select File
                    {!! Form::file('file', null, ['class' => 'form-control']) !!}
                </span>
                {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group">
                <p class="help-block">The file must be a comma-seperated textfile (CSV) of the following format:</p>
                <ul class="help-block">
                    <li>Latitude (required)</li>
                    <li>Longtitude (required)</li>
                    <li>Name (optional)</li>
                    <li>Image URL (optional)</li>
                    <li>Comma seperated list of Tag IDs e.g. 1,3,4 (optional)</li>
                </ul>
                <p class="help-block">Each field must be enclosed by double quotes. <a href="{!! asset('assets/import_template.csv') !!}">Download template</a>.</p>
            </div>

            <div class="form-group{{ $errors->has('organization_id') ? ' has-error' : '' }}">
                {!! Form::label('organization_id', 'Organization') !!}
                <select id="organization_id" name="organization_id" class="form-control">
                    <option value="">None</option>
                    @foreach ($organizations as $organization)
                        <option value="{{ $organization->id }}" {!! option_selected($organization->id, [old('organization_id')]) !!}>{{ $organization->name }}</option>
                    @endforeach
                </select>
                {!! $errors->first('organization_id', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                <p class="help-block">When an organization is selected, each location in the imported file will be associated with the organization. Also the first assigned organization owner is defined as the location creator.</p>
                <p class="help-block">When no organization is selected you (currently logged in user) are defined as the location creator.</p>
            </div>

            <div class="form-group">
                {!! Form::submit('Start', ['class' => 'btn btn-primary btn-embossed']) !!}
            </div>

        </div>
    </div>
    {!! Form::close() !!}

    <h3>Completed Imports</h3>

    <div class="row">
        <div class="col-md-12 text-center">
            <table class="table table-bordered table-condensed table-rounded index-table text-left">
                <tr>
                    <th>Filename</th>
                    <th></th>
                </tr>
                @foreach ($logFiles as $file)
                    <tr>
                        <td><a href="{!! route('admin.import.get_log_file', ['path' => basename($file)]) !!}">{{ basename($file) }}</a></td>
                        <td>
                            {!! Form::open(['route' => ['admin.import.delete_log_file', basename($file)], 'method' => 'DELETE']) !!}
                            <button class="btn btn-xs btn-embossed btn-danger" type="submit">Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@stop

@section('scripts')
<script>
    $(function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });
        $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                $(this).parent().parent().find('.label.file').remove();
                $(this).parent().after('<span class="file label label-default" style="display: inline-block; margin-left: 10px;">'+ label +'</span>');
            });
        });
    });
</script>
@stop
