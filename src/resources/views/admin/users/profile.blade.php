@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Your profile</h1>
        </div>
    </div>

    {!! Form::model($user, ['route' => ['admin.users.update_profile'], 'method' => 'PUT', 'files' => true]) !!}

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                {!! Form::label('first_name', 'First name') !!}
                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="row form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <div class="col-md-8">
                    {!! Form::label('last_name', 'Last name') !!}
                    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('last_name_prefix', 'Last name prefix') !!}
                    {!! Form::text('last_name_prefix', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email address') !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'readonly' => 'reanonly']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Form::label('phone', 'Phone') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
            </div>

            <h4>Change your password</h4>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password_confirmation', 'Repeat password') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                {!! link_to_route('admin.users.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                {!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
            </div>
        </div>

        <?php
        $user_image = false;
        if($user->image) {
            $user_image = route('images.get_asset', ['path' => $user->image]);
        }
        ?>
        <div class="col-md-3">
            <h4 class="no-margin-top">Profile image</h4>
            <div class="form-group">
                <div class="fileinput fileinput-new profile-image-container" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="<?php echo $user_image ? 'background-image:url(' . $user_image . ')' : ''?>"></div>
                    <div>
                        <span class="hide"><input type="file" name="image"></span>
                        <a href="#" class="btn btn-primary btn-embossed fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span> Remove</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function(){
            $('select#role_id').select2({dropdownCssClass: 'dropdown-inverse'});
        });
    </script>
@stop