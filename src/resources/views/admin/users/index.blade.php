@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="inline-block"><h1 class="page-title">Manage Users</h1></div>
            <div class="inline-block"><a class="btn btn-xs btn-primary mhm" href="{!! route('admin.users.get_invite') !!}">Invite</a></div>
        </div>
        <div class="col-md-4 text-right">
            <div class="form-group">
                <form>
                <div class="input-group input-group-sm">
                    <input class="form-control input-append" type="search" name="q" placeholder="Search" value="{{ !empty($_GET['q']) ? $_GET['q'] : '' }}">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><span class="fui-search"></span></button>
					</span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        @if(!isset($_GET['trash']))
            <a href="#" class="trash disabled" style="float: left;"><span class="fui-trash"></span></a>
            {!! link_to_route('admin.users.index', 'Show trash', ['trash' => true], ['class' => 'link-small']) !!}
        @else
            <a href="#" class="trash recover disabled" style="float: left;"><span class="fui-check"></span></a>
            {!! link_to_route('admin.users.index', 'Cancel', false, ['class' => 'link-small']) !!}
        @endif
        </div>
    </div>
    <div class="row">
        @include('admin.users._table', ['users' => $users, 'allowDelete' => true])
    </div>
@stop

@section('scripts')
<script>
    $('#check-all').on('change.radiocheck', function() {
        if($(this).is(':checked')) {
            $('.trash-checkbox').radiocheck('check');
        } else {
            $('.trash-checkbox').radiocheck('uncheck');
        }
    });

    var recoverUrl = '{{ route('admin.users.recover_multiple') }}';
</script>
@stop
