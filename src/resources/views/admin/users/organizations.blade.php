@extends('admin.layouts.master')
<?php
$tabs = ['edit' => 'Info', 'get_organizations' => 'Organizations'];
$active = 'get_organizations';
$baseRoute = 'admin.users';
?>
{{-- TODO add validation failed feedback (unlikely) --}}
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Edit User {{ $user->display_name }}</h1>
            @include('admin.partials._tabs')
        </div>
    </div>
    @if (has_role('admin'))
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::open(['route' => ['admin.users.patch_organizations', to_hashid($user->id)], 'method' => 'PATCH']) !!}
                <div class="row">
                    <div class="col-md-7">
                        <select class="form-control" id="organization_id" name="organization_id">
                            <option value="">Select organization</option>
                            @foreach ($organizationNotAttachedToUser as $organization)
                                <option value="{{ $organization->id }}">{{ $organization->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" id="role" name="role">
                            <option value="member">member</option>
                            <option value="owner">owner</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-embossed btn-primary" type="submit">Add</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    @endif

    @if (count($organizations))
        <div class="row">
            <div class="col-md-12 text-center">
                <table class="table table-bordered table-rounded index-table text-left">
                    <tr>
                        <th style="width: 60px;"></th>
                        <th>Name</th>
                        <th>Role</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach ($organizations as $organization)
                        <tr>
                            <td>{!! organization_thumbnail($organization->image) !!}</td>
                            <td>{!! link_to_route('admin.organizations.edit', $organization->name, ['id' => to_hashid($organization->id)]) !!}</td>
                            <td>{{ $user->isOwner($organization) ? 'owner' : 'member' }}</td>
                            <td>
                                {!! Form::open(['route' => ['admin.users.patch_organizations', to_hashid($user->id)], 'method' => 'PATCH']) !!}
                                    <input type="hidden" id="role" name="role" value="{{ $user->isOwner($organization) ? 'member' : 'owner' }}">
                                    <input type="hidden" id="organization_id" name="organization_id" value="{{ $organization->id }}">
                                    <button class="btn btn-xs btn-embossed btn-default" type="submit"><i class="fa fa-pencil"></i> Change to {{ $user->isOwner($organization) ? 'member' : 'owner' }}</button>
                                {!! Form::close() !!}
                            </td>
                            <td>
                                {!! Form::open(['route' => ['admin.users.delete_organization', to_hashid($user->id)], 'method' => 'DELETE']) !!}
                                <input type="hidden" id="organization_id" name="organization_id" value="{{ $organization->id }}">
                                <button class="btn btn-xs btn-embossed btn-danger" type="submit"><i class="fa fa-remove"></i> Delete</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
@stop

@section('scripts')
    <script>
        $(function(){ });
    </script>
@stop