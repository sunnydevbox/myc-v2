<div class="col-md-12 text-center">
    @if ($allowDelete)
        {!! Form::open(['route' => ['admin.users.delete_multiple'], 'method' => 'POST']) !!}
    @endif
    <table class="table table-bordered table-rounded index-table text-left">
        <tr>
            @if ($allowDelete)
            <th>
                <label class="checkbox" for="check-all">
                    <input type="checkbox" id="check-all" data-toggle="checkbox">
                </label>
            </th>
            @endif
            <th></th>
            <th>{!! order_link('last_name', 'Name') !!}</th>
            <th>{!! order_link('email', 'Email') !!}</th>
            <th>{!! order_link('created_at', 'Registered at', true) !!}</th>
            <th style="width: 160px;">{!! order_link('confirmed', 'Email Confirmed') !!}</th>
            <th>Roles</th>
            <th></th>
        </tr>
        @foreach ($users as $user)
            <tr>
                @if ($allowDelete)
                <td>
                    <label class="checkbox" for="check{{ $user->id }}">
                        <input type="checkbox" id="check{{ $user->id }}" name="users[]" value="{{ $user->id }}" class="trash-checkbox" data-toggle="checkbox">
                    </label>
                </td>
                @endif
                <td style="width: 44px; text-align: center">{!! user_avatar($user->image) !!}</td>
                <td>{{ $user->display_name }}</td>
                <td>{!! link_to('mailto:'.$user->email, $user->email) !!}</td>
                <td><span class="label label-default">{{ $user->created_at->format('d-m-Y H:i') }}</span></td>
                <td class="text-center">{!! ($user->isConfirmed()) ? '<span class="fui-check-circle text-primary"><span>' : '<span class="fui-cross-circle text-primary"></span>' !!}</td>
                <td>{{ user_roles_description($user) }}</td>
                <td><a class="btn btn-xs btn-primary btn-embossed mhm" href="{!! route('admin.users.edit', ['id' => to_hashid($user->id)]) !!}"><i class="fa fa-pencil"></i> Edit</a></td>
            </tr>
        @endforeach
    </table>
    @if ($allowDelete)
        <?php
        $delete_actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
        $delete_actions .= '<button type="submit" class="btn btn-primary delete-confirm">Ok</button>';

        $recover_actions = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
        $recover_actions .= '<button type="submit" class="btn btn-primary recover-confirm">Ok</button>';
        ?>
        @include('admin.partials._modal', ['modalClass' => 'remove-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to delete these users?', 'actions' => $delete_actions])
        @include('admin.partials._modal', ['modalClass' => 'recover-items-modal', 'title' => 'Notice', 'body' => 'Are you sure you want to recover these users?', 'actions' => $recover_actions])
        {!! Form::close() !!}
    @endif


    {!! pagination($users) !!}
</div>
