@extends('admin.layouts.master')
<?php
$tabs = ['edit' => 'Info', 'get_organizations' => 'Organizations'];
$active = 'edit';
$baseRoute = 'admin.users';
?>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Edit User {{ $user->display_name }}</h1>
            @include('admin.partials._tabs')
        </div>
    </div>

    {!! Form::model($user, ['route' => ['admin.users.update', to_hashid($user->id)], 'method' => 'PATCH']) !!}

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('first_name', 'First name') !!}
                {!! Form::text('first_name', null, ['class' => 'form-control', 'readonly' => true]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('last_name_prefix', 'Last name prefix') !!}
                {!! Form::text('last_name_prefix', null, ['class' => 'form-control', 'readonly' => true]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('last_name', 'Last name') !!}
                {!! Form::text('last_name', null, ['class' => 'form-control', 'readonly' => true]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('role_id', 'Role') !!}
                {!! Form::select('role_id', \Myc\Domain\Users\Role::all()->lists('name', 'id'), old('role_id'), ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! link_to_route('admin.users.index', 'Back', [], ['class' => 'btn btn-default btn-embossed']); !!}
                {!! Form::submit('Save changes', ['class' => 'btn btn-primary btn-embossed']) !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function(){});
    </script>
@stop