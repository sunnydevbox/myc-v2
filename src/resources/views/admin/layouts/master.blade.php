<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1"> --}}
    <title>@yield('title', 'Map Your City | Admin')</title>
	<link rel="icon" type="image/png" href="/assets/img/icon_app.png">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!--<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/css/select2.min.css" rel="stylesheet" />-->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/flat-ui-pro.css" rel="stylesheet">
    <link href="/assets/css/app.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid main-container">
	<div class="row">
		<div class="col-md-12 text-right user-container">
			<a href="{!! route('admin.users.profile') !!}">
				{!! user_avatar(\Auth::user()->image) !!} Hi {{ Auth::user()->first_name }}
			</a>
			<a class="logout btn btn-xs btn-default btn-embossed" style="color: white;" href="{!! route('auth.logout') !!}"><i class="fa fa-power-off"></i> Logout</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 nav-container">@include('admin.partials._nav')</div>
		<div class="col-md-10 content-container">
			<div class="row">
				<div class="col-md-12">@include('admin.partials._flash')</div>
			</div>
			@yield('content')
		</div>
	</div>
</div>
<script src="//maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="/flat-ui-pro/js/flat-ui-pro.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/js/select2.min.js"></script>
<script src="/assets/js/app.js"></script>
@yield('scripts')
</body>
</html>
