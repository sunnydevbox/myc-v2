@extends('layouts.email')

@section('content')
<p>Hey,</p>

<p>You have asked us to reset your password. Please click on the link below to change it.</p>

<p>
	<a style="color:#f28e3c" href="{{ route('redirect.url', ['action' => 'password_reset','token' => $token, 'email' => $user->email]) }}">Reset your password</a><br />
</p>

<p>Did you not ask for a password reset for Map Your City, please contact our customer support at <a style="color:#f28e3c" href="mailto:customerhappiness@mapyour.city">customerhappiness@mapyour.city</a></p>

<p>Map Your City – Helping you find, map share and connect to places you ♥.</p>
@stop