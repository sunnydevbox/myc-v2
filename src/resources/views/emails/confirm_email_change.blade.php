@extends('layouts.email')

@section('content')
<p>Hi there {{ $user->first_name }}.</p>

<p>You have recently changed your e-mail for your MYC account. We take your privacy very seriously and therefore ask you to acknowledge that the e-mail that has been provided by you is in fact yours.</p>

<p>Just confirm your email to continue to use MYC and all its amazing features.</p>

<p>
	<a style="color:#f28e3c" href="{{ route('redirect.url', ['action' => 'confirm_account','confirmation_code' => $user->confirmation_code, 'email' => $user->email]) }}">Confirm e-mail</a><br />
</p>

<p>If you haven’t changed your e-mail, please contact our customer support at <a href="mailto:customerhappiness@mapyour.city" style="color:#f28e3c">customerhappiness@mapyour.city</a></p>

<p>Happy mapping!</p>

<p>O, and don’t forget to reply within 7 days of this e-mail otherwise you will unfortunately no longer be able to login</p>
@stop
