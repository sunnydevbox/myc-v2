@extends('layouts.email')

@section('content')
<p>Hi {{ $user->first_name }},</p>

<p>We are sorry, but you have been disconnected by {{ $inviter }} from {{ $organization->name }}. You are no longer able to map together.</p>

<p>If you think this might be a mistake please contact us at <a href="mailto:customerhappiness@mapyour.city" style="color:#f28e3c">customerhappiness@mapyour.city</a></p>

<p>We love you to continue to be part of the Map Your City community. You are most welcome to keep exploring, mapping, sharing and connecting to places you ♥.</p>

<p>Have a great day ahead!</p>
@stop