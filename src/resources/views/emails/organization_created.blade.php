@extends('layouts.email')

@section('content')
<p>Hey,</p>

<p>Great to have you and {{ $organization->name }} on board! We will do everything we can to help you set up {{ $organization->name }} and start using Map Your City with your community.</p>

<p>Shortly you will receive an invite to connect your user account to your organization.</p>

<p>{{ $organization->name }} and your community are now in charge of a very powerful tool to create engagement with your organization’s goals, vision, products and services.</p>

<p>We'll bring you tips for getting started with our tools and tell you all about how to invite your user community.</p>

<p>But you can also check out our <a href="http://mapyour.city/customer-happiness/" style="color:#f28e3c">Map Your City User Guides</a></p>

<p>We are always there to help, just get in touch at <a href="mailto:customerhappiness@mapyour.city" style="color:#f28e3c">customerhappiness@mapyour.city</a></p>

<p>Happy mapping!</p>
@stop