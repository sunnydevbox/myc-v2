@extends('layouts.email')

@section('content')
<p>Hey,</p>

<p>Congratulations! You have been invited by {{ $inviter }} to join {{ $organization->name }} and start using Map Your City.</p>

<p>Map Your City helps everyone to find, map, share and connect to the places they love.</p>

<p>With the Map Your City mobile app you and {{ $organization->name }} can now start building communities around places in your city. You can showcase your portfolio of buildings and spaces you own, are working on, or care about in the Map Your City app and on your {{ $organization->name }} website.</p>

<p>As a member you can help {{ $organization->name }} to add custom information to their portfolio when exploring and mapping "on the the go" using the app.</p>

@if($register)
	<p>Please follow the link to register your account.<br /><a style="color:#f28e3c" href="{!! route('auth.get_register', ['email' => $email, 'token' => $token]) !!}">Register</a></p>
@endif

<p>From now on you will be able to see the {{ $organization->name }} Profile and Custom Fields when adding buildings or spaces to the MYC map.</p>

<p>To add custom information for {{ $organization->name }} just activate their Profile under the building or space you are mapping, and you are good to go!</p>

<p>Thanks for joining Map Your City. We look forward to you and {{ $organization->name }} mapping with us!</p>

<p>Have a great day ahead!</p>
@stop