@extends('layouts.email')

@section('content')
<p>Hey {{ $user->first_name }}.</p>

<p>How is the mapping going? Is there anything we can help out with?</p>

<p>Maybe it slipped your mind, but you haven’t confirmed your e-mail yet.<br />
Are you experiencing any problems? If you would like, we can give you a phone call to help you set up. No problem, just drop us a note.</p>

<p>
	<a style="color:#f28e3c" href="{{ route('redirect.url', ['action' => 'confirm_account','confirmation_code' => $user->confirmation_code, 'email' => $user->email]) }}">Confirm account</a><br />
</p>

<p>If you are looking for an answer that you could not find in <a style="color:#f28e3c" href="http://mapyour.city/customer-happiness/" style="color:#f28e3c">our userguides</a>, be sure to connect with us. We are here to help.</p>

<p>We take your privacy very seriously and therefore urge you to acknowledge that the e-mail that has been provided by you is in fact yours.</p>

<p>If you haven’t registered for a Map Your City membership please get in touch with us, as someone might have used your e-mail address without your permission.</p>

<p>Map Your City – Made with a ♥ in Amsterdam.</p>

@stop
