@extends('layouts.email')

@section('content')
<p>Hi there {{ $user->first_name }}.</p>

<p>Thanks for joining Map Your City! We want to help you find, map, share and connect to places you ♥︎. Just confirm your email to get started.</p>

<p>
	<a style="color:#f28e3c" href="{{ route('redirect.url', ['action' => 'confirm_account','confirmation_code' => $user->confirmation_code, 'email' => $user->email]) }}">Confirm account</a><br />
</p>

<p>We'll bring you tips for getting started with our tools, but you can also check out our <a href="http://mapyour.city/customer-happiness/" style="color:#f28e3c">Map Your City User Guides.</a></p>

<p>We are always there to help, just get in touch at <a href="mailto:customerhappiness@mapyour.city" style="color:#f28e3c">customerhappiness@mapyour.city</a></p>

<p>Happy mapping!</p>
@stop
