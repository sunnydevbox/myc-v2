@extends('layouts.email')

@section('content')
<p>Hi there {{ $user->first_name }}.</p>

<p>We love to have you on board, but we still didn’t receive your confirmation on your e-mail. We take the privacy of our users very seriously and do need everyone to acknowledge that the e-mail that has been provided is in fact theirs.</p>

<p>Maybe you are experiencing problems? If you would like, we can give you a phone call to help you set up. No problem, just drop us a note.</p>

<p>
	<a style="color:#f28e3c" href="{{ route('redirect.url', ['action' => 'confirm_account','confirmation_code' => $user->confirmation_code, 'email' => $user->email]) }}">Confirm account</a><br />
</p>

<p>Did you check <a style="color:#f28e3c" href="http://mapyour.city/customer-happiness/">our userguides?</a> The answer not there? Be sure to connect with us. We are always here to help.</p>

<p>Map Your City – Helping you find, map, share and connect to places you ♥.</p>
@stop
