<!DOCTYPE html>
<html class="page404">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# place: http://ogp.me/ns/place#">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Not found | Map Your City</title>
    <link rel="icon" type="image/png" href="/assets/img/icon_app.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-itunes-app" content="app-id={{ config('app.ios_app_id') }}">
    <link rel="stylesheet" href="{!! asset('assets/css/normalize.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/location.css') !!}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick-theme.css"/>
</head>
<body style="height:100%">

<header>
    <div class="header-inner">
        <a href="http://www.mapyour.city"><img class="logo" src="{!! asset('assets/img/logo.svg') !!}"></a>
        @include('locations._download_app')
    </div>
</header>

<div class="container" style="padding: 0;min-height: 100%;">
    <div class="header-404">
        <h1 class="title">Error</h1>
        <h2 class="subtitle">The page you requested<br />could not be found :(</h2>
    </div>
</div>

<footer>
    <div class="footer-inner">
        <p class="credits">MAP YOUR CITY&reg;</p>
        @include('locations._download_app')
    </div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.2/slick.min.js"></script>
<script>
(function($) {
    $('.slider').slick({dots: true});
    $('.organization__header').on('click', function() {
        $(this).toggleClass('open');
        $(this).next().slideToggle(300);
        $('html, body').animate({
            scrollTop: $(this).next().offset().top + $('window').height()
        }, 300);
    });
})(jQuery);
</script>
</body>
</html>