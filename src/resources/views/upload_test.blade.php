<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>upload test</title>
</head>
<body>

<p>Test file upload for location {{ $locationId }}.</p>

{!! Form::open(['route' => ['images.create', 'hashid' => $locationId], 'files' => true]) !!}
{!! Form::file('image') !!}
{!! Form::submit() !!}
{!! Form::close() !!}

</body>
</html>
