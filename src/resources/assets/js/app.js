function initialize_map() {
    /*var lat = window.lat || 52.66871680,
        lng = window.lng || 4.77196140;*/

	var lat =  parseFloat(document.getElementById('lat').value),
        lng = parseFloat(document.getElementById('lon').value);

    if (isNaN(lat)) {
        lat = 52.66871680;
    }

    if (isNaN(lng)) {
        lng = 4.77196140;
    }

	var locationLatlng = new google.maps.LatLng(lat, lng);
	var mapOptions = {
		center: locationLatlng,
		zoom: 15
	};
	var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	var input = /** @type {HTMLInputElement} */(document.getElementById('address-input'));

	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', map);

	var geocoder = new google.maps.Geocoder();

	var image = {
    url: '/assets/img/marker.png',
    // This marker is 20 pixels wide by 32 pixels tall.
    size: new google.maps.Size(33, 48),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(16, 48),
    scaledSize: new google.maps.Size(33,48),
  };

	var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: locationLatlng,
		icon: image,
	});

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		marker.setVisible(false);
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			return;
		}

		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
				map.setZoom(15);
		}
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);
		marker.setDraggable(true);

		$('#address').val(place.formatted_address);
		$('#lat').val(place.geometry.location.lat());
		$('#lon').val(place.geometry.location.lng());
	});

	google.maps.event.addListener(marker,'dragend',function(event) {
		geocoder.geocode({'latLng': event.latLng }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					$('#address').val(results[0].formatted_address);
					$('#lat').val(results[0].geometry.location.lat());
					$('#lon').val(results[0].geometry.location.lng());
				}
			}
		});
	});
}

$(document).ready(function() {
	$('[data-toggle="checkbox"]').radiocheck();

	$('[data-toggle="select"]').select2();

	if ($('[data-toggle="switch"]').length) {
		$('[data-toggle="switch"]').bootstrapSwitch();
    }


	// Focus state for append/prepend inputs
	$('body').on('focus', '.input-prepend, .input-append', function () {
		$(this).closest('.input-group').addClass('focus');
	}).on('blur', 'input', function () {
		$(this).closest('.input-group').removeClass('focus');
	});

	$('body').on('click', '.index-table :checkbox', function() {
		if($(this).is(':checked')) {
			$(this).closest('tr').addClass('selected-row');
		} else {
			$(this).closest('tr').removeClass('selected-row');
		}
	});

	$('body').on('click', '.index-table tr', function(event) {
		if(!$(event.target).hasClass('action') && !$(event.target).is('a')) {
			if($(this).find(':checkbox').is(':checked')) {
				$(this).closest('tr').removeClass('selected-row');
				$(this).find(':checkbox').radiocheck('uncheck');
				//check if any item is selected, otherwise disable trash icon
				// if()
				if($(this).closest('table').find(':checkbox:checked').length < 1) {
					$('.trash').addClass('disabled');
				}
			} else {
				$('.trash').removeClass('disabled');
				$(this).closest('tr').addClass('selected-row');
				$(this).find(':checkbox').radiocheck('check');
			}
		}
	});

	if($('#map').length > 0) {
		google.maps.event.addDomListener(window, 'load', initialize_map);
	}

	$('.tag').on('click', function() {
		if($(this).find(':checkbox:checked').length > 0) {
			$(this).find(':checkbox').prop('checked', false);
			$(this).find('.label').removeClass('label-primary');
		} else {
			$(this).find(':checkbox').prop('checked', true);
			$(this).find('.label').addClass('label-primary');
		}
	});

	$('.trash').on('click', function(evt) {
		evt.preventDefault();

		if(!$(this).hasClass('disabled')) {
			if($(this).hasClass('recover')) {
				$('.recover-items-modal').modal('show');
			} else {
				$('.remove-items-modal').modal('show');
			}
		}
	});

	$('.recover-confirm').on('click', function(evt) {
		evt.preventDefault();
		$(this).closest('form').attr('action', recoverUrl).submit();

	});
});
