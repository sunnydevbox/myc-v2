var boundsToQueryParam = function(bounds) {
    var parts = bounds.toBBoxString().split(',');
    return [
        parts[3],
        parts[0],
        parts[1],
        parts[2]
    ].join(',');
};

var boundingBoxToLatLngBounds = function(boundingBox) {
    return L.latLngBounds(
        [boundingBox.top_left.lat, boundingBox.top_left.lon],
        [boundingBox.bottom_right.lat, boundingBox.bottom_right.lon]
    );
};

var State = Backbone.Model.extend({
    defaults: {selectedLocation: null} ,

    initialize: function(attributes, options) {
        this.pins = options.pins;
        this.organizationId = options.organization_id;
        this.queryParams = new Backbone.Model({
            organization_id: this.organizationId
        });
        this.details = new Backbone.Model();

        this.on('change:selectedLocation', this.onSelectedLocationChange, this);
        this.queryParams.on('change', this.fetchPins, this);

        // Flag if filter settings have changed
        this.filterChanged = false;
    },

    setFilterChanged: function() {
        this.filterChanged = true;
    },

    fetchPins: function() {
        // Abort any running request
        if (!_.isUndefined(this.xhr) && !_.isNull(this.xhr)) {
            this.xhr.abort();
        }

        // Filter out empty values
        var params = _.pick(this.queryParams.toJSON(), function(value, key, object) {
            return !_.isEmpty(value);
        });
        params.profile_fields = _.pick(params.profile_fields, function(value, key, object) {
            return !_.isEmpty(value);
        });

        // TODO!
        /*if (params.profile_fields || params.q) {
            delete params.bounding_box;
        }*/

        this.xhr = this.pins.fetch({data: params});

        var _this = this;
        this.xhr.done(function(data, textStatus, jqXHR) {
            if (_this.filterChanged) {
                var bounds = (data.bounding_box) ? boundingBoxToLatLngBounds(data.bounding_box) : _this.mapView.bounds;
                _this.mapView.toBoundsWithoutListening(bounds);
            }
            _this.filterChanged = false;
        });

        this.xhr.always(function() {
            _this.xhr = null;
        });
    },

    onSelectedLocationChange: function(model, pin) {
        if (!_.isNull(this.previous('selectedLocation'))) {
            this.previous('selectedLocation').set('selected', false);
        }
        if (!_.isNull(pin)) {
            pin.set('selected', true);
        } else {
            this.details.clear();
        }

    }
});

var Pin = Backbone.Model.extend({
    urlRoot: '/locations',

    defaults: {selected: false},

    initialize: function() {
        this.on('change:selected', this.onSelectedChange);
        this.on('change:location_count', this.onLocationCountChange);
    },

    hasMarkerIcon: function() {
        return (!_.isNull(this.marker) && !_.isNull(this.marker.valueOf()._icon));
    },

    onSelectedChange: function(model, selected) {
        if (this.hasMarkerIcon()) { // Marker may be removed
            if (!selected) {
                this.marker.valueOf()._icon.style.transform = this.marker.valueOf()._icon.style.transform.replace(' scale(2)', '');
                var _this = this;
                // Wait for css transition
                setTimeout(function() {
                    _this.marker.valueOf()._icon.className = _this.marker.valueOf()._icon.className.replace(' is-selected', '');
                }, 200);
            } else {
                this.marker.valueOf()._icon.className = this.marker.valueOf()._icon.className + ' is-selected';
                this.marker.valueOf()._icon.style.transform += ' scale(2)';
            }
        }
    },

    onLocationCountChange: function(model, count) {
        if (this.get('type') != 'buckets') {
            return;
        }

        if (this.hasMarkerIcon()) {
            this.marker.valueOf()._icon.innerHTML = '<span class="location-count-badge">' + count + '</span';
        }
    },

    parse: function(resp) {
        return resp;
    },

    getLatLon: function() {
        return [
            this.get('lat'),
            this.get('lon')
        ];
    },

    getClassName: function() {
        if ('locations' === this.get('type')) {
            if ('building' === this.get('icon')) {
                return 'building';
            } else {
                return 'open-space'
            }
        } else {
            return 'cluster';
        }
    }
});

var PinCollection = Backbone.Collection.extend({
    model: Pin,
    url: '/pins',

    parse: function(resp) {
        /*if (!_.isUndefined(resp.bounding_box)) {
            this.trigger('change:bounds', boundingBoxToLatLngBounds(resp.bounding_box));
        }*/
        return resp.data;
    }
});

var MapView = Backbone.View.extend({
    el: $('body'),
    initialize: function(options) {
        this.state = options.state;
        this.state.mapView = this;

        this.pins = this.state.pins;
        this.bounds = boundingBoxToLatLngBounds(options.bounds);

        this.map = L.mapbox.map($('#map').get(0), 'mapyourcity.61507bd7', {});

        //this.clusters = new L.MarkerClusterGroup();

        this.mapFitBounds(this.bounds);
        this.state.queryParams.set('bounding_box', boundsToQueryParam(this.map.getBounds()), {silent: true});

        this.listenTo(this.map, 'moveend', this.onMoveEnd);
        //this.listenTo(this.map, 'zoomstart', this.onZoomStart);
        this.listenTo(this.map, 'click', this.onMapClick);

        this.listenTo(this.pins, 'add', this.onPinAdd);
        this.listenTo(this.pins, 'remove', this.onPinRemove);
        this.listenTo(this.pins, 'reset', this.onPinsReset);
        this.listenTo(this.pins, 'change:selected', this.fetchDetails);

        this.listenTo(this.pins, 'change:bounds', function(bounds) {
            this.mapFitBounds(bounds);
        });

        this.addPins();

        this.map.legendControl.addLegend(document.getElementById('myc-copyright').innerHTML);
    },

    mapFitBounds: function(bounds) {
        this.map.fitBounds(bounds, {padding: [0, 0]});
    },

    toInitialBounds: function() {
        this.toBoundsWithoutListening(this.bounds);
    },

    toBoundsWithoutListening: function(bounds) {
        /*this.stopListening(this.map, 'moveend', this.onMoveEnd);
        this.listenToOnce(this.map, 'moveend', function() {
            this.listenTo(this.map, 'moveend', this.onMoveEnd);
        }, this);*/
        this.mapFitBounds(bounds);
    },

    onZoomStart: function(evt) {
        this.state.set('selectedLocation', null);
    },

    fetchDetails: function(model, selected) {
        if (!selected) {
            return;
        }

        // Abort any running request
        if (!_.isUndefined(this.xhr) && !_.isNull(this.xhr)) {
            this.xhr.abort();
        }

        this.xhr = $.ajax({
            dataType: 'json',
            url: '/locations/' + model.get('id'),
            type: 'GET'
        });

        _this = this;
        this.xhr.done(function(resp, textStatus, jqXHR) {
            _this.state.details.set(resp.data);
        });

        var _this = this;
        this.xhr.always(function() {
            _this.xhr = null;
        });
    },

    onMapClick: function(evt) {
        this.state.set('selectedLocation', null);
    },

    onMoveEnd: function(evt) {
        this.state.queryParams.set('bounding_box', boundsToQueryParam(this.map.getBounds()));
    },

    onPinsReset: function() {
        this.addPins();
    },

    onPinAdd: function(pin) {
        this.addPin(pin);
    },

    onPinRemove: function(pin) {
        this.removePin(pin);
        if (pin.get('selected')) {
            this.state.set('selectedLocation', null);
        }
    },

    addPins: function() {
        this.pins.each(function(pin) {
            this.addPin(pin);
        }, this);
    },

    addPin: function(pin) {
        var options = {className: pin.getClassName()};
        if ('buckets' === pin.get('type')) {
            options.html = '<span class="location-count-badge">' + pin.get('location_count') + '</span';
        }
        pin.marker = L.marker(pin.getLatLon(), {icon: L.divIcon(options)});
        var callback;
        if ('buckets' === pin.get('type')) {
            callback = _.bind(function() {
                this.fitMapToBounds(pin);
            }, this);
        } else {
            callback = _.bind(function() {
                this.state.set('selectedLocation', pin);
            }, this);
        }

        pin.marker.on('click', callback);

        //if ('buckets' !== pin.get('type')) {
        //    this.clusters.addLayer(pin.marker);
        //} else {
            pin.marker.addTo(this.map);
        //}
    },

    fitMapToBounds: function(pin) {
        // TODO remove all markers
        this.removePin(pin);
        this.mapFitBounds(boundingBoxToLatLngBounds(pin.get('bounding_box')));
    },

    removePin: function(pin) {
        this.map.removeLayer(pin.marker);
    }
});

var FilterView = Backbone.View.extend({
    el: $('.filter'),

    events: {
        'click .close': 'onCloseClick',
        'keyup input': 'onInputKeyup',
        'change input': 'onInputChange',
        'change select': 'onSelectChange',
        'click .reset-filter': 'onResetFilterClick'
    },

    onResetFilterClick: function(evt) {
        evt.preventDefault();
        this.$('form').trigger('reset');

        /*_.each(this.sliders, function(sliderEl) {
            sliderEl.noUiSlider.set([
                $(sliderEl).data('min'),
                $(sliderEl).data('max')
            ]);
        });*/

        this.state.mapView.toInitialBounds();
        this.serializeForm();
    },

    onInputKeyup: _.debounce(function(evt) {
        this.state.setFilterChanged();
        this.serializeForm();
    }, 200),

    onInputChange: function(evt) {
        this.state.setFilterChanged();
        this.serializeForm();
    },

    onSelectChange: function(evt) {
        this.state.setFilterChanged();
        this.serializeForm();
    },

    serializeForm: function() {
        var data = Backbone.Syphon.serialize(this);

        if (data.profile_fields) {
            _.each(data.profile_fields, function(value, key) {
               if (_.isObject(value)) {
                   data.profile_fields[key] = _.reduce(data.profile_fields[key], function(memo, anotherValue, anotherKey) {
                       if (anotherValue) {
                           memo.push(anotherKey);
                       }
                       return memo;
                   }, []).join(',');
               }
            });
        }

        if (!_.isUndefined(data.term)) {
            data.q = data.term;
            delete data.term;
        }
        this.state.queryParams.set('bounding_box', boundsToQueryParam(this.state.mapView.bounds), {silent: true});
        this.state.queryParams.set(data);
    },

    initialize: function(options) {
        this.state = options.state;

        $('.toggle-filter').on('click', function(evt) {
            evt.preventDefault();
            $('body').addClass('filter-is-visible');
        });

        this.sliders = [];

        var _this = this;
        this.$('.range-slider').each(function(idx, el) {
            var $el = $(el),
                max = $el.data('max'),
                min = $el.data('min');

            var slider = noUiSlider.create(el, {
                start: [min, max],
                connect: true,
                tooltips: [true, true],
                step: 1,
                range: {
                    min: min,
                    max: max
                },
                format: {
                    to: function(value) {
                        return parseInt(value);
                    },
                    from: function(value)  {
                        return value;
                    }
                }
            });

            slider.on('set', function(values) {
                var $input = $el.siblings('input[type="hidden"]');
                $input.val(
                    "['" + values[0] + "','" + values[1] + "']"
                ).trigger('change');
            });

            _this.sliders.push(el);
        });
    },

    onCloseClick: function(evt) {
        evt.preventDefault();
        $('body').removeClass('filter-is-visible');
    }
});

var DetailView = Backbone.View.extend({
    el: '.details',
    template: JST['details.html'],

    events: {
        'click .show-filters': 'onShowFiltersClick'
    },

    onShowFiltersClick: function(evt) {
        evt.preventDefault();
        this.state.set('selectedLocation', null);
    },

    initialize: function(options) {
        this.state = options.state;
        this.model = options.state.details;
        this.listenTo(this.model, 'change', this.onModelChange);
    },

    onModelChange: function() {
        if (!this.model.has('id')) {
            $('body').removeClass('details-are-visible');
            var _this = this;
            // Wait for css transition
            setTimeout(function() {
                _this.$el.empty();
            }, 300);
        } else {
            $('body').addClass('details-are-visible');
            this.render();
        }
    },

    urlReplaceHelper: function(text) {
        var GRUBERS_URL_RE = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/i,
            HAS_PROTOCOL = /^[a-z][\w-]+:/;

        function escapeHTML(text) {
            return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;').replace(/>/g, '&gt;');
        }

        function wordToURL(word, index, array) {
            var m = word.match(GRUBERS_URL_RE),
                result = escapeHTML(word),
                url, escapedURL, text;

            if (m) {
                text = escapeHTML(m[1]);
                url = HAS_PROTOCOL.test(text) ? text : 'http://' + text;
                result = result.replace(text, '<a href="' + url + '" target="_blank">' + text + '</a>');
            }
            return result;
        }

        var map = Array.prototype.map ? function(arr, callback) {
            return arr.map(callback);
        } : function (arr, callback) {
            var arr2 = [], i, l;
            for (i = 0, l = arr.length; i < l; i = i + 1) {
                arr2[i] = callback(arr[i], i, arr);
            }
            return arr2;
        };

        return map(text.split(/\s+/), wordToURL).join(' ');
    },

    render: function() {
        var data = this.model.toJSON();
        // Filter out the profile of this organization only
        data.organizations.data = _.filter(data.organizations.data, function(obj) {
            return (obj.id == this.state.organizationId);
        }, this);

        // TODO localized address notation?

        var parts = [];
        if (!_.isEmpty(data.street_name)) {
            parts.push(data.street_name);
        }

        if (!_.isEmpty(data.street_name) && !_.isEmpty(data.street_number)) {
            parts.push(data.street_number);
        }

        // Newline _if_ there is something in here
        if (parts.length > 0) {
            parts.push('<br/>');
        }

        if (!_.isEmpty(data.postal_code)) {
            parts.push(data.postal_code);
        }
        if (!_.isEmpty(data.city)) {
            parts.push(data.city);
        }
        data.address_line = parts.join(' ');

        // https://gist.github.com/RCanine/492947#file-3-linkify-js
        data.urlReplaceHelper = this.urlReplaceHelper;

        var html = this.template(data);
        this.$el.html(html);

        var _this = this;
        setTimeout(function() {
            _this.$('.slider').slick({dots: true});
        }, 0);

        return this;
    }

});