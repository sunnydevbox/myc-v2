<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

    'facebook' => [
        'client_id' => '1556353597987690',
        'client_secret' => 'bff288415e713d7f10fece6b8c50754e',
	    'app_id' => '1556353597987690'
    ],

	'mailgun' => [
		'domain' => 'mg.mapyour.city',
		'secret' => 'key-1ab8not4up1j0ku-x05p0b01g9de32s5',
	],

	'mandrill' => [
		'secret' => 'S_wEbaMoNHdJMbkUAJ2FqQ',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

    'acs' => [
        'username' => 'qikker@mdnq.net',
        'password' => 'vZHXd@JBGKfDwG6xrEg&8CJyrAYiwwmj',
        //'api_key' => 'yBWojk4SjiAljQ1vQIfR6kXj7OnxCG8L',
        'api_key' => 'jqpHEYQ4HbRe5tRq29CDipspOcOf4nZq'
    ],

	'mapbox' => [
		'secret_token' => 'sk.eyJ1IjoibWFweW91cmNpdHkiLCJhIjoiS0pLdTY4SSJ9.0Lh7SSssmrMQhpRBsZVxlw',
		'public_token' => 'pk.eyJ1IjoibWFweW91cmNpdHkiLCJhIjoibGdpR1JqUSJ9.foNKFIhEbdoNMX4hPJmxEg'
	]
];
